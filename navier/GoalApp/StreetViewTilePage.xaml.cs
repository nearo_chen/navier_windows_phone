﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Resources;
using System.IO;
using System.Windows.Media.Imaging;
using MyUtility;
using System.Windows.Media;
using System.Windows.Input;


namespace GoalApp
{
    public partial class StreetViewTilePage : PhoneApplicationPage
    {
        const string API_KEY = "AIzaSyCozczT3UxKGvmArwySKTbgOAz74ag8PpE";
        string mLat, mLng;
        int mHeadingDegreeCount = 0;
        int mFOV = 50;
        int mPitch = 0;

        string mHUDFileName, mHUDShowName;
        int mTravelMode;
        string mGoalName;

        bool mIWantStreetView = true;
        bool mGetStreetViewSuccess = false;

        bool bFirstEnter = true;
        public StreetViewTilePage()
        {
            InitializeComponent();

            mDownloading.Visibility = System.Windows.Visibility.Collapsed;
            mStreetImage.Visibility = System.Windows.Visibility.Collapsed;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (bFirstEnter)
            {
                bFirstEnter = false;
                if (this.NavigationContext.QueryString.ContainsKey("latitude"))
                    mLat = this.NavigationContext.QueryString["latitude"];
                if (this.NavigationContext.QueryString.ContainsKey("longitude"))
                    mLng = this.NavigationContext.QueryString["longitude"];
                if (this.NavigationContext.QueryString.ContainsKey("HUDFileName"))
                    mHUDFileName = this.NavigationContext.QueryString["HUDFileName"];
                if (this.NavigationContext.QueryString.ContainsKey("HUDShowName"))
                    mHUDShowName = this.NavigationContext.QueryString["HUDShowName"];
                if (this.NavigationContext.QueryString.ContainsKey("TravelMode"))
                    mTravelMode = int.Parse(this.NavigationContext.QueryString["TravelMode"]);

                mGoalName = NavigationContext.QueryString["GoalName"];
                mTileShowNameBlack.Text = mTileShowNameWhite.Text = mTileName.Text = mGoalName;

                mHeadingDegreeCount = mPitch = 0;
                mFOV = 50;
                GetImage(mHeadingDegreeCount, mFOV, mPitch);

                BitmapImage tn = new BitmapImage();
                if (mTravelMode == (int)Microsoft.Phone.Maps.Services.TravelMode.Driving)
                    tn.SetSource(Application.GetResourceStream(new Uri(@"Assets/images/travel/Car.png", UriKind.Relative)).Stream);
                else
                    tn.SetSource(Application.GetResourceStream(new Uri(@"Assets/images/travel/Cycle.png", UriKind.Relative)).Stream);

                mTravelModeImageShadow.ImageSource = mTravelModeImage.ImageSource = tn;
                mTileIcon.Source = Utility.TileIcon;
            }

            //mLat = "38.250334";
          //  mLng = "-122.409155";

           // mLat = "48.860229";
           //mLng = "2.29154";
        }

        private static string _streetViewTempImageFolder = "StreetViewTempImage";
        /// <summary>
        /// 將streetview暫存清空
        /// </summary>
        public static void CleanStreetViewFolder()
        {
            IsolatedStorageHelper.DeleteFolder(_streetViewTempImageFolder);
            IsolatedStorageHelper.CreateFolder(_streetViewTempImageFolder);
        }

        private string _createStreetViewFileName()
        {
            return string.Format("Lat={0}_Lng={1}_Heading{2}_Pitch{3}.jpg",
                mLat, mLng, mHeadingDegreeCount, mPitch);
        }

       
        private async void GetImage(int headingDegree, int fov, int pitch)
        {

            //先從暫存中找尋是否成功的得到影像，萬一有任何的失敗就去由google要圖
            bool bLoadFromCache = false;
            string goalName = mTileName.Text;
            string sTempImageName = _createStreetViewFileName();
            try
            {
                if (IsolatedStorageHelper.IsFolderExist(_streetViewTempImageFolder))
                {
                    //看圖檔有沒有存在，將圖設定給imageview
                    string filePathName = _streetViewTempImageFolder + "/" + sTempImageName;
                    var userStoreForApplication = System.IO.IsolatedStorage.IsolatedStorageFile.GetUserStoreForApplication();
                    if (userStoreForApplication.FileExists(filePathName))
                    {
                        var isolatedStorageFileStream = userStoreForApplication.OpenFile(filePathName, FileMode.Open);
                        var bitmapImage = new BitmapImage { CreateOptions = BitmapCreateOptions.None };
                        bitmapImage.SetSource(isolatedStorageFileStream);
                        mStreetImage.Source = bitmapImage;

                        _processShowImageDone();
                        bLoadFromCache = true;

                        isolatedStorageFileStream.Close();                        
                    }

                    //byte[] data = await IsolatedStorageHelper.ReadFile_Local(sTempImageName, _streetViewTempImageFolder);
                    //if (data != null)
                    //{
                    //    MemoryStream sr = new MemoryStream(data);
                     
                    //    BitmapImage bitmapImage = new BitmapImage { CreateOptions = BitmapCreateOptions.None };
                    //    bitmapImage.SetSource(sr);
                    //    mStreetImage.Source = bitmapImage;

                    //    bLoadFromCache = true;
                    //}
                }
            }
            catch { }

            if (bLoadFromCache)
                return;

            //  int pitch = 0;
            string url = string.Empty;
            // TODO: Want to associaction with accelerometer sensor? 
            if (true)
            {
                url = string.Format("http://maps.googleapis.com/maps/api/streetview?size=600x300&location={0},{1}&heading={2}&fov={3}&pitch={4}&sensor=false&key={5}", mLat, mLng, headingDegree, fov, pitch, API_KEY);
            }
            else
            {
                url = string.Format("http://maps.googleapis.com/maps/api/streetview?size=600x300&location={0},{1}&sensor=false&key={2}", mLat, mLng, API_KEY);
            }

            WebClient wc = new WebClient();
            wc.DownloadProgressChanged += wc_DownloadProgressChanged;
            wc.OpenReadCompleted += new OpenReadCompletedEventHandler(wc_OpenReadCompleted);
            wc.AllowReadStreamBuffering = true;
            wc.OpenReadAsync(new Uri(url), wc);

            mDownloading.Visibility = System.Windows.Visibility.Visible;
            mDownloading.Text = "";// "Downloading...";
            

            _processGetImage();
        }
        void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
        }
        void wc_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            mRetangle.Visibility = System.Windows.Visibility.Visible;
            mGetStreetViewSuccess = false;
            if (e.Error == null && !e.Cancelled)
            {
                mStreetImage.Visibility = System.Windows.Visibility.Collapsed;
                try
                {
                    ShowImage(e.Result);
                }
                catch (Exception ex)
                {
                    Utility.HideProgressIndicator();
                    mDownloading.Text = "Error:-" + ex.Message;
                }
            }
            else
            {
                Utility.HideProgressIndicator();
                mDownloading.Text = "Image Stream Error";
                mStreetImage.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private async void _saveStreetViewTempImage(BitmapImage bitmapImage)
        {
            string strFileNameTemp = _createStreetViewFileName();

            var userStoreForApplication = System.IO.IsolatedStorage.IsolatedStorageFile.GetUserStoreForApplication();
            if (userStoreForApplication.FileExists(strFileNameTemp))
            {
                userStoreForApplication.DeleteFile(strFileNameTemp);
            }
            var isolatedStorageFileStream = userStoreForApplication.CreateFile(_streetViewTempImageFolder+"/"+ strFileNameTemp);
            var writeableBitmap = new WriteableBitmap(bitmapImage);
            writeableBitmap.SaveJpeg(isolatedStorageFileStream, writeableBitmap.PixelWidth, writeableBitmap.PixelHeight, 0, 85);
            isolatedStorageFileStream.Close();
            _processShowImageDone();

            //await IsolatedStorageHelper.WriteToFile_Local(
            //    strFileNameTemp, sr, WriteToFile_Local_Success, _streetViewTempImageFolder);


        }

        //private void WriteToFile_Local_Success(string fileName)
        //{
        //    _processShowImageDone();
        //}

        void _processGetImage()
        {
            mTileShowNameWhite.Opacity = mTileShowNameBlack.Opacity = 0.1;
            if (mGetStreetViewSuccess)
            {
                mStreetImage.Visibility = System.Windows.Visibility.Visible;
                mStreetImage.Opacity = 0.4;
            }
            else
                mStreetImage.Visibility = System.Windows.Visibility.Collapsed;

            Utility.ShowProgressIndicator("Downloading", true);
        }

        void _processShowImageDone()
        {
            mDownloading.Visibility = System.Windows.Visibility.Collapsed;
            mStreetImage.Opacity = 1;
            mTileShowNameWhite.Opacity = mTileShowNameBlack.Opacity = 1;
            mStreetImage.Visibility = System.Windows.Visibility.Visible;
            mGetStreetViewSuccess = true;
            mRetangle.Visibility = System.Windows.Visibility.Collapsed;

            Utility.HideProgressIndicator();
        }

        private void ShowImage(Stream sr)
        {
            var streamResourceInfo = new StreamResourceInfo(sr, null);

            BitmapImage bitmapImage = new BitmapImage { CreateOptions = BitmapCreateOptions.None };
            bitmapImage.SetSource(streamResourceInfo.Stream);

            //WriteableBitmap btmMap = new WriteableBitmap(bitmapImage);
            //using (MemoryStream ms = new MemoryStream())
            //{
            //    WriteableBitmapExtensions.WritePNG(btmMap, ms, true);

            //    //    WriteableBitmap btmMap = new WriteableBitmap
            //    //        (bitmapImage.PixelWidth, bitmapImage.PixelHeight);

            //    //    // write an image into the stream
            //    //    Extensions.SaveJpeg(btmMap, ms,
            //    //        bitmapImage.PixelWidth, bitmapImage.PixelHeight, 0, 100);

            //    // result = ms.ToArray();

            //    bitmapImage = new BitmapImage { CreateOptions = BitmapCreateOptions.None };
            //    bitmapImage.SetSource(ms);
            //}

            //將image存檔
            _saveStreetViewTempImage(bitmapImage);

            mStreetImage.Source = bitmapImage;
        }

        private void Button_Pin_To_Tile(object sender, RoutedEventArgs e)
        {
            BitmapImage bitmap = null;
            if (mStreetImage.Visibility == System.Windows.Visibility.Visible)
            {
                //要搞成PNG設定進去bitmap他顏色才不會RB調換(或是說搞成save PNG他顏色會是調換的，設定在image上面，就又是倒過來)
                WriteableBitmap btmMap = new WriteableBitmap((BitmapImage)mStreetImage.Source);
                MemoryStream ms = new MemoryStream();
                WriteableBitmapExtensions.WritePNG(btmMap, ms);
                bitmap = new BitmapImage();
                bitmap.SetSource(ms);
                ms.Close();
                ms.Dispose();
            }

            string goalName = mTileName.Text;
            string sTileProperties =
                string.Format("Navi_HUDSettingFileName={0}&GoalName={1}&Lat={2}&Lng={3}&TravelMode={4}",
           mHUDFileName, goalName, mLat, mLng, mTravelMode);

            //string sTileProperties = "HUDSettingFileName=" + sHUDFileName;
            ShellTile tile = ShellTile.ActiveTiles.FirstOrDefault(
                    x => x.NavigationUri.ToString().Contains(sTileProperties));
            // if (tile == null)
            {
                SolidColorBrush tmp = (SolidColorBrush)mTileShowNameWhite.Foreground;

                bool textColorBlack = (tmp.Color.Equals(Colors.Black));
                Utility.SetTileData_GoTo_Navi(goalName, mHUDShowName, mHUDFileName, sTileProperties,
                    (Microsoft.Phone.Maps.Services.TravelMode)mTravelMode,
                    bitmap,
                    tile,
                   textColorBlack);
            }
            //else
            //{
            //    //Update Live Tile
            //   // tile.Update(flipTile);
            //   // Utility.ShowToast("already pinned", 2000, true);
            //}


        }


        private void Button_Click_Add_Positive_10Degree(object sender, RoutedEventArgs e)
        {
            mHeadingDegreeCount += 10;
            if (mHeadingDegreeCount >360)
                mHeadingDegreeCount -= 360;
            GetImage(mHeadingDegreeCount, mFOV, mPitch);
        }

        private void Button_Click_Add_Positive_30Degree(object sender, RoutedEventArgs e)
        {
            mHeadingDegreeCount += 30;
            if (mHeadingDegreeCount > 360)
                mHeadingDegreeCount -= 360;
            GetImage(mHeadingDegreeCount, mFOV, mPitch);
        }

        private void Button_Click_Add_Negative_30Degree(object sender, RoutedEventArgs e)
        {
            mHeadingDegreeCount -= 30;
            if (mHeadingDegreeCount < 0)
                mHeadingDegreeCount += 360;
            GetImage(mHeadingDegreeCount, mFOV, mPitch);
        }

        private void Button_Click_Add_Negative_10Degree(object sender, RoutedEventArgs e)
        {
            mHeadingDegreeCount -= 10;
            if (mHeadingDegreeCount < 0)
                mHeadingDegreeCount += 360;
            GetImage(mHeadingDegreeCount, mFOV, mPitch);
        }

        private void Button_Click_Pitch_Add_10Degree(object sender, RoutedEventArgs e)
        {
            mPitch += 10;
            if (mPitch > 90)
                mPitch = 90;
            GetImage(mHeadingDegreeCount, mFOV, mPitch);
        }

        private void Button_Click_Pitch_Del_10Degree(object sender, RoutedEventArgs e)
        {
            mPitch -= 10;
            if (mPitch < -90)
                mPitch = -90;
            GetImage(mHeadingDegreeCount, mFOV, mPitch);
        }

        private void mTileName_TextChanged(object sender, TextChangedEventArgs e)
        {
            mTileShowNameWhite.Text = mTileShowNameBlack.Text = mTileName.Text;

        }

        private void mTileName_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                // focus the page in order to remove focus from the text box 
                // and hide the soft keyboard 
                this.Focus();
            }
        }

        private void ChangeTextColor_Click(object sender, RoutedEventArgs e)
        {
            SolidColorBrush tmp = (SolidColorBrush)mTileShowNameWhite.Foreground;

            mTravelModeColor.Fill =
            mTileShowNameWhite.Foreground = mTileShowNameBlack.Foreground;

            mTravelModeColorShadow.Fill =
            mTileShowNameBlack.Foreground = tmp;
        }

        private void StreetView_Click(object sender, RoutedEventArgs e)
        {
            mIWantStreetView = !mIWantStreetView;

            if (mIWantStreetView)
            {
                GetImage(mHeadingDegreeCount, mFOV, mPitch);

                mAddRight10.Visibility = mAddRight30.Visibility = mAddUP10.Visibility =
                    mDelLeft10.Visibility = mDelLeft30.Visibility = mDelUP10.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                mStreetImage.Visibility = System.Windows.Visibility.Collapsed;
                mRetangle.Visibility = System.Windows.Visibility.Visible;

                //Created background color as Accent color
                SolidColorBrush backColor = new SolidColorBrush((Color)Application.Current.Resources["PhoneAccentColor"]);
                mBackGround.Background = backColor;

                mAddRight10.Visibility = mAddRight30.Visibility = mAddUP10.Visibility =
                    mDelLeft10.Visibility = mDelLeft30.Visibility = mDelUP10.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

    }
}