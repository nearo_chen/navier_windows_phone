﻿
using GoogleAds;
using Microsoft.Advertising.Mobile.UI;
using Microsoft.Phone.Controls;
using MyUtility;
using SOMAWP8;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;


namespace GoalApp
{
    public partial class GoalPage : PhoneApplicationPage
    {



        #region ADMob廣告
        static int mADMobCount = 0;
        static AdView AddADMob(Grid adPanelCanvas)
        {
            AdView bannerAd = new AdView
            {
                Format = AdFormats.Banner,
                AdUnitID = "ca-app-pub-5305750977417349/5489296117"
            };
            //bannerAd.ReceivedAd += OnAdReceived;
            bannerAd.FailedToReceiveAd += OnFailedToReceiveAd;

            AdRequest adRequest = new AdRequest();
            // adRequest.ForceTesting = true;
            bannerAd.LoadAd(adRequest);

            bannerAd.Tag = adPanelCanvas;
            //bannerAd.Visibility = Visibility.Collapsed;不能看不見不然會一直要廣告失敗
            return bannerAd;
        }

        //static private void OnAdReceived(object sender, AdEventArgs e)
        //{
        //    AdView ad = (AdView)sender;

        //    Utility.GetCurrentDispatcher().BeginInvoke(() =>
        //    {
        //        ad.Visibility = System.Windows.Visibility.Visible;
        //    });
        //}

        static private void OnFailedToReceiveAd(object sender, AdErrorEventArgs errorCode)
        {
            AdView ad = (AdView)sender;
            ad.FailedToReceiveAd -= OnFailedToReceiveAd;

            Grid adPanelCanvas = ad.Tag as Grid;

            Utility.GetCurrentDispatcher().BeginInvoke(() =>
            {
                //當遇到錯誤時，清掉放置廣告的StackPanel中的子項目；
                if (adPanelCanvas.Children.Count > 0)
                    adPanelCanvas.Children.Clear();
                //加入新的廣告元件：Admob
                if (adPanelCanvas.Children.Count == 0)
                {
                    mADMobCount++;
                    if (mADMobCount >= 2)
                    {
                        mADMobCount = 0;
                        adPanelCanvas.Children.Add(AddPubCenterAd_480x80(adPanelCanvas));
                    }
                    else
                        adPanelCanvas.Children.Add(AddADMob(adPanelCanvas));
                }
            });
        }
        #endregion

        #region smaato廣告
        static SomaAdViewer mADVSmaato = null;
        static SomaAdViewer _addSmaato(Grid adPanelCanvas)
        {
            SomaAdViewer smaato;

            smaato = new SomaAdViewer();
            smaato.ShowErrors = false;
            smaato.LocationUseOK = true;
            //mADVSmaato.Format = SOMAWP8.SomaAd.FormatRequested.img;
            //mADVSmaato.Countrycode = "us";
            //mADVSmaato.Country = "united states";
            // smaato.NewAdAvailable += somaAd_NewAdAvailable;
            smaato.AdError += somaAd_AdError;

            //mADVSmaato.Kws = "game";
            smaato.AdInterval = 5000;
            smaato.Adspace = 65818761;
            smaato.Pub = 923874017;
            // smaato.Adspace = smaato.Pub = 0;//for test

            smaato.StartAds();

            smaato.Tag = adPanelCanvas;

            mADVSmaato = smaato;

            //smaato.Visibility = System.Windows.Visibility.Collapsed;因為其他廣告商設看不見都會有問題，所以這邊我也不要設了
            return smaato;
        }

        static void somaAd_AdError(object sender, string ErrorCode, string ErrorDescription)
        {
            if (mADVSmaato == null)
                return;

            SomaAdViewer smaato = mADVSmaato;// sender as SomaAdViewer;
            mADVSmaato = null;
            // smaato.NewAdAvailable -= somaAd_NewAdAvailable;
            smaato.AdError -= somaAd_AdError;

            Grid adPanelCanvas = smaato.Tag as Grid;

            smaato.StopAds();

            Utility.GetCurrentDispatcher().BeginInvoke(() =>
            {
                //當遇到錯誤時，清掉放置廣告的StackPanel中的子項目；
                if (adPanelCanvas.Children.Count > 0)
                    adPanelCanvas.Children.Clear();
                //加入新的廣告元件：Admob
                if (adPanelCanvas.Children.Count == 0)
                    adPanelCanvas.Children.Add(AddADMob(adPanelCanvas));
            });
        }

        //static void somaAd_NewAdAvailable(object sender, EventArgs e)
        //{
        //    if (mADVSmaato == null)
        //        return;

        //    SomaAdViewer smaato = mADVSmaato;// sender as SomaAdViewer;

        //    Utility.GetCurrentDispatcher().BeginInvoke(() =>
        //    {
        //          smaato.Visibility = System.Windows.Visibility.Visible;
        //    });
        //}
        #endregion

        #region Pubcenter廣告
        //static AdControl AddPubCenterAd_640x100(Grid adPanelCanvas)
        //{
        //    //建立新的AdControl元件
        //    AdControl adControl = new AdControl("cdd5fe20-f371-43de-82ee-d6a5a6fd8c69", "10327001", true);
        //        //new AdControl("test_client", "Image640_100", true);
        //    adControl.Width = 640;
        //    adControl.Height = 100;

        //    //更新AdControl的Templated()
        //    adControl.ApplyTemplate();

        //    //註冊ErrorOccurred事件
        //    adControl.ErrorOccurred += adControl_ErrorOccurred_640x100;
        //    adControl.AdRefreshed += AdControl_AdRefreshed;
        //    adControl.IsAutoRefreshEnabled = true;

        //    //   adControl.Visibility = System.Windows.Visibility.Collapsed;
        //    adControl.Tag = adPanelCanvas;
        //    return adControl;
        //}

        //static void adControl_ErrorOccurred_640x100(object sender, Microsoft.Advertising.AdErrorEventArgs e)
        //{
        //    AdControl ad = (AdControl)sender;
        //    Grid adPanelCanvas = ad.Tag as Grid;
        //    Utility.GetCurrentDispatcher().BeginInvoke(() =>
        //    {
        //        //當遇到錯誤時，清掉放置廣告的StackPanel中的子項目；
        //        if (adPanelCanvas.Children.Count > 0)
        //            adPanelCanvas.Children.Clear();
        //        //加入新的廣告元件：Admob
        //        if (adPanelCanvas.Children.Count == 0)
        //            adPanelCanvas.Children.Add(AddPubCenterAd_480x80(adPanelCanvas));
        //    });
        //}

        static AdControl AddPubCenterAd_480x80(Grid adPanelCanvas)
        {
            //建立新的AdControl元件
            AdControl adControl = new AdControl("cdd5fe20-f371-43de-82ee-d6a5a6fd8c69", "10327031", true);
            //new AdControl("test_client", "Image480_80", true);
            adControl.Width = 480;
            adControl.Height = 80;

            //更新AdControl的Templated()
            adControl.ApplyTemplate();

            //註冊ErrorOccurred事件
            adControl.ErrorOccurred += adControl_ErrorOccurred_480x80;
            // adControl.AdRefreshed += AdControl_AdRefreshed;
            adControl.IsAutoRefreshEnabled = true;

            adControl.Tag = adPanelCanvas;
            //adControl.Visibility = System.Windows.Visibility.Collapsed;這個不能設成看不見，不然他不會跑，事件不會觸發
            return adControl;
        }

        static void adControl_ErrorOccurred_480x80(object sender, Microsoft.Advertising.AdErrorEventArgs e)
        {
            AdControl ad = (AdControl)sender;
            Grid adPanelCanvas = ad.Tag as Grid;

            Utility.GetCurrentDispatcher().BeginInvoke(() =>
            {
                Utility.GetCurrentDispatcher().BeginInvoke(() =>
                {
                    //當遇到錯誤時，清掉放置廣告的StackPanel中的子項目；
                    if (adPanelCanvas.Children.Count > 0)
                        adPanelCanvas.Children.Clear();
                    //加入新的廣告元件：Admob
                    if (adPanelCanvas.Children.Count == 0)
                        adPanelCanvas.Children.Add(_addSmaato(adPanelCanvas));
                });
            });

        }

        //static void AdControl_AdRefreshed(object sender, EventArgs args)
        //{
        //    AdControl ad = (AdControl)sender;

        //    Utility.GetCurrentDispatcher().BeginInvoke(() =>
        //    {
        //        ad.Visibility = System.Windows.Visibility.Visible;
        //    });
        //}
        #endregion


    }
}