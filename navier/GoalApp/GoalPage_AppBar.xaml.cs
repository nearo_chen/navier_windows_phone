﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using ResourceApp.Resources;
using MyUtility;

namespace GoalApp
{
    public partial class GoalPage : PhoneApplicationPage
    {
        private NavierHUD.HUD.SaveUserSetting.UserSetting mUserSetting;
        private System.Windows.Controls.Primitives.Popup mNowPopup;

        // Sample code for building a localized ApplicationBar
        private void BuildLocalizedApplicationBar()
        {
            if (ApplicationBar != null)
            {
                ApplicationBar.IsVisible = !ApplicationBar.IsVisible;
                return;
            }

            // Set the page's ApplicationBar to a new instance of ApplicationBar.
            ApplicationBar = new ApplicationBar();

            ApplicationBar.IsVisible = false;

            // Create a new button and set the text value to the localized string from AppResources.
            ApplicationBarIconButton appBarButton;

            appBarButton = new ApplicationBarIconButton(new Uri("/Assets/appbarIcon/DownloadMap.png", UriKind.Relative));
            appBarButton.Text = AppResources.GOALBAR_MapDownloader;
            appBarButton.Click += MapDownload_EventHandler;
            ApplicationBar.Buttons.Add(appBarButton);

            appBarButton = new ApplicationBarIconButton();
            appBarButton.IconUri = new Uri("/Assets/AppbarIcon/Settings.png", UriKind.Relative);
            appBarButton.Text = AppResources.Main_SETTINGS;
            appBarButton.Click += Setting_EventHandler;
            ApplicationBar.Buttons.Add(appBarButton);

            //appBarButton = new ApplicationBarIconButton(new Uri("/Assets/images/appbar/CurrentPosition.png", UriKind.Relative));
            //appBarButton.Text = AppResources.GOALBAR_CurrentPosition;
            //appBarButton.Click += CurrentPosition_EventHandler;
            //ApplicationBar.Buttons.Add(appBarButton);

            ApplicationBar.Opacity = 0.6;

            // Create a new menu item with the localized string from AppResources.
            //ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.GOALBAR_MapDownloader);
            //ApplicationBar.MenuItems.Add(appBarMenuItem);
        }

        //private void SearchFind_EventHandler(object sender, EventArgs e)
        //{
        //    ProcessSearchEnter(TextBox_Keyin_Goal.Text);
        //}

        //private void CurrentPosition_EventHandler(object sender, EventArgs e)
        //{
        //    StartPositionTracking();
        //}

        private void MapDownload_EventHandler(object sender, EventArgs e)
        {
            try
            {
                MapDownloaderTask map = new MapDownloaderTask();
                map.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        void Setting_EventHandler(object sender, EventArgs e)
        {
            ApplicationBar.IsVisible = false;

            Utility.GetCurrentDispatcher().BeginInvoke(() =>
            {
                NavierHUD.SettingsControl settingsControl = new NavierHUD.SettingsControl(mUserSetting);
                double hostWidth = Utility.GetDeviceWidth(true) - ApplicationBar.DefaultSize;
                double hostHeight = Utility.GetDeviceHeight(true);
                settingsControl.Width = hostWidth;
                settingsControl.Height = hostHeight;

                // 因為landscape所以X跟Y反過來
                System.Windows.Controls.Primitives.Popup popup = settingsControl.Create(
                    (hostHeight - hostWidth) * 0.5f,
                    (hostWidth - hostHeight) * 0.5f + ApplicationBar.DefaultSize);

                SetPopupWindow(popup);
            });
        }

        /// <summary>
        /// 設定目前popup control是哪個，原則上同一時間只能有一個popup
        /// (主要是管理popup window的開開關關)
        /// </summary>
        /// <param name="popup"></param>
        void SetPopupWindow(System.Windows.Controls.Primitives.Popup popup)
        {
            ClosePopupWindow();
            mNowPopup = popup;
        }

        bool ClosePopupWindow()
        {
            if (mNowPopup != null && mNowPopup.IsOpen == true)
            {
                //離開頁面前將設定存檔
                if (mNowPopup.Child.GetType() == typeof(NavierHUD.SettingsControl))
                    NavierHUD.HUD.SaveUserSetting.SaveSettingData(mUserSetting);

                mNowPopup.IsOpen = false;
                mNowPopup = null;
                return true;
            }
            return false;
        }
    }
}