﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
//using GoalApp.Resources;
using Microsoft.Phone.Maps.Controls;
using System.Device.Location;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Input;

using MapApp;
using Microsoft.Phone.Maps.Services;
using System.Collections.ObjectModel;
using MyUtility;
using System.Collections;

namespace GoalApp
{
    public partial class GoalPage : PhoneApplicationPage
    {
        private const string SEARCH_SAVE_FILE = "SearchResults.txt";
        //private List<string> mSearchDataHistory = new List<string>(64);


        public class SearchHistoryData
        {
            public string Address { get; set; }
            public string Geocoordinate { get; set; }
        }

        /// <summary>
        /// 由LOCAL暫存檔中，將以前的搜尋紀錄都挖出來
        /// </summary>
        private async void LoadSearchHistoryFromFile()
        {
#if DEBUG
            //如果你ㄉ存檔消失了就打開這些作個預設存檔吧
            //if (mSearchedRrecord.Count == 0)//第一次才去寫
            //{
            //    mSearchedRrecord.Add("台北市");
            //    mSearchedRrecord.Add("碧潭");
            //    mSearchedRrecord.Add("新北市新店區中興路三段3號");
            //    mSearchedRrecord.Add("新北市板橋區長安街255巷42弄3之2號");
            //    SaveSearchHistoryToFile();
            //}
#endif


            mSearchedRrecord.Clear();
            byte[] bbb = await IsolatedStorageHelper.ReadFile_Local(SEARCH_SAVE_FILE, null);

            if (bbb == null || bbb.Length <= 0)
                return;

            mSearchedRrecord = (List<SearchHistoryData>)IsolatedStorageHelper.Deserialize_JSON(bbb, typeof(List<SearchHistoryData>));
        }

        //bool SearchFilter(string search, object value)
        //{
        //    if (search != null)
        //    {
        //        if (value.ToString().ToLower().Contains(search.ToLower()))
        //            return true;

        //    }
        //    return false;
        //}

        /// <summary>
        /// 將搜尋記錄存LOACL檔
        /// </summary>
        private async void SaveSearchHistoryToFile()
        {
            byte[] bbb = IsolatedStorageHelper.Serialize_JSON(mSearchedRrecord);
            await IsolatedStorageHelper.WriteToFile_Local(SEARCH_SAVE_FILE, bbb);
        }

        ///// <summary>
        ///// 將搜尋記錄清空
        ///// </summary>
        //private async void SaveSearchHistoryClearToFile()
        //{
        //    mSearchDataHistory.Clear();
        //    byte[] bbb = IsolatedStorageHelper.Serialize_JSON(mSearchDataHistory);
        //    await IsolatedStorageHelper.WriteToFile_Local(SEARCH_SAVE_FILE, bbb);
        //}

        /// <summary>
        /// 將搜尋結果加到暫存紀錄中
        /// </summary>
        private void _processSearchHistory(SearchHistoryData sSearchResult, bool bSave)
        {
            if (sSearchResult == null)
                return;

            int count = 0;
            int selectIndex = -1;
            foreach (SearchHistoryData hisdata in mSearchedRrecord)
            {
                if (hisdata.Address == sSearchResult.Address)
                {
                    selectIndex = count;
                    break;
                }
                count++;
            }

            if (selectIndex < 0)          
            {
                mSearchedRrecord.Insert(0, sSearchResult);
            }
            else
            {
                //把他換到第一個
                if (selectIndex > 0)//第零個插到第零個資源就把他省下來吧
                {
                    mSearchedRrecord.RemoveAt(selectIndex);
                    mSearchedRrecord.Insert(0, sSearchResult);
                }
            }

            if (bSave)
            {
                //將USER自己搜尋的目的地作儲存(先不管有無搜到一律儲存，以後在考慮有找到才儲存關鍵字)
                SaveSearchHistoryToFile();
            }
        }
    }
}