﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using Microsoft.Phone.Maps.Controls;
using System.Device.Location;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Input;

using MapApp;
using Microsoft.Phone.Maps.Services;
using System.Collections.ObjectModel;
using MyUtility;
using System.Windows.Threading;

namespace GoalApp
{
    public partial class GoalPage : PhoneApplicationPage
    {
        private const string HISTORY_SIGN = "▼";

        //private bool mIsMyGlobalPositionDone = false;//整個view的操作，請先確認自己座標已經定位

        //public delegate void Callback_GO(GeoCoordinate goalCoordinate, TravelMode travelMode);
        //private Callback_GO mCallback_GO;

        //定位器更新自己座標
        private MyGeolocatorTracking myGeolocator;

        //隨時紀錄自己定位的更新
        private GeoCoordinate mNowMyGeoCoordinate = null;

        private GeoCoordinate mNowDestGeoCoordinate = null;

        //地標住址搜索用
        private GeocodeQueryCombine mGeocodeQueryCombine = null;


        //路徑規畫
        private MyRouteQuery myRouteQuery;

        //搜尋成功的紀錄
        private List<SearchHistoryData> mSearchedRrecord = new List<SearchHistoryData>();

        //由下拉式選單中選到的文字
        private SearchHistoryData mListPicker_SearchHistory_SelectText;

        //地圖畫標示
        private MapOverlay mMyOverlay = null;
        private MapOverlay mDestOverlay = null;

        //// 秀進度條 Progress indicator shown in system tray
        ////private ProgressIndicator mProgressIndicator = null;
        //MyProgressIndicator mMyProgressIndicator = null;

        //用來顯示MAP上的路徑
        private MapRoute myMapRoute = null;

        // Travel mode used when calculating route      
        private TravelMode _travelMode = TravelMode.Driving;

        //目前路徑規劃的目標名稱
        private SearchHistoryData _travelName = new SearchHistoryData();

        // Travel mode used when calculating route      
        private List<GeoCoordinate> _routeQueryList = new List<GeoCoordinate>(2);

        //用來記錄我編輯的view設定(因為在coding時，我都把要隱藏的grid打開比較好編輯，建構子才去隱藏)
        //double[] viewe_width_LayoutRoot = null;
        double column_route_information_height, column_select_goal_width;
        Thickness mRec_myADV_Thickness,
            mRec_MyMap_Thickness;

        private void CreateGPS()
        {
#if DEBUG
            Utility.OutputDebugString("[GoalPage]->CreateGPS()");
#endif

            //myGeolocator = MyGeolocator.Create();
            //myGeocodeQuery = MyGeocodeQuery.Create();
            //myRouteQuery = MyRouteQuery.Create();
            myGeolocator = MyGeolocatorTracking.Create(-1, 1);
            myRouteQuery = MapUtility.GetMyRouteQuery();



            // mMyProgressIndicator = new MyProgressIndicator(this);
            if (mGeocodeQueryCombine == null)
                mGeocodeQueryCombine = new GeocodeQueryCombine();


        }

        private void ReleaseGPS()
        {
            if (mGeocodeQueryCombine != null)
                mGeocodeQueryCombine.Dispose();
            mGeocodeQueryCombine = null;

            myGeolocator.Dispose();
            myGeolocator = null;
            //myGeocodeQuery.Dispose();
            //myRouteQuery.Dispose();

#if DEBUG
            Utility.OutputDebugString("[GoalPage]->ReleaseGPS()");
#endif

            MapUtility.ReleaseMyRouteQuery(ref myRouteQuery);

            Utility.HideProgressIndicator();
            //mMyProgressIndicator = null;
        }

        // Constructor
        public GoalPage()
        {
            //上述程式省略
            //#region App自訂處理範圍
            //將CurrentCulture改為en-US，這樣廣告就可以出現了；
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            //至於程式的多語系，改的是CurrentUICulture，二種是不同的；
            //System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("zh-TW");
            //#endregion

            InitializeComponent();

            #region 廣告
            AdPanel.Children.Add(AddADMob(AdPanel));
            #endregion

            if (Utility.mHideAdvertisement)
                RemoveAdvertisement();

            //把他的Z拉到最高，不然下拉式選單會被擋住
            //ListPicker_SearchHistory.SetValue(Canvas.ZIndexProperty, 100);

            // Sample code to localize the ApplicationBar
            BuildLocalizedApplicationBar();

            mPinToStart.Visibility=
            mFitSize.Visibility =
             mGoalPosition.Visibility =
                   System.Windows.Visibility.Collapsed;


            //在這邊作清空streeview暫存資料夾的動作
            StreetViewTilePage.CleanStreetViewFolder();
        }



        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            Utility.OutputDebugString("Goapage的初始化");//, LayoutRoot);

            //Hide information view 先記錄設定位置
            RecordAllExpand();

            //不能直接在xaml讓他Collapsed，不然第一次點會壞掉，在navigatorTo把他OPEN，然後loading完這邊把他關起來
            ListPicker_SearchHistory.Visibility = System.Windows.Visibility.Collapsed;




            if (mIsLeaveToListpicker)
            {
                mIsLeaveToListpicker = false;
                if (mSelectItemInHistory_Listpicker)
                {
                    mSelectItemInHistory_Listpicker = false;

                    _processRouteQueryAll();
                }
            }

            Utility.SetCurrentDispatcher(this.Dispatcher);
        }

        private void _processRouteQueryAll()
        {
            //直接做路徑規劃
            string sGeocoordinate = mListPicker_SearchHistory_SelectText.Geocoordinate.Replace("(", "");
            sGeocoordinate = sGeocoordinate.Replace(")", "");
            string[] sss = sGeocoordinate.Split(new char[] { ',' });
            string sLat = sss[0].Trim();
            string sLon = sss[1].Trim();
            double fLat = double.Parse(sLat, System.Globalization.CultureInfo.InvariantCulture);
            double fLon = double.Parse(sLon, System.Globalization.CultureInfo.InvariantCulture);
            GeoCoordinate coordDest = new GeoCoordinate(fLat, fLon);
            _SetOpenRouteDetail(coordDest, mListPicker_SearchHistory_SelectText.Address);

            //改變一下廣告的位置，讓USER更容易誤按
            //_switchToSmallADV();
        }

        /// <summary>
        /// 記錄全展開的位置
        /// </summary>
        void RecordAllExpand()
        {
            //記錄設定位置
            column_route_information_height = 2;// grid_route_information.Height.Value;
            column_select_goal_width = 1;// column_select_goal.Width.Value;

            mRec_myADV_Thickness = new Thickness(myADVBig.Margin.Left, myADVBig.Margin.Top, myADVBig.Margin.Right, myADVBig.Margin.Bottom);
            mRec_MyMap_Thickness = new Thickness(0, 72, 0, 10);//MyMap.Margin;

            initExpandControl();
        }

        /// <summary>
        /// 將展開的東西都收起來
        /// </summary>
        void initExpandControl()
        {
            //記錄完成後將表單收起來
            grid_route_information.Height = new GridLength(0, GridUnitType.Star);
            column_select_goal.Width = new GridLength(0, GridUnitType.Star);

            MyMap.Margin = mRec_MyMap_Thickness;

            myADVBig.Margin = mRec_myADV_Thickness;
        }

        int mGeolocatorCount = 0;

        void PageStart_PositionTracking_CallBack(ref Point outPoint, double speedValue, double accuracyMeter)
        {
            mNowMyGeoCoordinate = new GeoCoordinate(outPoint.X, outPoint.Y);

            MyMap.Dispatcher.BeginInvoke(() =>
            {
                MyMap.Center = mNowMyGeoCoordinate;
                CreateMapOverly();
                // MyMap.SetView(mNowMyGeoCoordinate, 10, MapAnimationKind.Parabolic);
            });

            //mIsMyGlobalPositionDone = true;

            //只做One Shot，只要從頭開始搜尋，就把他定位n次
            if (mGeolocatorCount >= 0)
                StopPositionTracking();

            mGeolocatorCount++;

#if DEBUG
            Utility.OutputDebugString("PositionTracking count : " + mGeolocatorCount);
            Utility.OutputDebugString("[定位完成] =>" + outPoint.ToString());
#endif
        }

        //        void PageStart_initializeMyGlobalPosition_CallBack(ref System.Windows.Point outPoint)
        //        {
        //            //MyMap.Visibility = System.Windows.Visibility.Visible;
        //            mNowMyGeoCoordinate = new GeoCoordinate(outPoint.X, outPoint.Y);
        //            MyMap.SetView(mNowMyGeoCoordinate, 10, MapAnimationKind.Parabolic);

        //            mIsMyGlobalPositionDone = true;
        //            CreateMapOverly();

        //            // myGeolocator.StartPositionTracking(PageStart_PositionTracking_CallBack);

        //#if DEBUG
        //            MyUtility.Utility.OutputDebugString("MainPage:MapInitialize Done =>" + outPoint.ToString());
        //#endif
        //        }

        private Image _pinIMG = null;
        void CreateDestOverly()
        {
            if (mDestOverlay != null)
            {
                mDestOverlay.GeoCoordinate = mNowDestGeoCoordinate;
                return;
            }

            if (_pinIMG == null)
            {
                _pinIMG = new Image();
                _pinIMG.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri("/Assets/images/travel/flag.png", UriKind.Relative));
                _pinIMG.Width = _pinIMG.Height = 40;
            }

            mDestOverlay = new MapOverlay();
            mDestOverlay.Content = _pinIMG;
            mDestOverlay.GeoCoordinate = mNowDestGeoCoordinate;
            mDestOverlay.PositionOrigin = new Point(0.1f, 1f);
            MyMap.Layers[0].Add(mDestOverlay);
            //MapLayer MyLayer = new MapLayer();
            //MyLayer.Add(mDestOverlay);
            //MyMap.Layers.Add(MyLayer);
        }

        private Image _MyLocation_pinIMG = null;
        void CreateMapOverly()
        {
            if (mMyOverlay != null)
            {
                mMyOverlay.GeoCoordinate = mNowMyGeoCoordinate;
                return;
            }

            if (_MyLocation_pinIMG == null)
            {
                _MyLocation_pinIMG = new Image();
                _MyLocation_pinIMG.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri("/Assets/images/travel/place_pin.png", UriKind.Relative));
                _MyLocation_pinIMG.Width = _MyLocation_pinIMG.Height = 60;
            }

            mMyOverlay = new MapOverlay();
            mMyOverlay.Content = _MyLocation_pinIMG;// CreateMark();
            mMyOverlay.GeoCoordinate = mNowMyGeoCoordinate;
            mMyOverlay.PositionOrigin = new Point(0.5f, 1f);
            MapLayer MyLayer = new MapLayer();
            MyLayer.Add(mMyOverlay);
            MyMap.Layers.Add(MyLayer);

        }

        /// <summary>
        /// 建立MAP上的標示圖形
        /// </summary>
        private Grid CreateMark()
        {
            Grid MyGrid = new Grid();
            MyGrid.RowDefinitions.Add(new RowDefinition());
            MyGrid.RowDefinitions.Add(new RowDefinition());
            MyGrid.Background = new SolidColorBrush(Colors.Transparent);

            //Creating a Rectangle
            Rectangle MyRectangle = new Rectangle();
            MyRectangle.Fill = new SolidColorBrush(Colors.Black);
            MyRectangle.Height = 20;
            MyRectangle.Width = 20;
            MyRectangle.SetValue(Grid.RowProperty, 0);
            MyRectangle.SetValue(Grid.ColumnProperty, 0);

            //Adding the Rectangle to the Grid
            MyGrid.Children.Add(MyRectangle);

            //Creating a Polygon
            Polygon MyPolygon = new Polygon();
            MyPolygon.Points.Add(new Point(2, 0));
            MyPolygon.Points.Add(new Point(22, 0));
            MyPolygon.Points.Add(new Point(2, 40));
            MyPolygon.Stroke = new SolidColorBrush(Colors.Red);
            MyPolygon.Fill = new SolidColorBrush(Colors.Red);
            MyPolygon.SetValue(Grid.RowProperty, 1);
            MyPolygon.SetValue(Grid.ColumnProperty, 0);

            //Adding the Polygon to the Grid
            MyGrid.Children.Add(MyPolygon);

            return MyGrid;
        }

        private void _processShowHistroy(bool bShowAllHistory)
        {
            TextBox_Keyin_Goal.Visibility = System.Windows.Visibility.Collapsed;

            List<SearchHistoryData> showHistory = new List<SearchHistoryData>(8);
            SearchHistoryData ddd = new SearchHistoryData();
            ddd.Address = HISTORY_SIGN;
            ddd.Geocoordinate = "";
            showHistory.Add(ddd);

            if (mSearchedRrecord.Count <= 4)
            {
                foreach (SearchHistoryData sss in mSearchedRrecord)
                    showHistory.Add(sss);
            }
            else
            {
                int count = 0;
                foreach (SearchHistoryData sss in mSearchedRrecord)
                {
                    showHistory.Add(sss);
                    count++;

                    if (bShowAllHistory == false)
                    {
                        if (count == 3)
                        {
                            SearchHistoryData ttt = new SearchHistoryData();
                            ttt.Address = ResourceApp.Resources.AppResources.Goal_History_List;
                            ttt.Geocoordinate = "";
                            showHistory.Add(ttt);
                            break;
                        }
                    }
                }
            }

            if (bShowAllHistory)
                ListPicker_SearchHistory.ExpansionMode = ExpansionMode.FullScreenOnly;
            else
                ListPicker_SearchHistory.ExpansionMode = ExpansionMode.ExpansionAllowed;

            ListPicker_SearchHistory.ItemsSource = showHistory;
            ListPicker_SearchHistory.SelectedIndex = 0;

            //select changed 在ItemsSource設定完之後再設定，就不會有事沒事初始化也在衝進去
            ListPicker_SearchHistory.SelectionChanged += ListPicker_SearchHistory_SelectionChanged;

            ShowHistoryBox();



        }

        private void Button_Click_History(object sender, RoutedEventArgs e)
        {
            mSelectItemInHistory_Listpicker = false;

            _processShowHistroy(false);

            //點到地圖時一律讓app bar消失
            if (ApplicationBar != null)
                ApplicationBar.IsVisible = false;
        }

        void ShowHistoryBox()
        {
            ListPicker_SearchHistory.IsEnabled = true;
            ListPicker_SearchHistory.Visibility = System.Windows.Visibility.Visible;
            ListPicker_SearchHistory.Focus();
            ListPicker_SearchHistory.Open();

            TextBox_Keyin_Goal.Visibility = System.Windows.Visibility.Collapsed;
        }

        void ShowKeyinBox()
        {
            ListPicker_SearchHistory.Visibility = System.Windows.Visibility.Collapsed;
            Button_History.Visibility =
            TextBox_Keyin_Goal.Visibility = System.Windows.Visibility.Visible;


            //搜尋目標時，將歷史紀錄LISTBOX作初始化動作，這個很重要，有設定null他在設定值的時候才會吃新的值
            //if (ListPicker_SearchHistory.ItemsSource != null)
            //    ListPicker_SearchHistory.ItemsSource = null;

            //myADV.Margin = mRec_myADV_Thickness;
            MyMap.Margin = mRec_MyMap_Thickness;

            if (Utility.mHideAdvertisement == false)
            {
                //myADV.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                //myADVBig.Visibility = System.Windows.Visibility.Visible;
                //myADVsmall.Visibility = System.Windows.Visibility.Collapsed;
            }
        }


        void HideKeyinBox()
        {
            Button_History.Visibility =
            TextBox_Keyin_Goal.Visibility = System.Windows.Visibility.Collapsed;

            //_switchToSmallADV();

            //myADV.Margin = new Thickness(0);
            MyMap.Margin = new Thickness(0);
        }

        void TextBox_KeyinGoal_KeyUp(object sender, KeyEventArgs e)
        {
            // if the enter key is pressed 
            if (e.Key == Key.Enter)
            {
                ProcessSearchEnter();
            }
        }


        // string searchOldRec = "";
        void ProcessSearchEnter()
        {
            string sKeyinText = TextBox_Keyin_Goal.Text.Trim();
            TextBox_Keyin_Goal.Text = string.Empty;

            if (string.IsNullOrEmpty(sKeyinText))
                return;

            //   if (sKeyinText == searchOldRec)
            //        return;
            //    searchOldRec = sKeyinText;

            // focus the page in order to remove focus from the text box 
            // and hide the soft keyboard 
            this.Focus();

            //這個要搬家，搬到搜尋成功才給紀錄(再看看吧)
            // ListPicker_SearchHistory_AddItems(sKeyinText);

            _searchForTerm(sKeyinText);
        }

        void TextBox_KeyinGoal_LostFocus(object sender, RoutedEventArgs e)
        {
            //ListPicker_SearchHistory.Visibility = System.Windows.Visibility.Visible;

            //不用輸入時點旁邊讓輸入框消失
            // TextBox textBox = sender as TextBox;
            // textBox.Visibility = System.Windows.Visibility.Collapsed;
        }

        void ListPicker_SearchHistory_LostFocus(object sender, RoutedEventArgs e)
        {
            ShowKeyinBox();//這邊放這個會造成CRASH我也不知為何(物件超過5個他lostFocus裡面設定sourceItem繪壞掉)
        }

        private void MyMap_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //點到地圖時一律讓app bar消失
            if (ApplicationBar != null)
                ApplicationBar.IsVisible = false;

            //點到地圖就讓ListBox消失(應為LISTBOX的LOSTFOCUS在滑地圖時會沒反應)
            if (bSearchStep2)
            {
            }
            else
            {
                //MAP的CENTERCHANGED目前被拿來判斷要讓list提示消失

                //點到地圖就讓ListBox消失(應為LISTBOX的LOSTFOCUS在滑地圖時會沒反應)
                ShowKeyinBox();


            }
        }

        private void MyMap_CenterChanged(object sender, MapCenterChangedEventArgs e)
        {
            //點到地圖時一律讓app bar消失
            if (ApplicationBar != null)
                ApplicationBar.IsVisible = false;

            if (bSearchStep2)
            {
            }
            else
            {
                //MAP的CENTERCHANGED目前被拿來判斷要讓list提示消失

                //點到地圖就讓ListBox消失(應為LISTBOX的LOSTFOCUS在滑地圖時會沒反應)
                ShowKeyinBox();


            }
        }


        private void ListPicker_SearchHistory_ManipulationCompleted_1(object sender, ManipulationCompletedEventArgs e)
        {
            List<SearchHistoryData> items = null;
            if (ListPicker_SearchHistory.ItemsSource != null)
                items = ListPicker_SearchHistory.ItemsSource as List<SearchHistoryData>;

            ShowKeyinBox();

            //最一開始都沒有紀錄的時候，item source會是陣列0，要處理點到沒東西的list picker直接跳去 key in box
            if (ListPicker_SearchHistory.ItemsSource == null ||
                (items.Count == 1 && items[0].Address == HISTORY_SIGN)
                )
            {
                TextBox_Keyin_Goal.Focus();
            }

        }

        //bool mIsFirstOpenSelectIndex0 = false;

        /// <summary>
        /// ListPicker點選會跑這邊，Search完成後將名稱加入history也會跑這邊(但要是，點同一名稱，或是搜尋同一地標目前沒處理)
        /// </summary>
        private void ListPicker_SearchHistory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListPicker_SearchHistory.SelectionChanged -= ListPicker_SearchHistory_SelectionChanged;


            ////把一開始秀LISTPICKER自動去選的index0擋住(不要讓他自動選，問題發生在LISTPICKER沒法控制不要自動選第一個)
            //if (mHistoryListPickerJustOpen && ListPicker_SearchHistory.SelectedIndex == 0)
            //{
            //    mHistoryListPickerJustOpen = false;
            //    return;
            //}

            //if (ListPicker_SearchHistory.SelectedItem == null)
            //    return;

            //mListPicker_SearchHistory_SelectText = ListPicker_SearchHistory.SelectedItem as string;


            if (ListPicker_SearchHistory.SelectedItem == null)
                return;

            if (ListPicker_SearchHistory.SelectedIndex == 0)
            {
                ShowKeyinBox();
                return;//0被"▼"用去了
            }

            mListPicker_SearchHistory_SelectText = ListPicker_SearchHistory.SelectedItem as SearchHistoryData;

            if (mListPicker_SearchHistory_SelectText.Address == HISTORY_SIGN)
            {
                //ListPicker_SearchHistory.SelectedItem = "shit";
                return;
            }

            //選到歷史紀錄全打開頁面
            if (mListPicker_SearchHistory_SelectText.Address == ResourceApp.Resources.AppResources.Goal_History_List)
            {
                //這邊注意要送到dispatcher去，給目前的select changed處理完，才能再去打開list picker才會運作正常
                Utility.GetCurrentDispatcher().BeginInvoke(() =>
                {
                    _processShowHistroy(true);
                });

                return;
            }

            _processSearchHistory(mListPicker_SearchHistory_SelectText, false);//只做整理不存檔

            //_searchForTerm(mListPicker_SearchHistory_SelectText.Address); 不用搜尋了，直接拿座標來用

            TextBox_Keyin_Goal.Text = mListPicker_SearchHistory_SelectText.Address;

            mSelectItemInHistory_Listpicker = true;
            if (mIsLeaveToListpicker == false)
            {
                _processRouteQueryAll();
            }

        }
        bool mSelectItemInHistory_Listpicker = false;

        /// <summary>
        /// 改變一下廣告的位置，讓USER更容易誤按
        /// </summary>
        //private void _switchToSmallADV()
        //{
        //    if (Utility.mNotShowAdvertisement == false)
        //    {

        //        //myADV.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
        //        //myADV.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
        //        myADVBig.Visibility = System.Windows.Visibility.Collapsed;
        //        myADVsmall.Visibility = System.Windows.Visibility.Visible;
        //    }
        //}

        private void _searchForTerm(string sSearch)
        {
            mPinToStart.Visibility=
            mFitSize.Visibility =
            mGoalPosition.Visibility =
                  System.Windows.Visibility.Collapsed;

            Utility.ShowProgressIndicator(ResourceApp.Resources.AppResources.Goal_Search_Places, true);

            //關閉ListPicker_SearchHistory
            ShowKeyinBox();

            TextBox_Keyin_Goal.Text = sSearch;

            mGeocodeQueryCombine.SearchForTerm(sSearch, mNowMyGeoCoordinate, _callback_combime_QuerySearchTerm);
        }

        //取得該SearchHistory座標
        //void Callback_SearchHistory_QueryGlobalPosition(ref System.Windows.Point outPoint)
        //{
        //    GeoCoordinate nowMyGeoCoordinate = new GeoCoordinate(outPoint.X, outPoint.Y);
        //    myGeocodeQuery.SearchForTerm(mListPicker_SearchHistory_SelectText, nowMyGeoCoordinate, Callback_QuerySearchTerm);
        //}

        bool bSearchStep2 = false;
        void _callback_combime_QuerySearchTerm(List<MapApp.GeocodeQueryCombine.SearchResult> results, object source)
        {
            Utility.HideProgressIndicator();

            if (results == null)
            {
                Utility.ShowMessageBox(ResourceApp.Resources.AppResources.Goal_Data_query_fail);
                //MessageBox.Show("找不到阿!");
            }
            else
            {
                if (results.Count == 0)
                {
                    //MessageBox.Show("SearchTerm => Callback_QuerySearchTerm(List<GeoCoordinate> searchCoordinates) => searchCoordinates.size 0");
                }
                else
                {
                    bSearchStep2 = true;
                    if (results.Count > 1)
                    {
                        //到選單內，選到的項目才路徑規劃
                        _openGoalSelectListBox(results);
                        myADVBig.Margin = new Thickness(0);
                    }
                    else
                    {
                        //直接做路徑規劃
                        GeoCoordinate coordDest = new GeoCoordinate(
                            double.Parse(results[0].lat, System.Globalization.CultureInfo.InvariantCulture),
                            double.Parse(results[0].lng, System.Globalization.CultureInfo.InvariantCulture));
                        _SetOpenRouteDetail(coordDest, results[0].formatted_address);
                    }

                    //改變一下廣告的位置，讓USER更容易誤按
                    //_switchToSmallADV();


                }

                string sss = ResourceApp.Resources.AppResources.Goal_N_Places_Found.Replace("{N}", results.Count.ToString());
                Utility.ShowToast(sss, 2000, true);
            }


        }

        SearchInfos mSearchInfos;
        //要是目標搜索不只一個，就秀出一個LIST給USER選擇
        private void _openGoalSelectListBox(List<MapApp.GeocodeQueryCombine.SearchResult> results_list)
        {
            mSearchInfos = new SearchInfos();
            foreach (MapApp.GeocodeQueryCombine.SearchResult result in results_list)
            {
                string subtitle = "";
                if (result.City != null && result.District != null && result.City != string.Empty && result.District != string.Empty)
                    subtitle = result.City + ", " + result.District;
                else if (result.City != null && result.City != string.Empty)
                    subtitle = result.City;
                else if (result.District != null && result.District != string.Empty)
                    subtitle = result.District;
                else if (result.Street != null && result.Street != string.Empty)
                    subtitle = result.Street;

                string title = "";
                if (result.formatted_address != null && result.formatted_address != string.Empty)
                {
                    title = result.formatted_address;
                }
                else
                {
                    title = subtitle;
                    subtitle = "";
                }

                SearchInfo searchInfo = new SearchInfo(title, subtitle, result.lat, result.lng);

                mSearchInfos.Add(searchInfo);
            }
            ListBox_GoalSelect.ItemsSource = mSearchInfos;

            column_select_goal.Width = new GridLength(column_select_goal_width, GridUnitType.Star);
            //show slect list box
            column_select_goal.Width = new GridLength(column_select_goal_width, GridUnitType.Star);

            //select list box NO: 1
            ListBox_GoalSelect.SelectedIndex = 0;

            //Map at left, don't need textbox            
            HideKeyinBox();
        }



        /// <summary>
        /// 由列出的清單中，選一個目標並對他進行路徑規劃
        /// </summary>
        private void ListBox_GoalSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListBox_GoalSelect.SelectedItem == null || ListBox_GoalSelect.SelectedIndex < 0)
                return;

            SearchInfo select = mSearchInfos[ListBox_GoalSelect.SelectedIndex];

            //改變type讓他變色
            foreach (SearchInfo info in mSearchInfos)
                info.Type = "normal";
            select.Type = "selected";
            ListBox_GoalSelect.ItemsSource = mSearchInfos.ToArray();//利用toArray()觸發他換顏色的templete

            _SetOpenRouteDetail(select.geoCoordinate, select.Title);
        }


        /// <summary>
        /// 由列出的清單中，選一個目標並對他進行路徑規劃
        /// </summary>
        private void _SetOpenRouteDetail(GeoCoordinate coordDest, string nameDest)
        {
            if (_travelMode == TravelMode.Driving)
            {
                CompositeTransform transform = Button_Drive.RenderTransform as CompositeTransform;
                transform.ScaleX = 1.2f;
                transform.ScaleY = 1.2f;
                Button_Drive.Background = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));

                transform = Button_Walk.RenderTransform as CompositeTransform;
                transform.ScaleX = 1f;
                transform.ScaleY = 1f;
                Button_Walk.Background = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            }
            else if (_travelMode == TravelMode.Walking)
            {
                CompositeTransform transform = Button_Drive.RenderTransform as CompositeTransform;
                transform.ScaleX = 1f;
                transform.ScaleY = 1f;
                Button_Drive.Background = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));

                transform = Button_Walk.RenderTransform as CompositeTransform;
                transform.ScaleX = 1.2f;
                transform.ScaleY = 1.2f;
                Button_Walk.Background = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
            }

            mPinToStart.Visibility=
            mFitSize.Visibility =
            mGoalPosition.Visibility =
                System.Windows.Visibility.Visible;

            if (Utility.mHideAdvertisement)
                RemoveAdvertisement();

            // Route from current location to first search result
            _routeQueryList.Clear();
            _routeQueryList.Add(mNowMyGeoCoordinate);
            _routeQueryList.Add(coordDest);
            QueryRoute(_routeQueryList, _travelMode);

            _travelName.Address = nameDest;//設定_travelName等下路線規劃完成後要秀主題用
            _travelName.Geocoordinate = string.Format("( {0} , {1} )",
                coordDest.Latitude.ToString(System.Globalization.CultureInfo.InvariantCulture),
                coordDest.Longitude.ToString(System.Globalization.CultureInfo.InvariantCulture)
                );


            mNowDestGeoCoordinate = coordDest;
            CreateDestOverly();

            _openRouteInformationListBox();
        }

        /// <summary>
        /// Method to initiate a route query.
        /// </summary>
        /// <param name="route">List of geocoordinates representing the route</param>
        //private void CalculateRoute()
        //{
        //    //Show Route Information
        //    OpenRouteInformationListBox();//打開路線規劃資訊操控view(唯一可以選擇開車或是步行的地方)
        //}


        // Brush TravelButtonBrush = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
        //  Brush TravelButtonBrush_Pressed = new SolidColorBrush(Color.FromArgb(255, 0, 255, 255));
        //顯示導航的路線文字說明
        private void _openRouteInformationListBox()
        {
            grid_route_information.Height = new GridLength(column_route_information_height, GridUnitType.Star);

            //利用按 開車  步行 的事件觸發去啟動導航
            //if (_travelMode == TravelMode.Driving)
            //{
            //    Button_Drive.BorderBrush = TravelButtonBrush_Pressed;
            //    Button_Walk.BorderBrush = TravelButtonBrush;

            //    //Color ccc = Color.FromArgb(255, 150, 150, 150);
            //    //Brush brrr = new SolidColorBrush(ccc);
            //    //Button_Drive.Background = brrr;
            //    //Button_Walk.Background = brrr;
            //}
            //else
            //{
            //    Button_Drive.BorderBrush = TravelButtonBrush;
            //    Button_Walk.BorderBrush = TravelButtonBrush_Pressed;

            //    //Color ccc = Color.FromArgb(255, 150, 150, 150);
            //    //Brush brrr = new SolidColorBrush(ccc);
            //    //Button_Drive.Background = brrr;
            //    //Button_Walk.Background = brrr;
            //}
        }

        private void QueryRoute(List<GeoCoordinate> routeQuery, TravelMode travelMode)
        {
            Utility.ShowProgressIndicator(ResourceApp.Resources.AppResources.Goal_Route_Planning, true);
            myRouteQuery.QueryRoute(routeQuery, Callback_RouteQueryDone, travelMode);

            mRouteQueryFail = true;
        }

        bool mRouteQueryFail = true;//紀錄路徑規劃是否成功

        LocationRectangle mLocationRectangle = null;
        private void Callback_RouteQueryDone(Route MyRoute, Object tag)
        {

            if (myMapRoute != null)
            {
                MyMap.RemoveRoute(myMapRoute);
                myMapRoute = null;
            }

            if (MyRoute == null)
            {
                mRouteQueryFail = true;
                Utility.ShowToast(ResourceApp.Resources.AppResources.Goal_Route_Planning_Fail, 2000, true);
                Utility.HideProgressIndicator();
                return;
            }

            mRouteQueryFail = false;



            myMapRoute = new MapRoute(MyRoute);
            MyMap.AddRoute(myMapRoute);

            // Center map and zoom so that whole route is visible
            MyMap.SetView(MyRoute.Legs[0].BoundingBox, MapAnimationKind.Parabolic);

            mLocationRectangle = new LocationRectangle(
                MyRoute.Legs[0].BoundingBox.Northwest,
                MyRoute.Legs[0].BoundingBox.Southeast);

            // Update route information and directions
            DestinationText.Text = _travelName.Address;
            double distanceInKm = (double)MyRoute.LengthInMeters / 1000;
            DestinationDetailsText.Text = distanceInKm.ToString("0.0") + " km, "
                                             + MyRoute.EstimatedDuration.Hours + " hrs "
                                             + MyRoute.EstimatedDuration.Minutes + " mins.";

            List<string> routeInstructions = new List<string>();
            foreach (RouteLeg leg in MyRoute.Legs)
            {
                for (int i = 0; i < leg.Maneuvers.Count; i++)
                {
                    RouteManeuver maneuver = leg.Maneuvers[i];
                    string instructionText = maneuver.InstructionText;
                    distanceInKm = 0;

                    if (i > 0)
                    {
                        distanceInKm = (double)leg.Maneuvers[i - 1].LengthInMeters / 1000;
                        instructionText += " (" + distanceInKm.ToString("0.0") + " km)";
                    }
                    routeInstructions.Add(instructionText);
                }
            }

            RouteLLS.ItemsSource = routeInstructions;

            Utility.HideProgressIndicator();
        }

        ///// <summary>
        ///// Helper method to show progress indicator in system tray
        ///// </summary>
        ///// <param name="msg">Text shown in progress indicator</param>
        //private void ShowProgressIndicator(String msg)
        //{
        //    if (mProgressIndicator == null)
        //    {
        //        mProgressIndicator = new ProgressIndicator();
        //        mProgressIndicator.IsIndeterminate = true;
        //    }
        //    mProgressIndicator.Text = msg;
        //    mProgressIndicator.IsVisible = true;
        //    SystemTray.SetProgressIndicator(this, mProgressIndicator);
        //}

        ///// <summary>
        ///// Helper method to hide progress indicator in system tray
        ///// </summary>
        //private void HideProgressIndicator()
        //{
        //    mProgressIndicator.IsVisible = false;
        //    SystemTray.SetProgressIndicator(this, mProgressIndicator);
        //}

        private void Button_Drive_Click(object sender, RoutedEventArgs e)
        {
            if (_travelMode == TravelMode.Driving)
                return;
            _travelMode = TravelMode.Driving;



            CompositeTransform transform = Button_Drive.RenderTransform as CompositeTransform;
            transform.ScaleX = 1.2f;
            transform.ScaleY = 1.2f;
            Button_Drive.Background = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));

            transform = Button_Walk.RenderTransform as CompositeTransform;
            transform.ScaleX = 1f;
            transform.ScaleY = 1f;
            Button_Walk.Background = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));

            QueryRoute(_routeQueryList, _travelMode);
            _openRouteInformationListBox();
        }

        private void Button_Walk_Click(object sender, RoutedEventArgs e)
        {
            if (_travelMode == TravelMode.Walking)
                return;
            _travelMode = TravelMode.Walking;

            CompositeTransform transform = Button_Drive.RenderTransform as CompositeTransform;
            transform.ScaleX = 1f;
            transform.ScaleY = 1f;
            Button_Drive.Background = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));

            transform = Button_Walk.RenderTransform as CompositeTransform;
            transform.ScaleX = 1.2f;
            transform.ScaleY = 1.2f;
            Button_Walk.Background = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));

            QueryRoute(_routeQueryList, _travelMode);
            _openRouteInformationListBox();
        }

        bool mCkickGO = true;
        private void Button_GO_Click(object sender, RoutedEventArgs e)
        {
            if (mCkickGO)
                return;
            mCkickGO = true;

            if (mRouteQueryFail)
            {
                Utility.ShowToast(ResourceApp.Resources.AppResources.Goal_Route_Planning_Fail, 2000, true);
                return;
            }

            _processSearchHistory(_travelName, true);//真正有使用導航才記錄下來

            Utility.OutputDebugString("Button_GO_Click NavigationService.Navigate start");
            //mCallback_GO(_routeQueryList[1], _travelMode);
            string addr = string.Format("/NavierHUD;component/HUD.xaml?longitude={0}&latitude={1}&travelMode={2}&HUDSettingFileName={3}",
                _routeQueryList[1].Longitude, _routeQueryList[1].Latitude, (int)_travelMode, mUserSelectHUDSetting);
            NavigationService.Navigate(new Uri(addr, UriKind.Relative));
            Utility.OutputDebugString("Button_GO_Click NavigationService.Navigate end");
        }

        string mUserSelectHUDSetting = null;
        string mUserSelectHUDShowName;
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            mCkickGO = false;

            if (mIsLeaveToListpicker)
            {
#if DEBUG
                Utility.OutputDebugString("[GoalPage]->OnNavigatedTo() come back form Listpicker");
#endif
            }
            else//從listPicker 回來，就不需要再作一些資料處理的事情了
            {
#if DEBUG
                Utility.OutputDebugString("[GoalPage]->OnNavigatedTo()");
#endif

                CreateGPS();

                //此頁初始狀態，先將自己定位
                StartPositionTracking();

                LoadSearchHistoryFromFile();

                if (this.NavigationContext.QueryString.ContainsKey("HUDSettingFileName"))
                    mUserSelectHUDSetting = this.NavigationContext.QueryString["HUDSettingFileName"];
                if (this.NavigationContext.QueryString.ContainsKey("HUDSettingShowName"))
                    mUserSelectHUDShowName = this.NavigationContext.QueryString["HUDSettingShowName"];
                

                mUserSetting = NavierHUD.HUD.SaveUserSetting.LoadSettingData();
                if (mUserSetting == null)
                    mUserSetting = await NavierHUD.HUD.SaveUserSetting.get();
            }


            ShowKeyinBox();

            //這邊先把他visible，不然會壞掉，loaded完再把他關起來
            ListPicker_SearchHistory.Visibility = System.Windows.Visibility.Visible;

        }

        bool mIsLeaveToListpicker = false;
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            //如果是要打開Listpicker頁面，GPS可以釋放，但是定位座標不用釋放，等等回來時Nav...To那邊直接使用剛剛定位的位置
            mIsLeaveToListpicker = false;
            if (e.Content != null && e.Content.GetType() == typeof(ListPickerPage))
            {
                mIsLeaveToListpicker = true;
            }

            if (mIsLeaveToListpicker)//去list picker就不需要做釋放資料的事情
            {
#if DEBUG
                Utility.OutputDebugString("[GoalPage]->OnNavigatedFrom() goto ListPicker");
#endif
            }
            else
            {
#if DEBUG
                Utility.OutputDebugString("[GoalPage]->OnNavigatedFrom()");
#endif

                ReleaseGPS();

            }


        }

        private void PhoneApplicationPage_BackKeyPress_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ApplicationBar != null && ApplicationBar.IsVisible == true)
            {
                ApplicationBar.IsVisible = false;
                e.Cancel = true;
            }
            else if (ClosePopupWindow())
            {
                e.Cancel = true;
            }
            else if (bSearchStep2)
            {
                initExpandControl();
                ShowKeyinBox();
                bSearchStep2 = false;
                e.Cancel = true;

                //這裡回到一開使操作狀態的話也再把他定位一次
                //StartPositionTracking();
            }

        }

        void StartPositionTracking()
        {
            Utility.ShowProgressIndicator(ResourceApp.Resources.AppResources.Utility_Positioning_PleaseWait, true);

            myGeolocator.StartPositionTracking(PageStart_PositionTracking_CallBack);
            mGeolocatorCount = 0;
        }

        void StopPositionTracking()
        {
            myGeolocator.StopTracking();

            Utility.HideProgressIndicator();
        }

        /// <summary>
        /// We must satisfy Maps API's Terms and Conditions by specifying
        /// the required Application ID and Authentication Token.
        /// See http://msdn.microsoft.com/en-US/library/windowsphone/develop/jj207033(v=vs.105).aspx#BKMK_appidandtoken
        /// </summary>
        private void MyMap_Loaded(object sender, RoutedEventArgs e)
        {
#if DEBUG
#warning Please obtain a valid application ID and authentication token.
#else
#warning You must specify a valid application ID and authentication token.
#endif
            if (Utility.mHideAdvertisement)
            {
                //上傳AP後記得來改這兒(done)
                Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "ab4c59e5-8156-4a2e-bf55-6202baf9a402";
                Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "VK32ukjcfWDf3ieKgUZswg";
            }
            else
            {
                //上傳AP後記得來改這兒(done)
                Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "a84690da-f4c9-4f28-b82f-54d1df33f28c";
                Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "vW4cSEkVM-pF1CpVcy1xlQ";
            }
        }



        /// <summary>
        /// 設定那顆按鈕"齒輪"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Setting_Click(object sender, RoutedEventArgs e)
        {
            BuildLocalizedApplicationBar();
        }

        /// <summary>
        /// 搜索那顆按鈕"放大鏡"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Search_Click(object sender, RoutedEventArgs e)
        {
            //點到地圖時一律讓app bar消失
            if (ApplicationBar != null)
                ApplicationBar.IsVisible = false;

            if (TextBox_Keyin_Goal.Text != null && TextBox_Keyin_Goal.Text.Length > 0)
            {
                ProcessSearchEnter();
            }
            else
            {
                TextBox_Keyin_Goal.Focus();
            }
        }

        /// <summary>
        /// 自己位置至中的按鈕"指南針圖像"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_CurrentPosition_Click(object sender, RoutedEventArgs e)
        {
            //如果已經有目的地出現，就不要再使用GPS定位，功能變成地圖將自己位置致中
            if (mLocationRectangle != null)
                MyMap.SetView(mNowMyGeoCoordinate, 13);
            else
                StartPositionTracking();
        }

        private void Button_GoalPosition_Click(object sender, RoutedEventArgs e)
        {
            MyMap.SetView(mNowDestGeoCoordinate, 13);
        }

        private void Button_FitSize_Click(object sender, RoutedEventArgs e)
        {
            if (mLocationRectangle != null)
                MyMap.SetView(mLocationRectangle, MapAnimationKind.Parabolic);
        }

        /// <summary>
        /// 這東西要再不想看見的廣告有出現去把他visibility看不見才有用
        /// </summary>
        public void RemoveAdvertisement()
        {
            myADVBig.Visibility = System.Windows.Visibility.Collapsed;
            //  myADVsmall.Visibility = System.Windows.Visibility.Collapsed;

            //foreach (UIElement uie in LayoutRoot.Children)
            //{
            //    Microsoft.Advertising.Mobile.UI.AdControl adv = uie as Microsoft.Advertising.Mobile.UI.AdControl;
            //    if(adv != null)
            //    {
            //        if()
            //    }
            //}
        }


        private DispatcherTimer _timerADV = null;
        private void CloseADV_Button_Click_1(object sender, RoutedEventArgs e)
        {
            myADVBig.Visibility = System.Windows.Visibility.Collapsed;

            if (_timerADV == null)
            {
                _timerADV = new DispatcherTimer();
                _timerADV.Interval = TimeSpan.FromMilliseconds(8000);
                _timerADV.Tick += _timer_Tick;
            }

            _timerADV.Stop();
            _timerADV.Start();
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            myADVBig.Visibility = System.Windows.Visibility.Visible;
            _timerADV.Tick -= _timer_Tick;
            _timerADV.Stop();
            _timerADV = null;
        }

        private void TextBox_KeyinGoal_Focus(object sender, RoutedEventArgs e)
        {
            TextBox_Keyin_Goal.SelectAll();
        }

        private void Button_Pin_To_Start(object sender, RoutedEventArgs e)
        {
            string addr = string.Format("/GoalApp;component/StreetViewTilePage.xaml?longitude={0}&latitude={1}&HUDFileName={2}&TravelMode={3}&HUDShowName={4}&GoalName={5}",
                _routeQueryList[1].Longitude, _routeQueryList[1].Latitude, mUserSelectHUDSetting, (int)_travelMode, mUserSelectHUDShowName, TextBox_Keyin_Goal.Text);
            NavigationService.Navigate(new Uri(addr, UriKind.Relative));
        }

        private void PhoneApplicationPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            Utility.ShowProgressIndicator_OrientationChange(e.Orientation);
        }

    }



}