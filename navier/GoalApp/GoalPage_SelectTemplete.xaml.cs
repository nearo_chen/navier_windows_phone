﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;


namespace GoalApp
{
    public abstract class DataTemplateSelector : ContentControl
    {
        public virtual DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            return null;
        }

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);

            ContentTemplate = SelectTemplate(newContent, this);
        }
    }

    public class GoalTemplateSelector : DataTemplateSelector
    {
        public DataTemplate normal
        {
            get;
            set;
        }

        public DataTemplate selected
        {
            get;
            set;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            SearchInfo foodItem = item as SearchInfo;
            if (foodItem != null)
            {
                if (foodItem.Type == "normal")
                {
                    return normal;
                }
                else// if (foodItem.Type == "selected")
                {
                    return selected;
                }
            }

            return base.SelectTemplate(item, container);
        }
    }

    /// <summary>
    /// 搜尋目標多於一個時，用來秀出選項用
    /// </summary>
    public class SearchInfo
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        //   public string District { get; set; }
        //   public string City { get; set; }

       // public string Distance { get; set; }
       // public string costTime { get; set; }
        public GeoCoordinate geoCoordinate { get; set; }
        public SearchInfo(string Title, string SubTitle,
            //string dist, string city,
            //string Distance, string costTime,
            string lat,
            string lng)
        {
            this.Title = Title;
            this.SubTitle = SubTitle;
            //   this.District = dist;
            //   this.City = city;

        //    this.Distance = Distance;
         //   this.costTime = costTime;
            this.geoCoordinate = new GeoCoordinate(
                double.Parse(lat, System.Globalization.CultureInfo.InvariantCulture),
                double.Parse(lng, System.Globalization.CultureInfo.InvariantCulture));
            Type = "normal";
        }

        public string Type
        {
            get;
            set;
        }
    }

    public class SearchInfos : ObservableCollection<SearchInfo>
    {
    }

    
}