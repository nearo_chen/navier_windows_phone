﻿using Microsoft.Phone.BackgroundAudio;
using MyUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GoogleVoiceApp1
{
    /// <summary>
    /// on navigate from 時要DISPOSE()
    /// </summary>
    public class GoogleTranslateVoice : IDisposable
    {
        private readonly string FOLDER = "google_translate_voice";
        int mNowSaveFileIndex;
        List<int> mVoiceNameList = new List<int>();


        public GoogleTranslateVoice()
        {
            mNowSaveFileIndex = 0;

            try
            {
                BackgroundAudioPlayer.Instance.Stop();
                IsolatedStorageHelper.DeleteFolder(FOLDER);
            }
            catch (Exception e) { }

            try
            {
                IsolatedStorageHelper.CreateFolder(FOLDER);
            }
            catch (Exception e) { }
            //IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
            //if (myIsolatedStorage.DirectoryExists(FOLDER) != true)
            //    myIsolatedStorage.CreateDirectory(FOLDER);
        }

        ~GoogleTranslateVoice()
        {
            Dispose();
        }

        public void Dispose()
        {
            BackgroundAudioPlayer.Instance.Stop();

            //把google translate voice資料夾刪除
            //IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
            //if (myIsolatedStorage.DirectoryExists(FOLDER))
            //    myIsolatedStorage.DeleteDirectory(FOLDER);

            Utility.GetCurrentDispatcher().BeginInvoke(() =>
            {
                IsolatedStorageHelper.DeleteFolder(FOLDER);
            });
        }

        string[] mSaveSpeechText = new string[16];
        string mOldText = "";
        public void QueryVoice(string text)
        {
            if (Utility.IsNetworkAvailable() == false)
            {
                Utility.ShowToast(text, 2, true);
                return;
            }

            if (mOldText == text)
            {
                BackgroundAudioPlayer.Instance.Stop();
                if (mAudioTrack != null)
                {
                    BackgroundAudioPlayer.Instance.Track = mAudioTrack;
                    BackgroundAudioPlayer.Instance.Play();
                }
                return;
            }
            mOldText = text;

            mAudioTrack = null;

            try
            {
                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += _client_OpenReadCompleted;

                string language = Utility.GetCurrentLanguageName();
                if (language == "fa")//伊朗文 ISO 639-1 iran [Farsi]
                    language = "ar";

                string content =
                    string.Format("http://translate.google.com/translate_tts?ie=utf-8&tl={0}&q={1}",
                    language,
                    text);

                // Convert the string into a byte[].
                //byte[] asciiBytes = Encoding.UTF8.GetBytes(content);
                // char[] asciiChars = Encoding.UTF8.GetChars(asciiBytes);
                //string contentASCII = new string(asciiChars);


                Uri uri = new Uri(content);
                webClient.OpenReadAsync(uri);

                if (mNowSaveFileIndex >= mSaveSpeechText.Length)
                    Array.Resize(ref mSaveSpeechText, mSaveSpeechText.Length + 16);
                mSaveSpeechText[mNowSaveFileIndex] = text;
            }
            catch (Exception e)
            {
            }
        }

        private async void _client_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                //var file = IsolatedStorageFile.GetUserStoreForApplication();

                Stream sss = e.Result;
                mNowSaveFileIndex++;
                string fileName = string.Format("google_translate_voice_{0}.mp3", mNowSaveFileIndex);

                await IsolatedStorageHelper.WriteToFile_Local(
                   fileName,
                    //  FOLDER + "/google_translate_voice_" + mNowSaveFileIndex.ToString() + ".mp3",
                    sss,
                    _writeToFile_Local_Success,
                    FOLDER
                    );

            }
            else
            {
                //連google translate都念不出來的語言，就沒法了，用TOAST吧
                Utility.ShowToast(mSaveSpeechText[mNowSaveFileIndex], 2, true);
                mNowSaveFileIndex++;
            }

            WebClient webClient = sender as WebClient;
            webClient.OpenReadCompleted -= _client_OpenReadCompleted;
        }

        AudioTrack mAudioTrack;
        private void _writeToFile_Local_Success(string filename)
        {
            BackgroundAudioPlayer.Instance.Stop();

            mAudioTrack = new AudioTrack(
                        new Uri(
                //filename,
                            string.Format("{0}/{1}", FOLDER, filename),
                            UriKind.Relative),
                            "",
                            "",
                            "",
                            null);

            BackgroundAudioPlayer.Instance.Track = mAudioTrack;
            BackgroundAudioPlayer.Instance.Play();
        }


        /// <summary>
        /// 檢查目前語言是否有win phone的TTS
        /// </summary>
        /// <returns></returns>
        public static bool CheckTTS_Language(string sLanguageISO639_1_CurrentLanguageRegionName)
        {
            //檢查目前語言是否有win phone的TTS，要是沒有就使用google translate 的TTS
            bool bIsWPTTS = false;

            try
            {
                var allLanguage =
                      from voice in Windows.Phone.Speech.Synthesis.InstalledVoices.All
                      orderby voice.Language
                      select voice;

               // string myCulture = Utility.GetCurrentLanguageRegionName();
                foreach (Windows.Phone.Speech.Synthesis.VoiceInformation vi in allLanguage)
                {
                    if (vi.Language == sLanguageISO639_1_CurrentLanguageRegionName)
                    {
                        bIsWPTTS = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show("Could not initialize the ListBox: " + ex.Message);
            }

            return bIsWPTTS;
        }
    }
}
