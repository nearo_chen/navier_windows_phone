﻿using BingTranslationApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BingTranslationApp
{
    /// <summary>
    /// 用來做翻譯的class
    /// </summary>
    public class BingTranslation
    {
        public delegate void Callback_TranslateDone(string[] finalText);
        Callback_TranslateDone mCallback_TranslateDone = null;

        TranslationService _translator;
        string mLanguageFrom, mLanguageTo;
        string[] mTranslateTexts;//要送去翻譯的字陣列

        string[] mResultTexts;
        int mCount;
        bool mDone = false;
        object mTranslateLock = new object();

        public BingTranslation()
        {
            _translator = new TranslationService();
            _translator.TranslationComplete += _translator_TranslationComplete;
            _translator.TranslationFailed += _translator_TranslationFailed;
        }

        void _translator_TranslationComplete(object sender, TranslationCompleteEventArgs e)
        {
            lock (mTranslateLock)
            {
                mResultTexts[mCount] = e.Translation;

                if (_moveTranslateNext() == false)
                {
                    mDone = true;
                    if (mCallback_TranslateDone != null)
                        mCallback_TranslateDone(mResultTexts);
                }
            }

            //Deployment.Current.Dispatcher.BeginInvoke(() =>
            //{
            //    Textbox2.Text = e.Translation;
            //});
        }

        void _translator_TranslationFailed(object sender, TranslationFailedEventArgs e)
        {
            //Deployment.Current.Dispatcher.BeginInvoke(() =>
            //{
            //    MessageBox.Show("Bummer, the translation failed. \n " + e.ErrorDescription);
            //});
        }

        public void TranslateString(string languageFrom, string languageTo, string[] translateTexts, Callback_TranslateDone callback_TranslateDone)
        {
            lock (mTranslateLock)
            {
                mDone = false;
                mCallback_TranslateDone = callback_TranslateDone;

                mLanguageFrom = languageFrom;
                mLanguageTo = languageTo;
                mTranslateTexts = translateTexts;
                mResultTexts = new string[translateTexts.Length];
                mCount = 0;

                _translator.GetTranslation(translateTexts[mCount], languageFrom, languageTo);
            }
        }

        /// <summary>
        /// 因為我不會 TranslateArray的用法，所以一個一個把他翻完
        /// </summary>
        bool _moveTranslateNext()
        {
            mCount++;
            if (mCount == mTranslateTexts.Length)
                return false;

            _translator.GetTranslation(mTranslateTexts[mCount], mLanguageFrom, mLanguageTo);
            return true;
        }

        public string[] GetResult()
        {
            lock (mTranslateLock)
            {
                if (mDone)
                {
                    string[] sss = mResultTexts;
                    mResultTexts = null;
                    return sss;
                }
                return null;
            }
        }
    }
}
