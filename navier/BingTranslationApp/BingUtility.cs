﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BingTranslationApp
{
    public static class BingUtility
    {
        private static BingTranslation _BingTranslation = new BingTranslation();
        private static bool bTranslating = false;

        /// <summary>
        /// 輸入中文，將它翻譯成各國語言(meter會翻成米，要是我用公尺去翻就不會有米這種結果)
        /// </summary>
        /// <param name="to"></param>
        /// <param name="testsInChineseTW"></param>
        public static void TryTranslationString(string toLanguage, string[] testsInChineseTW)
        {
            if (bTranslating)
                return;
            bTranslating = true;
            _BingTranslation.TranslateString("zh-TW", toLanguage, testsInChineseTW, null);
        }

        public static string[] TranslationResult()
        {
            string[] results = _BingTranslation.GetResult();
            if (results != null)
                bTranslating = false;
            return results;
        }

        

    }
}
