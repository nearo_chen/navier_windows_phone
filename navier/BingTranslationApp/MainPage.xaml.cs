﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BingTranslationApp.Resources;
using BingTranslationApp.Services;

//做法是抄這邊來的
//http://matthiasshapiro.com/2012/10/09/azure-microsoft-translation-in-windows-phone-the-easy-way/

//MSDN也有詳細的解說
//http://msdn.microsoft.com/en-us/library/ff512437.aspx

namespace BingTranslationApp
{
   
    

    public partial class MainPage : PhoneApplicationPage
    {
        BingTranslation mBingTranslation;
        public MainPage()
        {
            InitializeComponent();
            mBingTranslation = new BingTranslation();
        }

        private void click_event(object sender, RoutedEventArgs e)
        {
            mBingTranslation.TranslateString("zh-TW", "en", new string[] { "公尺", "公里" }, Callback_TranslateDone);
            //mBingTranslation.TranslateString("zh-TW", "jp", new string[] { "公尺", "公里" }, Callback_TranslateDone);
            //mBingTranslation.TranslateString("en", "zh-TW", new string[] { "kilometer", "Meter" }, Callback_TranslateDone);
        }

        void Callback_TranslateDone(string[] finalText)
        {
            Dispatcher.BeginInvoke(() =>
            {
                Textbox1.Text = finalText[0];
                Textbox2.Text = finalText[1];
            });
        }
    }
}