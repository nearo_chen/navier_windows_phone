﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ResourceApp.Resources;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MyUtility;
using System.IO.IsolatedStorage;
using System.Collections.ObjectModel;
using ComponentEditorApp;
using System.Threading.Tasks;

namespace NavierHUD
{
    public partial class MainPage : PhoneApplicationPage
    {
    }

    public class HUDFileDataList : ObservableCollection<HUDFileData>
    {
    }

    public class HUDFileData
    {
        public string NameShow { get; set; }
        public string FileName { get; set; }
        public ComponentEditorData ComponentData { get; set; }
    }

    public abstract class DataTemplateSelector : ContentControl
    {
        public virtual DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            return null;
        }

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);

            ContentTemplate = SelectTemplate(newContent, this);
        }
    }

    public class HUDNameTemplateSelector : DataTemplateSelector
    {
        public DataTemplate Navigator //有導航的原件
        {
            get;
            set;
        }

        public DataTemplate HUD  //沒導航的原件
        {
            get;
            set;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            HUDFileData foodItem = item as HUDFileData;
            if (foodItem != null)
            {
                bool isNavigator = EditorSetting.CheckHasNavigationComponent(foodItem.ComponentData);

                if (isNavigator)
                {
                    return Navigator;
                }
                else// if (foodItem.Type == "selected")
                {
                    return HUD;
                }
            }
            return base.SelectTemplate(item, container);
        }
    }
}