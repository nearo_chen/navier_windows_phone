﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ResourceApp.Resources;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MyUtility;
using System.IO.IsolatedStorage;
using System.Collections.ObjectModel;
using ComponentEditorApp;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Animation;
using System.Threading.Tasks;
using Microsoft.Phone.Maps.Services;


namespace NavierHUD
{
    public partial class MainPage : PhoneApplicationPage
    {
        private const string sLocationPrivacyStatement =
            "http://www.windowsphone.com/xxxxxx/legal/wp8/windows-phone-privacy-statement?signin=true#CollectUseLocPosInfo";

        HUDFileDataList mHUDNameList = new HUDFileDataList();
        string mHUDSettingFileName_FromTile = null;
        string mLat_FromTile, mLng_FromTile, mGoalName_FromTile, mTravelMode_FromTile;

        // Constructor
        public MainPage()
        {
            Utility.mHideAdvertisement = true;


            InitializeComponent();


            ShowAppBar();

            mListBox_HUDFiles.Width = Utility.GetDeviceWidth(true);// -ApplicationBar.DefaultSize;//設定寬看起來比較正常

            //    Utility.EnableDebugSetting();
            Utility.SetCurrentDispatcher(this.Dispatcher);
        }

        private void ShowConfirmPrivacy()
        {
            StackPanel stackPanel = new StackPanel();
            stackPanel.Orientation = System.Windows.Controls.Orientation.Vertical;

            /*TextBlock textBlockTitle = new TextBlock();
            textBlockTitle.Text = AppResources.LocationUsageQueryText;
            textBlockTitle.Margin = new Thickness(12,0,12,0);         
            textBlockTitle.TextWrapping = TextWrapping.Wrap;
            textBlockTitle.FontWeight = new System.Windows.FontWeight();
            textBlockTitle.FontSize = 44;
           
            stackPanel.Children.Add(textBlockTitle);

            TextBlock textBlockContent = new TextBlock();
            textBlockContent.Text = AppResources.LocationUsageQueryText;
            textBlockContent.Margin = new Thickness(12,0,12,0);
            textBlockContent.TextWrapping = TextWrapping.Wrap;
            textBlockTitle.FontSize = 30;
            stackPanel.Children.Add(textBlockContent);
            */

            Image image = new Image();
            BitmapImage imageSource = new BitmapImage(new Uri("/Assets/icon_template.png", UriKind.Relative));
            image.Source = imageSource;
            image.Width = image.Height = 100;
            image.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            image.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            stackPanel.Children.Add(image);

            string statement = sLocationPrivacyStatement.Replace("xxxxxx", Utility.GetCurrentLanguageRegionName());
            HyperlinkButton hyperlinkButton = new HyperlinkButton()
            {
                Content = AppResources.LocationPrivacyStatement,
                Margin = new Thickness(0, 28, 0, 8),
                HorizontalAlignment = HorizontalAlignment.Left,
                NavigateUri = new Uri(statement, UriKind.Absolute)
            };
            stackPanel.Children.Add(hyperlinkButton);

            TiltEffect.SetIsTiltEnabled(stackPanel, true);

            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Caption = AppResources.LocationUsageQueryText,
                Message = AppResources.LocationUsageInfoText,
                Content = stackPanel,
                LeftButtonContent = AppResources.Utility_ButtonOK,
                RightButtonContent = AppResources.Utility_ButtonCancel,
                IsFullScreen = true,
            };

            messageBox.Dismissed += OnMessageBoxDismissed;
            messageBox.Show();
        }

        private void OnMessageBoxDismissed(object sender, DismissedEventArgs e)
        {
            if (CustomMessageBoxResult.LeftButton == e.Result)
            {
                _saveLocationConsent(true);

                Utility.CheckLocation();
            }
            else if (CustomMessageBoxResult.RightButton == e.Result)
            {
                _saveLocationConsent(false);

                //參考資料:
                //www.stackoverflow.com/questions/15052311/how-to-programatically-through-code-to-exit-or-quit-from-windows-phone-8-app
                Application.Current.Terminate();
            }
            else
            {
            }
        }

        private async void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {

        }

        private void _saveLocationConsent(bool bConfirm)
        {
            //就存個true吧，false輪不到儲存我就已經把程式關閉了，所以不會有false這種東西
            IsolatedStorageSettings.ApplicationSettings["LocationConsent"] = bConfirm;
            IsolatedStorageSettings.ApplicationSettings.Save();
        }


        HUD.SaveUserSetting.UserSetting mUserSetting;
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            //檢查是否有經由TILE啟動傳入的資訊，要是有的畫，更新component 清單之後要直接去啟動
            mHUDSettingFileName_FromTile = null;
            mGoalName_FromTile = null;
            if (this.NavigationContext.QueryString.ContainsKey("User_HUDSettingFileName"))
            {
                mHUDSettingFileName_FromTile = this.NavigationContext.QueryString["User_HUDSettingFileName"];
                Utility.ShowProgressIndicator("...", true);
            }
            else if (this.NavigationContext.QueryString.ContainsKey("Navi_HUDSettingFileName"))
            {
                mHUDSettingFileName_FromTile = this.NavigationContext.QueryString["Navi_HUDSettingFileName"];
                mLat_FromTile = this.NavigationContext.QueryString["Lat"];
                mLng_FromTile = this.NavigationContext.QueryString["Lng"];
                mGoalName_FromTile = this.NavigationContext.QueryString["GoalName"];
                mTravelMode_FromTile = this.NavigationContext.QueryString["TravelMode"];
                Utility.ShowProgressIndicator(mGoalName_FromTile, true);
                Utility.ShowProgressIndicator_updateInfo(
                    (mTravelMode_FromTile=="0")?"Driving":"Walking" );
            }
            this.NavigationContext.QueryString.Clear();

            RouteRendererComponent.ReCreatDistanceSpeedCalculator();//為了保險起見這邊也放一個初始化

            if (IsolatedStorageSettings.ApplicationSettings.Contains("LocationConsent")
                && (bool)IsolatedStorageSettings.ApplicationSettings["LocationConsent"] == true)
            {
                // User has already opted in or out of Location
                //return;

                Utility.CheckLocation();
            }
            else if (IsolatedStorageSettings.ApplicationSettings.Contains("LocationConsent")
                && (bool)IsolatedStorageSettings.ApplicationSettings["LocationConsent"] == false)
            {
                ShowConfirmPrivacy();
            }
            else
            {
                ShowConfirmPrivacy();
            }

            //讀取User 設定檔
            mUserSetting = HUD.SaveUserSetting.LoadSettingData();
            if (mUserSetting == null)
                mUserSetting = await NavierHUD.HUD.SaveUserSetting.get();

            mListBox_HUDFiles.SelectedIndex = -1;

            //(觸發)抓取翻譯資訊
            Utility.GetTranslate_Meter();
            Utility.GetTranslate_Next();

            //更新component 清單
            await RefreshComponentEditorFiles();

           
        }

        async Task RefreshComponentEditorFiles()
        {
            //更新HUD清單
            mHUDNameList.Clear();

            //預設的2個
            HUDFileData data;
            data = new HUDFileData();
            data.NameShow = AppResources.Main_ClassicNavigation;
            data.FileName = ComponentEditorPage.DefaultNavi_Filename;

            //async 似乎不要弄2層，不然會往嚇跑
            byte[] bytearray = await IsolatedStorageHelper.ReadFile_Installation("HUDData/" + data.FileName);
            data.ComponentData =
                (ComponentEditorData)IsolatedStorageHelper.Deserialize_JSON(bytearray, typeof(ComponentEditorData));
            //data.ComponentData = await EditorSetting.get_Installation("HUDData/" + data.FileName);

            mHUDNameList.Add(data);

            data = new HUDFileData();
            data.NameShow = AppResources.Main_DigitalDashboard;// "數位儀錶板";
            data.FileName = ComponentEditorPage.DefaultHUD_Filename;

            //async 似乎不要弄2層，不然會往嚇跑
            bytearray = await IsolatedStorageHelper.ReadFile_Installation("HUDData/" + data.FileName);
            data.ComponentData =
                (ComponentEditorData)IsolatedStorageHelper.Deserialize_JSON(bytearray, typeof(ComponentEditorData));
            //data.ComponentData = await EditorSetting.get_Installation("HUDData/" + data.FileName);

            mHUDNameList.Add(data);

            //搜尋所有component page .txt檔
            List<string> allfilenames = new List<string>();
            await IsolatedStorageHelper.GetFolder_AllFileName(ComponentEditorPage.FOLDER, allfilenames);
            foreach (string filename in allfilenames)
            {
                data = new HUDFileData();

                bytearray = await IsolatedStorageHelper.ReadFile_Local(filename, ComponentEditorPage.FOLDER);
                data.ComponentData =
                    (ComponentEditorData)IsolatedStorageHelper.Deserialize_JSON(bytearray, typeof(ComponentEditorData));
                // data.ComponentData = await EditorSetting.get_Local(filename, ComponentEditorPage.FOLDER);

                data.NameShow = data.ComponentData.ShowName;
                data.FileName = data.ComponentData.FileName;

                mHUDNameList.Add(data);
            }
            this.mListBox_HUDFiles.ItemsSource = mHUDNameList;


            //檢查是否要直接由TILE啟動
            if(mHUDSettingFileName_FromTile!=null)
            {
                foreach(HUDFileData huddata in mHUDNameList)
                {
                    if (huddata.FileName == mHUDSettingFileName_FromTile)
                    {
                        this.mListBox_HUDFiles.SelectedItem = huddata;
                        Utility.HideProgressIndicator();
                        break;
                    }
                }
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);


        }

       

        ///<summary>
        ///直接拿導航需要的元件去HUD
        ///</summary>
        void GoTo_HUD_Navi(string sHUDFileName, string sGoalName)
        {
            //mCallback_GO(_routeQueryList[1], _travelMode);
            string addr = string.Format("/NavierHUD;component/HUD.xaml?longitude={0}&latitude={1}&travelMode={2}&HUDSettingFileName={3}&goalName={4}",
               mLng_FromTile, mLat_FromTile, mTravelMode_FromTile, sHUDFileName, sGoalName);
            NavigationService.Navigate(new Uri(addr, UriKind.Relative));

            Utility.ShowProgressIndicator(mGoalName_FromTile, true);
        }

        void GoTo_GoalPage(string sHUDFileName, string sHUDShowName)
        {
            //NavigationService.Navigate(new Uri("/GoalApp;component/GoalPage.xaml", UriKind.Relative));
            string addr = string.Format("/GoalApp;component/GoalPage.xaml?HUDSettingFileName={0}&HUDSettingShowName={1}",
                sHUDFileName /*"Default.txt"*/, sHUDShowName);
            NavigationService.Navigate(new Uri(addr, UriKind.Relative));

            Utility.ShowProgressIndicator("starting...", true);
        }

        void GoTo_HUDPage(string sHUDFileName)
        {
            //mCallback_GO(_routeQueryList[1], _travelMode);
            string addr = string.Format("/NavierHUD;component/HUD.xaml?longitude={0}&latitude={1}&travelMode={2}&HUDSettingFileName={3}",
                -1, -1, -1, sHUDFileName);
            NavigationService.Navigate(new Uri(addr, UriKind.Relative));

            // Utility.ShowProgressIndicator("starting...", true);
        }

        void GoTo_ComponentEditorPage(string sOpenfilename)
        {
            //mCallback_GO(_routeQueryList[1], _travelMode);
            string addr = string.Format("/ComponentEditorApp;component/ComponentEditorPage.xaml?openfilename={0}",
                sOpenfilename);
            NavigationService.Navigate(new Uri(addr, UriKind.Relative));

            // Utility.ShowProgressIndicator("starting...", true);
        }

        //bool bListBox_HUDFiles_MouseLeftButtonDown = false;//徒手幹一個SELECT CHANGED
        //string sListBox_HUDFiles_MouseLeftButtonDownName="";
        //private void ListBox_HUDFiles_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        //{
        //    //HUDFileData data = mListBox_HUDFiles.SelectedItem as HUDFileData;
        //    //if (data == null)
        //    //    return;
        //    //sListBox_HUDFiles_MouseLeftButtonDownName = data.NameShow;
        //    //bListBox_HUDFiles_MouseLeftButtonDown = true;
        //}

        private void mListBox_HUDFiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HUDFileData data = mListBox_HUDFiles.SelectedItem as HUDFileData;
            if (data == null)
                return;
            IsolatedStorageSettings.ApplicationSettings["SelectedHUDFile"] = data.ComponentData;

            //如果有要導航的原件，就要去GOAL PAGE選擇目的地
            if (EditorSetting.CheckHasNavigationComponent(data.ComponentData))
            {
                if (string.IsNullOrEmpty(mGoalName_FromTile))
                    GoTo_GoalPage(data.FileName, data.NameShow);
                else
                    GoTo_HUD_Navi(data.FileName, mGoalName_FromTile);
            }
            else
            {
                GoTo_HUDPage(data.FileName);
            }
        }


        //private void ListBox_HUDFiles_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        //{
        //    //if (bListBox_HUDFiles_MouseLeftButtonDown == false)
        //    //    return;
        //    //HUDFileData data = mListBox_HUDFiles.SelectedItem as HUDFileData;
        //    //if (data == null)
        //    //    return;
        //    //if (data.NameShow != sListBox_HUDFiles_MouseLeftButtonDownName)
        //    //    return;

        //    //bListBox_HUDFiles_MouseLeftButtonDown = false;

        //    //IsolatedStorageSettings.ApplicationSettings["SelectedHUDFile"] = data.ComponentData;

        //    ////如果有要導航的原件，就要去GOAL PAGE選擇目的地
        //    //if (EditorSetting.CheckHasNavigationComponent(data.ComponentData))
        //    //{
        //    //    GoTo_GoalPage(data.FileName);
        //    //}
        //    //else
        //    //{
        //    //    GoTo_HUDPage(data.FileName);
        //    //}
        //}



        private void Upgrade_Button_Click_1(object sender, RoutedEventArgs e)
        {
            MarketplaceDetailTask marketplaceDetailTask = new MarketplaceDetailTask();
            marketplaceDetailTask.ContentIdentifier = "c14e93aa-27d7-df11-a844-00237de2db9e";
            //marketplaceDetailTask.ContentType = MarketplaceContentType.Applications; ContentType默认为MarketplaceContentType.Applications，不用设置就可以
            marketplaceDetailTask.Show();

            //MarketplaceSearchTask marketplaceSearchTask = new MarketplaceSearchTask();
            //marketplaceSearchTask.ContentType = MarketplaceContentType.Applications;           
            //marketplaceSearchTask.SearchTerms = "Skype";
            //marketplaceSearchTask.Show();
        }

        private void ButtonAddNewComponentPage_Click(object sender, RoutedEventArgs e)
        {
            GoTo_ComponentEditorPage("");
        }

        private void MenuItem_Click_MODIFY(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            GoTo_ComponentEditorPage((string)menuItem.Tag);
        }

        private async void MenuItem_Click_DELETE(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            string filename = menuItem.Tag as string;
            if (filename.Equals(ComponentEditorPage.DefaultNavi_Filename) || filename.Equals(ComponentEditorPage.DefaultHUD_Filename))
            {
                Utility.ShowToast("Cannot delete!", 1000, true);
                return;
            }

            await IsolatedStorageHelper.DeleteFile_Local(filename, ComponentEditorPage.FOLDER);

            await RefreshComponentEditorFiles();
        }

        private void MenuItem_Click_PinToStart(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            HUDFileData HUDData = menuItem.Tag as HUDFileData;

            //如果有要導航的原件
            if (EditorSetting.CheckHasNavigationComponent(HUDData.ComponentData))
            {
                _pinHUDToStart(HUDData.NameShow, HUDData.FileName, Utility.NavigationComponentColor);
            }
            else
            {
                _pinHUDToStart(HUDData.NameShow, HUDData.FileName, Color.FromArgb(255, 142, 194, 207));
            }


            
        }

        private void _pinHUDToStart(string sHUDShowName, string sHUDFileName, Color HUDTypeColor)
        {
            //  sHUDFileName = "國國國國國國國國國國國國";
            string sTileProperties = "User_HUDSettingFileName=" + sHUDFileName;
            ShellTile tile = ShellTile.ActiveTiles.FirstOrDefault(
                    x => x.NavigationUri.ToString().Contains(sTileProperties));
            if (tile == null)
            {
                Utility.SetTileData_HUD(sHUDShowName, sTileProperties, tile, HUDTypeColor);
            }
            else
            {
                //Update Live Tile
                //tile.Update(flipTile);
                Utility.ShowToast("already pinned", 2000, true);
            }
        }

        






    }
}