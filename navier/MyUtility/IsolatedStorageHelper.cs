﻿using System.IO;
using System.IO.IsolatedStorage;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using System;
using System.Collections.Generic;

namespace MyUtility
{
    /// <summary>
    /// IsolatedStorageHelper class
    /// </summary>
    public static class IsolatedStorageHelper
    {
        ///// <summary>
        ///// Get T object
        ///// </summary>
        ///// <typeparam name="T">The T parameter</typeparam>
        ///// <param name="key">The key</param>
        ///// <returns>One T</returns>
        //public static T GetObject<T>(string key)
        //{
        //    if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
        //    {
        //        string serializedObject = IsolatedStorageSettings.ApplicationSettings[key].ToString();
        //        return Deserialize<T>(serializedObject);
        //    }

        //    return default(T);
        //}

        ///// <summary>
        ///// Save object T
        ///// </summary>
        ///// <typeparam name="T">The T</typeparam>
        ///// <param name="key">The key</param>
        ///// <param name="objectToSave">The object to save</param>
        //public static void SaveObject<T>(string key, T objectToSave)
        //{
        //    string serializedObject = Serialize(objectToSave);
        //    IsolatedStorageSettings.ApplicationSettings[key] = serializedObject;
        //}

        ///// <summary>
        ///// Delete an object
        ///// </summary>
        ///// <param name="key">The key</param>
        //public static void DeleteObject(string key)
        //{
        //    IsolatedStorageSettings.ApplicationSettings.Remove(key);
        //}


        /// <summary>
        /// 寫入檔案。
        /// </summary>
        /// <returns></returns>
        //public static async Task WriteToFile_Local(string filename, byte[] fileBytes)
        //{
        //    // 將輸入於TextBox中的內容，轉成byte[]準備成為Stream寫入
        //    // byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(this.textBox1.Text.ToCharArray());

        //    // 取得Local Folder的根目錄
        //    StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;

        //    try
        //    {
        //        // 建立一個filename的檔案，並且指定如果遇到同名稱檔案將它覆寫
        //        StorageFile file = await local.CreateFileAsync(filename,
        //                               CreationCollisionOption.ReplaceExisting);

        //        // 透過StorageFile將檔案非同步使用Stream寫入；
        //        var s = await file.OpenStreamForWriteAsync();
        //        s.Write(fileBytes, 0, fileBytes.Length);

        //        s.Close();
        //    }
        //    catch(Exception e)
        //    {
        //        Utility.ShowMessageBox(e.Message);
        //    }
        //}

        private static byte[] WriteToFile_Local_buffer = new byte[1024];

        /// <summary>
        /// 寫入檔案。
        /// </summary>
        /// <returns></returns>
        public static async Task WriteToFile_Local(string filename, byte[] fileBytes)
        {

            MemoryStream fileBytesStream = new MemoryStream(fileBytes);
            await WriteToFile_Local(filename, fileBytesStream, null, null);
        }

        /// <summary>
        /// 寫入檔案。
        /// </summary>
        /// <returns></returns>
        public static async Task WriteToFile_Local(string filename, Stream fileBytesStream)
        {

            await WriteToFile_Local(filename, fileBytesStream, null, null);
        }

        public delegate void WriteToFile_Local_Success(string fileName);

        /// <summary>
        /// 寫入檔案。
        /// </summary>
        /// <returns></returns>
        public static async Task WriteToFile_Local(string filename, Stream fileBytesStream, WriteToFile_Local_Success callback, string oneLevelFolder)
        {

            // 將輸入於TextBox中的內容，轉成byte[]準備成為Stream寫入
            // byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(this.textBox1.Text.ToCharArray());

            try
            {
                // 取得Local Folder的根目錄
                StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;

                StorageFile file = null;
                if (oneLevelFolder != null && oneLevelFolder.Length > 0)
                {
                    var dataFolder = await local.CreateFolderAsync(oneLevelFolder, CreationCollisionOption.OpenIfExists);
                    file = await dataFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);
                }
                else
                {
                    // 建立一個filename的檔案，並且指定如果遇到同名稱檔案將它覆寫
                    file = await local.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);
                }


                // 建立一個filename的檔案，並且指定如果遇到同名稱檔案將它覆寫
                //StorageFile file = await local.CreateFileAsync(filename,
                //                       CreationCollisionOption.ReplaceExisting);

                // 透過StorageFile將檔案非同步使用Stream寫入；
                var s = await file.OpenStreamForWriteAsync();
                // s.Write(fileBytes, 0, fileBytes.Length);

                fileBytesStream.Seek(0, SeekOrigin.Begin);
                int byteCount;

                //一次寫1024
                while ((byteCount = fileBytesStream.Read(WriteToFile_Local_buffer, 0, WriteToFile_Local_buffer.Length)) > 0)
                {
                    s.Write(WriteToFile_Local_buffer, 0, byteCount);
                }

                s.Close();



                if (callback != null)
                    callback(filename);
            }
            catch (Exception e)
            {
#if DEBUG
                Utility.ShowMessageBox(filename + "  " + e.Message);
#else
                Utility.ShowToast(filename + "  " + e.Message, 10000, true);
#endif
            }
        }


        /// <summary>
        /// 讀取檔案。Local
        /// </summary>
        /// <returns></returns>
        public static async Task<byte[]> ReadFile_Local(string filename, string oneLevelFolder)
        {
            // 取得Local Folder的根目錄
            StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;

            if (local != null)
            {
                try
                {
                    Stream file = null;
                    if (oneLevelFolder != null && oneLevelFolder.Length > 0)
                    {
                        var dataFolder = await local.CreateFolderAsync(oneLevelFolder, CreationCollisionOption.OpenIfExists);
                        file = await dataFolder.OpenStreamForReadAsync(filename);
                    }
                    else
                    {
                        // 建立一個filename的檔案，並且指定如果遇到同名稱檔案將它覆寫
                        file = await local.OpenStreamForReadAsync(filename);
                    }


                    // 指定local folder\test.txt開啟檔案為Stream物件
                    //var file = await local.OpenStreamForReadAsync(filename);

                    // 透過StreamReader讀取Stream物件
                    StreamReader streamReader = new StreamReader(file);

                    //  this.textBlock1.Text = streamReader.ReadToEnd();
                    char[] cccc = streamReader.ReadToEnd().ToCharArray();
                    byte[] bbb = Encoding.UTF8.GetBytes(cccc);

                    streamReader.Close();

                    return bbb;
                }
                catch (Exception ex)
                {
#if DEBUG
                    Utility.ShowMessageBox("IsolatedStorageHelper::ReadFile_Local(...)" + ex.Message);
#else
                    //代表沒有找到指定的filename
                    Utility.OutputDebugString("IsolatedStorageHelper::ReadFile_Local(...)" + ex.Message,  MyUtility.Utility.LOG_LEVEL.warring);
#endif
                }
            }

            return null;
        }

        /// <summary>
        /// 讀取檔案。Installation
        /// </summary>
        /// <returns></returns>
        public static async Task<byte[]> ReadFile_Installation(string filename)
        {
            try
            {
                //透過URI的方式取得指定的檔案
                Uri tUri = new Uri("ms-appx:///Assets/" + filename, UriKind.Absolute);
                var tFile = await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(tUri);
                if (tFile != null)
                {
                    Stream tStream = await tFile.OpenStreamForReadAsync();


                    byte[] bbb = new byte[tStream.Length];

                    tStream.Read(bbb, 0, bbb.Length);
                    return bbb;
                }
            }
            catch (Exception ex)
            {
                //代表沒有找到指定的filename
                Utility.ShowMessageBox("IsolatedStorageHelper::ReadFile_Installation(...)" + ex.Message);
            }
            return null;
        }


        /// <summary>
        /// Serialize an object
        /// </summary>
        /// <param name="objectToSerialize">The object to serialize</param>
        /// <returns>A string</returns>
        public static byte[] Serialize_JSON(object objectToSerialize)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(objectToSerialize.GetType());
                serializer.WriteObject(ms, objectToSerialize);
                return ms.ToArray();

                /*ms.Position = 0;//讀出來瞧瞧是否有寫正確

                using (StreamReader reader = new StreamReader(ms))
                {
                    return reader.ReadToEnd();
                }*/

                // ms.WriteTo(

                //await WriteToFile(filename, ms.ToArray());
            }
        }

        /// <summary>
        /// Deserialize an object
        /// </summary>
        /// <typeparam name="T">The T</typeparam>
        /// <param name="jsonString">The JSON string</param>
        /// <returns>A T</returns>
        public static object Deserialize_JSON(string jsonString, Type type)
        {
            using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(jsonString)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(type);
                return serializer.ReadObject(ms);
            }
        }

        public static object Deserialize_JSON(byte[] jsonString, Type type)
        {
            string sss = Encoding.UTF8.GetString(jsonString, 0, jsonString.Length);
            return Deserialize_JSON(sss, type);
        }

        public static async Task DeleteFile_Local(string fileName, string oneLevelFolder)
        {
            try
            {
                // 取得Local Folder的根目錄
                StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;

                StorageFile file = null;
                if (oneLevelFolder != null && oneLevelFolder.Length > 0)
                {
                    StorageFolder dataFolder = await local.GetFolderAsync(oneLevelFolder);
                    file = await dataFolder.GetFileAsync(fileName);
                }
                else
                {
                    file = await local.GetFileAsync(fileName);
                }
                await file.DeleteAsync();
            }
            catch (Exception e)
            {
#if DEBUG
                Utility.ShowMessageBox(fileName + "  " + e.Message);
#else
                Utility.ShowToast(fileName + "  " + e.Message, 10000, true);
#endif
            }
        }

        //public static void DeleteFile(string fileName)
        //{
        //    IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
        //    if (myIsolatedStorage.FileExists(fileName))
        //        myIsolatedStorage.DeleteFile(fileName);
        //}

        public static void CreateFolder(string FOLDER)
        {
            IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
            if (myIsolatedStorage.DirectoryExists(FOLDER) == false)
                myIsolatedStorage.CreateDirectory(FOLDER);
        }

        public static bool IsFolderExist(string FOLDER)
        {
            IsolatedStorageFile iso = IsolatedStorageFile.GetUserStoreForApplication();
            return (iso.DirectoryExists(FOLDER));
        }

        public static bool IsFileExist(string filename)
        {
            IsolatedStorageFile iso = IsolatedStorageFile.GetUserStoreForApplication();
           
            return (iso.FileExists(filename));
        }

        public static void DeleteFolder(string FOLDER)
        {
            //參考這網頁的最後一段
            //http://www.geekchamp.com/tips/all-about-wp7-isolated-storage-files-and-folders

            IsolatedStorageFile iso = IsolatedStorageFile.GetUserStoreForApplication();
            if (iso.DirectoryExists(FOLDER))
            {
                string[] files = iso.GetFileNames(FOLDER + @"/*");
                foreach (string file in files)
                {
                    iso.DeleteFile(FOLDER + @"/" + file);
                    // Debug.WriteLine("Deleted file: " + directory + @"/" + file); 
                }
                string[] subDirectories = iso.GetDirectoryNames(FOLDER + @"/*");
                foreach (string subDirectory in subDirectories)
                {
                    DeleteFolder(FOLDER + @"/" + subDirectory);
                }

                iso.DeleteDirectory(FOLDER);
                //Debug.WriteLine("Deleted directory: " + directory);

            }
        }

        /// <summary>
        /// 取得folder內所有的檔案名稱
        /// </summary>
        public async static Task GetFolder_AllFileName(string folder, List<string> getFilenames)
        {
            getFilenames.Clear();

            //  IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
            // string[] filenames = myIsolatedStorage.GetFileNames();

            StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
            IStorageFolder dataFolder = null;
            try
            {
                dataFolder = await local.GetFolderAsync(folder);
            }
            catch (Exception e)
            {
                return;
            }
            IEnumerable<IStorageFile> files = await dataFolder.GetFilesAsync();

            foreach (IStorageFile anyfile in files)
            {
                if (anyfile.Path.EndsWith(".txt"))
                {
                    var name = anyfile.Name;
                    getFilenames.Add(name);
                }
            }

        }
    }
}
