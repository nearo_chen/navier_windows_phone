﻿
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Windows.Phone.Speech.Synthesis;
using Coding4Fun.Toolkit.Controls;
using ResourceApp.Resources;
using BingTranslationApp;
using System.Collections.Generic;
using System.Windows.Threading;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;


namespace MyUtility
{


    public static partial class Utility
    {
        public const int SpeedMaxKM = 160;
        public const int SpeedMaxMil = 100;

        /// <summary>
        /// 給拖拉編輯元件顯示的反白色
        /// </summary>
        public static Color DragControl_Color = Color.FromArgb(70, 0, 255, 255);

        /// <summary>
        /// 給拖拉編輯元件顯示的反白筆刷
        /// </summary>
        public static SolidColorBrush DragControl_ColorBrush = new SolidColorBrush(DragControl_Color);
        public static SolidColorBrush DragControl_TransparantBrush = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));

        /// <summary>
        /// 廣告是否要隱藏起來，true為付費版
        /// </summary>
        public static bool mHideAdvertisement = false;

        //相關設定值===========================================================================================
        public static float ROAD_WIDTH_LonLat = (float)Utility.Kilometer2LonLat(0.05f);  //用來判斷是否脫離路線的路寬

        /// <summary>
        /// 時速為0時最小可視範圍(整個route renderer的寬高)
        /// </summary>
        public const float VIEW_RANGE_MIN_METER = 200;

        /// <summary>
        /// 時速為120時後最大可視範圍(整個route renderer的寬高)
        /// </summary>
        //public const float VIEW_RANGE_MAX = 2000;

        /// <summary>
        /// 是否要提示轉彎的距離，目前拿來用轉彎的圓圈圈半徑大小，設定為200公尺
        /// 畫路徑的範圍，會根據時速作放大縮小，轉彎的圓圈圈也會跟著放大縮小。
        /// </summary>
        public const double TURN_NOTICE_DIS_METER = 50;


        // public static double NOW_TURN_NOTICE_DIS = (float)Utility.Kilometer2LonLat(0.10f);//是否要提示現在轉彎的距離

        /// <summary>
        /// 背景格線的透明度
        /// </summary>
        public const double GridOpacity = 10f;

        /// <summary>
        /// 背景格線的粗細
        /// </summary>
        public const double GridThickness = 0.35f;


        /// <summary>
        /// 目前HUD的設定顏色
        /// </summary>
        //public static SelectColor NowHUDColor = SelectColor.color_orangered;
        //====================================================================================================

        /// <summary>
        /// user可以選擇的顏色(同時也代表顏色陣列位址)
        /// </summary>
        public enum SelectColor
        {
            color_cyan = 0,      //洋青 "#00FFFF";
            color_lightgray,     //淺灰 "#cccccc";
            color_white,         //白   "#FFFFFF";
            color_yellow,        //黃   "#ffff00";
            color_green,         //綠   "#00ff00";
            color_skyblue,       //藍   "#0000ff";
            color_orangered,     //橘紅 "#ff4500";
            color_blue,          //天藍 "#87ceeb";
            final_size,
            //color_gray    ,
            //color_swhite  ,
        }
        private static Color[] mSelectColorArray = null;
        private static String[] mSelectColorArrayInt = null;

        public static SelectColor GetColorSelect(String colorIntName)
        {
            if (mSelectColorArrayInt == null)
            {
                mSelectColorArrayInt = new String[(int)SelectColor.final_size];
                mSelectColorArrayInt[0] = "#00FFFF";
                mSelectColorArrayInt[1] = "#cccccc";
                mSelectColorArrayInt[2] = "#FFFFFF";
                mSelectColorArrayInt[3] = "#ffff00";
                mSelectColorArrayInt[4] = "#00ff00";
                mSelectColorArrayInt[5] = "#0000ff";
                mSelectColorArrayInt[6] = "#ff4500";
                mSelectColorArrayInt[7] = "#87ceeb";
            }
            int count = 0;
            foreach (string sss in mSelectColorArrayInt)
            {
                if (sss == colorIntName)
                    return (SelectColor)count;
                count++;
            }
            return SelectColor.color_orangered;
        }

        public static Color GetHUDColor(SelectColor selectColor)
        {
            if (mSelectColorArray == null)
            {
                mSelectColorArray = new Color[(int)SelectColor.final_size];
                mSelectColorArray[0] = Color.FromArgb(255, 0, 255, 255);   //#00FFFF
                mSelectColorArray[1] = Color.FromArgb(255, 204, 204, 204); //#cccccc
                mSelectColorArray[2] = Color.FromArgb(255, 255, 255, 255); //#FFFFFF
                mSelectColorArray[3] = Color.FromArgb(255, 255, 255, 0);   //#ffff00
                mSelectColorArray[4] = Color.FromArgb(255, 0, 255, 0);     //#00ff00
                mSelectColorArray[5] = Color.FromArgb(255, 0, 0, 255);     //#0000ff
                mSelectColorArray[6] = Color.FromArgb(255, 255, 69, 0);    //#ff4500 
                mSelectColorArray[7] = Color.FromArgb(255, 135, 206, 235); //#87ceeb
            }
            return mSelectColorArray[(int)selectColor];
        }


        public static string GenerateUniqueID()
        {
            return Guid.NewGuid().ToString("N");
        }

        ///// <summary>
        ///// 計算點陣列組成的BoundingBox，回傳點陣列的，寬高最大值，
        ///// 可用於隨時將路徑規劃的，規劃點，傳入然後計算Bound的寬高，
        ///// 然後傳給，路線指示RouteRenderControl讓他根據bound範圍做縮放
        ///// </summary>
        ///// <returns></returns>
        //public static Point GetBoundingBoxWH(Point[] points)
        //{
        //    Point WH = new Point();

        //    return WH;
        //}

        /// <summary>
        /// 會根據傳入告知的landscape mode回傳螢幕的寬
        /// </summary> 
        public static double GetDeviceWidth(bool ifLandscape)
        {
            if (ifLandscape)
                return System.Windows.Application.Current.Host.Content.ActualHeight;
            return System.Windows.Application.Current.Host.Content.ActualWidth;
        }

        /// <summary>
        /// 會根據傳入告知的landscape mode回傳螢幕的高
        /// </summary>       
        public static double GetDeviceHeight(bool ifLandscape)
        {
            if (ifLandscape)
                return System.Windows.Application.Current.Host.Content.ActualWidth;
            return System.Windows.Application.Current.Host.Content.ActualHeight;
        }


        //----------------------------------------------------------------------------------------------
        private static float PercentToResulution_UnitPixel = -1;
        /// <summary>
        /// 我以相數少的那個邊單座單位基準，如 720p, 480p，再去做百分比的換算
        /// 將傳入的寬高百分比丟進來，回傳目前機器換算解析度後的像素寬高
        /// </summary>
        public static Point PercentToResulution(double percentWidth, double percentHeight)
        {
            //不同解析度下面 我要 以低解析度的那個邊 當單位的基準720p, 480p就以這個為基準
            //然後 用這個單位去做比例的換算
            if (PercentToResulution_UnitPixel == -1)
            {
                PercentToResulution_UnitPixel = (float)System.Windows.Application.Current.Host.Content.ActualWidth;
                if (System.Windows.Application.Current.Host.Content.ActualHeight <
                    System.Windows.Application.Current.Host.Content.ActualWidth)
                    PercentToResulution_UnitPixel = (float)System.Windows.Application.Current.Host.Content.ActualHeight;
            }

            Point outWH = new Point();

            //不要讓他Dispacher，不然無法及時使用
            // System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
            // {
            outWH.X = percentWidth * PercentToResulution_UnitPixel;
            outWH.Y = percentHeight * PercentToResulution_UnitPixel;
            // });           
            return outWH;
        }

        /// <summary>
        /// 取得目前全螢幕的像素寬高
        /// </summary>
        /// <returns></returns>
        public static Point GetDeviceWH(bool ifLandscape)
        {
            if (ifLandscape)
            {
                return new Point(
                     System.Windows.Application.Current.Host.Content.ActualHeight,
                     System.Windows.Application.Current.Host.Content.ActualWidth);
            }
            else
            {
                return new Point(
                   System.Windows.Application.Current.Host.Content.ActualWidth,
                   System.Windows.Application.Current.Host.Content.ActualHeight);
            }
        }

        //----------------------------------------------------------------------------------------------
        public static void SetControlPosition(Control control, float x, float y, bool ifLandscape)
        {
            //移動control座標
            Thickness tk = control.Margin;
            if (ifLandscape)
            {
                tk.Top = y;
                tk.Left = x;
            }
            else
            {
                tk.Top = x;
                tk.Left = y;
            }
            control.SetValue(System.Windows.Controls.Grid.MarginProperty, tk);
        }

        public static void SetPanelPositionADD(FrameworkElement panel, double addX, double addY, bool ifLandscape)
        {
            //移動control座標
            Thickness tk = panel.Margin;
            if (ifLandscape)
            {
                tk.Top += addY;
                tk.Bottom = tk.Top;
                tk.Left += addX;
                tk.Right = tk.Left;
            }
            else
            {
                tk.Top += addX;
                tk.Bottom = tk.Top;
                tk.Left += addY;
                tk.Right = tk.Left;
            }
            panel.SetValue(System.Windows.Controls.Grid.MarginProperty, tk);
        }

        public static void SetPanelPosition(FrameworkElement panel, double x, double y, bool ifLandscape)
        {
            //移動control座標
            Thickness tk = panel.Margin;
            if (ifLandscape)
            {
                tk.Top = y;
                tk.Left = x;
            }
            else
            {
                tk.Top = x;
                tk.Left = y;
            }
            panel.SetValue(System.Windows.Controls.Grid.MarginProperty, tk);
        }

        public static Point GetPanelPosition(FrameworkElement panel, bool ifLandscape)
        {
            Thickness tk = panel.Margin;
            if (ifLandscape)
            {
                return new Point(tk.Left, tk.Top);
            }
            else
            {
                return new Point(tk.Top, tk.Left);
            }
        }

        /// <summary>
        /// 直接設定寬高的pixel值給元件
        /// </summary>
        public static void SetControlWH(Control control, double w, double h, bool ifLandscape)
        {

            //這是不可能的
            ////移動control座標
            //Thickness tk = control.Margin;
            //if (ifLandscape)
            //{
            //    tk.Right = tk.Left + w;
            //    tk.Bottom = tk.Top + h;
            //}
            //else
            //{
            //    tk.Right = tk.Left + h;
            //    tk.Bottom = tk.Top + w;
            //}
            //control.SetValue(System.Windows.Controls.Grid.MarginProperty, tk);

            //使用control設定的w,h計算他該有的 scale 讓他實際的w,h有達到我的要求
            double scaleX, scaleY;
            if (ifLandscape)
            {
                scaleX = h / control.Height;
                scaleY = w / control.Width;
            }
            else
            {
                scaleX = w / control.Width;
                scaleY = h / control.Height;
            }
            SetControlScale(control, scaleX, scaleY, ifLandscape);
        }

        public static void GetControlWH(Control control, out double outW, out double outH, bool ifLandscape)
        {
            double outScaleX, outScaleY;
            GetControlScale(control, out outScaleX, out outScaleY, ifLandscape);
            if (ifLandscape)
            {
                outH = outScaleX * control.Height;
                outW = outScaleY * control.Width;
            }
            else
            {
                outH = outScaleY * control.Height;
                outW = outScaleX * control.Width;
            }
        }

        public static void GetControlScale(Control control, out double outScaleX, out double outScaleY, bool ifLandscape)
        {
            CompositeTransform ct;
            if (control.RenderTransform.GetType() == typeof(CompositeTransform))
                ct = control.RenderTransform as CompositeTransform;
            else
                ct = new CompositeTransform();

            if (ifLandscape)
            {
                outScaleX = ct.ScaleY;
                outScaleY = ct.ScaleX;
            }
            else
            {
                outScaleX = ct.ScaleX;
                outScaleY = ct.ScaleY;
            }
        }

        public static void SetControlScale(Control control, double scaleX, double scaleY, bool ifLandscape)
        {
            CompositeTransform ct;
            if (control.RenderTransform.GetType() == typeof(CompositeTransform))
                ct = control.RenderTransform as CompositeTransform;
            else
                ct = new CompositeTransform();

            if (ifLandscape)
            {
                ct.ScaleX = scaleY;
                ct.ScaleY = scaleX;
            }
            else
            {
                ct.ScaleX = scaleX;
                ct.ScaleY = scaleY;
            }
            control.RenderTransform = ct;
        }

        public static Point Vector3ToPoint(Microsoft.Xna.Framework.Vector3 vec)
        {
            return new Point(vec.X, vec.Y);
        }

        //--------------------------------------------------------------------------------
        public enum LOG_LEVEL//LEVEL直接等於顏色
        {
            verbose,
            warring,
        }

#if DEBUG
        private static MyDebugViewer mMyDebugViewer = null;
#endif

        public static void OutputDebugString(string sss, LOG_LEVEL level)
        {
            OutputDebugString(sss, null, level);
        }

        public static void OutputDebugString(string sss)
        {
            OutputDebugString(sss, null, LOG_LEVEL.verbose);
        }

        public static void OutputDebugString(string sss, System.Windows.Controls.Panel layoutRoot)
        {
            OutputDebugString(sss, layoutRoot, LOG_LEVEL.verbose);
        }

        public static void OutputDebugString(string sss, System.Windows.Controls.Panel layoutRoot, LOG_LEVEL level)
        {

#if DEBUG
            if (layoutRoot != null)
            {
                if (mMyDebugViewer == null)
                    mMyDebugViewer = new MyDebugViewer();
                mMyDebugViewer.AddText(sss, layoutRoot);
            }

            System.Diagnostics.Debug.WriteLine(sss);
#endif

        }

        ///// <summary>
        /////  /****点到直线的距离***
        ///// 过点（x1,y1）和点（x2,y2）的直线方程为：KX -Y + (x2y1 - x1y2)/(x2-x1) = 0
        ///// 设直线斜率为K = (y2-y1)/(x2-x1),C=(x2y1 - x1y2)/(x2-x1)
        ///// 点P(x0,y0)到直线AX + BY +C =0DE 距离为：d=|Ax0 + By0 + C|/sqrt(A*A + B*B)
        ///// 点（x3,y3）到经过点（x1,y1）和点（x2,y2）的直线的最短距离为：
        ///// distance = |K*x3 - y3 + C|/sqrt(K*K + 1)
        ///// </summary>
        ///// <param name="pt1">直線上的點1</param>
        ///// <param name="pt2">直線上的點2</param>
        ///// <param name="pt3">計算點</param>     
        //public static double GetMinDistance(Point pt1, Point pt2, Point pt3)
        //{
        //    double dis = 0;
        //    if (pt1.X == pt2.X)
        //    {
        //        dis = Math.Abs(pt3.X - pt1.X);
        //        return dis;
        //    }
        //    double lineK = (pt2.Y - pt1.Y) / (pt2.X - pt1.X);
        //    double lineC = (pt2.X * pt1.Y - pt1.X * pt2.Y) / (pt2.X - pt1.X);
        //    dis = Math.Abs(lineK * pt3.X - pt3.Y + lineC) / (Math.Sqrt(lineK * lineK + 1));
        //    return dis;
        //}



        //-----------------------------------------------------------------------------------       
        public static void ShowMessageBox(string text)
        {
            GetCurrentDispatcher().BeginInvoke(() =>
                {
                    MessageBox.Show(text);
                }
            );
        }



        /// <summary>
        /// 取得目前語言的名稱
        /// </summary>
        public static string GetCurrentLanguageRegionName()
        {
            System.Globalization.CultureInfo info = System.Globalization.CultureInfo.CurrentUICulture;
            return info.Name;
        }

        /// <summary>
        /// 取得目前language的名稱
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentLanguageName()
        {
            System.Globalization.CultureInfo info = System.Globalization.CultureInfo.CurrentUICulture;
            return info.TwoLetterISOLanguageName;
            /*
            int a = info.Name.IndexOf('-');
            string region = info.Name.Remove(a, info.Name.Length - a);
            return region;*/
        }

        /// <summary>
        /// 取得目前region的名稱
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentRegionName()
        {
            System.Globalization.CultureInfo info = System.Globalization.CultureInfo.CurrentUICulture;

            string region = info.Name.Remove(0, info.Name.LastIndexOf('-') + 1);
            return region;
        }

        public static void SetScreenAutoSleep(bool OnOff)
        {
            if (OnOff)
            {
                Microsoft.Phone.Shell.PhoneApplicationService.Current.UserIdleDetectionMode =
                    Microsoft.Phone.Shell.IdleDetectionMode.Enabled;
            }
            else
            {
                Microsoft.Phone.Shell.PhoneApplicationService.Current.UserIdleDetectionMode =
                    Microsoft.Phone.Shell.IdleDetectionMode.Disabled;
            }
        }

        public static void SetCurrentDispatcher(System.Windows.Threading.Dispatcher disp)
        {
            mDispatcher = disp;
        }

        private static System.Windows.Threading.Dispatcher mDispatcher = null;
        public static System.Windows.Threading.Dispatcher GetCurrentDispatcher()
        {
            //Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher
            //return System.Windows.Deployment.Current.Dispatcher;

            //http://www.sandor.fr/2012/11/26/lastuce-du-lundi-acceder-au-dispatcher-dans-une-application-winrt/
            return mDispatcher;
        }

        public static string Byte2String(byte[] bbb)
        {
            return Encoding.UTF8.GetString(bbb, 0, bbb.Length);
        }

        public static byte[] String2Byte(string sss)
        {
            return Encoding.UTF8.GetBytes(sss);
        }

        public static void ShowProgressIndicator_OrientationChange(Microsoft.Phone.Controls.PageOrientation orientation)
        {
            if (mMyProgressIndicator == null)
                mMyProgressIndicator = new MyProgressIndicator();

            mMyProgressIndicator.ShowProgressIndicator_updateOrientation(orientation);
            
        }

        public static bool ShowProgressIndicator_IsShowing = false;
        private static MyProgressIndicator mMyProgressIndicator = null;//new MyProgressIndicator();
        public static void ShowProgressIndicator(String msg, bool isLandScape)
        {
            // mMsgCatch.Clear();
            ShowProgressIndicator_IsShowing = true;

            System.Windows.DependencyObject element = System.Windows.Deployment.Current;
            GetCurrentDispatcher().BeginInvoke(() =>
                {
                    if (mMyProgressIndicator == null)
                        mMyProgressIndicator = new MyProgressIndicator();
                    mMyProgressIndicator.ShowProgressIndicator(element, msg, isLandScape);

                    while (mMsgCatch.Count != 0)
                    {
                        mMyProgressIndicator.ShowProgressIndicator_updateInfo(mMsgCatch[0]);
                        mMsgCatch.RemoveAt(0);
                    }
                });
        }

        private static List<string> mMsgCatch = new List<string>();
        public static void ShowProgressIndicator_updateInfo(String msg)
        {
            GetCurrentDispatcher().BeginInvoke(() =>
               {
                   if (mMyProgressIndicator == null)
                   {
                       mMsgCatch.Insert(0, msg);
                   }
                   else
                   {
                       mMyProgressIndicator.ShowProgressIndicator_updateInfo(msg);
                   }
               });
        }

        public static void HideProgressIndicator()
        {
            GetCurrentDispatcher().BeginInvoke(() =>
               {
                   if (mMyProgressIndicator != null)
                       mMyProgressIndicator.HideProgressIndicator();
               });

            ShowProgressIndicator_IsShowing = false;
        }

        /// <summary>
        /// 將GRID的XY格子編號，轉換成PIXEL單位
        /// </summary>
        public static int GetPixelX(double screenWidth, double gridX)
        {
            //double screenWidth = Utility.GetDeviceWidth(true);
            return (int)(gridX * _getGridIntervel_accordingWidth(screenWidth));
        }

        /// <summary>
        /// 將GRID的XY格子編號，轉換成PIXEL單位
        /// </summary>
        public static int GetPixelY(double screenWidth, double gridY, double regionHeight)
        {
            // double screenWidth = Utility.GetDeviceWidth(true);
            //double screenHeight = regionHeight;// 
            double interval = _getGridIntervel_accordingWidth(screenWidth);


            //高度會有填不滿ㄉ狀況，所以要將填不滿的pixel上下平分
            double endHeight = interval * Grid_Height;
            double restHeight = regionHeight - endHeight;
            double topStart = restHeight * 0.5;

            return (int)(topStart + gridY * interval);
        }

        /// <summary>
        /// 將PIXEL的單位，轉換成GRID格子單位
        /// </summary>
        public static int GetGridLength(double screenWidth, double pixelLength)
        {
            //double screenWidth = Utility.GetDeviceWidth(true);
            return (int)Math.Floor(pixelLength / _getGridIntervel_accordingWidth(screenWidth) + 0.5f);
        }

        /// <summary>
        /// 將GRID的格子單位，轉換成PIXEL單位
        /// </summary>
        public static int GetPixelLength(double screenWidth, double gridLength)
        {
            //double screenWidth = Utility.GetDeviceWidth(true);
            return (int)(gridLength * _getGridIntervel_accordingWidth(screenWidth));
        }

        private static int Grid_Width = 16;
        private static int Grid_Height = 8;
        // private static double mGridInterval = -1;
        private static double _getGridIntervel_accordingWidth(double screenWidth)
        {
            //if (mGridInterval != -1)
            //    return mGridInterval;

            ////double screenWidth = Utility.GetDeviceWidth(true);
            //mGridInterval = screenWidth / Grid_Width;//因為這支APP是永遠的landscape模式，因為寬要塞滿，所以以寬為基準除以 16格 X 8格 的 16
            //return mGridInterval;

            return screenWidth / Grid_Width;
        }

        /// <summary>
        ///  Grid_Width 垂直切線要有幾條,
        ///  Grid_Height 水平切線要有幾條
        ///  這FUNC主要是根據傳入screenWidth去產生他該有的height。
        /// </summary>
        public static Grid DrawGridLine(double screenWidth,
                                        double strokeThickness, double opacity, Color color)
        {
            //完全match screenheight就用這個
            double intervel = _getGridIntervel_accordingWidth(screenWidth);
            double screenHeight = intervel * Grid_Height;

            Grid grid = DrawGridLine(screenWidth, screenHeight,
                strokeThickness, opacity, color);


            grid.Width = screenWidth;
            grid.Height = screenHeight;
            return grid;
        }

        /// <summary>
        ///  Grid_Width 垂直切線要有幾條,
        ///  Grid_Height 水平切線要有幾條
        /// </summary>
        public static Grid DrawGridLine(double screenWidth, double screenHeight,
                                        double strokeThickness, double opacity, Color color)
        {
            Grid pGridRoot = new Grid();
            pGridRoot.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));

            double topStart = 0;//從高度多少往下畫           
            //double screenHeight = Utility.GetDeviceHeight(true);
            //double screenWidth = Utility.GetDeviceWidth(true);
            double intervel = _getGridIntervel_accordingWidth(screenWidth);
            SolidColorBrush brush = new SolidColorBrush(color);

            //高度會有填不滿ㄉ狀況，所以要將填不滿的pixel上下平分
            double endHeight = intervel * Grid_Height;
            double restHeight = screenHeight - endHeight;
            topStart = restHeight * 0.5;


            //畫出底下網格線條(這邊畫出垂直切線)
            for (int w = 0; w <= Grid_Width; w++)
            {
                Line line = new Line();
                line.X1 = w * intervel;
                line.Y1 = topStart;
                line.X2 = line.X1;
                line.Y2 = line.Y1 + endHeight;
                line.Stroke = brush;
                line.StrokeThickness = strokeThickness;
                pGridRoot.Children.Add(line);
            }

            double leftStart = 0;//從左邊多少往右畫

            for (int h = 0; h <= Grid_Height; h++)
            {
                Line line = new Line();
                line.X1 = leftStart;
                line.Y1 = topStart + h * intervel;
                line.X2 = line.X1 + screenWidth;
                line.Y2 = line.Y1;
                line.Stroke = brush;
                line.StrokeThickness = strokeThickness;

                pGridRoot.Children.Add(line);
            }

            pGridRoot.Opacity = opacity;

            // pGridRoot.CacheMode = new BitmapCache();
            return pGridRoot;
        }

        ////************************************************************************************************
        //// 為了編輯介面可以拖拉的功能
        ////************************************************************************************************
        //public class DragableGrid
        //{
        //    //編輯介面那邊請設定這個
        //    public static double mDragableGridScreenWidth;


        //    private Panel mParentPanel;

        //    private double _gridIntervel;

        //    public DragableGrid(Panel parentPanel)
        //    {
        //        mParentPanel = parentPanel;
        //    }

        //    public void DragableGrid_MouseLeftButtonDown(MouseButtonEventArgs e, Control thisPanel)
        //    {
        //        //标示开始拖拽
        //        _isDraging = true;
        //        //鼠标的起始位置（控件内部）
        //        // _beginPoint = e.GetPosition(thisPanel);
        //        //鼠标相对于Grid的位置
        //        _beginPoint = e.GetPosition(mParentPanel);
        //        _MarginSave = thisPanel.Margin;

        //        _gridIntervel = _getGridIntervel_accordingWidth(mDragableGridScreenWidth);
        //    }

        //    public void DragableGrid_MouseMove(MouseEventArgs e, Control thisPanel)
        //    {
        //        if (!_isDraging)
        //            return;
        //        Point newPoint = e.GetPosition(mParentPanel);

        //        //移動control座標
        //        //Thickness tk = thisPanel.Margin;
        //        //if (ifLandscape)
        //        {
        //            _MarginSave.Top += (float)(newPoint.Y - _beginPoint.Y);
        //            _MarginSave.Left += (float)(newPoint.X - _beginPoint.X);
        //        }
        //        //  else
        //        //  {
        //        //      tk.Top = x;
        //        //      tk.Left = y;
        //        //  }

        //        _beginPoint = newPoint;

        //        //預防超過範圍
        //        double top = _MarginSave.Top;
        //        //Math.Floor(_MarginSave.Top / _gridIntervel) * _gridIntervel;
        //        double left = _MarginSave.Left;
        //        //Math.Floor(_MarginSave.Left / _gridIntervel) * _gridIntervel;
        //        double controlW, controlH;
        //        Utility.GetControlWH(thisPanel, out controlW, out controlH, true);

        //        Thickness tk = thisPanel.Margin;
        //        if (top >= 0 && top + controlH <= mParentPanel.Height)
        //            tk.Top = top;
        //        if (left >= 0 && left + controlW <= mParentPanel.Width)
        //            tk.Left = left;
        //        thisPanel.SetValue(System.Windows.Controls.Grid.MarginProperty, tk);

        //        //Utility.OutputDebugString();
        //    }
        //}



        private static Control _draging_Control = null;
        private static Thickness _draging_marginSave;
        private static int _draging_Lock = 0;
        private static Point _draging_beginPoint;
        private static double _gridIntervel;

        public delegate void ShowConfirmDragging();

        //用來呼叫外部秀出確認或是刪除的控制按鈕callback
        private static ShowConfirmDragging mShowConfirmDragging = null;
        private static ShowConfirmDragging mHideConfirmDragging = null;
        //private static ComponentEditorData mComponentEditorData = null;

        /// <summary>
        /// page初始化時一定要做的動作
        /// </summary>
        public static void DragableGrid_MustSetShowConfirmDraggingCallback(
            ShowConfirmDragging s, ShowConfirmDragging h)
        {
            mShowConfirmDragging = s;
            mHideConfirmDragging = h;

            _dragableGrid_CancelNowDragging();
        }

        public static Control DragableGrid_GetDraggingControl()
        {
            _draging_Lock = 0;
            return _draging_Control;
        }

        private static Rect _dragging_Position_Rect;
        public static Rect DragableGrid_GetNowDraggingRect()
        {
            return _dragging_Position_Rect;
        }

        /// <summary>
        /// 點中control時的資訊設定(由control裡面的tag抓出ComponentData編輯資訊，可以不用傳入rect)
        /// </summary>
        public static void DragableGrid_ControlSelected(Control thisControl)
        {
            ComponentData data = thisControl.Tag as ComponentData;
            if (data == null)
                return;

            Rect r = new Rect(data.mPosition.X, data.mPosition.Y,
                data.mRange.X, data.mRange.Y);
            DragableGrid_ControlSelected(thisControl, r);
        }




        private static void DragableGrid_SetDragColor_Timer_Tick(object sender, EventArgs e)
        {
            GetCurrentDispatcher().BeginInvoke(() =>
           {
               Panel parent = _draging_Control.Parent as Panel;
               foreach (UIElement uie in parent.Children)
               {
                   ISetDragColor dc = uie as ISetDragColor;
                   if (dc == null)
                       continue;
                   if (dc == _draging_Control)
                       dc.SetDragColor();
                   else
                       dc.RevertDragColor();
               }
               mShowConfirmDragging();
           });

            DispatcherTimer _timer = sender as DispatcherTimer;
            _timer.Tick -= DragableGrid_SetDragColor_Timer_Tick;
            _timer.Stop();
        }

        /// <summary>
        /// 點中control時的資訊設定(傳入rect資訊用來新增control時，tag裡面沒有ComponentData)
        /// </summary>
        public static void DragableGrid_ControlSelected(Control thisControl,
            Rect dragging_Position_Rect)
        {
            //點到不同的control而且是lock狀態，就乎略這次的control點擊
            if (_draging_Lock > 0 && _draging_Control != thisControl)
                return;
            _draging_Lock = 1;

            //ISetDragColor setDragColor = thisControl as ISetDragColor;
            //if (setDragColor != null)
            //    setDragColor.SetDragColor();

            _draging_Control = thisControl;
            _draging_marginSave = _draging_Control.Margin;
            //_draging_isDraging = true;

            _draging_beginPoint.X = _draging_beginPoint.Y = 0;

            //把parent下面的dragable comtrol都設定為非dragging狀態
            //把目前控制的control設定為dragging狀態
            DispatcherTimer _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromMilliseconds(30);
            _timer.Tick += DragableGrid_SetDragColor_Timer_Tick;
            _timer.Start();

            _dragging_Position_Rect = dragging_Position_Rect;


        }

        private static double _dragableGridScreenWidth;
        private static double _dragableGridScreenHeight;
        private static void _dragableGrid_CancelNowDragging()
        {
            if (_draging_Control != null)
            {
                //將目前拖曳中的control設定回初始位置
                ComponentData cd = _draging_Control.Tag as ComponentData;
                float x = Utility.GetPixelX(_dragableGridScreenWidth, cd.mPosition.X);
                float y = Utility.GetPixelY(_dragableGridScreenWidth, cd.mPosition.Y, _dragableGridScreenHeight);
                SetControlPosition(_draging_Control, x, y, true);

                //返回選擇顏色
                ISetDragColor dc = _draging_Control as ISetDragColor;
                dc.RevertDragColor();
            }

            mHideConfirmDragging();
            _draging_Lock = 0;
            _draging_Control = null;
        }

        /// <summary>
        /// 拖曳control時處理mouse down作為開始基準的滑鼠資訊
        /// </summary>
        public static void DragableGrid_EditRegionMouseDown(ref Point newPoint,
            double dragableGridScreenWidth, double dragableGridScreenHeight)
        {
            _dragableGridScreenWidth = dragableGridScreenWidth;
            _dragableGridScreenHeight = dragableGridScreenHeight;

            //如果點中可拖曳的control，_draging_Lock會是1，然後同一時間會接著觸發EditRegionMouseDown時把他變成2，
            //這樣之後EditRegionMouseDown再跑進來時，已經是2的話表示沒有點到control，就將user的拖曳狀態取消
            if (_draging_Lock > 1)
            {
                _dragableGrid_CancelNowDragging();
                return;
            }
            _draging_Lock = 2;

            _draging_beginPoint = newPoint;
            _gridIntervel = _getGridIntervel_accordingWidth(dragableGridScreenWidth);
        }

        /// <summary>
        /// 滑鼠移動時回傳給外面移動到block座標哪邊ㄉ資訊
        /// </summary>
        public static void DragableGrid_EditRegionMouseMove(ref Point newPoint, ref Rect regionRect)
        {
            if (_draging_Lock == 0)
                return;// new Point(0, 0);

            if (_draging_Control == null)
                return;// new Point(0, 0);

            if (_draging_beginPoint.X == 0 && _draging_beginPoint.Y == 0)
            {
                _draging_beginPoint = newPoint;
                return;// new Point(0, 0);
            }

            //Point newPoint = e.GetPosition(parentPanel);
            double yOffset = (float)(newPoint.Y - _draging_beginPoint.Y);
            double xOffset = (float)(newPoint.X - _draging_beginPoint.X);
            _draging_marginSave.Top += yOffset;
            _draging_marginSave.Left += xOffset;

            _draging_beginPoint = newPoint;

            //預防超過範圍
            Point posData = new Point(
                Math.Floor(_draging_marginSave.Top / _gridIntervel + 0.5f),
                Math.Floor(_draging_marginSave.Left / _gridIntervel + 0.5f));
            double top = //_draging_marginSave.Top;
            posData.X * _gridIntervel;
            double left = //_draging_marginSave.Left;
            posData.Y * _gridIntervel;
            double controlW, controlH;
            Utility.GetControlWH(_draging_Control, out controlW, out controlH, true);

            Thickness tk = _draging_Control.Margin;
            if (top >= 0 && top + controlH <= regionRect.Height)
                tk.Top = top;
            else if (top < 0)
                tk.Top = 0;
            else
                tk.Top = regionRect.Height - controlH;

            if (left >= 0 && left + controlW <= regionRect.Width)
                tk.Left = left;
            else if (left < 0)
                tk.Left = 0;
            else
                tk.Left = regionRect.Width - controlW;

            _draging_Control.SetValue(System.Windows.Controls.Grid.MarginProperty, tk);

            _dragging_Position_Rect.X = tk.Left / _gridIntervel;
            _dragging_Position_Rect.Y = tk.Top / _gridIntervel;
#if DEBUG
            Utility.OutputDebugString("EditRegion_MouseMove: " + count++ + "X: " + _dragging_Position_Rect.X + "Y: " + _dragging_Position_Rect.Y);
#endif
        }

#if DEBUG
        private static int count;
#endif

        public static void DragableGrid_EditRegionMouseUp(ref Point newPoint, ref Rect regionRect)
        {
            DragableGrid_EditRegionMouseMove(ref newPoint, ref regionRect);
        }

        public static void DragableGrid_DeleteNowDraggingControl()
        {

            if (_draging_Control == null)
                return;
            Panel parent = _draging_Control.Parent as Panel;
            if (parent.Children.IndexOf(_draging_Control) > 0)
                parent.Children.Remove(_draging_Control);

            _dragableGrid_CancelNowDragging();
        }

        /// <summary>
        /// 將layoutRoot底下的HUD元件，呼叫Interface_SetUnit設定單位
        /// </summary>
        public static void SetHUDUnit(System.Windows.Controls.Panel layoutRoot, bool isUnitKM)
        {
            foreach (UIElement uie in layoutRoot.Children)
            {
                ISetUnit setUnit = uie as ISetUnit;
                if (setUnit != null)
                    setUnit.Interface_SetUnit(isUnitKM);
            }
        }

        /// <summary>
        /// 將layoutRoot底下的HUD元件，呼叫Interface_SetColor設定顏色
        /// </summary>
        public static void SetHUDColor(System.Windows.Controls.Panel layoutRoot, Utility.SelectColor selectColor)
        {
            Color color = Utility.GetHUDColor(selectColor);

            foreach (UIElement uie in layoutRoot.Children)
            {
                ISetColor setColor = uie as ISetColor;
                if (setColor != null)
                    setColor.Interface_SetColor(color);
            }
        }

        /// <summary>
        /// 將layoutRoot底下的HUD元件，呼叫Interface_SetTimeFormat設定時間格式
        /// </summary>
        public static void SetHUDTimeFormat(System.Windows.Controls.Panel layoutRoot, bool is24HR)
        {
            foreach (UIElement uie in layoutRoot.Children)
            {
                ITime24HR setTimeFormat = uie as ITime24HR;
                if (setTimeFormat != null)
                    setTimeFormat.Interface_SetTimeFormat(is24HR);
            }
        }

        /// <summary>
        /// 將layoutRoot底下的HUD元件，呼叫Interface_SetSpeedLimit設定速限
        /// </summary>
        public static void SetHUDSpeedLimit(System.Windows.Controls.Panel layoutRoot, bool isUnit, int speedLimitKMH, int speedLimitMPH)
        {
            foreach (UIElement uie in layoutRoot.Children)
            {
                ISetSpeedLimit setSpeedLimit = uie as ISetSpeedLimit;
                if (setSpeedLimit != null)
                    setSpeedLimit.Interface_SetSpeedLimit(isUnit, speedLimitKMH, speedLimitMPH);
            }
        }



        /// <summary>
        /// 將layoutRoot底下的HUD元件，呼叫Interface_SetGPSAcuracy設定精確度還有座標
        /// </summary>
        public static void SetHUDGPSAcuracy(System.Windows.Controls.Panel layoutRoot,
            double acuracyMeter, float viewRangeInSpeedLonLat, float base2DViewWidth, double lat, double lon,
            double moveDistanceMeter)
        {

            foreach (UIElement uie in layoutRoot.Children)
            {
                ISetAcuracy setAcuracy = uie as ISetAcuracy;
                if (setAcuracy != null)
                    setAcuracy.Interface_SetGPSAcuracy(acuracyMeter, viewRangeInSpeedLonLat, base2DViewWidth, lat, lon, moveDistanceMeter);

            }
        }


        //-------------------------------------------------------------------------------------------------------------
        public delegate void MyPopUpPromptStringCompleted(object sender, string sResult, bool isOK);
        private static MyPopUpPromptStringCompleted mMyPopUpPromptStringCompleted;
        private static object mMyPopUpPromptStringSender;

        /// <summary>
        /// 秀出一個POPUP輸入框
        /// </summary>      
        public static void ShowInputPrompt(bool ifLandscape, object sender, MyPopUpPromptStringCompleted completed,
            string title, string description, string value,
             InputScopeNameValue scope)
        {
            mMyPopUpPromptStringCompleted = completed;
            mMyPopUpPromptStringSender = sender;

            SolidColorBrush _aliceBlueSolidColorBrush = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 240, 248, 255));
            SolidColorBrush _naturalBlueSolidColorBrush = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 135, 189));
            SolidColorBrush _cornFlowerBlueSolidColorBrush = new SolidColorBrush(System.Windows.Media.Color.FromArgb(200, 100, 149, 237));

            var input = new InputPrompt
            {

                Title = title,
                Message = description,
                Background = _naturalBlueSolidColorBrush,
                Foreground = _aliceBlueSolidColorBrush,
                Overlay = _cornFlowerBlueSolidColorBrush,
                IsCancelVisible = true
            };
            input.Value = value;
            input.Completed += PopUpPromptStringCompleted;

            input.InputScope = new InputScope { Names = { new InputScopeName { NameValue = scope } } };

            input.Width = GetDeviceWidth(ifLandscape);
            input.Height = GetDeviceHeight(ifLandscape);
            input.Show();
        }

        private static void PopUpPromptStringCompleted(object sender, PopUpEventArgs<string, PopUpResult> e)
        {
            bool isOK = false;
            if (e.PopUpResult == PopUpResult.Ok)
                isOK = true;

            mMyPopUpPromptStringCompleted(mMyPopUpPromptStringSender, e.Result, isOK);
        }




        //-------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// 秀出關於頁面
        /// </summary>
        public static void ShowAbout(string sAPName, string sVersion)
        {
            var about = new AboutPrompt { Title = AppResources.Main_ABOUT, VersionNumber = sVersion };
            //about.Completed += PopUpPromptObjectCompleted;

            //  AboutPromptItem APNamePrompt = new AboutPromptItem { AuthorName = sAPName };
            //  APNamePrompt.FontSize = 24;
            // namePrompt.Foreground = new System.Windows.Media.SolidColorBrush(color);//可以把字換顏色

            about.Footer = new AboutControl();
            about.WaterMark = new WaterMarkControl();

            about.Show(
                //  APNamePrompt
                //  new AboutPromptItem { Role = "dev", AuthorName = "Nearo Chen" },
                //   new AboutPromptItem { Role = "site", WebSiteUrl = "asnippets.blogspot.com" }
                    );

            //System.Windows.Media.RotateTransform rotTransform = new System.Windows.Media.RotateTransform();
            //rotTransform.Angle = 90;
            //about.RenderTransformOrigin = new Point(0.5, 0.5);
            //about.RenderTransform = rotTransform;
        }

        public static void PlaySound(string fileName)
        {
            Stream stream = Microsoft.Xna.Framework.TitleContainer.OpenStream(fileName);//"Assets/cowbell.wav");
            PlaySound(stream);
            //  Microsoft.Xna.Framework.Audio.SoundEffect effect = Microsoft.Xna.Framework.Audio.SoundEffect.FromStream(stream);
            //  Microsoft.Xna.Framework.FrameworkDispatcher.Update();
            //  effect.Play();
        }

        public static void PlaySound(Stream stream)
        {
            Microsoft.Xna.Framework.Audio.SoundEffect effect = Microsoft.Xna.Framework.Audio.SoundEffect.FromStream(stream);
            Microsoft.Xna.Framework.FrameworkDispatcher.Update();
            effect.Play();
        }

        //關於TOAST----------------------------------------------------------------------------------------------
        public static void ShowToast(string msg, int millisecondsUntilHidden, bool ifLandscape)
        {
            GetCurrentDispatcher().BeginInvoke(() =>
            {
                // return;
                ToastPrompt toast =
                    new ToastPrompt
                {
                    Opacity = 0.8f,
                    FontSize = 44,
                    Width = GetDeviceWidth(ifLandscape),
                    MillisecondsUntilHidden = millisecondsUntilHidden,
                    Message = msg,
                };

                toast.Show();
            });
        }



        //------------------------------------------------------------------------
        ///// <summary>
        ///// 檢查數值是否含有小數位數
        ///// </summary>
        ///// <returns></returns>
        //public static bool IfNumberContainPoint()
        //{
        //}

        /// <summary>
        /// Gets a value that indicates whether the network is available.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance hass network available; otherwise, <c>false</c>.
        /// </value>
        public static bool IsNetworkAvailable()
        {
            if (//Microsoft.Phone.Net.NetworkInformation.DeviceNetworkInformation.IsWiFiEnabled ||
                //Microsoft.Phone.Net.NetworkInformation.DeviceNetworkInformation.IsCellularDataEnabled ||
                Microsoft.Phone.Net.NetworkInformation.DeviceNetworkInformation.IsNetworkAvailable)
                return true;
            return false;

            //get { return Microsoft.Phone.Net.NetworkInformation.DeviceNetworkInformation.IsNetworkAvailable; }
        }

        /// <summary>
        /// 檢查GPS是否有開啟，詢問是否要打開，不開啟GPS，程式就直接結束
        /// </summary>
        public static void CheckLocation()
        {
            System.Device.Location.GeoCoordinateWatcher tWatcher = new System.Device.Location.GeoCoordinateWatcher();
            /*  tWatcher.PositionChanged +=
                 new EventHandler<System.Device.Location.GeoPositionChangedEventArgs<System.Device.Location.GeoCoordinate>>
                     (_tWatcher_PositionChanged);
              tWatcher.StatusChanged +=
                      new EventHandler<System.Device.Location.GeoPositionStatusChangedEventArgs>
                          (_tWatcher_StatusChanged);*/

            // 識別是否有開啟Locaiton Service
            //if (tWatcher.TryStart(true, new TimeSpan()) == false)
            if (tWatcher.Permission != System.Device.Location.GeoPositionPermission.Granted)
            {
                Microsoft.Phone.Controls.CustomMessageBox messageBox = new Microsoft.Phone.Controls.CustomMessageBox()
                {
                    Caption = "Tip",
                    Message = AppResources.Utility_IfOpenGPS,

                    LeftButtonContent = AppResources.Utility_ButtonOK,
                    RightButtonContent = AppResources.Utility_ButtonCancel,
                    IsFullScreen = true,
                };

                messageBox.Dismissed += _checkLocation_OnMessageBoxDismissed;
                messageBox.Show();
            }

        }

        private async static void _checkLocation_OnMessageBoxDismissed(object sender, Microsoft.Phone.Controls.DismissedEventArgs e)
        {
            if (Microsoft.Phone.Controls.CustomMessageBoxResult.LeftButton == e.Result)
            {

                //這邊教的  http://www.dotblogs.com.tw/pou/archive/2013/03/13/96535.aspx
                await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings-location:"));
            }
            else if (Microsoft.Phone.Controls.CustomMessageBoxResult.RightButton == e.Result)
            {
                //參考資料:
                //www.stackoverflow.com/questions/15052311/how-to-programatically-through-code-to-exit-or-quit-from-windows-phone-8-app
                Application.Current.Terminate();
            }
            else
            {
                Application.Current.Terminate();
            }
        }



        public delegate void IfOOXX_PopUpPromptStringCompleted(bool IfYes);
        private static IfOOXX_PopUpPromptStringCompleted _IfOOXX_CompletedEvent;
        private static MessagePrompt mShowIfOOXX_MessagePrompt = null;
        private static DispatcherTimer mShowIfOOXX_timer = new DispatcherTimer();

        /// <summary>
        /// 秀出一個POPUP視窗尋問是否
        /// </summary>
        public static void ShowIfOOXX(bool ifLandscape, string sTitle, object oContent, IfOOXX_PopUpPromptStringCompleted CompletedEvent,
            SolidColorBrush backgroundColor)
        {
            if (mShowIfOOXX_MessagePrompt != null && mShowIfOOXX_MessagePrompt.IsOpen)
            {
                mShowIfOOXX_MessagePrompt.Hide();
                mShowIfOOXX_timer.Tick -= ShowIfOOXX_Timer_Tick;
                mShowIfOOXX_timer.Stop();
                return;
            }

            _IfOOXX_CompletedEvent = CompletedEvent;

            mShowIfOOXX_MessagePrompt = new MessagePrompt
            {
                Title = sTitle,
                Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 255, 255, 255)),
                Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 0, 0)),
                //  Overlay = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 0, 255)),
                IsCancelVisible = true,
            };

            mShowIfOOXX_MessagePrompt.Background = backgroundColor;

            // Image _pinIMG = new Image();
            // _pinIMG.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri("/Assets/Processing/Leave.png", UriKind.Relative));
            // _pinIMG.Width = _pinIMG.Height = 100;
            mShowIfOOXX_MessagePrompt.Body = oContent;// _pinIMG;

            mShowIfOOXX_MessagePrompt.Completed += _ifOOXX_PopUpPromptStringCompleted;

            mShowIfOOXX_MessagePrompt.Width = MyUtility.Utility.GetDeviceWidth(ifLandscape);
            mShowIfOOXX_MessagePrompt.Height = MyUtility.Utility.GetDeviceHeight(ifLandscape);
            mShowIfOOXX_MessagePrompt.Show();

            mShowIfOOXX_timer.Interval = TimeSpan.FromMilliseconds(2000);
            mShowIfOOXX_timer.Tick += ShowIfOOXX_Timer_Tick;
            mShowIfOOXX_timer.Start();
        }

        private static void ShowIfOOXX_Timer_Tick(object sender, EventArgs e)
        {
            if (mShowIfOOXX_MessagePrompt != null && mShowIfOOXX_MessagePrompt.IsOpen)
                mShowIfOOXX_MessagePrompt.Hide();
            mShowIfOOXX_timer.Tick -= ShowIfOOXX_Timer_Tick;
            mShowIfOOXX_timer.Stop();
        }

        private static void _ifOOXX_PopUpPromptStringCompleted(object sender, PopUpEventArgs<string, PopUpResult> e)
        {
            if (e.PopUpResult == PopUpResult.Ok)
                _IfOOXX_CompletedEvent(true);
            else
                _IfOOXX_CompletedEvent(false);
        }


        public static void EnableDebugSetting()
        {

            // 顯示目前的畫面播放速率計數器。
            Application.Current.Host.Settings.EnableFrameRateCounter = true;

            // 顯示每個畫面中重新繪製的應用程式區域。
            Application.Current.Host.Settings.EnableRedrawRegions = true;

            // 啟用非實際執行分析視覺化模式，
            // 用彩色重疊在頁面上顯示交給 GPU 的區域。
            // Application.Current.Host.Settings.EnableCacheVisualization = true;

            // 防止螢幕在執行偵錯工具時關閉，方法是停用
            // 應用程式的閒置偵測。
            // 注意: 這項功能僅限偵錯模式使用。當使用者不使用電話時，停用使用者閒置偵測
            // 的應用程式仍會繼續執行，並消耗電池的電力。
            Microsoft.Phone.Shell.PhoneApplicationService.Current.UserIdleDetectionMode =
                Microsoft.Phone.Shell.IdleDetectionMode.Disabled;
        }

        public static Color NavigationComponentColor = Color.FromArgb(255, 171, 197, 119);
        public static BitmapImage TileIcon = new BitmapImage(new Uri(@"Assets/tiles/ic_launcher.png", UriKind.Relative));

        /// <summary>
        /// 郵船入字串計算出hash code(使用SHA1)
        /// </summary>
        /// <returns></returns>
        public static string GetStringHash(string sText)
        {
            byte[] Message = Encoding.Unicode.GetBytes(sText);
            System.Security.Cryptography.SHA1Managed SHhash = new System.Security.Cryptography.SHA1Managed();
            byte[] HashValue = SHhash.ComputeHash(Message);
            return BitConverter.ToString(HashValue);
        }

        /// <summary>
        /// 為直接導航建立TILE
        /// </summary>
        public static void SetTileData_GoTo_Navi(
            string sGoalName, string sHUDShowName, string sHUDFileName,
            string sTileProperties,
            Microsoft.Phone.Maps.Services.TravelMode travelMode,
            BitmapImage streetViewImage,
            ShellTile tile,
            bool textColorBlack)
        {
            //sHUDShowName 拿去當tile的系統名稱
            //    sGoalName 拿去畫出大字

            Color HUDTypeColor = NavigationComponentColor;

            FlipTileData flipTile = new FlipTileData()
           {
               Title = sHUDShowName,
               BackTitle = sHUDShowName,
               BackContent = "",
               WideBackContent = "",
           };

            string sBackgroundImage = sTileProperties + "_BackgroundImage";
            sBackgroundImage = sBackgroundImage.ToLower();
            sBackgroundImage = Utility.GetStringHash(sBackgroundImage);
           

            //Medium size Tile 336x336 px
            //Crete image for BackBackgroundImage in IsoStore
            if (sGoalName.Length >= 135)
                _renderText(sGoalName.Substring(0, 135) + "...", 336, 336, 55, sBackgroundImage, false, HUDTypeColor, travelMode, streetViewImage, textColorBlack);
            else
                _renderText(sGoalName, 336, 336, 55, sBackgroundImage, false, HUDTypeColor, travelMode, streetViewImage, textColorBlack);
            flipTile.BackBackgroundImage = new Uri(@"isostore:/Shared/ShellContent/" + sBackgroundImage + ".png", UriKind.Absolute); //Generated image for Back Background 336x336
            flipTile.BackgroundImage = flipTile.BackBackgroundImage;// new Uri(@"isostore:/Shared/ShellContent/" + sBackgroundImage + ".jpg", UriKind.Absolute); //Generated image for Back Background 336x336

            string sWideBackgroundImage = sTileProperties + "_WideBackgroundImage";
            sWideBackgroundImage = sWideBackgroundImage.ToLower();
            sWideBackgroundImage = Utility.GetStringHash(sWideBackgroundImage);


            //Wide size Tile 691x336 px
            //Crete image for WideBackBackgroundImage in IsoStore
            _renderText(sGoalName, 691, 336, 90, sWideBackgroundImage, true, HUDTypeColor, travelMode, streetViewImage, textColorBlack);
            flipTile.WideBackBackgroundImage = new Uri(@"isostore:/Shared/ShellContent/" + sWideBackgroundImage + ".png", UriKind.Absolute);
            flipTile.WideBackgroundImage = flipTile.WideBackBackgroundImage;// new Uri(@"isostore:/Shared/ShellContent/WideBackBackgroundImage.jpg", UriKind.Absolute);

            if (tile == null)
            {
                try
                {
                    //string addr = string.Format("/NavierMain.xaml?HUDSettingFileName={0}", sHUDFileName);
                    string addr = string.Format("/NavierMain.xaml?" + sTileProperties);
                    ShellTile.Create(new Uri(addr, UriKind.Relative), flipTile, true);
                }
                catch (Exception e)
                {
                    OutputDebugString(e.Message);
                }
            }
            else
            {
                tile.Update(flipTile);
                Utility.ShowToast("Update Tile", 2000, true);
            }
        }

        /// <summary>
        /// 為編輯面版建立TILE
        /// </summary>
        public static void SetTileData_HUD(string sHUDShowName, string sTileProperties,
            ShellTile tile, Color HUDTypeColor)
        {
            string sAppName = "Navier HUD";
            if (MyUtility.Utility.mHideAdvertisement)
                sAppName = "Navier HUD premium";

            FlipTileData flipTile = new FlipTileData()
            {
                Title = sAppName,
                BackTitle = sHUDShowName,
                BackContent = "",
                WideBackContent = "",
            };

            string sBackgroundImage = sHUDShowName + "_BackgroundImage";
            //Medium size Tile 336x336 px
            //Crete image for BackBackgroundImage in IsoStore
            if (sHUDShowName.Length >= 135)
                _renderText(sHUDShowName.Substring(0, 135) + "...", 336, 336, 55, sBackgroundImage, false, HUDTypeColor, null, null, false);
            else
                _renderText(sHUDShowName, 336, 336, 55, sBackgroundImage, false, HUDTypeColor, null, null, false);
            flipTile.BackBackgroundImage = new Uri(@"isostore:/Shared/ShellContent/" + sBackgroundImage + ".png", UriKind.Absolute); //Generated image for Back Background 336x336
            flipTile.BackgroundImage = flipTile.BackBackgroundImage;// new Uri(@"isostore:/Shared/ShellContent/" + sBackgroundImage + ".jpg", UriKind.Absolute); //Generated image for Back Background 336x336

            string sWideBackgroundImage = sHUDShowName + "_WideBackgroundImage";
            //Wide size Tile 691x336 px            
            //Crete image for WideBackBackgroundImage in IsoStore
            _renderText(sHUDShowName, 691, 336, 90, sWideBackgroundImage, true, HUDTypeColor, null, null, false);
            flipTile.WideBackBackgroundImage = new Uri(@"isostore:/Shared/ShellContent/" + sWideBackgroundImage + ".png", UriKind.Absolute);
            flipTile.WideBackgroundImage = flipTile.WideBackBackgroundImage;// new Uri(@"isostore:/Shared/ShellContent/WideBackBackgroundImage.jpg", UriKind.Absolute);

            try
            {
                //string addr = string.Format("/NavierMain.xaml?HUDSettingFileName={0}", sHUDFileName);
                string addr = string.Format("/NavierMain.xaml?{0}" , sTileProperties);
                ShellTile.Create(new Uri(addr, UriKind.Relative), flipTile, true);
            }
            catch (Exception e)
            {
                OutputDebugString(e.Message);
            }
        }

        private static void _renderText(string text, int width, int height, int fontSize, string imagename,
            bool bLargeTile, Color HUDTypeColor, Microsoft.Phone.Maps.Services.TravelMode? travelMode,
            BitmapImage backGroundImage,
            bool textColorBlack)
        {
            WriteableBitmap b = new WriteableBitmap(width, height);

            var canvas = new Grid();
            canvas.Width = b.PixelWidth;
            canvas.Height = b.PixelHeight;

            var background = new Canvas();
            background.Height = b.PixelHeight;
            background.Width = b.PixelWidth;

            //Created background color as Accent color
            //if (System.Environment.OSVersion.Version.Major <= 8 &&
            //   System.Environment.OSVersion.Version.Minor <= 0)
            //{
            //    SolidColorBrush backColor = new SolidColorBrush((Color)Application.Current.Resources["PhoneAccentColor"]);
            //    background.Background = backColor;
            //}
            //else//>=8.1

            if (backGroundImage != null)
            {
                ImageBrush image = new ImageBrush();
                image.ImageSource = backGroundImage;
                image.Stretch = Stretch.UniformToFill;
                background.Background = image;
            }
            else
            {
                SolidColorBrush backColor = new SolidColorBrush(Colors.Transparent);
                background.Background = backColor;
            }

            var retangle = new Rectangle();
            retangle.Width = width;
            retangle.Height = height * 0.8f;
            retangle.HorizontalAlignment = HorizontalAlignment.Center;
            retangle.VerticalAlignment = VerticalAlignment.Center;
            retangle.Stroke = new SolidColorBrush(HUDTypeColor);//Color.FromArgb(255, 171, 197, 119));
            retangle.RadiusX = retangle.RadiusY = 20;
            retangle.StrokeThickness = 5;
            canvas.Children.Add(retangle);

            //retangle = new Rectangle();
            //retangle.Width = width-20;
            //retangle.Height = height * 0.7f;
            //retangle.HorizontalAlignment = HorizontalAlignment.Center;
            //retangle.VerticalAlignment = VerticalAlignment.Top;
            //retangle.Margin = new Thickness(10);
            //retangle.Fill = new SolidColorBrush(Color.FromArgb(200, 171, 197, 119));
            //retangle.RadiusX = retangle.RadiusY = 10;
            //canvas.Children.Add(retangle);



            Image logoImage = new Image();
            BitmapImage tn =  new BitmapImage();
             tn.SetSource(Application.GetResourceStream(new Uri(@"Assets/tiles/ic_launcher.png", UriKind.Relative)).Stream);
            logoImage.Source = tn;
            logoImage.Width = logoImage.Height = height * 0.4f;
            float posX = (float)(width - logoImage.Width);
            float posY = (float)(height - logoImage.Height);
            logoImage.Stretch = Stretch.Fill;
            logoImage.Margin = new Thickness(posX, posY, 0, 0);
            canvas.Children.Add(logoImage);

            Color textColor = (textColorBlack) ? Colors.Black : Colors.White;
            Color textColorShadow = (textColorBlack) ? Colors.White : Colors.Black;

            if (travelMode.HasValue)
            {
                //Shadow
                retangle = new Rectangle();
                tn = new BitmapImage();
                ImageBrush imageBrush = new ImageBrush();
                if (travelMode.Value == Microsoft.Phone.Maps.Services.TravelMode.Driving)
                    tn.SetSource(Application.GetResourceStream(new Uri(@"Assets/images/travel/Car.png", UriKind.Relative)).Stream);
                else
                    tn.SetSource(Application.GetResourceStream(new Uri(@"Assets/images/travel/Cycle.png", UriKind.Relative)).Stream);
                imageBrush.ImageSource = tn;
                imageBrush.Stretch = Stretch.Fill;
                retangle.OpacityMask = imageBrush;
                retangle.Fill = new SolidColorBrush(textColorShadow);
                retangle.Width = retangle.Height = height * 0.4f * 0.5f;
                posX = (float)(width - logoImage.Width - retangle.Width);
                posY = (float)(height - logoImage.Height);
                retangle.Margin = new Thickness(posX + 3, posY + 3, 0, 0);
                canvas.Children.Add(retangle);

               
                retangle = new Rectangle();
                tn = new BitmapImage();
                imageBrush = new ImageBrush();
                if (travelMode.Value == Microsoft.Phone.Maps.Services.TravelMode.Driving)
                    tn.SetSource(Application.GetResourceStream(new Uri(@"Assets/images/travel/Car.png", UriKind.Relative)).Stream);
                else
                    tn.SetSource(Application.GetResourceStream(new Uri(@"Assets/images/travel/Cycle.png", UriKind.Relative)).Stream);
                imageBrush.ImageSource = tn;
                imageBrush.Stretch = Stretch.Fill;
                retangle.OpacityMask = imageBrush;
                retangle.Fill = new SolidColorBrush(textColor);
                retangle.Width = retangle.Height = height * 0.4f * 0.5f;
                posX = (float)(width - logoImage.Width - retangle.Width);
                posY = (float)(height - logoImage.Height);
                retangle.Margin = new Thickness(posX, posY, 0, 0);
                canvas.Children.Add(retangle);
            }

            //Shadow text
            var textBlock = new TextBlock();
            textBlock.Text = text;
            textBlock.Width = width - 20 - 10;
            textBlock.Height = height * 0.7f - 10;
            textBlock.TextAlignment = TextAlignment.Left;
            if (bLargeTile)
                textBlock.Margin = new Thickness(25, 25, 0, 0);
            else
                textBlock.Margin = new Thickness(15, 15, 0, 0);
            textBlock.FontSize = fontSize;
            textBlock.TextWrapping = TextWrapping.Wrap;
            textBlock.Foreground = new SolidColorBrush(textColorShadow); //color of the text on the Tile
            textBlock.FontWeight = FontWeights.Bold;
            canvas.Children.Add(textBlock);

            textBlock = new TextBlock();
            textBlock.Text = text;
            textBlock.Width = width - 20 - 10;
            textBlock.Height = height * 0.7f - 10;
            textBlock.TextAlignment = TextAlignment.Left;
            if (bLargeTile)
                textBlock.Margin = new Thickness(20, 20, 0, 0);
            else
                textBlock.Margin = new Thickness(10, 10, 0, 0);
            textBlock.FontSize = fontSize;
            textBlock.TextWrapping = TextWrapping.Wrap;
            textBlock.Foreground = new SolidColorBrush(textColor); //color of the text on the Tile
            textBlock.FontWeight = FontWeights.Bold;
            canvas.Children.Add(textBlock);

            b.Render(background, null);
            b.Render(canvas, null);
            b.Invalidate(); //Draw bitmap



            //Save bitmap as jpeg file in Isolated Storage
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream imageStream = new IsolatedStorageFileStream("/Shared/ShellContent/" + imagename + ".png", System.IO.FileMode.Create, isf))
                {
                    //http://blogs.windows.com/windows/b/buildingapps/archive/2014/05/19/developers-make-your-tiles-transparent-for-windows-phone-8-1.aspx
                    WriteableBitmapExtensions.WritePNG(b, imageStream);

                    //  b(imageStream, b.PixelWidth, b.PixelHeight, 0, 100);
                }
            }
        }

        public static void WritePNG(WriteableBitmap bmp, System.IO.Stream destination)
        {
            WriteableBitmapExtensions.WritePNG(bmp, destination);
        }
    }

    /// <summary>
    /// 紀錄編輯好的各種元件資訊
    /// </summary>
    public class ComponentEditorData
    {
        public string ShowName = "";
        public string FileName;
        public int idcount = 1;
        public List<ComponentData> mComponents = new List<ComponentData>();
    }

    /// <summary>
    /// 記錄一個元件的資訊
    /// </summary>
    public class ComponentData
    {
        //不能有建構子，好像會CRASH
        //public ComponentData(int posX, int posY, int w, int h)
        //{
        //    mPosition.posX = posX;
        //    mPosition.posY = posY;
        //    mRange.width = w;
        //    mRange.height = h;
        //}
        public int id;                               //元件產生時給他一個獨一無二的流水號ID
        public string name;                          //根據name決定要產生哪種元件
        public position mPosition = new position();  //元件的位置
        public position mRange = new position();     //元件的寬高
    }
    public class position
    {
        public int X = 0;
        public int Y = 0;
    }
}


