////---------------------------------------------------------------------
//// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
//// KIND, WHETHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
//// PURPOSE.
////---------------------------------------------------------------------
//using System;
//using System.Diagnostics;
//using System.Threading;

//using System.IO;
//using System.Collections;

//namespace Microsoft.Services.Helpers
//{
//    public class traceHelper
//    {
//        private static void internalWriteLine( string message )
//        {		
//            System.Diagnostics.Trace.WriteLine( message );
//        }

//        private static void internalLogToFile(string message)
//        {
//            if(_traceToFile == true)
//            {
//                try
//                {
//                    lock(typeof(traceHelper))
//                    {
//                        if(_stream == null)
//                        {
//                            string filename = string.Format(@"{0}{1}_{2}_PID{3}.log", _tracePath, _traceFilename, DateTime.Now.ToString("yyyyMMddTHHmmss"), Process.GetCurrentProcess().Id);
//                            _stream = File.AppendText(filename);
//                            System.Diagnostics.Trace.WriteLine( "traceHelper: Creating trace file: " + filename );

//                            FileInfo	fileInfo = new FileInfo(filename);
//                            _fileSize = fileInfo.Length;
//                        }

//                        message = string.Format("{0} {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), message);

//                        _stream.WriteLine(message);
//                        _stream.Flush();

//                        _fileSize += message.Length;
//                        if(_fileSize > _maxSize)
//                        {
//                            _stream.Close();
//                            _stream = null;

//                            DeleteOldFiles();
//                        }
//                    }
//                }
//                catch(Exception ex)
//                {
//                    internalWriteLine("traceHelper: internalLogToFile: Error while tracing to file: " + ex.ToString());
//                    _traceToFile = false;
//                }
//            }
//        }

//        public class FileInfoComparer : IComparer  
//        {
//            // Calls CaseInsensitiveComparer.Compare with the parameters reversed.
//            int IComparer.Compare( object x, object y )  
//            {
//                FileInfo	fi_x = x as FileInfo;
//                FileInfo	fi_y = y as FileInfo;

//                return( fi_x.LastWriteTimeUtc.CompareTo(fi_y.LastWriteTimeUtc) );
//            }
//        }

//        private static void DeleteOldFiles()
//        {
//            try
//            {
//                DirectoryInfo	dirInfo = new DirectoryInfo(_tracePath);
//                FileInfo[]		fileInfos = dirInfo.GetFiles(_traceFilename + "*.log");
//                long			totalFileSize = 0;

//                foreach(FileInfo fi in fileInfos)
//                {
//                    totalFileSize += fi.Length;
//                }

//                if(totalFileSize > _maxTotalTraceSize)
//                {
//                    FileInfoComparer comp = new FileInfoComparer();
//                    Array.Sort(fileInfos, comp);

//                    for(int index = 0; totalFileSize > _maxTotalTraceSize && index < fileInfos.Length; index++)
//                    {
//                        totalFileSize -= fileInfos[index].Length;
//                        File.Delete(fileInfos[index].FullName);
//                    }
//                }
//            }
//            catch(Exception ex)
//            {
//                internalWriteLine("traceHelper.DeleteOldFiles: Error while removing old trace files: " + ex.ToString());
//            }
//        }

//        public static void WriteLine(string message)
//        {
//            if(_traceEnabled == true)
//            {
//                string formattedMessage = string.Format( "[{0}] {1}", AppDomain.GetCurrentThreadId(), message );

//                internalWriteLine( formattedMessage );
//                internalLogToFile( formattedMessage );
//            }
//        }

//        public static void WriteLine(int level, string message)
//        {
//            Debug.Assert(level > 0);

//            if( _traceEnabled == true && level <= _traceLevel )
//            {
//                string formattedMessage = string.Format( "[{0}] {1}", AppDomain.GetCurrentThreadId(), message );

//                internalWriteLine( formattedMessage );
//                internalLogToFile( formattedMessage );
//            }
//        }

//        private static int			_traceLevel = -1;
//        private static bool			_traceEnabled = false;
//        private static bool			_traceToFile = false;
//        private static string		_traceFilename = "traceHelper";
//        private static string		_tracePath = @".\";
//        private static StreamWriter	_stream = null;
//        private static long			_fileSize = 0;
//        private static int			_maxSize = 50*1024*1024;
//        private static long			_maxTotalTraceSize = _maxSize * 10;

//        static traceHelper( )
//        {
//            RefreshSettings( );
//        }

//        public static int TraceLevel
//        {
//            get { return _traceLevel; }
//        }

//        static void RefreshSettings( )
//        {
//            try
//            {
//                string enabled = configurationHelper.AppSettings["traceHelper.TraceEnabled"];
//                if(enabled.ToLower().CompareTo("true") == 0)
//                {
//                    internalWriteLine("traceHelper: Trace has been Enabled");
//                    _traceEnabled = true;
//                }
//                else
//                {
//                    internalWriteLine("traceHelper: Trace has been Disabled");
//                    _traceEnabled = false;
//                }

//            }
//            catch
//            {
//                // NOP
//                _traceEnabled = false;
//            }

//            if( _traceEnabled == true)
//            {
//                try
//                {
//                    string level = configurationHelper.AppSettings["traceHelper.TraceLevel"];
//                    _traceLevel = Convert.ToInt32(level);

//                    if(_traceLevel < 0)
//                    {
//                        traceHelper.WriteLine("Error: traceHelper: traceHelper.TraceLevel must be equal to or greater than zero. Disabling trace!");
//                        _traceLevel = -1;
//                    }

//                    internalWriteLine("traceHelper: Trace level has been set to " + _traceLevel.ToString());
//                }
//                catch
//                {
//                    _traceLevel = -1;
//                }

//                try
//                {
//                    string traceToFile = configurationHelper.AppSettings["traceHelper.File.Enabled"];

//                    if(traceToFile.ToLower().CompareTo("true") == 0)
//                    {
//                        _traceToFile = true;
//                    }
//                    else
//                    {
//                        _traceToFile = false;
//                    }

//                    try
//                    {
//                        _traceFilename = configurationHelper.AppSettings["traceHelper.File.Filename"];
//                        internalWriteLine("traceHelper: TraceFilename= " + _traceFilename);
//                    }
//                    catch
//                    {
//                        _traceFilename = "traceHelper.log";
//                    }

//                    try
//                    {
//                        _tracePath = configurationHelper.AppSettings["traceHelper.File.Path"];
//                        internalWriteLine("traceHelper: TracePath= " + _tracePath);
//                    }
//                    catch
//                    {
//                        _tracePath = @".\";
//                    }
//                }
//                catch(Exception)
//                {
//                    _traceToFile = false;
//                }

//                internalWriteLine("traceHelper: TraceToFile= " + _traceToFile.ToString());

//                try
//                {
//                    _maxSize = System.Convert.ToInt32(configurationHelper.AppSettings["traceHelper.File.MaxFileSizeInMb"]);
//                    internalWriteLine("traceHelper: MaxFileSizeInMb= " + _maxSize);
//                    _maxSize *= 1024 * 1024;
//                }
//                catch
//                {
//                    _maxSize = 10;
//                    _maxSize *= 1024 * 1024; // Default to 50 MB
//                }

//                try
//                {
//                    _maxTotalTraceSize = System.Convert.ToInt32(configurationHelper.AppSettings["traceHelper.File.MaxTotalFileSizeInMb"]);
//                    internalWriteLine("traceHelper: MaxTotalSizeInMb= " + _maxTotalTraceSize);
//                    _maxTotalTraceSize *= 1024 * 1024;
//                }
//                catch
//                {
//                    _maxTotalTraceSize = 10 * _maxSize;
//                }
//            }
//        }
//    }
//}
