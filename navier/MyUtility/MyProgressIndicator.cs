﻿using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;

namespace MyUtility
{
    public class MyProgressIndicator
    {
        // 秀進度條 Progress indicator shown in system tray
        private ProgressIndicator mProgressIndicator = null;

        System.Windows.DependencyObject _WindowsDependencyObject;

        private Popup gPopupControl;
        private ProcessingControl mProcessingControl;

        public MyProgressIndicator()
        {
            //_WindowsDependencyObject = element;

            gPopupControl = new Popup();
            mProcessingControl = new ProcessingControl();
            gPopupControl.Child = mProcessingControl;
        }

        //public void ShowProgressIndicator(String msg, bool isLandScape)
        //{
        //    ShowProgressIndicator(_WindowsDependencyObject, msg, isLandScape);
        //}

        /// <summary>
        /// Helper method to show progress indicator in system tray
        /// </summary>
        /// <param name="msg">Text shown in progress indicator</param>
        public void ShowProgressIndicator(System.Windows.DependencyObject element, String msg, bool isLandScape)
        {
            _WindowsDependencyObject = element;

            if (mProgressIndicator == null)
            {
                mProgressIndicator = new ProgressIndicator(); //馬的new 這個會當掉，看來事情沒這麼簡單
                mProgressIndicator.IsIndeterminate = true;
            }

            mProgressIndicator.Text = msg;
            mProgressIndicator.IsVisible = true;

            SystemTray.SetProgressIndicator(_WindowsDependencyObject, mProgressIndicator);

            gPopupControl.IsOpen = true;
            mProcessingControl.SetText(msg);



            double hostWidth = Utility.GetDeviceWidth(true);
            double hostHeight = Utility.GetDeviceHeight(true);
            mProcessingControl.Width = hostWidth;
            mProcessingControl.Height = hostHeight;

            //設定X與Y軸
            gPopupControl.HorizontalOffset = (hostHeight - hostWidth) * 0.5f;
            gPopupControl.VerticalOffset = (hostWidth - hostHeight) * 0.5f;



        }

        public void ShowProgressIndicator_updateOrientation(Microsoft.Phone.Controls.PageOrientation orientation)
        {
            mProcessingControl.UpdateOrientation(orientation);
        }

        public void ShowProgressIndicator_updateInfo(string msg)
        {
            mProcessingControl.AddSubText(msg);
        }

        /// <summary>
        /// Helper method to hide progress indicator in system tray
        /// </summary>
        public void HideProgressIndicator()
        {
            if (mProgressIndicator == null)
                return;

            mProgressIndicator.IsVisible = false;
            SystemTray.SetProgressIndicator(_WindowsDependencyObject, mProgressIndicator);

            mProgressIndicator = null;

            gPopupControl.IsOpen = false;
        }
    }
}
