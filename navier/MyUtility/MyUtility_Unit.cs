﻿
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Windows.Phone.Speech.Synthesis;
using Coding4Fun.Toolkit.Controls;
using ResourceApp.Resources;
using BingTranslationApp;


namespace MyUtility
{


    public static partial class Utility
    {
        //-------------------------------------------------------------------------------------
        //輸入經緯度的度
        //傳回值單位公里
        //(((X1-X2)^2+(Y1-Y2)^2 )^0.5)*111
        //範例
        //((121.1234567-121.1234566)^2+(23.1234567-23.1234566)^2)^0.5*111

        /// <summary>
        /// 將公里轉換成經緯度單位(這樣計算是有錯誤的，不精確)
        /// </summary>
        public static double Kilometer2LonLat(double kilometer)
        {
            return kilometer / 111f;
        }


        /// <summary>
        /// 根据经纬度计算地面两点间的距离
        /// </summary>
        /// <param name="prevLong">前一个点的经度</param>
        /// <param name="prevLat">前一个点的纬度</param>
        /// <param name="currLong">当前点的经度</param>
        /// <param name="currLat">当前点的纬度</param>
        /// <returns>两点间的距离(米)</returns>
        public static double LonLat2Meter(double prevLong, double prevLat, double currLong, double currLat)
        {
            //return lonlat * 111f;

            const double degreesToRadians = (Math.PI / 180.0);
            const double earthRadius = 6371; // 地球半径平均值为6371千米

            var prevRadLong = prevLong * degreesToRadians;
            var prevRadLat = prevLat * degreesToRadians;
            var currRadLong = currLong * degreesToRadians;
            var currRadLat = currLat * degreesToRadians;

            double cosX = Math.Cos(prevRadLat) * Math.Cos(currRadLat) * Math.Cos(prevRadLong - currRadLong) + Math.Sin(prevRadLat) * Math.Sin(currRadLat);
            double X = Math.Acos(cosX);//两点与球心连线的夹角
            double d = earthRadius * X * 1000;//单位转换为米
            return d;
        }

        //-------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// 英里轉換成公里
        /// </summary>
        /// <param name="mil"></param>
        /// <returns></returns>
        public static double Mile2Kilometer(double mil)
        {
            //1Mile = 約1.61km
            return mil * 1.61f;
        }

        /// <summary>
        /// 公尺轉換成英里
        /// </summary>
        /// <param name="mil"></param>
        /// <returns></returns>
        public static double Meter2Mile(double meter)
        {
            //1Mile = 約1610 m
            return meter / 1610f;
        }


        /// <summary>
        /// 公尺轉換成YARD
        /// </summary>
        /// <param name="mil"></param>
        /// <returns></returns>
        public static double Meter2Yard(double meter)
        {
            //1碼(yard) = 0.9144m
            return meter / 0.9144f;
        }

        /// <summary>
        /// 根據目前設定的單位，回傳單位的"字串"，以及 距離的"字串"
        /// 若是可用yard或是meter可進位，就使用mile或是kilometer為單位
        /// </summary>
        public static void ProcessDistanceUnit(double remainDisMeter, bool isUnit, out string sUnit, out string sDistance)
        {
            sUnit = "";
            sDistance = "";

            if (isUnit)
            {
                double ddd = remainDisMeter / 1000f;

                if (ddd >= 1f)
                {   //ex:2.099=>2 diff=0.099(same) , 2.9=>2 diff=0.9(not same) 
                    bool bSameNumber = false;
                    if (Math.Abs(ddd - Math.Floor(ddd)) < 0.1f)//只判斷到小數一位
                        bSameNumber = true;

                    sUnit = Utility.GetTranslate_KiloMeter() + " ";
                    if (bSameNumber)
                        sDistance = ddd.ToString("F0");
                    else
                        sDistance = ddd.ToString("F1");//原本要是 30公里就不要30.0公里了，但是這種剛好沒小數的狀況很少，所以一概都抓到小數第一位
                }
                else
                {
                    sUnit = Utility.GetTranslate_Meter() + " ";
                    sDistance = remainDisMeter.ToString("F0");
                }
            }
            else
            {
                //英制單位先看有幾MILE，要是MILE小於1就用YARD
                double ddd = Utility.Meter2Mile(remainDisMeter);
                if (ddd >= 1f)
                {
                    bool bSameNumber = false;
                    if (Math.Abs(ddd - Math.Floor(ddd)) < 0.1f)//只判斷到小數一位
                        bSameNumber = true;

                    sUnit = Utility.GetTranslate_Mile() + " ";
                    if (bSameNumber)
                        sDistance = ddd.ToString("F0");
                    else
                        sDistance = ddd.ToString("F1");
                }
                else
                {
                    sUnit = Utility.GetTranslate_Yard() + " ";
                    sDistance = Utility.Meter2Yard(remainDisMeter).ToString("F0");
                }
            }
        }
    }
}
