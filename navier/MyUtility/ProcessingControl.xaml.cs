﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.Devices.Sensors;
using Windows.Foundation;

namespace MyUtility
{
    public partial class ProcessingControl : UserControl
    {
       // private Inclinometer _inclinometer;
        private string[][] mSubText = new string[2][];
        private int switchNumber = 0;

        public ProcessingControl()
        {
            InitializeComponent();

            mRotate.Rotation = 90;

            //_inclinometer = Inclinometer.GetDefault();
            //if (_inclinometer != null)
            //{
            //    // Establish the report interval for all scenarios
            //    uint minReportInterval = _inclinometer.MinimumReportInterval;
            //    uint reportInterval = minReportInterval > 100 ? minReportInterval : 100;
            //    _inclinometer.ReportInterval = reportInterval;

            //    // Establish the event handler
            //   // _inclinometer.ReadingChanged += new TypedEventHandler<Inclinometer, InclinometerReadingChangedEventArgs>(ReadingChanged);
            //}
        }

        //   private const int mSubTextSize = 8;
        public void AddSubText(string text)
        {
            if (mSubText[switchNumber] == null)
            {
                mSubText[switchNumber] = new string[8];
            }

            int prevNumber = 0;
            if (switchNumber == 0)
                prevNumber = 1;

            for (int a = mSubText[switchNumber].Length - 1; a >= 1; a--)
            {
                if (mSubText[prevNumber] != null)
                    mSubText[switchNumber][a] = mSubText[prevNumber][a - 1];
            }
            mSubText[switchNumber][0] = text;

            mProcessingSubText.ItemsSource = mSubText[switchNumber];

            mProcessingSubText.SelectedIndex = -1;
            mProcessingSubText.SelectedItem = null;

            switchNumber++;
            switchNumber %= 2;

        }

        //private void ReadingChanged(object sender, InclinometerReadingChangedEventArgs e)
        //{
        //    InclinometerReading reading = e.Reading;
        //    Dispatcher.BeginInvoke(() =>
        //    {
        //        if (reading.RollDegrees < 0 && mRotate.Rotation < 0)
        //            mRotate.Rotation = 90;
        //        else if (reading.RollDegrees > 0 && mRotate.Rotation > 0)
        //            mRotate.Rotation = -90;
        //    });
        //}

        public void UpdateOrientation(PageOrientation orientation)
        {
            if (orientation == PageOrientation.LandscapeLeft)
                 mRotate.Rotation = 90;
            else if (orientation == PageOrientation.LandscapeRight)
                 mRotate.Rotation = -90;
        }

        public void SetText(string sText)
        {
            //Utility.ShowToast(sText, 1000, true);
            mProcessingText.Text = sText;


        }

    }
}
