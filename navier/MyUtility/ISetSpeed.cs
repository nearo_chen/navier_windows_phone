﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace MyUtility
{
    public interface ISetSpeed
    {
        /// <summary>
        /// 設定時速為幾公里
        /// nowMovedDistanceMeter現在已經總共移動多少公尺，目前位置距離起點
        /// totalDistanceMeter整個路線為幾公尺(起點到終點全長)
        /// speedAVG_KMHR 平均時速KM/HR
        /// remainDistanceToNextTurnPoint 還剩多遠到達下一個轉彎點
        /// </summary>
        /// <param name="speedKMHR"></param>
        void Interface_SetSpeedKMHR(double speedKMHR, 
            int nowMovedDistanceMeter, 
            int totalDistanceMeter, 
            double speedAVG_KMHR,
            double remainDistanceToNextTurnPoint);
    }

    /// <summary>
    /// 設定目前顯示的單位
    /// </summary>
    public interface ISetUnit
    {
        /// <summary>
        /// 設定目前顯示的單位
        /// </summary>
        void Interface_SetUnit(bool isUnit);


    }

    /// <summary>
    /// 給設定顏色的Control需求去設定顏色
    /// </summary>
    public interface ISetColor
    {
        void Interface_SetColor(Color color);
    }

    public interface ISetAcuracy
    {
        /// <summary>
        /// movedDistanceMeterCount傳入每次經緯度之間的距離
        /// </summary>
        void Interface_SetGPSAcuracy(double acuracyMeter, float viewRangeInSpeedLonLat, float base2DViewWidth, double lat, double lon,
                                     double movedDistanceMeterCount);
    }

    public interface ITime24HR
    {
        void Interface_SetTimeFormat(bool is24HR);
    }

    public interface ISetSpeedLimit
    {
        void Interface_SetSpeedLimit(bool isUnit, int speedLimitKMH, int speedLimitMPH);
    }

    public interface ISetDragColor
    {
        void SetDragColor();
        void RevertDragColor();
    }

    ///// <summary>
    ///// 給有需要在按home鍵之後又返回儲存資料使用
    ///// </summary>
    //public interface INeedSaveData
    //{
    //    void Interface_NeedSaveData();
    //}
}
