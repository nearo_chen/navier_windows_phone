﻿#define ENABLE_VIEW

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MyUtility
{
    /// <summary>
    /// 會在畫面上的一個輸出訊息的view
    /// </summary>
    public class MyDebugViewer
    {

        ScrollViewer mScrollViewer;
        TextBlock mTextBlock;
        List<string> mStrings = new List<string>();
        bool bTextColorSwitch = true;
        System.Windows.Media.Color colorWhite = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
        System.Windows.Media.Color colorBlack = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);

        public MyDebugViewer()
        {
            CreateMyDebugViewer(null);
        }

        private void CreateMyDebugViewer(System.Windows.Controls.Panel layoutRoot)
        {
            mScrollViewer = new ScrollViewer();
            mScrollViewer.Height = 400;
            mScrollViewer.Width = 700;
            mScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
            mScrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;

            mTextBlock = new TextBlock();
            mTextBlock.Width = 700;
            mTextBlock.TextWrapping = System.Windows.TextWrapping.Wrap;
            mTextBlock.DoubleTap += TextBox_DoubleTap_1;

            mScrollViewer.Content = mTextBlock;

#if ENABLE_VIEW
            if (layoutRoot != null)
                layoutRoot.Children.Add(mScrollViewer);
#endif
        }

        private void TextBox_DoubleTap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            System.Windows.Media.Color color = colorWhite;
            if (bTextColorSwitch)
                color = colorBlack;
            bTextColorSwitch = !bTextColorSwitch;

            System.Windows.Media.SolidColorBrush brush = new System.Windows.Media.SolidColorBrush(color);
            mTextBlock.Foreground = brush;
        }

        public void AddText(string sss)
        {
#if ENABLE_VIEW
#else
            return;
#endif


            System.Windows.Controls.Panel parent = mScrollViewer.Parent as System.Windows.Controls.Panel;
            AddText(sss, parent);
        }

        public void AddText(string sss, System.Windows.Controls.Panel layoutRoot)
        {
#if ENABLE_VIEW
#else
            return;
#endif


            Utility.GetCurrentDispatcher().BeginInvoke(() =>
                {

                    if (layoutRoot != null)
                    {
                        //處理一下parent的事情(若是新parent跟就parent不一樣，就把他從舊parent中移除)
                        if (mScrollViewer.Parent != null && mScrollViewer.Parent != layoutRoot)
                        {
                            System.Windows.Controls.Panel parent = mScrollViewer.Parent as System.Windows.Controls.Panel;
                            parent.Children.Remove(mScrollViewer);
                        }

                        if (mScrollViewer.Parent == null)
                            layoutRoot.Children.Add(mScrollViewer);
                    }

                    mStrings.Add(sss);

                    while (mStrings.Count > 50)
                        mStrings.RemoveAt(0);

                    mTextBlock.Text = string.Empty;

                    foreach (string temp in mStrings)
                        mTextBlock.Text += temp + "\r\n";

                });

        }
    }
}
