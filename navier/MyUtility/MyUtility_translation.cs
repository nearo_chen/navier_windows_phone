﻿
using BingTranslationApp;
using System;
using System.IO.IsolatedStorage;
using System.Net;
using System.Windows;
using System.Windows.Controls;


namespace MyUtility
{


    public static partial class Utility
    {
        private const string _sBrightnessMaxEng = //"Changing the screen brightness: 1.Sweep the home screen to the left to open the list of applications. Scroll down and tap Settings. 2.Scroll down and tap Brightness. 3.Tap the Automatically Adjust icon to de-activate it. 4.Tap Level to select the desired level of brightness: low, medium or high.";
            "更改螢幕亮度： 1.掃向左打開應用程式清單的主畫面。向下滾動並點擊設置。2.向下滾動，然後點擊亮度。3.點擊自動調整圖示來取消啟動它。4.點擊要選擇所需的亮度級別級別： 低、 中或高。";

        //公尺英里-------------------------------------------------------------------------------------------------------
        private static string[] translateDatas = new string[] { "公尺", "然後", "公里", "注意", _sBrightnessMaxEng, "關閉", "男姓", "女姓" };//, "碼" };// 碼跟公尺太相近了，不使用這種東西ㄌ而且翻譯很怪
        private static string sKiloMeter = null;
        private static string sMeter = null;
        //private static string sYard = "yards";//用碼的話就全都用英文，因為有些國家根本沒有碼這種概念和文字
        private static string sNext = null;//然後
        private static string sAttention = null;//注意
        private static string sHowBrightnessMax = null;//螢幕亮度調最亮
        private static string sVoiceNO = null;
        private static string sVoiceMale = null;
        private static string sVoiceFemale = null;

        /// <summary>
        /// 在AP啟動時都去檢查看看是否有我要送翻譯的字有沒有翻好
        /// </summary>
        private static void _triggerToTranslateString()
        {
            BingUtility.TryTranslationString(GetCurrentLanguageRegionName(), translateDatas);
            string[] results = BingUtility.TranslationResult();
            if (results != null)
            {
                sMeter = results[0];
                //sYard = results[1];
                sNext = results[1];
                sKiloMeter = results[2];
                sAttention = results[3];
                sHowBrightnessMax = results[4];

                sVoiceNO = results[5];
                sVoiceMale = results[6];
                sVoiceFemale = results[7];


                IsolatedStorageSettings.ApplicationSettings["Translate_Meter"] = sMeter;
                //IsolatedStorageSettings.ApplicationSettings["Translate_Yard"] = sYard;
                IsolatedStorageSettings.ApplicationSettings["Translate_NextStep"] = sNext;
                IsolatedStorageSettings.ApplicationSettings["Translate_KiloMeter"] = sKiloMeter;
                IsolatedStorageSettings.ApplicationSettings["Translate_Attention"] = sAttention;
                IsolatedStorageSettings.ApplicationSettings["Translate_HowBrightnessMax"] = sHowBrightnessMax;

                IsolatedStorageSettings.ApplicationSettings["Translate_NoVoice"] = sVoiceNO;
                IsolatedStorageSettings.ApplicationSettings["Translate_MaleVoice"] = sVoiceMale;
                IsolatedStorageSettings.ApplicationSettings["Translate_FemaleVoice"] = sVoiceFemale;


                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }

        private static void _checkTranslate()
        {
            string language = null;
            if (IsolatedStorageSettings.ApplicationSettings.TryGetValue("Translate_Language", out language))
            {
            }

            if (language == null || language != Utility.GetCurrentLanguageRegionName())
            {
                IsolatedStorageSettings.ApplicationSettings["Translate_Language"] = Utility.GetCurrentLanguageRegionName();

                if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_Meter"))
                    IsolatedStorageSettings.ApplicationSettings.Remove("Translate_Meter");
                if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_NextStep"))
                    IsolatedStorageSettings.ApplicationSettings.Remove("Translate_NextStep");
                if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_KiloMeter"))
                    IsolatedStorageSettings.ApplicationSettings.Remove("Translate_KiloMeter");
                if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_Attention"))
                    IsolatedStorageSettings.ApplicationSettings.Remove("Translate_Attention");
                if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_HowBrightnessMax"))
                    IsolatedStorageSettings.ApplicationSettings.Remove("Translate_HowBrightnessMax");

                if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_NoVoice"))
                    IsolatedStorageSettings.ApplicationSettings.Remove("Translate_NoVoice");
                if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_MaleVoice"))
                    IsolatedStorageSettings.ApplicationSettings.Remove("Translate_MaleVoice");
                if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_FemaleVoice"))
                    IsolatedStorageSettings.ApplicationSettings.Remove("Translate_FemaleVoice");

            }
        }

        public static string GetTranslate_VoiceNO()
        {
            _checkTranslate();
            if (sVoiceNO != null)
                return sVoiceNO;
            if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_NoVoice"))
            {
                sVoiceNO = IsolatedStorageSettings.ApplicationSettings["Translate_NoVoice"] as string;
                return sVoiceNO;
            }
            return "";
        }

        public static string GetTranslate_VoiceMale()
        {
            _checkTranslate();
            if (sVoiceMale != null)
                return sVoiceMale;
            if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_MaleVoice"))
            {
                sVoiceMale = IsolatedStorageSettings.ApplicationSettings["Translate_MaleVoice"] as string;
                return sVoiceMale;
            }
            return "";
        }

        public static string GetTranslate_VoiceFemale()
        {
            _checkTranslate();
            if (sVoiceFemale != null)
                return sVoiceFemale;
            if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_FemaleVoice"))
            {
                sVoiceFemale = IsolatedStorageSettings.ApplicationSettings["Translate_FemaleVoice"] as string;
                return sVoiceFemale;
            }
            return "";
        }

        /// <summary>
        /// 呼叫這個function，會觸發他去取得翻譯文字，不會馬上回傳直，外部使用者可以依直呼叫此FUNC，
        /// 來不斷檢查他是否取得字串成功了
        /// </summary>      
        public static string GetTranslate_BrightnessMax()
        {
            _checkTranslate();

            if (sHowBrightnessMax != null)
                return sHowBrightnessMax;

            if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_HowBrightnessMax"))
                sHowBrightnessMax = IsolatedStorageSettings.ApplicationSettings["Translate_HowBrightnessMax"] as string;
            else
                _triggerToTranslateString();

            if (sHowBrightnessMax == null)
                return _sBrightnessMaxEng;
            return sHowBrightnessMax;
        }

        /// <summary>
        /// 呼叫這個function，會觸發他去取得翻譯文字，不會馬上回傳直，外部使用者可以依直呼叫此FUNC，
        /// 來不斷檢查他是否取得字串成功了
        /// </summary>      
        public static string GetTranslate_Meter()
        {
            _checkTranslate();

            if (sMeter != null)
                return sMeter;

            if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_Meter"))
                sMeter = IsolatedStorageSettings.ApplicationSettings["Translate_Meter"] as string;
            else
                _triggerToTranslateString();

            if (sMeter == null)
                return "";
            return sMeter;
        }

        /// <summary>
        /// 呼叫這個function，會觸發他去取得翻譯文字，不會馬上回傳直，外部使用者可以依直呼叫此FUNC，
        /// 來不斷檢查他是否取得字串成功了
        /// </summary>      
        public static string GetTranslate_KiloMeter()
        {
            _checkTranslate();

            if (sKiloMeter != null)
                return sKiloMeter;

            if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_KiloMeter"))
                sKiloMeter = IsolatedStorageSettings.ApplicationSettings["Translate_KiloMeter"] as string;
            else
                _triggerToTranslateString();

            if (sKiloMeter == null)
                return "";
            return sKiloMeter;
        }

        /// <summary>
        /// 呼叫這個function，會觸發他去取得翻譯文字，不會馬上回傳直，外部使用者可以依直呼叫此FUNC，
        /// 來不斷檢查他是否取得字串成功了
        /// </summary>   
        public static string GetTranslate_Next()
        {
            _checkTranslate();

            if (sNext != null)
                return sNext;

            if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_NextStep"))
                sNext = IsolatedStorageSettings.ApplicationSettings["Translate_NextStep"] as string;
            else
                _triggerToTranslateString();

            if (sNext == null)
                return "Next";
            return sNext;
        }

        /// <summary>
        /// 呼叫這個function，會觸發他去取得翻譯文字，不會馬上回傳直，外部使用者可以依直呼叫此FUNC，
        /// 來不斷檢查他是否取得字串成功了
        /// </summary>   
        public static string GetTranslate_Attention()
        {
            _checkTranslate();

            if (sAttention != null)
                return sAttention;

            if (IsolatedStorageSettings.ApplicationSettings.Contains("Translate_Attention"))
                sAttention = IsolatedStorageSettings.ApplicationSettings["Translate_Attention"] as string;
            else
                _triggerToTranslateString();

            if (sAttention == null)
                return "Attention";
            return sAttention;
        }

        public static string GetTranslate_Yard()
        {
            return "yards";//用碼的話就全都用英文，因為有些國家根本沒有碼這種概念和文字
        }

        public static string GetTranslate_Mile()
        {
            return "mile";
        }
      
    }
}
