﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ResourceApp.Resources;
using Microsoft.Phone.Tasks;


namespace MyUtility
{
    public partial class AboutControl : UserControl
    {
        //ScrollViewer mScrollViewer;

        public AboutControl()
        {
            InitializeComponent();

            string sss = AppResources.About_IfYouLike.Replace("Google Play", "Marketplace");

            sss = sss.Replace("Google play", "Marketplace");
            sss = sss.Replace("google Play", "Marketplace");
            sss = sss.Replace("google play", "Marketplace");

            sss = sss.Replace("GooglePlay", "Marketplace");
            sss = sss.Replace("googleplay", "Marketplace");
            mTextMarketPlace.Text = sss;
          //  mTextMarketPlace.Text = AppResources.About_IfYouLike;

            //mScrollViewer = new ScrollViewer();
            //mScrollViewer.Height = Utility.GetDeviceHeight(true);
            //mScrollViewer.Width = Utility.GetDeviceWidth(true); ;
            //mScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
            //mScrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;

            //mScrollViewer.Content = LayoutRoot;

        }

        private void Marketplace_StackPanel_ManipulationCompleted_1(object sender, System.Windows.Input.ManipulationCompletedEventArgs e)
        {
            //将用户带到Windows Phone Marketplace中当前程序的评论页。
            MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();
            marketplaceReviewTask.Show();
        }

        private void Mail_StackPanel_ManipulationCompleted_1(object sender, System.Windows.Input.ManipulationCompletedEventArgs e)
        {
            var email = new EmailComposeTask
            {
                To = "pencialbox@gmail.com ; pencialbox@msn.com ; Lu.shangchiun@gmail.com",
                Subject = "Navier HUD Feedback"
            };

            email.Show();
        }

        private void TextBlock_ManipulationCompleted_1(object sender, System.Windows.Input.ManipulationCompletedEventArgs e)
        {
            var web = new WebBrowserTask { Uri = new Uri("http://www.youtube.com/watch?v=uL612TaO2Io") };

            web.Show();
        }
    }
}
