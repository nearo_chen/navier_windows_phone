﻿using System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;


using Windows.UI.Core;
using Windows.Devices.Sensors;
using Microsoft.Phone.Controls;

namespace InclinometerApp
{
    public partial class MainPage : PhoneApplicationPage
    {
        Inclinometer _inclinometer;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

            _inclinometer = Inclinometer.GetDefault();


            if (_inclinometer != null)
            {
                // Establish the report interval for all scenarios
                uint minReportInterval = _inclinometer.MinimumReportInterval;
                uint reportInterval = minReportInterval > 100 ? minReportInterval : 100;
                _inclinometer.ReportInterval = reportInterval;

                // Establish the event handler
                _inclinometer.ReadingChanged += new TypedEventHandler<Inclinometer, InclinometerReadingChangedEventArgs>(ReadingChanged);
            }

        }

        private void ReadingChanged(object sender, InclinometerReadingChangedEventArgs e)
        {
            InclinometerReading reading = e.Reading;
            Dispatcher.BeginInvoke(() =>
            {
                
                txtPitch.Text = String.Format("{0,5:0.00}", reading.PitchDegrees);
                txtRoll.Text = String.Format("{0,5:0.00}", reading.RollDegrees);
                txtYaw.Text = String.Format("{0,5:0.00}", reading.YawDegrees);
            });

        }
    }
}