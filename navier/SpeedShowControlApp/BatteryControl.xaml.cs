﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.Phone.Devices.Power;
using MyUtility;
using System.Windows.Media;
using Microsoft.Phone.Info;
using System.Windows.Threading;

namespace SpeedShowControlApp
{
    public partial class BatteryControl : UserControl, ISetColor, ISetDragColor
    {
        //每秒更新一次
        private DispatcherTimer _timer = new DispatcherTimer();

        readonly Battery _battery;

        public static BatteryControl AddToRoot(System.Windows.Controls.Panel layoutRoot, int viewPosX, int viewPosY,
         int w, int h)
        {
            BatteryControl batteryControl = new BatteryControl();
            layoutRoot.Children.Add(batteryControl);

            //移動control座標
            Utility.SetControlPosition(batteryControl, viewPosX, viewPosY, true);
            Utility.SetControlWH(batteryControl, w, h, true);
            return batteryControl;
        }

        public BatteryControl()
        {
            InitializeComponent();

            Width = 80;
            Height = 40;

            //获取当前设备电源对象,注意：不需要创建对象实体
            _battery = Battery.GetDefault();

            _battery.RemainingChargePercentChanged += _battery_RemainingChargePercentChanged;
            //更新用户界面操作
            UpdateUI();

            _timer.Interval = TimeSpan.FromMilliseconds(1000);
            _timer.Tick += Timer_Tick;
            _timer.Start();
        }

        //电源百分比变化时，及时更新UI
        void _battery_RemainingChargePercentChanged(object sender, object e)
        {
            UpdateUI();
        }

        void UpdateUI()
        {
            Utility.GetCurrentDispatcher().BeginInvoke(() =>
            {
                this.mBatteryPercentText.Text = string.Format("{0} %", _battery.RemainingChargePercent);
                //显示剩余电量使用时间,注意：RenainingDischargeTime包含多种格式的时间显示方式,可自行取值。
                //this.tblBatteryDisplayTime.Text = string.Format("{0} 分钟", _battery.RemainingDischargeTime.TotalMinutes);

                _setNowFill(_battery.RemainingChargePercent);
            });
        }

        private float _mFillStart;
        private float _mRemainLength;
        private void _setNowFill(float percent)
        {
            float intervel = 410f - 30f;
            _mFillStart = 30f + (intervel * percent / 100f);
            mBatteryFill.X2 = _mFillStart;

            _mRemainLength = 410f - _mFillStart;//受下多少空間先算一下
        }

        public void Interface_SetColor(System.Windows.Media.Color color)
        {
            mBatteryFill.Stroke = mBatteryShape.Stroke = mBatteryPercentText.Foreground = new SolidColorBrush(color);
        }

        private int _mChargingAnimPosCount = 0;
        private void Timer_Tick(object sender, EventArgs e)
        {
            //判斷是否正在充電中
            if (DeviceStatus.PowerSource == PowerSource.External)
            {
                float intervel = _mRemainLength / 2f;

                //float remainTime = (float)Math.Floor(_mRemainLength / intervel);
                //_mChargingAnimPos = _mFillStart +
                float chargingAnimPos = intervel * (float)_mChargingAnimPosCount;
                
                _mChargingAnimPosCount++;
                if (_mChargingAnimPosCount == 3)
                    _mChargingAnimPosCount = 0;

                mBatteryFill.X2 = _mFillStart + chargingAnimPos;
            }
            else
                mBatteryFill.X2 = _mFillStart;
        }

        //************************************************************************************************
        // 為了編輯介面可以拖拉的功能
        //************************************************************************************************
        private void UserControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Utility.DragableGrid_ControlSelected(this);
        }

        public void SetDragColor()
        {
            Interface_SetColor(Utility.DragControl_Color);

            mBackGroundColor.Fill = Utility.DragControl_ColorBrush;
            mBackGroundColor.Stroke = null;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        public void RevertDragColor()
        {
            Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));

            //用成這樣才不會有點不到的現象(透空的地方點不到)
            mBackGroundColor.Fill = new SolidColorBrush(Color.FromArgb(5, 0, 0, 0));
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }
    }
}
