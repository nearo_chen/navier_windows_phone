﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using MyUtility;
using System.Windows.Media;
using System.IO;

namespace SpeedShowControlApp
{
    public partial class SpeedRendererControl : UserControl, ISetSpeed, ISetColor, ISetUnit, ISetSpeedLimit, ISetDragColor
    {
        private DispatcherTimer _timer = new DispatcherTimer();//animation 自己做LOW PASS

        private double _fNowValue = 0;
        private double _fDestValueKM = 0;

        public static SpeedRendererControl AddToRoot(System.Windows.Controls.Panel layoutRoot, int viewPosX, int viewPosY,
            int w, int h)
        {
            SpeedRendererControl speedRendererControl = new SpeedRendererControl();
            layoutRoot.Children.Add(speedRendererControl);


            //移動control座標
            Utility.SetControlPosition(speedRendererControl, viewPosX, viewPosY, true);

            //Point wh = Utility.PercentToResulution(WRatio, HRatio);
            Utility.SetControlWH(speedRendererControl, w, h, true);
            // Utility.SetControlScale(speedRendererControl, 0.9f, 0.9f, true);
            return speedRendererControl;
        }

        //public static void RemoveFromRoot(System.Windows.Controls.Panel layoutRoot, SpeedRendererControl speedRendererControl)
        //{
        //    layoutRoot.Children.Remove(speedRendererControl);
        //}



        public SpeedRendererControl()
        {
            InitializeComponent();

            Width = Height = 240;

            _timer.Interval = TimeSpan.FromMilliseconds(200);
            _timer.Tick += Timer_Tick;

            //Interface_SetSpeedKMHR(0, 0, 0);
        }

        private Brush mRECBrush = null;
        private void Timer_Tick(object sender, EventArgs e)
        {
            _fNowValue = _fNowValue * 0.8f + _fDestValueKM * 0.2f;
            mSpeedBar.StrokeDashArray[0] = (_fNowValue / 10f) * 1.008f;

            //偵測對應目前顯示單位，是否已經超速了
            if (
                   (mIsUnit && _fDestValueKM > mSpeedLimitKMH)
                || (mIsUnit == false && _fDestValueKM > Utility.Mile2Kilometer(mSpeedLimitMPH))
                )
            {
                if (mRECBrush == null)
                {
                    mRECBrush = mSpeedBar.Stroke;
                    mSpeedBar.Stroke = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));

                    Utility.PlaySound("Assets/music/cowbell.wav");
                }
            }
            else
            {
                if (mRECBrush != null)
                {
                    mSpeedBar.Stroke = mRECBrush;
                    mRECBrush = null;
                }
            }

            if (Math.Abs(_fNowValue - _fDestValueKM) < 0.1)
                _timer.Stop();
        }


        public void Interface_SetColor(Color color)
        {
            mCircleMain.Stroke = mSpeed1.Stroke = mSpeed2.Stroke = new SolidColorBrush(color);

            //超速警示時要維持警告色，先把新色彩設定在記錄色裡面
            if (mRECBrush == null)
                mSpeedBar.Stroke = mCircleMain.Stroke;
            else
                mRECBrush = new SolidColorBrush(color);

            Color orangered = Utility.GetHUDColor(Utility.SelectColor.color_orangered);
            if (color.Equals(orangered))
                mSpeedLimit.Stroke = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            else
                mSpeedLimit.Stroke = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
        }


        float speedLimit_KM_OldREC = -1;

        /// <summary>
        /// 根據KM設定單位，來設定正確的數值
        /// </summary>      
        private void _updateUI_SpeedLimit(float speedLimit_KM)
        {

            if (speedLimit_KM_OldREC.Equals(speedLimit_KM))
                return;
            speedLimit_KM_OldREC = speedLimit_KM;

            //speedLimit_KM / x = 10 / 4 ; x = 4 / 10 * speedLimit_KM
            //StrokeDashArray第一線段間格每跳4為10公里
            mSpeedLimit.StrokeDashArray[1] = 4f / 10f * speedLimit_KM;

        }


        int mSpeedLimitKMH, mSpeedLimitMPH;
        public void Interface_SetSpeedKMHR(double speedKMHR, int nowMovedDistanceMeter, int totalDistanceMeter,
            double speedAVG_KMHR,
            double remainDistanceToNextTurnPoint)
        {
            _fDestValueKM = speedKMHR;
            // Dispatcher.BeginInvoke(() =>
            // {
            if (_timer.IsEnabled == false)
                _timer.Start();
            // });

            if (mIsUnit)
                _updateUI_SpeedLimit(mSpeedLimitKMH);
            else
                _updateUI_SpeedLimit((float)Utility.Mile2Kilometer(mSpeedLimitMPH));

            //mSpeedBar.StrokeDashArray[0]  = speed / 10;//因為我介面的刻度調整成 每加1 代表時速10 KM/HR


            ////設置動畫
            //DoubleAnimation anima = new DoubleAnimation();
            //anima.AutoReverse = false;
            //anima.RepeatBehavior = RepeatBehavior.Forever;
            //anima.Duration = new Duration(TimeSpan.FromSeconds(1));  //设置动画耗时，这里为0.5秒
            //anima.From = 0;
            //anima.To = 360;

            //// 设置附加属性
            //Storyboard.SetTarget(anima, mSpeedBar);  //使动画与特定对象关联
            //Storyboard.SetTargetProperty(anima, new PropertyPath("(Path.StrokeDashArray[0])"));  //使动画与特定属性关联

            //// 生成storyboard，把动画加进去并启动
            //Storyboard storyboard = new Storyboard();
            //storyboard.Children.Add(anima);
            //storyboard.Begin();
        }

        bool mIsUnit = true;
        public void Interface_SetUnit(bool isUnit)
        {
            mIsUnit = isUnit;

            if (mIsUnit)
            {
                _updateUI_SpeedLimit(mSpeedLimitKMH);

                mSpeed1.StrokeDashArray[1] = 3.7f;
            }
            else
            {
                _updateUI_SpeedLimit((float)Utility.Mile2Kilometer(mSpeedLimitMPH));

                mSpeed1.StrokeDashArray[1] = 6.2f;
            }
        }

        public void Interface_SetSpeedLimit(bool isUnit, int speedLimitKMH, int speedLimitMPH)
        {
            mSpeedLimitKMH = speedLimitKMH;
            mSpeedLimitMPH = speedLimitMPH;

            Interface_SetUnit(isUnit);
            //if (mIsUnit)
            //    _updateUI_SpeedLimit(mSpeedLimitKMH);
            //else
            //    _updateUI_SpeedLimit((float)Utility.Mile2Kilometer(mSpeedLimitMPH));
        }


        //************************************************************************************************
        // 為了編輯介面可以拖拉的功能
        //************************************************************************************************
        private void UserControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Utility.DragableGrid_ControlSelected(this);
        }

        public void SetDragColor()
        {
            Interface_SetColor(Utility.DragControl_Color);

            mBackGroundColor.Fill = Utility.DragControl_ColorBrush;
            mBackGroundColor.Stroke = null;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        public void RevertDragColor()
        {
            Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));

            //用成這樣才不會有點不到的現象(透空的地方點不到)
            mBackGroundColor.Fill = new SolidColorBrush(Color.FromArgb(5, 0, 0, 0));
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }
    }
}
