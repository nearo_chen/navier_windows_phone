﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyUtility;
using System.Windows.Media;

namespace SpeedShowControlApp
{
    /// <summary>
    /// 用來固定計算，每次更新GPS定位後，經緯度之間的距離
    /// 放在GPS 的position changed上更新，並且傳給有需要的元件使用(如FB share data, MovedDistanceControl)
    /// </summary>
    public class MovedDistanceCalculator
    {
        double mLatOld = 0;
        double mLonOld = 0;
     
        /// <summary>
        /// 計算與剛剛位移多少
        /// </summary>
        public double CalculateMoveDistanceMeter(double lat, double lon)
        {
            if (mLatOld == 0 || mLonOld == 0)//只要有一個等於0表示沒有舊值可參考
            {
                mLatOld = lat;
                mLonOld = lon;
                return 0;
            }

            //if (acuracyMeter > 70)//精確度太低就不計算已經移動ㄉ距離ㄌ
            //     return;

            //double latTmp = lat - mLatOld;
            // double lonTmp = lon - mLonOld;

            double offset = Utility.LonLat2Meter(mLonOld, mLatOld, lon, lat);
            mLatOld = lat;
            mLonOld = lon;

            //double offset = latTmp * latTmp + lonTmp * lonTmp;
            //offset = Math.Sqrt(offset);
            //mMovedDistanceMeterCount += Utility.LonLat2Kilometer(offset) * 1000;
           // mMovedDistanceMeterCount += offset;

            return offset;
        }
    }

    public partial class MovedDistanceControl : UserControl, ISetUnit, ISetColor, ISetAcuracy, ISetDragColor
    {
        //double mLatOld = 0;
        //double mLonOld = 0;
        bool mIsUnit = true;
        double mMovedDistanceMeterCount = 9999;

        public static MovedDistanceControl AddToRoot(System.Windows.Controls.Panel layoutRoot, int viewPosX, int viewPosY,
            int w, int h)
        {
            MovedDistanceControl movedDistanceControl = new MovedDistanceControl();
            layoutRoot.Children.Add(movedDistanceControl);

            //移動control座標
            Utility.SetControlPosition(movedDistanceControl, viewPosX, viewPosY, true);
            Utility.SetControlWH(movedDistanceControl, w, h, true);
            return movedDistanceControl;
        }

        public MovedDistanceControl()
        {
            InitializeComponent();

            Width = 120;
            Height = 60;
        }

        public void init()
        {
            mMovedDistanceMeterCount = 0;
            _SetDisValue();
        }

        public void Interface_SetUnit(bool isUnit)
        {
            mIsUnit = isUnit;

            _SetDisValue();
        }

        public void Interface_SetColor(System.Windows.Media.Color color)
        {
            //float alpha = (float)mDistance.Opacity * 255f;
            mDistance.Foreground = //new SolidColorBrush(Color.FromArgb((byte)alpha, color.R, color.G, color.B));
            mUnit.Foreground = mMovedDis.Foreground = new SolidColorBrush(color);//Color.FromArgb(255, color.R, color.G, color.B));
        }

        public void Interface_SetGPSAcuracy(double acuracyMeter, float viewRangeInSpeedLonLat, float base2DViewWidth,
            double lat, double lon, double movedDistanceMeter)
        {
            // if (mLatOld == 0 || mLonOld == 0)//只要有一個等於0表示沒有舊值可參考
            // {
            //     mMovedDistanceMeterCount = 0;
            //     mLatOld = lat;
            //     mLonOld = lon;
            //     _SetDisValue();
            //     return;
            // }

            // //if (acuracyMeter > 70)//精確度太低就不計算已經移動ㄉ距離ㄌ
            ////     return;

            // //double latTmp = lat - mLatOld;
            //// double lonTmp = lon - mLonOld;

            // double offset = Utility.LonLat2Meter(mLonOld, mLatOld, lon, lat);
            // mLatOld = lat;
            // mLonOld = lon;

            // //double offset = latTmp * latTmp + lonTmp * lonTmp;
            // //offset = Math.Sqrt(offset);
            // //mMovedDistanceMeterCount += Utility.LonLat2Kilometer(offset) * 1000;
            // mMovedDistanceMeterCount += offset;

            // _SetDisValue();

            mMovedDistanceMeterCount += movedDistanceMeter;
            _SetDisValue();
        }

        void _SetDisValue()
        {
            if (mIsUnit)
            {
                mDistance.Text = "DISTANCE(METER)";
                mMovedDis.Text = mMovedDistanceMeterCount.ToString("F0");
                mUnit.Text = " m";
            }
            else
            {
                mDistance.Text = "DISTANCE(YARD)";
                double yard = Utility.Meter2Yard(mMovedDistanceMeterCount);// mMovedDistanceMeterCount * 10000f / 9144f;
                mMovedDis.Text = yard.ToString("F0");
                mUnit.Text = " yd";
            }
        }

        //************************************************************************************************
        // 為了編輯介面可以拖拉的功能
        //************************************************************************************************
        private void UserControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Utility.DragableGrid_ControlSelected(this);
        }

        public void SetDragColor()
        {
            Interface_SetColor(Utility.DragControl_Color);

            mBackGroundColor.Fill = Utility.DragControl_ColorBrush;
            mBackGroundColor.Stroke = null;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        public void RevertDragColor()
        {
            Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));

            //用成這樣才不會有點不到的現象(透空的地方點不到)
            mBackGroundColor.Fill = new SolidColorBrush(Color.FromArgb(5, 0, 0, 0));
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

    }
}
