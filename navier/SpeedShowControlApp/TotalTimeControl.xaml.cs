﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using MyUtility;
using System.Text;
using System.Windows.Media;

namespace SpeedShowControlApp
{
    public partial class TotalTimeControl : UserControl, ISetColor, ISetDragColor
    {
        //每秒更新一次
        private DispatcherTimer _timer = new DispatcherTimer();

        StringBuilder mStringBuilder = new StringBuilder();
        DateTime mDataTimeStart;

        public static TotalTimeControl AddToRoot(System.Windows.Controls.Panel layoutRoot, int viewPosX, int viewPosY,
           int w, int h)
        {
            TotalTimeControl totalTimeControl = new TotalTimeControl();
            layoutRoot.Children.Add(totalTimeControl);

            //移動control座標
            Utility.SetControlPosition(totalTimeControl, viewPosX, viewPosY, true);
            Utility.SetControlWH(totalTimeControl, w, h, true);
            return totalTimeControl;
        }

        public TotalTimeControl()
        {
            InitializeComponent();

            Width = 120;
            Height = 60;
            _timer.Interval = TimeSpan.FromMilliseconds(1000);
            _timer.Tick += Timer_Tick;
            _timer.Start();

            mDataTimeStart = DateTime.Now;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            //http://www.ezineasp.net/post/C-DateTime-Format-String.aspx 這邊有教

            mStringBuilder.Clear();
            TimeSpan dataTimeOffset = DateTime.Now.Subtract(mDataTimeStart);
            int hour = dataTimeOffset.Hours;
            if (hour > 0)
            {
                mStringBuilder.AppendFormat("{0}h {1}'", hour, dataTimeOffset.Minutes);
                mTotalTimeValue.Text = mStringBuilder.ToString();
            }
            else
            {
                //mTotalTimeValue.Text = dataTimeOffset.Minutes + "' " + dataTimeOffset.Seconds + "\"";

                if (dataTimeOffset.Minutes > 0)
                    mStringBuilder.AppendFormat("{0}' {1}\"", dataTimeOffset.Minutes, dataTimeOffset.Seconds);
                else
                    mStringBuilder.AppendFormat("{0}\"", dataTimeOffset.Seconds);
                mTotalTimeValue.Text = mStringBuilder.ToString();
            }
        }

        public void Interface_SetColor(System.Windows.Media.Color color)
        {
            mTotalTimeTitle.Foreground = mTotalTimeValue.Foreground =
                new SolidColorBrush(color);
        }

        //************************************************************************************************
        // 為了編輯介面可以拖拉的功能
        //************************************************************************************************
        private void UserControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Utility.DragableGrid_ControlSelected(this);
        }

        public void SetDragColor()
        {
            Interface_SetColor(Utility.DragControl_Color);

            mBackGroundColor.Fill = Utility.DragControl_ColorBrush;
            mBackGroundColor.Stroke = null;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        public void RevertDragColor()
        {
            Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));

            //用成這樣才不會有點不到的現象(透空的地方點不到)
            mBackGroundColor.Fill = new SolidColorBrush(Color.FromArgb(5, 0, 0, 0));
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }
    }
}
