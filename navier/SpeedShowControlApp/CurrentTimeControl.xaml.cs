﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using MyUtility;
using System.Windows.Media;

namespace SpeedShowControlApp
{
    public partial class CurrentTimeControl : UserControl, ISetColor, ITime24HR, ISetDragColor
    {
        //每秒更新一次
        private DispatcherTimer _timer = new DispatcherTimer();

        public static CurrentTimeControl AddToRoot(System.Windows.Controls.Panel layoutRoot, int viewPosX, int viewPosY,
           int w, int h)
        {
            CurrentTimeControl currentTimeControl = new CurrentTimeControl();
            layoutRoot.Children.Add(currentTimeControl);

            //移動control座標
            Utility.SetControlPosition(currentTimeControl, viewPosX, viewPosY, true);
            Utility.SetControlWH(currentTimeControl, w, h, true);
            return currentTimeControl;
        }

        public CurrentTimeControl()
        {
            InitializeComponent();
            Width = 120;
            Height = 60;

            _timer.Interval = TimeSpan.FromMilliseconds(1000);
            _timer.Tick += Timer_Tick;
            _timer.Start();
        }

        ~CurrentTimeControl()
        {
         
        }


        private void Timer_Tick(object sender, EventArgs e)
        {
            if (mCURTimeSplit.Visibility == System.Windows.Visibility.Visible)
                mCURTimeSplit.Visibility = System.Windows.Visibility.Collapsed;
            else
                mCURTimeSplit.Visibility = System.Windows.Visibility.Visible;

       　　 //http://www.ezineasp.net/post/C-DateTime-Format-String.aspx 這邊有教

            _refreshTime();
        }

        private void _refreshTime()
        {
            if (mIs24HR)
            {
                string ttt = DateTime.Now.ToString("tt");
                mCURTimeTitle.Text = "CUR. TIME";
                mCURTimeHOUR.Text = DateTime.Now.ToString("HH");
                mCURTimeMinute.Text = DateTime.Now.ToString("mm");
            }
            else
            {
                string ttt = DateTime.Now.ToString("tt");
                mCURTimeTitle.Text = "CUR. TIME(" + ttt + ")";
                mCURTimeHOUR.Text = DateTime.Now.ToString("hh");
                mCURTimeMinute.Text = DateTime.Now.ToString("mm");
            }
        }

        public void Interface_SetColor(System.Windows.Media.Color color)
        {
            mCURTimeTitle.Foreground = mCURTimeHOUR.Foreground = mCURTimeSplit.Foreground = mCURTimeMinute.Foreground
                = new SolidColorBrush(color);
        }

        bool mIs24HR = true;
        public void Interface_SetTimeFormat(bool is24HR)
        {
            mIs24HR = is24HR;

            _refreshTime();
        }

        //************************************************************************************************
        // 為了編輯介面可以拖拉的功能
        //************************************************************************************************
        private void UserControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Utility.DragableGrid_ControlSelected(this);
        }

        public void SetDragColor()
        {
            Interface_SetColor(Utility.DragControl_Color);

            mBackGroundColor.Fill = Utility.DragControl_ColorBrush;
            mBackGroundColor.Stroke = null;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        public void RevertDragColor()
        {
            Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));

            //用成這樣才不會有點不到的現象(透空的地方點不到)
            mBackGroundColor.Fill = new SolidColorBrush(Color.FromArgb(5, 0, 0, 0));
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }
    }
}
