﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyUtility;
using System.Windows.Media;

namespace SpeedShowControlApp
{
    public partial class SpeedNumberBigControl : UserControl, ISetSpeed, ISetColor, ISetUnit, ISetSpeedLimit, ISetDragColor
    {
        public static SpeedNumberBigControl AddToRoot(System.Windows.Controls.Panel layoutRoot, int viewPosX, int viewPosY,
          int w, int h)
        {
            SpeedNumberBigControl speedNumberBigControl = new SpeedNumberBigControl();
            layoutRoot.Children.Add(speedNumberBigControl);

            //移動control座標
            Utility.SetControlPosition(speedNumberBigControl, viewPosX, viewPosY, true);
            Utility.SetControlWH(speedNumberBigControl, w, h, true);
            return speedNumberBigControl;
        }

        public SpeedNumberBigControl()
        {
            InitializeComponent();

            Width = 160; Height = 120;
        }


        public void Interface_SetColor(System.Windows.Media.Color color)
        {
            //超速警示時要維持警告色，先把新色彩設定在記錄色裡面
            if (mReushREC == null)
                mSpeedNumber.Foreground = new SolidColorBrush(color);
            else
                mReushREC = new SolidColorBrush(color);
        }

        Brush mReushREC = null;
        double mSpeedKMHR;
        public void Interface_SetSpeedKMHR(double speedKMHR, int nowMovedDistanceMeter, int totalDistanceMeter,
            double speedAVG_KMHR,
            double remainDistanceToNextTurnPoint)
        {
            mSpeedKMHR = speedKMHR;
            _setData();

            _considerColor();
        }

        private void _considerColor()
        {
            //超過速限讓字變色
            if (
                   (mIsUnit && (mSpeedKMHR > mSpeedLimitKMH))
                || (mIsUnit == false && (mSpeedKMHR > Utility.Mile2Kilometer(mSpeedLimitMPH)))
                )
            {
                if (mReushREC == null)
                {
                    mReushREC = mSpeedNumber.Foreground;
                    mSpeedNumber.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));

                    Utility.PlaySound("Assets/music/cowbell.wav");
                }
            }
            else
            {
                if (mReushREC != null)
                {
                    mSpeedNumber.Foreground = mReushREC;
                    mReushREC = null;
                }
            }
        }

        bool mIsUnit = true;
        public void Interface_SetUnit(bool isUnit)
        {
            mIsUnit = isUnit;
            _setData();
        }

        private void _setData()
        {
            if (mIsUnit)
            {
                mSpeedNumber.Text = mSpeedKMHR.ToString("0");
            }
            else
            {
                mSpeedNumber.Text = Utility.Meter2Mile(mSpeedKMHR * 1000f).ToString("0");
            }
        }

        int mSpeedLimitKMH;
        int mSpeedLimitMPH;
        public void Interface_SetSpeedLimit(bool isUnit, int speedLimitKMH, int speedLimitMPH)
        {
            mSpeedLimitKMH = speedLimitKMH;
            mSpeedLimitMPH = speedLimitMPH;

            Interface_SetUnit(isUnit);

            _considerColor();
        }

        //************************************************************************************************
        // 為了編輯介面可以拖拉的功能
        //************************************************************************************************
        private void UserControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Utility.DragableGrid_ControlSelected(this);
        }

        public void SetDragColor()
        {
            Interface_SetColor(Utility.DragControl_Color);

            mBackGroundColor.Fill = Utility.DragControl_ColorBrush;
            mBackGroundColor.Stroke = null;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        public void RevertDragColor()
        {
            Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));

            //用成這樣才不會有點不到的現象(透空的地方點不到)
            mBackGroundColor.Fill = new SolidColorBrush(Color.FromArgb(5, 0, 0, 0));
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }
    }
}
