﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyUtility;
using System.Windows.Media;

namespace SpeedShowControlApp
{
    public partial class GPSAccuracyControl : UserControl ,  ISetColor , ISetAcuracy, ISetDragColor
    {
        public static GPSAccuracyControl AddToRoot(System.Windows.Controls.Panel layoutRoot, int viewPosX, int viewPosY,
           int w, int h)
        {
            GPSAccuracyControl gpsaccuracyControl = new GPSAccuracyControl();
            layoutRoot.Children.Add(gpsaccuracyControl);

            //移動control座標
            Utility.SetControlPosition(gpsaccuracyControl, viewPosX, viewPosY, true);
            Utility.SetControlWH(gpsaccuracyControl, w, h, true);
            return gpsaccuracyControl;
        }

        public GPSAccuracyControl()
        {
            InitializeComponent();

            Width = 120;
            Height = 60;
        }

        public void Interface_SetGPSAcuracy(double acuracyMeter, float viewRangeInSpeedLonLat, float base2DViewWidth, double lat, double lon,
            double movedDisMeter)
        {
            mGPSAccuracyMeter.Text = acuracyMeter.ToString("F1") + " m";
        }

        public void Interface_SetColor(System.Windows.Media.Color color)
        {
            //float alpha = (float)mGPSAccuracyTitle.Opacity * 255f;
            mGPSAccuracyTitle.Foreground = //new SolidColorBrush(Color.FromArgb((byte)alpha, color.R, color.G, color.B));
            mGPSAccuracyMeter.Foreground = new SolidColorBrush(color);//Color.FromArgb(255, color.R, color.G, color.B));
        }

        //************************************************************************************************
        // 為了編輯介面可以拖拉的功能
        //************************************************************************************************
        private void UserControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Utility.DragableGrid_ControlSelected(this);
        }

        public void SetDragColor()
        {
            Interface_SetColor(Utility.DragControl_Color);

            mBackGroundColor.Fill = Utility.DragControl_ColorBrush;
            mBackGroundColor.Stroke = null;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        public void RevertDragColor()
        {
            Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));

            //用成這樣才不會有點不到的現象(透空的地方點不到)
            mBackGroundColor.Fill = new SolidColorBrush(Color.FromArgb(5, 0, 0, 0));
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }
    }
}
