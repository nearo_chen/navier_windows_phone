﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyUtility;
using System.Windows.Media;

namespace SpeedShowControlApp
{
    /// <summary>
    /// 計算GPS回傳的時速將它做平均，在GPS的position changed做更新
    /// </summary>
    public class AverageSpeedCalculator
    {
        double mSpeedAVG_KMHR = 0;
        double mCountNumber = 0;

        public double GetSpeedAVG_KMHR() { return mSpeedAVG_KMHR; }

        public double CalculateAVGSpeed_KMHR(double speedKMHR)
        {
            double total = (mSpeedAVG_KMHR * mCountNumber) + speedKMHR;
            mCountNumber++;
            mSpeedAVG_KMHR = total / mCountNumber;
            return mSpeedAVG_KMHR;
        }
    }

    public partial class AverageSpeedControl : UserControl, ISetUnit, ISetColor, ISetSpeed, ISetDragColor
    {
        bool mIsUnit = true;
        double mSpeedAVG_KMHR = 1234;
        //double mCountNumber;

        public static AverageSpeedControl AddToRoot(System.Windows.Controls.Panel layoutRoot, int viewPosX, int viewPosY,
            int w, int h)
        {
            AverageSpeedControl averageSpeedControl = new AverageSpeedControl();
            layoutRoot.Children.Add(averageSpeedControl);

            //移動control座標
            Utility.SetControlPosition(averageSpeedControl, viewPosX, viewPosY, true);
            Utility.SetControlWH(averageSpeedControl, w, h, true);
            return averageSpeedControl;
        }

        public AverageSpeedControl()
        {
            InitializeComponent();

            Width = 120;
            Height = 60;
        }

        public void init()
        {
            mSpeedAVG_KMHR = 0;
            //mCountNumber = 0;
        }

        public void Interface_SetUnit(bool isUnit)
        {
            mIsUnit = isUnit;

            _SetAVGValue();
        }

        public void Interface_SetColor(System.Windows.Media.Color color)
        {
            // float alpha = (float)mAVGTitle.Opacity * 255f;
            mAVGTitle.Foreground = //new SolidColorBrush(Color.FromArgb((byte)alpha, color.R, color.G, color.B));
            mAVGValue.Foreground = new SolidColorBrush(color);//Color.FromArgb(255, color.R, color.G, color.B));
        }

        void _SetAVGValue()
        {
            if (mIsUnit)
            {
                mAVGTitle.Text = "AVG.SPEED(KM/H)";
                mAVGValue.Text = mSpeedAVG_KMHR.ToString("F1");
            }
            else
            {
                //http://www.tlri.gov.tw/Term/Symbole.htm 轉換表這邊有

                mAVGTitle.Text = "AVG.SPEED(MPH)";
                double meterHR = mSpeedAVG_KMHR * 1000f;
                double yard_HR = meterHR * 10000f / 9144f;
                mAVGValue.Text = (yard_HR / 1760f).ToString("F1");
            }
        }

        public void Interface_SetSpeedKMHR(double speedKMHR, int nowMovedDistanceMeter, int totalDistanceMeter,
                                           double speedAVGKMHR,
                                            double remainDistanceToNextTurnPoint)
        {
            //double total = (mSpeedAVGMeter * mCountNumber) + (speedKMHR * 1000f);
            //mCountNumber++;
            //mSpeedAVGMeter = total / mCountNumber;

            mSpeedAVG_KMHR = speedAVGKMHR;
            _SetAVGValue();
        }

        //************************************************************************************************
        // 為了編輯介面可以拖拉的功能
        //************************************************************************************************
        private void UserControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Utility.DragableGrid_ControlSelected(this);
        }

        public void SetDragColor()
        {
            Interface_SetColor(Utility.DragControl_Color);

            mBackGroundColor.Fill = Utility.DragControl_ColorBrush;
            mBackGroundColor.Stroke = null;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        public void RevertDragColor()
        {
            Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));

            //用成這樣才不會有點不到的現象(透空的地方點不到)
            mBackGroundColor.Fill = new SolidColorBrush(Color.FromArgb(5, 0, 0, 0));
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }
    }
}
