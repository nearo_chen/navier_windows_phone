﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SpeedShowControlApp.Resources;

namespace SpeedShowControlApp
{
    public partial class SpeedShowMainPage : PhoneApplicationPage
    {
        // Constructor
        public SpeedShowMainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

            
        }

        

        double way = 1f;
        double mNowSpeedKM = 0;
        private async void Button_Click_SetSpeed(object sender, RoutedEventArgs e)
        {
            mNowSpeedKM += 10f * way;
            if (mNowSpeedKM >= 160 || mNowSpeedKM <= 0)
                way *= -1f;

            mSpeedShowControl.Interface_SetSpeedKMHR(mNowSpeedKM, 0, 0, 0, 0);
        
            mSpeedNumberControl.Interface_SetSpeedKMHR(mNowSpeedKM, 0, 0, 0,0);
          
          //  MyUtility.Utility.SetControlWH(mSpeedShowControl, 300, 300, false);


           
        
        }

        bool mIsUnit = true;
        private void Button_Click_SwitchUnit(object sender, RoutedEventArgs e)
        {
            mIsUnit = !mIsUnit;
            MyUtility.Utility.SetHUDUnit(LayoutRoot, mIsUnit);
        }

      
    }
}