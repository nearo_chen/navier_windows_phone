﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyUtility;
using System.Windows.Media;

namespace SpeedShowControlApp
{
    public partial class SpeedNumberSmallControl : UserControl, ISetSpeed, ISetColor, ISetUnit, ISetSpeedLimit, ISetDragColor
    {
        public static SpeedNumberSmallControl AddToRoot(System.Windows.Controls.Panel layoutRoot, int viewPosX, int viewPosY,
            int w, int h)
        {
            SpeedNumberSmallControl speedNumberSmallControl = new SpeedNumberSmallControl();
            layoutRoot.Children.Add(speedNumberSmallControl);

            //移動control座標
            Utility.SetControlPosition(speedNumberSmallControl, viewPosX, viewPosY, true);
            // Point wh = Utility.PercentToResulution(WRatio, HRatio);
            Utility.SetControlWH(speedNumberSmallControl, w, h, true);
            //Utility.SetControlScale(speedNumberSmallControl, 0.5f, 0.5f, true);
            return speedNumberSmallControl;
        }

        //public static void RemoveFromRoot(SpeedNumberSmallControl speedNumberSmallControl)
        //{
        //    System.Windows.Controls.Panel layoutRoot = speedNumberSmallControl.Parent as System.Windows.Controls.Panel;
        //    layoutRoot.Children.Remove(speedNumberSmallControl);
        //}

        public SpeedNumberSmallControl()
        {
            InitializeComponent();

            Width = 120;
            Height = 60;
        }

        public void Interface_SetColor(Color color)
        {
            mSpeedTitle.Foreground = new SolidColorBrush(color);

            //超速警示時要維持警告色，先把新色彩設定在記錄色裡面
            if (mReushREC == null)
                mSpeedNumber.Foreground = new SolidColorBrush(color);
            else
                mReushREC = new SolidColorBrush(color);
        }

        Brush mReushREC = null;
        double mSpeedKMHR;
        public void Interface_SetSpeedKMHR(double speedKMHR, int nowMovedDistanceMeter, int totalDistanceMeter,
            double speedAVG_KMHR,
            double remainDistanceToNextTurnPoint)
        {
            mSpeedKMHR = speedKMHR;

            _setData();

            _considerColor();
        }

        private void _considerColor()
        {
            //超過速限讓字變色
            if (
                   (mIsUnit && (mSpeedKMHR > mSpeedLimitKMH))
                || (mIsUnit == false && (mSpeedKMHR > Utility.Mile2Kilometer(mSpeedLimitMPH)))
                )
            {
                if (mReushREC == null)
                {
                    mReushREC = mSpeedNumber.Foreground;
                    mSpeedNumber.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));

                    Utility.PlaySound("Assets/music/cowbell.wav");
                }
            }
            else
            {
                if (mReushREC != null)
                {
                    mSpeedNumber.Foreground = mReushREC;
                    mReushREC = null;
                }
            }
        }

        bool mIsUnit = true;
        public void Interface_SetUnit(bool isUnit)
        {
            mIsUnit = isUnit;

            if (mIsUnit)
                mSpeedTitle.Text = "CUR. SPEED (KM/H)";
            else
                mSpeedTitle.Text = "CUR. SPEED (MPH)";

            _setData();
        }

        private void _setData()
        {
            if (mIsUnit)
            {
                mSpeedNumber.Text = mSpeedKMHR.ToString("000");
            }
            else
            {
                mSpeedNumber.Text = Utility.Meter2Mile(mSpeedKMHR * 1000f).ToString("000");
            }
        }

        int mSpeedLimitKMH;
        int mSpeedLimitMPH;
        public void Interface_SetSpeedLimit(bool isUnit, int speedLimitKMH, int speedLimitMPH)
        {
            mSpeedLimitKMH = speedLimitKMH;
            mSpeedLimitMPH = speedLimitMPH;

            Interface_SetUnit(isUnit);

            _considerColor();
        }

        //************************************************************************************************
        // 為了編輯介面可以拖拉的功能
        //************************************************************************************************
        private void UserControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Utility.DragableGrid_ControlSelected(this);
        }

        public void SetDragColor()
        {
            Interface_SetColor(Utility.DragControl_Color);

            mBackGroundColor.Fill = Utility.DragControl_ColorBrush;
            mBackGroundColor.Stroke = null;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        public void RevertDragColor()
        {
            Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));

            //用成這樣才不會有點不到的現象(透空的地方點不到)
            mBackGroundColor.Fill = new SolidColorBrush(Color.FromArgb(5, 0, 0, 0));
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }
    }
}
