﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BingTranslateApp.Resources;
using System.Collections.ObjectModel;
using BingTranslateApp.Services;

namespace BingTranslateApp
{
    public partial class MainPage : PhoneApplicationPage
    {
        TranslationService _translator;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            _translator = new TranslationService();
            _translator.TranslationComplete += _translator_TranslationComplete;
            _translator.TranslationFailed += _translator_TranslationFailed;
        }

        void _translator_TranslationComplete(object sender, TranslationCompleteEventArgs e)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                Textbox2.Text = e.Translation;
            });
        }

        void _translator_TranslationFailed(object sender, TranslationFailedEventArgs e)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                MessageBox.Show("Bummer, the translation failed. \n " + e.ErrorDescription);
            });
        }

        private void click_event(object sender, RoutedEventArgs e)
        {
            _translator.GetTranslation(Textbox1.Text, "en", "es");
        }

    }
}