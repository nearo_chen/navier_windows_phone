﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ResourceApp.Resources;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MyUtility;
using System.IO.IsolatedStorage;
using System.Collections.ObjectModel;
using ComponentEditorApp;
using System.Windows.Controls.Primitives;
using Microsoft.Phone.Tasks;

namespace NavierHUD
{
    public partial class MainPage : PhoneApplicationPage
    {
        //紀錄目前的popup up window是哪一個，原則是，同一時間只能有一個popup window
        Popup mNowPopup = null;

        void ShowAppBar()
        {
            /*
             * ApplicationBar - 应用程序栏
             *     Mode - 应用程序栏的样式。ApplicationBarMode.Default：默认（显示图标）；ApplicationBarMode.Minimized：最小化（右侧有3个圆点，用于提示单击可弹出 ApBar）
             *     IsVisible - 是否显示 ApBar
             *     IsMenuEnabled - 是否显示 ApBar 的 MenuItem
             *     BackgroundColor - AppBar 的背景色
             *     ForegroundColor - AppBar 的前景色
             *
             * ApplicationBarIconButton - ApBar 的 IconButon
             *     Buttons - IconButon 的集合
             *     IconUri - 按钮图标的地址
             *     Text - 用于描述按钮功能的文本
             *     Click - 单击按钮后所触发的事件
             *
             * ApplicationBarMenuItem - ApBar 的 MenuItem
             *     MenuItems - MenuItem 的集合
             *     Text - 菜单项的文本
             *     Click - 单击菜单项后所触发的事件
             */

            if (ApplicationBar == null)
            {
                ApplicationBar = new ApplicationBar();
                //ApplicationBar.StateChanged += OnApplicationBarStateChanged;
                ApplicationBar.Mode = ApplicationBarMode.Default;
                ApplicationBar.Opacity = 0.5;
                ApplicationBar.IsVisible = true;
                ApplicationBar.IsMenuEnabled = true;

                ApplicationBarIconButton btn = new ApplicationBarIconButton();
                btn.IconUri = new Uri("/Assets/AppbarIcon/About.png", UriKind.Relative);
                btn.Text = AppResources.Main_ABOUT;
                btn.Click += About_EventHandler;
                ApplicationBar.Buttons.Add(btn);

                btn = new ApplicationBarIconButton();
                btn.IconUri = new Uri("/Assets/AppbarIcon/Settings.png", UriKind.Relative);
                btn.Text = AppResources.Main_SETTINGS;
                btn.Click += Setting_EventHandler;
                ApplicationBar.Buttons.Add(btn);

                btn = new ApplicationBarIconButton(new Uri("/Assets/appbarIcon/DownloadMap.png", UriKind.Relative));
                btn.Text =  AppResources.GOALBAR_MapDownloader;
                btn.Click += MapDownload_EventHandler;
                ApplicationBar.Buttons.Add(btn);

                ApplicationBarMenuItem item1 = new ApplicationBarMenuItem();
                item1.Text = ResourceApp.Resources.AppResources.LocationPrivacyStatement;
                item1.Click += Privacy_EventHandler;
                ApplicationBar.MenuItems.Add(item1);

                //ApplicationBarMenuItem item2 = new ApplicationBarMenuItem();
                //item2.Text = AppResources.Main_ABOUT;
                //item1.Click += About_EventHandler;
                //ApplicationBar.MenuItems.Add(item2);
            }
            ApplicationBar.IsVisible = true;

            Utility.SetPanelPositionADD(mAddPartButton, ApplicationBar.DefaultSize, -40, true);
        }

        private void MapDownload_EventHandler(object sender, EventArgs e)
        {
            try
            {
                MapDownloaderTask map = new MapDownloaderTask();
                map.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Privacy_EventHandler(object sender, EventArgs e)
        {
            ShowConfirmPrivacy();
        }

        /// <summary>
        /// 設定目前popup control是哪個，原則上同一時間只能有一個popup
        /// (主要是管理popup window的開開關關)
        /// </summary>
        /// <param name="popup"></param>
        void SetPopupWindow(Popup popup)
        {
            ClosePopupWindow();
            mNowPopup = popup;
        }

        bool ClosePopupWindow()
        {
            if (mNowPopup != null && mNowPopup.IsOpen == true)
            {
                //離開頁面前將設定存檔
                if (mNowPopup.Child.GetType() == typeof(SettingsControl))
                    NavierHUD.HUD.SaveUserSetting.SaveSettingData(mUserSetting);

                mNowPopup.IsOpen = false;
                mNowPopup = null;
                return true;
            }
            return false;
        }

        void Setting_EventHandler(object sender, EventArgs e)
        {
            SettingsControl settingsControl = new SettingsControl(mUserSetting);
            double hostWidth = Utility.GetDeviceWidth(true) - ApplicationBar.DefaultSize;
            double hostHeight = Utility.GetDeviceHeight(true);
            settingsControl.Width = hostWidth;
            settingsControl.Height = hostHeight;

            // 因為landscape所以X跟Y反過來
            Popup popup = settingsControl.Create(
                (hostHeight - hostWidth) * 0.5f,
                (hostWidth - hostHeight) * 0.5f);
            
            SetPopupWindow(popup);
        }


        void About_EventHandler(object sender, EventArgs e)
        {
            ClosePopupWindow();
            Utility.ShowAbout("Navier HUD", "v1.0");
        }



        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ClosePopupWindow())
            {


                e.Cancel = true;
            }
        }
    }
}