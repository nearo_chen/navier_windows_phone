﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BatteryApp1.Resources;
using Windows.Phone.Devices.Power;

namespace BatteryApp1
{
    public partial class MainPage : PhoneApplicationPage
    {
        readonly Battery _battery;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            //获取当前设备电源对象,注意：不需要创建对象实体
            _battery = Battery.GetDefault();

            _battery.RemainingChargePercentChanged += _battery_RemainingChargePercentChanged;
            this.tblBatteryChargePercent.Text = string.Format("{0} %", _battery.RemainingChargePercent);

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        //电源百分比变化时，及时更新UI
        void _battery_RemainingChargePercentChanged(object sender, object e)
        {
            UpdateUI();
        }

        void UpdateUI()
        {
            System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                this.tblBatteryChargePercent.Text = string.Format("{0} %", _battery.RemainingChargePercent);
                //显示剩余电量使用时间,注意：RenainingDischargeTime包含多种格式的时间显示方式,可自行取值。
                this.tblBatteryDisplayTime.Text = string.Format("{0} 分钟", _battery.RemainingDischargeTime.TotalMinutes);
            });
        }
    }
}