﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MyUtility;
using System.IO;

namespace RouteInstructorApp
{
    public partial class NavigationProgressControl : UserControl, ISetColor, ISetSpeed, ISetUnit, ISetDragColor
    {
        public static NavigationProgressControl AddToRoot(System.Windows.Controls.Panel layoutRoot, int viewPosX, int viewPosY,
          int w, int h)
        {
            NavigationProgressControl navigationProgressControl = new NavigationProgressControl();
            layoutRoot.Children.Add(navigationProgressControl);

            //移動control座標
            Utility.SetControlPosition(navigationProgressControl, viewPosX, viewPosY, true);
            Utility.SetControlWH(navigationProgressControl, w, h, true);
            return navigationProgressControl;
        }

        public NavigationProgressControl()
        {
            InitializeComponent();

            Width = 45;
            Height = 180;
        }

        bool mMyVehicleIsCar = true;//預設XAML裡面就設定CAR的圖了，所以設定TRUE
        public void SetMyVehicle(bool isCar)
        {
            if (isCar && mMyVehicleIsCar == false)
            {
                BitmapImage bitmap = new BitmapImage(new Uri(@"/Assets/NaviProgress/Car.png", UriKind.Relative));
                ImageBrush brush = new ImageBrush();
                brush.ImageSource = (ImageSource)bitmap;
                this.mMyVehicle.OpacityMask = brush;
                mMyVehicleIsCar = true;
            }
            else if (isCar == false && mMyVehicleIsCar == true)
            {
                BitmapImage bitmap = new BitmapImage(new Uri(@"/Assets/NaviProgress/Cycle.png", UriKind.Relative));
                ImageBrush brush = new ImageBrush();
                brush.ImageSource = (ImageSource)bitmap;
                this.mMyVehicle.OpacityMask = brush;
                mMyVehicleIsCar = false;
            }
        }

        int mTotalDistanceMeter;

        /// <summary>
        /// 傳入目前已走距離，還有整個路線的距離
        /// </summary>
        /// <param name="progressPercent"></param>
        public void SetProgress(int movedDistanceMeter, int totalDistanceMeter)
        {
            mTotalDistanceMeter = totalDistanceMeter;

            float progressRatio = 0;
            if (totalDistanceMeter != 0)
                progressRatio = (float)movedDistanceMeter / (float)totalDistanceMeter;
            progressRatio *= 640f;

            if (progressRatio <= 80)//目前進度要是小於vehicle的圖高，就不用動了，等到進度有大於圖高，再開始跑動
                progressRatio = 0;

            Thickness tk = this.mMyVehicle.Margin;
            tk.Top = 640f - progressRatio;
            this.mMyVehicle.SetValue(System.Windows.Controls.Grid.MarginProperty, tk);


            _updateProgressQuaterNumber();
        }

        private void _updateProgressQuaterNumber()
        {
            if (mIsUnit)
            {
                float dis = (float)mTotalDistanceMeter / 1000f * (4f / 4f);
                mProgress4of4.Text = dis.ToString("F1");
                mProgress4of4.FontSize = (mProgress4of4.Text.Length <= 3) ? 46 : 36;

                dis = (float)mTotalDistanceMeter / 1000f * (3f / 4f);
                mProgress3of4.Text = dis.ToString("F1");
                mProgress3of4.FontSize = (mProgress3of4.Text.Length <= 3) ? 46 : 36;

                dis = (float)mTotalDistanceMeter / 1000f * (2f / 4f);
                mProgress2of4.Text = dis.ToString("F1");
                mProgress2of4.FontSize = (mProgress2of4.Text.Length <= 3) ? 46 : 36;

                dis = (float)mTotalDistanceMeter / 1000f * (1f / 4f);
                mProgress1of4.Text = dis.ToString("F1");
                mProgress1of4.FontSize = (mProgress1of4.Text.Length <= 3) ? 46 : 36;
            }
            else//mile
            {
                float disMile = (float)Utility.Meter2Mile(mTotalDistanceMeter);
                float dis = disMile * (4f / 4f);
                mProgress4of4.Text = dis.ToString("F1");
                mProgress4of4.FontSize = (mProgress4of4.Text.Length <= 3) ? 46 : 36;

                dis = disMile * (3f / 4f);
                mProgress3of4.Text = dis.ToString("F1");
                mProgress3of4.FontSize = (mProgress3of4.Text.Length <= 3) ? 46 : 36;

                dis = disMile * (2f / 4f);
                mProgress2of4.Text = dis.ToString("F1");
                mProgress2of4.FontSize = (mProgress2of4.Text.Length <= 3) ? 46 : 36;

                dis = disMile * (1f / 4f);
                mProgress1of4.Text = dis.ToString("F1");
                mProgress1of4.FontSize = (mProgress1of4.Text.Length <= 3) ? 46 : 36;
            }
        }

        public void Interface_SetColor(Color color)
        {

            mBackGroundColor.Visibility = System.Windows.Visibility.Collapsed;

            SolidColorBrush brush = new SolidColorBrush(color);

            int countAlpha = 4;
            foreach (UIElement uie in LayoutRoot.Children)
            {
                Rectangle retangle = uie as Rectangle;
                if (retangle != null)
                {
                    if (retangle.Name.Contains("Progress"))
                    {
                        byte alpha = (byte)(255f * (float)countAlpha / 4f);
                        if (alpha >= 255)
                            alpha = 240;

                        retangle.Opacity = (float)alpha / 255f;

                        retangle.Fill = brush;
                        //Color ccc = Color.FromArgb(alpha, color.R, color.G, color.B);
                        //retangle.Fill = new SolidColorBrush(ccc);

                        countAlpha--;
                    }
                }
                else
                {

                    TextBlock textblock = uie as TextBlock;
                    if (textblock != null &&
                        (textblock.Name.Contains("Progress") || textblock.Name.Contains("mUnit"))//有Progress為分成4份的進度數字
                        )
                        textblock.Foreground = brush;
                }
            }

            if (color.Equals(Utility.GetHUDColor(Utility.SelectColor.color_orangered)))
            {
                mGOAL.Foreground = mMyVehicle.Fill = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            }
            else
            {
                mGOAL.Foreground = mMyVehicle.Fill = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
            }
        }


        private bool mIsUnit;
        public void Interface_SetUnit(bool isKM)
        {
            if (isKM)
                mUnit.Text = "km";
            else
                mUnit.Text = "mile";

            mIsUnit = isKM;
            _updateProgressQuaterNumber();
        }

        public void Interface_SetSpeedKMHR(double speedKMHR, int nowMovedDistanceMeter, int totalDistanceMeter,
            double speedAVG_KMHR,
            double remainDistanceToNextTurnPoint)
        {
            SetProgress(nowMovedDistanceMeter, totalDistanceMeter);
        }

        //************************************************************************************************
        // 為了編輯介面可以換顏色的功能
        //************************************************************************************************
        public void SetDragColor()
        {
            Interface_SetColor(Utility.DragControl_Color);

            mBackGroundColor.Fill = Utility.DragControl_ColorBrush;
            mBackGroundColor.Stroke = null;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        public void RevertDragColor()
        {            
            Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));

            //用成這樣才不會有點不到的現象(透空的地方點不到)
            mBackGroundColor.Fill = new SolidColorBrush(Color.FromArgb(5, 0, 0, 0));
            //mBackGroundColor.Stroke = //Utility.DragControl_ColorBrush;
            //mBackGroundColor.StrokeThickness = 10;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        //************************************************************************************************
        // 為了編輯介面可以拖拉的功能
        //************************************************************************************************
        private void UserControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Utility.DragableGrid_ControlSelected(this);
        }

    }
}
