﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using RouteInstructorApp.Resources;
using System.Windows.Media;
using MyUtility;


namespace RouteInstructorApp
{
    public partial class MainPage : PhoneApplicationPage
    {
        DriveNoticeControl mDriveNoticeControl;
        RouteInstructorView mRouteInstructorView;
        List<Point> myPointCollection3, turnCircleList;
        int nowOnRoute = 0;
        RouteRendererControl mRouteRendererControl;

        float TurnCircleRadius = 0.6f;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            Utility.SetControlWH(mProgress, 100, 400, true);
            


            mRouteInstructorView = myRouteInstructorView;// new RouteInstructorView();
            //ContentPanel.Children.Add(mRouteInstructorView);

            mRouteRendererControl = mRouteInstructorView.GetRouteRendererControl();

            myPointCollection3 = new List<Point>();
            Point Point4 = new Point(41, 41);
            Point Point5 = new Point(42, 42);
            Point Point6 = new Point(40.5, 43);
            Point Point7 = new Point(43.5, 44.9);
            Point Point8 = new Point(41.5, 45.9);
            Point Point9 = new Point(42.5, 46.9);

            myPointCollection3.Add(Point5);
            myPointCollection3.Add(Point4);
            myPointCollection3.Add(Point6);
            myPointCollection3.Add(Point7);
            myPointCollection3.Add(Point8);
            myPointCollection3.Add(Point9);

            myPointCollection3.Add(new Point(41.5, 45.9));
            myPointCollection3.Add(new Point(42.5, 46.9));
            myPointCollection3.Add(new Point(31.5, 37.9));
            myPointCollection3.Add(new Point(39.5, 42.9));
            myPointCollection3.Add(new Point(37.5, 39.9));
            myPointCollection3.Add(new Point(39.5, 36.9));
            myPointCollection3.Add(new Point(38.5, 35));


            turnCircleList = new List<Point>();
            turnCircleList.Add(Point4);
            turnCircleList.Add(Point6);
            turnCircleList.Add(Point9);
            turnCircleList.Add(new Point(39.5, 42.9));
            turnCircleList.Add(new Point(38.5, 35));

            Point globalCentor = myPointCollection3[0];
            Point scaleRange = new Point(60, 60);
            mRouteRendererControl.SetGlobalLines(myPointCollection3);
            mRouteRendererControl.SetTurnCircleList(turnCircleList);
            mRouteRendererControl.SetGlobalCentor_WithOffset(globalCentor, scaleRange, TurnCircleRadius);

            mDriveNoticeControl = new DriveNoticeControl(null);


            LayoutRoot.Children.Add(mDriveNoticeControl);
            
            
        }

#if DEBUG
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            RouteInstructorApp.RouteRendererControl._OnRouteData onRouteData =
               mRouteRendererControl.GetNowIndexOnRoute(
               mRouteRendererControl.GetGlobalCentor_WithoutOffset().X,
               mRouteRendererControl.GetGlobalCentor_WithoutOffset().Y);

            MyUtility.Utility.OutputDebugString("oldIndex :" + onRouteData.oldIndex + " -> newIndex : " + onRouteData.nowIndex + " ; dis: " + onRouteData.drivedDistanceMeter);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Point globalCentor = myPointCollection3[nowOnRoute];
            Point scaleRange = new Point(4, 4);
            mRouteRendererControl.SetGlobalCentor_WithOffset(globalCentor, scaleRange, TurnCircleRadius);


            mRouteRendererControl.RenderDebug_MyWay();

        }

        private void Button_Click_rotate(object sender, RoutedEventArgs e)
        {
            mRouteRendererControl.SetRotationTransform(180);
        }

        private void Button_Click_move_next(object sender, RoutedEventArgs e)
        {
            Point globalCentor = myPointCollection3[nowOnRoute];

            Point scaleRange = new Point(4, 4);

            mRouteRendererControl.SetGlobalCentor_WithOffset(globalCentor, scaleRange, TurnCircleRadius);

            float degree = mRouteRendererControl.GetNowRouteUpDegree(nowOnRoute);
            mRouteRendererControl.SetRotationTransform(degree);


            mRouteRendererControl.RenderDebug_MyWay();

            nowOnRoute++;
        }

      

        float moveSpeed = 0.1f;
        private void Button_Click_UP(object sender, RoutedEventArgs e)
        {
            Point globalCentor = new Point(mRouteRendererControl.GetGlobalCentor_WithoutOffset().X, mRouteRendererControl.GetGlobalCentor_WithoutOffset().Y + moveSpeed);

            mRouteRendererControl.SetGlobalCentor_WithOffset(globalCentor, null, TurnCircleRadius);
            mRouteRendererControl.RenderDebug_MyWay();
        }

        private void Button_Click_DOWN(object sender, RoutedEventArgs e)
        {
            Point globalCentor = new Point(mRouteRendererControl.GetGlobalCentor_WithoutOffset().X, mRouteRendererControl.GetGlobalCentor_WithoutOffset().Y - moveSpeed);

            mRouteRendererControl.SetGlobalCentor_WithOffset(globalCentor, null, TurnCircleRadius);
            mRouteRendererControl.RenderDebug_MyWay();
        }

        private void Button_Click_LEFT(object sender, RoutedEventArgs e)
        {
            Point globalCentor = new Point(mRouteRendererControl.GetGlobalCentor_WithoutOffset().X - moveSpeed, mRouteRendererControl.GetGlobalCentor_WithoutOffset().Y);

            mRouteRendererControl.SetGlobalCentor_WithOffset(globalCentor, null, TurnCircleRadius);
            mRouteRendererControl.RenderDebug_MyWay();
        }

        private void Button_Click_RIGHT(object sender, RoutedEventArgs e)
        {
            Point globalCentor = new Point(mRouteRendererControl.GetGlobalCentor_WithoutOffset().X + moveSpeed, mRouteRendererControl.GetGlobalCentor_WithoutOffset().Y);

            mRouteRendererControl.SetGlobalCentor_WithOffset(globalCentor, null, TurnCircleRadius);
            mRouteRendererControl.RenderDebug_MyWay();
        }
#endif

        public void SetColor(Color color)
        {
            mRouteRendererControl.Interface_SetColor(color);
        }

        private void Button_drive_notice_arrow(object sender, RoutedEventArgs e)
        {
            mDriveNoticeControl.SetNotice(DriveNoticeControl.TurnType.turn, 70, "I wanna sleep  請向左轉請向右轉", 3, true, 1, 1, 14, 6, null);
       //     mDriveNoticeControl.Margin = new Thickness(40, 40, 0, 0);
        }

        private void Button_drive_notice(object sender, RoutedEventArgs e)
        {
            mDriveNoticeControl.SetNotice("I wanna sleep  請向左轉請向右轉", 3, true, 1, 1, 14, 6, null);
        
        }

    }
}