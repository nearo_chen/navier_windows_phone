﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyUtility;
using System.Windows.Media;
using System.Windows.Input;

namespace RouteInstructorApp
{
    public partial class RouteInstructorView : UserControl, ISetColor, ISetSpeed, ISetAcuracy, ISetDragColor, ISetUnit
    {
        public RouteInstructorView()
        {
            InitializeComponent();
            _init(null);
        }

        public RouteInstructorView(FlowDirection? flowDirection)
        {
            InitializeComponent();
            _init(flowDirection);
        }

        private void _init(FlowDirection? flowDirection)
        {
            Width = LayoutRoot.Width;
            Height = LayoutRoot.Height;
            RectangleGeometry clipRetangle = new RectangleGeometry();

            clipRetangle.Rect = new Rect(0, 0, Width, Height);
            LayoutRoot.Clip = clipRetangle;

            mCurrentRouteInstructText.Visibility = System.Windows.Visibility.Collapsed;

            if (flowDirection.HasValue)
            {
                mCurrentRouteInstructText.FlowDirection = flowDirection.Value;
            }
        }

        public RouteRendererControl GetRouteRendererControl()
        {
            return mRouteRenderer;
        }


        public void Interface_SetColor(Color color)
        {
            Brush brush = new SolidColorBrush(color);
            mCurrentRouteInstructText.Foreground = brush;

            mRouteRenderer.Interface_SetColor(color);

            mMyPositionMarker.Interface_SetColor(color);
        }



        /// <summary>
        /// 設定GPS精確度的範圍(公尺)
        /// baseWidth2DView 是給一個目前的VIEW寬，好讓精確度的半徑可以對應到VIEW的實際位置上
        /// </summary>
        public void Interface_SetGPSAcuracy(double acuracyMeter, float viewRangeInSpeedLonLat, float base2DViewWidth, double lat, double lon,
            double movedDisMeter)
        {
            float acuracyRadiusLonLat = (float)Utility.Kilometer2LonLat(acuracyMeter / 1000f);
            float ratio = acuracyRadiusLonLat / viewRangeInSpeedLonLat;
            acuracyRadiusLonLat = base2DViewWidth * ratio;

            mGPSAccuracy.Width = mGPSAccuracy.Height = acuracyRadiusLonLat * 2;

            double posX = (LayoutRoot.Width - mGPSAccuracy.Width) * 0.5f;
            double posY = (LayoutRoot.Height * 0.75f) - mGPSAccuracy.Height * 0.5f;
            mGPSAccuracy.Margin = new Thickness(posX, posY, 0, 0);
        }

        private bool mIsUnit = true;
        public void Interface_SetUnit(bool isUnit)
        {
            mIsUnit = isUnit;
        }

        public void Interface_SetSpeedKMHR(double speedKMHR, int nowMovedDistanceMeter, int totalDistanceMeter,
            double speedAVG_KMHR,
            double remainDistanceToNextTurnPoint)
        {
            //根據時速設定自己的mark的大小
            float ratio = (float)speedKMHR / 120f;
            if (ratio > 1)
                ratio = 1;
            ratio = 1 - ratio;

            double nowScale = 0.7f - 0.2f * ratio;//0.8~0.6

            CompositeTransform composite = new CompositeTransform();
            composite.ScaleX = composite.ScaleY = nowScale;
            mMyPositionMarker.RenderTransform = composite;
        }

        /// <summary>
        /// 設定目前路線導航提示文字
        /// </summary>
        /// <param name="instruction"></param>
        public void SetCurrentRouteInstruction(string instruction)
        {
            //之後新增的，文字小提示訊息在這個control，顯示 目前要怎麼走的小提示
            mCurrentRouteInstructText.Text = instruction;
        }

        public void ShowCurrentRouteInstruction(bool isShow)
        {
            mCurrentRouteInstructText.Visibility = (isShow) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
        }

        //************************************************************************************************
        // 為了編輯介面可以換顏色的功能
        //************************************************************************************************
        public void SetDragColor()
        {
            LayoutRoot.Background = Utility.DragControl_ColorBrush;
            Interface_SetColor(Utility.DragControl_Color);
        }

        public void RevertDragColor()
        {
            LayoutRoot.Background = Utility.DragControl_TransparantBrush;
            Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));
        }

        //************************************************************************************************
        // 為了編輯介面可以拖拉的功能
        //************************************************************************************************ 
        private void UserControl_MouseLeftButtonDown_2(object sender, MouseButtonEventArgs e)
        {
            Utility.DragableGrid_ControlSelected(this);
        }


    }
}
