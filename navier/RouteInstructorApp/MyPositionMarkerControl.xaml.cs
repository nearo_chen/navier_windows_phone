﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using MyUtility;

namespace RouteInstructorApp
{
    public partial class MyPositionMarkerControl : UserControl, ISetColor
    {
        public MyPositionMarkerControl()
        {
            InitializeComponent();

            Width = Height = 80;
        }


        public void Interface_SetColor(Color color)
        {
            Color orangered = Utility.GetHUDColor(Utility.SelectColor.color_orangered);
            if (color.Equals(orangered))
            {
                traingle.Fill =
                box.Fill =
                boxOut.Stroke =
                new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            }
            else
            {
                traingle.Fill =
                box.Fill =
                boxOut.Stroke =
                new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
            }

        }
    }
}
