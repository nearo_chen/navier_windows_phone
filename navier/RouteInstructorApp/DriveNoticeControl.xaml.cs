﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using Windows.Phone.Speech.Synthesis;
using MyUtility;
using System.Windows.Media;
using GoogleVoiceApp1;


namespace RouteInstructorApp
{
    public partial class DriveNoticeControl : UserControl, ISetColor, ISetUnit, ISetSpeed
    {
        GoogleTranslateVoice _googleTranslateVoice = null;

        private DispatcherTimer _timer = new DispatcherTimer();
        private long _startTime;
        private int _showTimeLengthSec;//秀出訊息的時間

        //這東西用static的
        private static SpeechSynthesizer _synth = null;
        private Windows.Foundation.IAsyncAction task;

        private DispatcherTimer __scale_show_timer = new DispatcherTimer();

        public DriveNoticeControl(FlowDirection? flowDirection)
        {
            InitializeComponent();

            //  Width = Height = 480;

            _timer.Interval = TimeSpan.FromSeconds(1);
            _timer.Tick += Timer_Tick;

            __scale_show_timer.Interval = TimeSpan.FromSeconds(0.1);
            __scale_show_timer.Tick += _timer_scale_Tick;

            mFrame.Visibility = mBigNotice.Visibility = System.Windows.Visibility.Collapsed;

            this.Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_orangered));

            if (flowDirection.HasValue)
            {
                mNoticeDisTextOnTop.FlowDirection = mNoticeTextOnTop.FlowDirection =
                mNoticeText.FlowDirection = flowDirection.Value;
            }

            //一開始先將提示訊息HIDE，這樣方便之後動畫秀出
            Margin = new Thickness(0);
            Height = mBigNotice.Height = mFrame.Height = 0;

            mNoticeDisTextOnTop.Visibility =
                mNoticeTextOnTop.Visibility = System.Windows.Visibility.Collapsed;
        }

        public void CreateGoogleTTS()
        {
            _googleTranslateVoice = new GoogleTranslateVoice();
        }

        public void ReleaseGoogleTTS()
        {
            if (_googleTranslateVoice != null)
                _googleTranslateVoice.Dispose();
            _googleTranslateVoice = null;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            TimeSpan runTime = TimeSpan.FromMilliseconds(System.Environment.TickCount - _startTime);
            // timeLabel.Text = runTime.ToString(@"hh\:mm\:ss");

            if (runTime.Seconds >= _showTimeLengthSec)
            {
                _timer.Stop();


                _start_timer_scale_hide();

                //mFrame.Visibility = mBigNotice.Visibility = System.Windows.Visibility.Collapsed;
                //Margin = new Thickness(0);
                //Width = Utility.GetDeviceWidth(true);
                //Height = Utility.GetDeviceHeight(true);
            }
        }

        public delegate void ShowTextIsShowOrHide(bool isShow);
        private ShowTextIsShowOrHide mShowTextIsShowOrHide = null;

        private double __timer_scale_start_time;
        double recMarginX, recMarginY, recHeight, scale_show_timer_MSlength;
        bool scale_show_isHide;
        private void _timer_scale_Tick(object sender, EventArgs e)
        {
            TimeSpan runTime = TimeSpan.FromMilliseconds(System.Environment.TickCount - __timer_scale_start_time);
            double ratio = 1 - (runTime.TotalMilliseconds / scale_show_timer_MSlength);
            if (ratio > 0)
            {
                if (scale_show_isHide)
                {
                    Height = mBigNotice.Height = mFrame.Height = recHeight * ratio;
                    Margin = new Thickness(recMarginX, recMarginY * ratio, 0, 0);
                }
                else
                {
                    Height = mBigNotice.Height = mFrame.Height = recHeight * (1 - ratio);
                    Margin = new Thickness(recMarginX, recMarginY * (1 - ratio), 0, 0);
                }
            }

            if (ratio <= 0)
            {
                ratio = 0;
                if (scale_show_isHide)
                {
                    mFrame.Visibility = mBigNotice.Visibility = System.Windows.Visibility.Collapsed;

                    //Height = mBigNotice.Height = mFrame.Height = recHeight * ratio;
                    Margin = new Thickness(0);
                    Width = Utility.GetDeviceWidth(true);
                    Height = Utility.GetDeviceHeight(true);

                    mNoticeDisTextOnTop.Visibility = mNoticeTextOnTop.Visibility = System.Windows.Visibility.Visible;

                    if (mShowTextIsShowOrHide != null)
                        mShowTextIsShowOrHide(false);
                }
                else
                {
                    Height = mBigNotice.Height = mFrame.Height = recHeight * (1 - ratio);
                    Margin = new Thickness(recMarginX, recMarginY * (1 - ratio), 0, 0);

                    if (mShowTextIsShowOrHide != null)
                        mShowTextIsShowOrHide(true);
                }

                __scale_show_timer.Stop();
            }
        }

        private void _start_timer_scale_hide()
        {
            __scale_show_timer.Stop();
            __timer_scale_start_time = System.Environment.TickCount;
            __scale_show_timer.Start();

            recMarginY = Margin.Top;
            recHeight = Height;

            scale_show_timer_MSlength = 500;
            scale_show_isHide = true;
        }

        private void _start_timer_scale_show(double destTLeft, double destTop, double destHeight)
        {
            __scale_show_timer.Stop();
            __timer_scale_start_time = System.Environment.TickCount;
            __scale_show_timer.Start();

            recMarginX = destTLeft;

            recMarginY = destTop;
            recHeight = destHeight;

            scale_show_timer_MSlength = 500;
            scale_show_isHide = false;
        }

        public enum TurnType
        {
            none,
            turn,
        }

        /// <summary>
        /// 沒有轉彎指示圖標的NOTICE
        /// </summary>
        public void SetNotice(string text, int showTimeLength, bool ifLandscape, int gridX, int gridY, int gridW, int gridH,
            ShowTextIsShowOrHide showTextTimeOut)
        {
            SetNotice(TurnType.none, 0, text, showTimeLength, ifLandscape, gridX, gridY, gridW, gridH,
                showTextTimeOut);
        }


        bool? mVoiceIsMale = null;

        /// <summary>
        /// 設定語音的開關與是否為男聲OR女聲
        /// </summary>
        public void SetSpeechVoice(bool? isMale)
        {
            mVoiceIsMale = isMale;
        }

        //bool mIfCreateVoiceSuccess = false;

        /// <summary>
        /// 畫面秀出轉彎提示，還有朗讀
        /// </summary>
        /// <param name="text"></param>
        public void SetNotice(TurnType turnType, float degree, string text, int showTimeLength, bool ifLandscape,
            int gridX, int gridY, int gridW, int gridH,
            ShowTextIsShowOrHide showTextTimeOut)
        {
            mShowTextIsShowOrHide = showTextTimeOut;

            Utility.PlaySound("Assets/music/cowbell.wav");

            //把/ 和 \拿掉
            text = text.Replace("/", "");
            text = text.Replace("\\", "");

            _showTimeLengthSec = showTimeLength;

            Point wh = MyUtility.Utility.GetDeviceWH(ifLandscape);

            double screenWidth = Utility.GetDeviceWidth(true);
            int x = Utility.GetPixelX(screenWidth, gridX);
            int y = Utility.GetPixelY(screenWidth, gridY,
                //mLayoutRoot.ActualHeight
                Utility.GetDeviceHeight(true)
                );
            double destTop = y;
            double destLeft = x;
            //Margin = new Thickness(x, y, 0, 0);

            if (turnType == TurnType.none)
            {
                mTrunArrow.Visibility = System.Windows.Visibility.Collapsed;
                arrowGrid.Width = new GridLength(0, GridUnitType.Star);
            }
            else
            {
                mTrunArrow.Visibility = System.Windows.Visibility.Visible;
                arrowGrid.Width = new GridLength(1, GridUnitType.Star);
            }

            if (turnType == TurnType.none)
            {
                mTrunArrow.SetTurnRotateDegree(0);
            }
            else if (turnType == TurnType.turn)
            {
                mTrunArrow.SetTurnRotateDegree(degree);
            }

            mBigNotice.Width = Width = mFrame.Width = Utility.GetPixelLength(screenWidth, gridW);// wh.X * 0.9f;
            //mBigNotice.Height = Height = mFrame.Height = 
            double destHeight =
                Utility.GetPixelLength(screenWidth, gridH); //wh.Y * 0.9f;

            mNoticeText.Text = text.Replace(Utility.GetTranslate_Attention() + " ", "");//顯示在畫面上的字，移除掉"接下來"
            mFrame.Visibility = mBigNotice.Visibility = System.Windows.Visibility.Visible;
            mNoticeDisTextOnTop.Visibility =
                mNoticeTextOnTop.Visibility = System.Windows.Visibility.Collapsed;

            _timer.Start();
            _startTime = System.Environment.TickCount;

            _start_timer_scale_show(destLeft, destTop, destHeight);


            _playVoice(text);
        }

        private void _playVoice(string text)
        {
            ////檢查目前語言是否有win phone的TTS，要是沒有就使用google translate 的TTS
            //bool bIsWPTTS = false;

            //try
            //{
            //    var allLanguage =
            //          from voice in InstalledVoices.All
            //          orderby voice.Language
            //          select voice;

            //    string myCulture = Utility.GetCurrentLanguageRegionName();
            //    foreach (VoiceInformation vi in allLanguage)
            //    {
            //        if (vi.Language == myCulture)
            //        {
            //            bIsWPTTS = true;
            //            break;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    // MessageBox.Show("Could not initialize the ListBox: " + ex.Message);
            //}



            if (_googleTranslateVoice == null)
            {
                if (mVoiceIsMale == null)
                    return;
                VoiceGender voiceGender = VoiceGender.Female;
                if (mVoiceIsMale.Value)
                    voiceGender = VoiceGender.Male;

                string language = Utility.GetCurrentLanguageRegionName();

                try
                {
                    if (_synth == null)
                    {
                        int gender = 0;
                        var voices = (from voice in InstalledVoices.All
                                      where
                                          voice.Language == language && voice.Gender == voiceGender
                                      select voice).OrderByDescending(v => v.Gender);

                        _synth = new SpeechSynthesizer();
                        _synth.SetVoice(voices.ElementAt(gender));
                        //mIfCreateVoiceSuccess = true;
                    }

                    //if (mIfCreateVoiceSuccess)
                    task = _synth.SpeakTextAsync(text);
                }
                catch (Exception e)
                {
                    //mIfCreateVoiceSuccess = false;
                    //Utility.ShowToast(text, 1000, true);
                    //Utility.ShowMessageBox("Notice control speech Error: " + e.Message);

                    //goto GOOGLE_TRANSLATE_TTS;
                    return;
                }

            }
            else
            {

                //  if (mGoogleTranslateVoice == null)
                //      mGoogleTranslateVoice = new GoogleTranslateVoice();
                _googleTranslateVoice.QueryVoice(text);
            }
        }

        public void ClearVocalReadText()
        {
            if (_synth != null)
            {
                _synth.CancelAll();
                _synth.Dispose();
            }
            _synth = null;

            if (task != null)
                task.Cancel();
        }

        /// <summary>
        /// 設定轉彎的資訊，把它存在DriveNoticeControl這邊，順便在這邊做計算轉彎點ㄉ動作
        /// </summary>
        public void SetDriveNoticeList(List<DriveNoticeData> driveNoticeDataList)
        {
            mDriveNoticeDataList = driveNoticeDataList;
        }

        private List<DriveNoticeData> mDriveNoticeDataList;

        /// <summary>
        /// 用來儲存路線規畫後的轉彎提示資訊
        /// </summary>
        public class DriveNoticeData
        {
            //1000以上每1000提示一次，500公尺以上，每500提示一次，500以下 200提示一次 "X公尺+提示" ，最後100公尺出現  現在轉彎  "現在+提示"
            //注意500公尺以下的提示要以百為單位，因為這樣語音比較簡潔
            public enum Pass_Level
            {
                level3000m = 3000,
                level1000m = 1000,
                level800m = 800,
                level700m = 700,
                level400m = 400,
                level300m = 300,
                level200m = 200,
                level150m = 150,
                level100m = 100,
                level50m = 50,
                pass = 0,
            }

            public DriveNoticeData(string s, double lon, double lat, int routeID)
            {
                onRouteID = routeID;
                ifPass = false;// Pass_Level.level1000m;
                speak = s; longitude = lon; latitude = lat;
            }

            /// <summary>
            /// 紀錄目前位置距離這個轉彎點還有多長
            /// </summary>
            public double remainDisMeter = 0;

            /// <summary>
            /// 是否已經經過此點，經過就不提示了
            /// </summary>
            public bool ifPass;

            /// <summary>
            /// 電腦要說的語音
            /// </summary>
            public string speak;

            /// <summary>
            /// 轉彎點的經度
            /// </summary>
            public double longitude;

            /// <summary>
            /// 轉彎點的緯度
            /// </summary>
            public double latitude;

            /// <summary>
            /// 這個轉彎點位於路線規畫的哪個線段上
            /// </summary>
            public int onRouteID;
        }

        bool mConsiderTooShortTurn = false;

        /// <summary>
        /// 根據目前傳入的GPS定位，檢查是否有接近轉彎點，
        /// 是否要安排語音提示
        /// (偵測轉彎點，必須根據目前在路線上的投影點，去計算投影點與轉彎點的距離)
        /// 所以傳入路線規畫資訊，讓你可以計算轉彎點之間ㄉ距離
        /// return DriveNoticeData純粹是要給外部秀debug資訊用 
        /// out outDistanceNoticData 為 是否要外部計算轉彎角度以供，轉彎箭頭使用，
        /// out outNextTurnRouteID 為 哪一個路段是轉彎後的線段
        /// </summary>
        public DriveNoticeData ConsiderNotice(
            double fNowSpeedKMHR,
            RouteRendererControl._OnRouteData onRouteData,
                                                RouteInstructorApp.RouteRendererControl.RouteSectionData[] routeSectionArray,
                                                bool ifLandscape,
                                                out TempDistanceNoticData outDistanceNoticData,
                                                out int outNextTurnRouteID,
            out DriveNoticeData outNowRouteDriveNoticeData,
            out DriveNoticeData outNextTurnRouteDriveNoticeData,
            ShowTextIsShowOrHide showTextTimeOut)
        {
            outNextTurnRouteID = -1;
            outDistanceNoticData = null;

            //根據目前所在線段ID，檢查看看目前線段中，是否要注意有沒有接近要秀出轉彎提示的線段            
            DriveNoticeData nextTurnRouteDriveNoticeData = null;
            DriveNoticeData nowRouteDriveNoticeData = null; //紀錄下一步和目前的導航語音

            //Utility.OutputDebugString("ooooooooooooooooooooooooo");

            bool bTheFirst = false;
            string sSuccessTurn = "";
            bool bSuccessTurn = false;

            List<DriveNoticeData>.Enumerator itr = mDriveNoticeDataList.GetEnumerator();
            //foreach (DriveNoticeData data in mDriveNoticeDataList)
            while (itr.MoveNext())
            {
                DriveNoticeData data = itr.Current;
                if (data.onRouteID == -1 && data.ifPass == false)//!= DriveNoticeData.Pass_Level.pass)
                {
                    //導航最一開始的資訊
                    nextTurnRouteDriveNoticeData = nowRouteDriveNoticeData = data;

                    if (itr.MoveNext())
                        nextTurnRouteDriveNoticeData = itr.Current;

                    bTheFirst = true;
                    break;
                }
                else if (onRouteData.nowIndex > data.onRouteID) //&& data.ifPass == false)//!= DriveNoticeData.Pass_Level.pass)
                {
                    if(data.ifPass == false)
                    {
    #if DEBUG
                        Utility.OutputDebugString("[DriveNoticeControl]處理通過RouteID : " + data.onRouteID);
    #endif

                        //把已經過去(轉彎過去)的轉彎點設成已通過，迴圈還是繼續跑，找下一個轉彎點
                        sSuccessTurn = Utility.GetTranslate_Next();//抓取翻譯資訊 //"接下來 ";
                        data.ifPass = true;// DriveNoticeData.Pass_Level.pass;
                        bSuccessTurn = true;
                    }
                    nowRouteDriveNoticeData = data;
                }
                else if (onRouteData.nowIndex <= data.onRouteID && data.ifPass == false)//!= DriveNoticeData.Pass_Level.pass)
                {
#if DEBUG
                    Utility.OutputDebugString("[DriveNoticeControl]處理通過RouteID 找到之後轉彎點 : " + data.onRouteID);
#endif
                   
                    nextTurnRouteDriveNoticeData = data;
                    outNextTurnRouteID = data.onRouteID + 1;
                    break;
                }
            }
            itr.Dispose();

            //Utility.OutputDebugString("nextTurnRouteDriveNoticeData == " + nextTurnRouteDriveNoticeData);
            //Utility.OutputDebugString("onRouteData.nowIndex == " + onRouteData.nowIndex);
            //Utility.OutputDebugString("xxxxxxxxxxxxxxxxxxxxxxxxxx");
            
            
            //計算距離下一個轉彎點還有多少距離
            //double remainDisLonLat = onRouteData.nowRouteTotalDistance - onRouteData.nowRouteMovingDistance;//目前線段上的剩餘距離
            double remainDisMeterToNextTurnPointNow = onRouteData.nowRouteTotalDistanceMeter - onRouteData.nowRouteMovingDistanceMeter;//目前線段上的剩餘距離Meter
            if (onRouteData.nowIndex < nextTurnRouteDriveNoticeData.onRouteID)//轉彎點在其他線段上
            {
                for (int a = onRouteData.nowIndex + 1; a <= nextTurnRouteDriveNoticeData.onRouteID; a++)
                {
                    remainDisMeterToNextTurnPointNow += routeSectionArray[a].LengthMeter;
                }
            }
            double remainDisMeterToNextTurnPointOld = nextTurnRouteDriveNoticeData.remainDisMeter;
            nextTurnRouteDriveNoticeData.remainDisMeter = remainDisMeterToNextTurnPointNow;

            if (bTheFirst)
            {
                //導航最一開始的資訊，趕快唸一唸解決
                //nowRouteDriveNoticeData = mDriveNoticeDataList[0];//設定要傳出去秀DEBUG的資訊

                string ttt = nowRouteDriveNoticeData.speak;
                if (nextTurnRouteDriveNoticeData != null)
                {
                    ttt = ttt + " " + Utility.GetTranslate_Next() +
                        " " + nextTurnRouteDriveNoticeData.speak;
                }

                SetNotice(ttt, 7, ifLandscape, 1, 1, 14, 6, showTextTimeOut);
                nowRouteDriveNoticeData.ifPass = true;//DriveNoticeData.Pass_Level.pass;
            }
            else// if (nextTurnRouteDriveNoticeData != null) 一定不會是NULL
            {
                //轉彎較先提示                   
                //如果目前所在線段，有轉彎點要偵測，就檢查線段上的位置，與轉彎點之間距離是否進入範圍
                //if (remainDisLonLat < Utility.TURN_NOTICE_DIS)
                //{
                //    SetNotice(remainDisMeter + " 公尺 " + nowRouteDriveNoticeData.speak, 10, ifLandscape);
                //    nowRouteDriveNoticeData.ifPass = DriveNoticeData.Pass_Level.pass;
                //} 
                bool bSlowSpeedNotice = true;
                if (fNowSpeedKMHR > 60)
                    bSlowSpeedNotice = false;

                const int showTimeSEC = 7;

                //Utility.OutputDebugString("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                //Utility.OutputDebugString("remainDisMeter : " + remainDisMeter);
                //Utility.OutputDebugString("nowRouteTotalDistanceMeter : " + onRouteData.nowRouteTotalDistanceMeter);
                //Utility.OutputDebugString("nowRouteTotalDistanceMeter : " + onRouteData.nowRouteMovingDistanceMeter);

                if (bSuccessTurn)
                {
                    mConsiderTooShortTurn = true;

                    if (bSlowSpeedNotice)//速度較慢時提示距離會較短
                    {
                        //大於xx才出現"接下來"轉彎提示
                        if (remainDisMeterToNextTurnPointNow > 500)
                        {
                            string unit;
                            string distance;
                            Utility.ProcessDistanceUnit(remainDisMeterToNextTurnPointNow, mIsUnit, out unit, out distance);

                            SetNotice(sSuccessTurn + " " + distance + unit + nextTurnRouteDriveNoticeData.speak,
                                showTimeSEC, ifLandscape, 1, 1, 14, 6, showTextTimeOut);
                        }
                    }
                    else
                    {
                        //大於一公里才出現"接下來"轉彎提示
                        if (remainDisMeterToNextTurnPointNow > 1000)
                        {
                            string unit;
                            string distance;
                            Utility.ProcessDistanceUnit(remainDisMeterToNextTurnPointNow, mIsUnit, out unit, out distance);

                            SetNotice(sSuccessTurn + " " + distance + unit + nextTurnRouteDriveNoticeData.speak,
                                showTimeSEC, ifLandscape, 1, 1, 14, 6, showTextTimeOut);
                        }
                    }


                }
                else
                {
                    if (bSlowSpeedNotice)//速度較慢時提示距離會較短
                    {
                        //用OLD來判斷，因為OLD要是沒大於1000表示，已經完全經過，要是OLD>1000而NOW已經<=1000，則表示正在經過
                        if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level1000m) //預估1000會提示
                        {
                            //Utility.OutputDebugString("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                            //Utility.OutputDebugString(" > 1000");

                            outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                                remainDisMeterToNextTurnPointNow, remainDisMeterToNextTurnPointOld,
                                (int)DriveNoticeData.Pass_Level.level1000m
                                , false);

                            //double unit = 1000;//1000以上每1000提示一次
                        }
                        else if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level700m)//預估700會提示 
                        {
                            outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                                 remainDisMeterToNextTurnPointNow, remainDisMeterToNextTurnPointOld,
                                 (int)DriveNoticeData.Pass_Level.level700m
                                 , false);

                            //int unit = 500;//500公尺以上，每500提示一次
                        }
                        else if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level400m)//預估400會提示
                        {
                            //Utility.OutputDebugString("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                            //Utility.OutputDebugString(" > 200");

                            outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                                remainDisMeterToNextTurnPointNow, remainDisMeterToNextTurnPointOld,
                                (int)DriveNoticeData.Pass_Level.level400m,
                                false);

                            //int unit = 200;//200公尺以上 200提示一次
                        }
                        else if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level200m)//預估200會提示
                        {
                            //Utility.OutputDebugString("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                            //Utility.OutputDebugString(" > 200");

                            outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                                remainDisMeterToNextTurnPointNow, remainDisMeterToNextTurnPointOld,
                                (int)DriveNoticeData.Pass_Level.level200m,
                                false);

                            //int unit = 200;//200公尺以上 200提示一次
                        }
                        else if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level150m)//把150,100消耗掉
                        {
                        }
                        else if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level100m)//把150,100消耗掉
                        //{
                        //}
                        //else if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level50m)//預估50會提示 注意
                        {
                            mConsiderTooShortTurn = false;

                            outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                                remainDisMeterToNextTurnPointNow, remainDisMeterToNextTurnPointOld,
                                (int)DriveNoticeData.Pass_Level.level50m
                                , true);//提示出現"注意"

                        }
                        else if (mConsiderTooShortTurn)//判斷到這邊
                        {
                            mConsiderTooShortTurn = false;

                            //Utility.OutputDebugString("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                            //Utility.OutputDebugString("直接就在100以內");

                            //outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                            //    remainDisMeterToNextTurnPointNow, remainDisMeterToNextTurnPointOld,
                            //    remainDisMeterToNextTurnPointOld, //給remainDisMeterToNextTurnPointOld，這樣他在之後的nowSection的判斷中就會不一樣
                            //    true);//提示出現"注意"

                            outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                              remainDisMeterToNextTurnPointNow - 10, remainDisMeterToNextTurnPointOld,
                              -1,//給-1表示不用判斷是否有跳section，直接可以念ㄌ
                              false);
                        }
                    }
                    else
                    {
                        if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level3000m)// 預估3000會提示
                        {
                            outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                                remainDisMeterToNextTurnPointNow, remainDisMeterToNextTurnPointOld,
                                 (int)DriveNoticeData.Pass_Level.level3000m
                                 , false);
                        }
                        else if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level1000m)//預估1000會提示
                        {
                            outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                                remainDisMeterToNextTurnPointNow, remainDisMeterToNextTurnPointOld,
                                 (int)DriveNoticeData.Pass_Level.level1000m
                                 , false);
                        }
                        else if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level800m)// 預估800會提示
                        {
                            outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                                remainDisMeterToNextTurnPointNow, remainDisMeterToNextTurnPointOld,
                                 (int)DriveNoticeData.Pass_Level.level800m
                                 , false);
                        }
                        else if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level400m)// 預估400會提示
                        {
                            outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                                remainDisMeterToNextTurnPointNow, remainDisMeterToNextTurnPointOld,
                                 (int)DriveNoticeData.Pass_Level.level400m
                                 , false);
                        }
                        else if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level300m)//把300消耗掉
                        {
                        }
                        else if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level200m)//把200消耗掉
                        //{
                        //}
                        //else if (remainDisMeterToNextTurnPointOld >= (int)DriveNoticeData.Pass_Level.level100m)// 預估100會提示
                        {
                            mConsiderTooShortTurn = false;
                            outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                              remainDisMeterToNextTurnPointNow, remainDisMeterToNextTurnPointOld,
                              (int)DriveNoticeData.Pass_Level.level100m,
                              true);

                            //最後x公尺出現  現在轉彎  "現在+提示"

                        }
                        else if (mConsiderTooShortTurn)//判斷到這邊
                        {
                            mConsiderTooShortTurn = false;

                            //Utility.OutputDebugString("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                            //Utility.OutputDebugString("直接就在200以內");

                            //outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                            //    remainDisMeterToNextTurnPointNow, remainDisMeterToNextTurnPointOld,
                            //    remainDisMeterToNextTurnPointOld, //給remainDisMeterToNextTurnPointOld，這樣他在之後的nowSection的判斷中就會不一樣
                            //    true);//提示出現"注意"

                            outDistanceNoticData = _createTempDistanceNoticData(nextTurnRouteDriveNoticeData.speak, showTimeSEC, ifLandscape,
                                remainDisMeterToNextTurnPointNow - 50, remainDisMeterToNextTurnPointOld,
                                -1,//給-1表示不用判斷是否有跳section，直接可以念ㄌ
                                false);
                        }
                    }
                }
            }


            outNowRouteDriveNoticeData = nowRouteDriveNoticeData;
            outNextTurnRouteDriveNoticeData = nextTurnRouteDriveNoticeData;
            return nextTurnRouteDriveNoticeData;//回傳這個的意義應該是給外部作畫DEEBUG線用
        }

        public class TempDistanceNoticData
        {
            public string text;
            public int showTimeLength;
            public bool ifLandscape;
            public double remainDisMeter;
            public double remainDisMeterOld;
            public double sectionMeter;
            public bool needAttention;
        }

        /// <summary>
        /// 將傳入的資料弄成一個TempDistanceNoticData，丟到外部去
        /// </summary>
        private TempDistanceNoticData _createTempDistanceNoticData(string text, int showTimeLength, bool ifLandscape,
            double remainDisMeter, double remainDisMeterOld, double disSectionMeter, bool needAttention)
        {
            if (remainDisMeter < 0)
                remainDisMeter = 0;
            if (remainDisMeterOld < 0)
                remainDisMeterOld = 0;

            TempDistanceNoticData data = new TempDistanceNoticData();
            data.text = text;
            data.showTimeLength = showTimeLength;
            data.ifLandscape = ifLandscape;
            data.remainDisMeter = remainDisMeter;
            data.remainDisMeterOld = remainDisMeterOld;
            data.sectionMeter = disSectionMeter;
            data.needAttention = needAttention;
            return data;
        }



        /// <summary>
        /// 去做有畫出轉彎圖示的Notic(會先判斷是否有通過section門檻才去做一次提示)
        /// </summary>
        public void DistanceNotic(TempDistanceNoticData data, float TurnDegree,
            ShowTextIsShowOrHide showTextTimeOut)
        {
            double boundRemainMeter = 0;
            bool bChangeSection = false;

            //假設每100m要提示一次，section就是100，每減少100就要提示
            if (data.sectionMeter > 0)
            {
                int oldSection = (int)Math.Floor(data.remainDisMeterOld / data.sectionMeter);
                int nowSection = (int)Math.Floor(data.remainDisMeter / data.sectionMeter);
                if (nowSection != oldSection)
                    bChangeSection = true;
                boundRemainMeter = data.sectionMeter * (double)oldSection;//取得整數的剩餘距離
            }

            //data.disSectionMeter給-1表示不用判斷是否有跳section，直接可以念ㄌ
            if (data.sectionMeter == -1 || bChangeSection)
            {
                string unit;
                string distance;
                string sAttention = "";

                if (data.needAttention)
                {
                    distance = unit = "";
                    sAttention = Utility.GetTranslate_Attention() + " ";
                }
                else
                {
                    if (data.sectionMeter > 0)
                        Utility.ProcessDistanceUnit(boundRemainMeter, mIsUnit, out unit, out distance);//念整數
                    else
                        Utility.ProcessDistanceUnit(data.remainDisMeter, mIsUnit, out unit, out distance);
                }

                SetNotice(TurnType.turn,
                    TurnDegree,
                    sAttention + distance + unit + data.text,
                    data.showTimeLength,
                    data.ifLandscape,
                    1, 1, 14, 6, showTextTimeOut);
            }
            else
            {

            }
        }

        /// <summary>
        /// 專門用來做你已到達目的地的提示，畫面顯示一個讚
        /// </summary>
        public void YouAreGoalNotice(bool ifLandscape, ShowTextIsShowOrHide showTextTimeOut)
        {
            int showTimeSEC = 20;
            SetNotice(mDriveNoticeDataList[mDriveNoticeDataList.Count() - 1].speak,
                           showTimeSEC,
                       ifLandscape,
                       1, 1, 14, 6, showTextTimeOut);
        }

        public void Interface_SetColor(Color color)
        {
            Brush brush = new SolidColorBrush(color);
            mFrame.Stroke =
            mNoticeText.Foreground =
            mNoticeDisTextOnTop.Foreground =
            mNoticeTextOnTop.Foreground = brush;

            mTrunArrow.SetColor(color);
        }


        private bool mIsUnit = true;
        public void Interface_SetUnit(bool isUnit)
        {
            mIsUnit = isUnit;

            //換單位的話也更新一下
            string unit;
            string distance;
            Utility.ProcessDistanceUnit(mNextTurnRemainDisMeter, mIsUnit, out unit, out distance);
            string disText = Utility.GetTranslate_Next() + " " + distance + unit;
            if (!mNoticeDisTextOnTop.Text.Equals(disText))
                mNoticeDisTextOnTop.Text = disText;
        }


        /// <summary>
        /// 在畫面的最上端一直顯示導航提示文字(跑馬燈: "300公尺 向右轉靠左邊走順著中山路前進")
        /// </summary>
        public void SetNextRouteInstruction(string nextTurnNoticeText, double remainDisMeter)
        {
            mNextTurnRemainDisMeter = remainDisMeter;
            string unit;
            string distance;
            Utility.ProcessDistanceUnit(mNextTurnRemainDisMeter, mIsUnit, out unit, out distance);
            string disText = Utility.GetTranslate_Next() + " " + distance + unit;
            if (!mNoticeDisTextOnTop.Text.Equals(disText))
                mNoticeDisTextOnTop.Text = disText;

            mNoticeTextOnTop.Text = nextTurnNoticeText;
            //mNextTurnNoticeText = nextTurnNoticeText;

        }
        double mNextTurnRemainDisMeter;
        //string mNextTurnNoticeText = string.Empty;

        public void Interface_SetSpeedKMHR(double speedKMHR,
            int nowMovedDistanceMeter, int totalDistanceMeter, double speedAVG_KMHR,
            double remainDistanceToNextTurnPoint)
        {
            string unit;
            string distance;
            Utility.ProcessDistanceUnit(remainDistanceToNextTurnPoint, mIsUnit, out unit, out distance);
            string disText = Utility.GetTranslate_Next() + " " + distance + unit;
            if (!mNoticeDisTextOnTop.Text.Equals(disText))
                mNoticeDisTextOnTop.Text = disText;

            //SetNotice(sSuccessTurn + " " +  + nextTurnRouteDriveNoticeData.speak,
            //    showTimeSEC, ifLandscape, 1, 1, 14, 6);
            //mDistanceCount.Text = 
        }
    }
}
