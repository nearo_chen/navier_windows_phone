﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;

namespace RouteInstructorApp
{
    public partial class TurnArrowControl : UserControl
    {
        public TurnArrowControl()
        {
            InitializeComponent();
           
        }

        //public void SetTurnLeft()
        //{
        //    TurnRotate.Rotation = -90;
        //}

        //public void SetTurnRight()
        //{
        //    TurnRotate.Rotation = 90;
        //}

        public void SetColor(Color color)
        {
            mBoxBottom.Fill =
                mBoxUpper.Fill =
                mTraingle.Fill = new SolidColorBrush(color);
        }

        public void SetTurnRotateDegree(float degree)
        {
            TurnRotate.Rotation = degree;
        }
    }
}
