﻿
//不用clip了，因為外圍VIEW會有一次clip就好
//#define VIEW_CLIP //設定veiw clip 關起來可以偵錯用，正常要打開 

//RUN_MY_VIEW_CLIP 這個過濾view內的線段計算，當線段量很大時，一定要過濾，不然路線旋轉時會畫不出來
#define RUN_MY_VIEW_CLIP //設定是否要我自己判斷篩選(我先過濾可省效能) mPointCollection_GlobalPosition的點是否超出範圍，正常要打開

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Media.Animation;
using MyUtility;

using Vector3 = Microsoft.Xna.Framework.Vector3;
using Plane = Microsoft.Xna.Framework.Plane;
using Ray = Microsoft.Xna.Framework.Ray;

namespace RouteInstructorApp
{
    public partial class RouteRendererControl : UserControl, ISetColor
    {
        // private Polyline mPolyline = null;    //畫路線用
        private Point mGlobalCentor;             //紀錄畫面中央的經緯度座標
        public Point GetGlobalCentor_WithoutOffset() { return new Point(mGlobalCentor.X, mGlobalCentor.Y - mLongitude_Latitude_Range.Y * Global_Center_Offset); }
        private const float Global_Center_Offset = 0;//0.5f;

        /// <summary>
        /// 紀錄外部傳入路線規畫導航的經緯度座標
        /// </summary>
        private List<Point> mGlobalPositionList = null;

        /// <summary>
        /// 紀錄外部傳入路線規畫導航的轉彎點用來畫轉彎點的圓
        /// </summary>
        private List<Point> mGlobalPositionTurnList = null;

        /// <summary>
        /// 作為賽車路線的檢查點資料用
        /// </summary>
        public class RouteSectionData
        {
            /// <summary>
            /// 線段的起始經緯度 lon , lat
            /// </summary>
            public Point startPoint;

            /// <summary>
            /// 線段的結束經緯度 lon , lat
            /// </summary>
            // public Point endPoint;

            /// <summary>
            /// 根據經緯度計算出地球表面的實際距離
            /// </summary>
            public double LengthMeter;

            /// <summary>
            /// 目前路段的長度
            /// </summary>
            //public float length;

            /// <summary>
            /// 目前路段的向量
            /// </summary>
            public Vector3 vector;

            /// <summary>
            /// 正規畫向量
            /// </summary>
            public Vector3 normallizeDir;


            /// <summary>
            /// 路段向量換算成一面強
            /// </summary>
            public Plane plane;
            //public Vector3 crossproduct;
        }

        /// <summary>
        /// 作為賽車路線的檢查點用
        /// </summary>
        private RouteSectionData[] mRouteSectionArray = null;

        /// <summary>
        /// 取得路線規畫的線段資訊(賽車路線的隱形牆計算資訊)
        /// </summary>
        public RouteSectionData[] GetRouteSectionArray() { return mRouteSectionArray; }


        private int miNowIndexOnRoute = 0;       //紀錄目前我位於路線陣列上的陣列位址
        private int miOldIndexOnRoute;           //紀錄舊的路線位置(紀錄有效的，不為-1)

        /// <summary>
        /// 控制目前view寬高，要跨多少經緯度，
        /// 設定view中心點到上或下左或右經緯度的範圍
        /// </summary>
        private Point mLongitude_Latitude_Range;

        /// <summary>
        /// 初始化Control內容(主要是將一些用不到的都拿掉)
        /// </summary>
        public void initLine(bool inEditorUI)
        {
            mRoutePolyline.Stroke = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));

            if (inEditorUI)
            {
                _removeDebugLine();

                //編輯介面的小圓不要讓他之後被改到顏色
                TurnCircleData TurnCircleData = new TurnCircleData();
                TurnCircleData.id = -1;
                TurnCircleData.bBigCircle = false;
                mForEditorTurnCircleSmall.Tag = TurnCircleData;
            }
            else
            {
                bool deleteAll = false;
                //把editor才會出現的轉彎圓拿掉
                while (deleteAll == false)
                {
                    deleteAll = true;
                    foreach (UIElement uie in LayoutRoot.Children)
                    {
                        Ellipse ellipse = uie as Ellipse;
                        if (ellipse != null && ellipse.Name == "mForEditorTurnCircle")
                        {
                            LayoutRoot.Children.Remove(uie);
                            deleteAll = false;
                            break;
                        }

                        if (ellipse != null && ellipse.Name == "mForEditorTurnCircleSmall")
                        {
                            LayoutRoot.Children.Remove(uie);
                            deleteAll = false;
                            break;
                        }
                    }
                }
                _removeDebugLine();
            }
        }

        void _removeDebugLine()
        {
            bool deleteAll = false;
            //把DEBUG的線拿掉
            while (deleteAll == false)
            {
                deleteAll = true;
                foreach (UIElement uie in LayoutRoot.Children)
                {
                    Polyline line = uie as Polyline;
                    if (line != null && line.Name == "mPolylineUp_Debug")
                    {
                        LayoutRoot.Children.Remove(uie);
                        deleteAll = false;
                        break;
                    }

                    if (line != null && line.Name == "mPolylineMe_Debug")
                    {
                        LayoutRoot.Children.Remove(uie);
                        deleteAll = false;
                        break;
                    }
                }
            }
        }

        public RouteRendererControl()
        {
            InitializeComponent();


            Width = mRoutePolyline.Width;
            Height = mRoutePolyline.Height;
            //mViewCoord2D = new Point(Margin.Left, Margin.Top);

            //#if VIEW_CLIP || !DEBUG   不用clip了，因為外圍VIEW會有一次clip就好       
            //  RectangleGeometry clipRetangle = new RectangleGeometry();
            //  clipRetangle.Rect = new Rect(0, 0, Width, Height);
            //   LayoutRoot.Clip = clipRetangle;
            //#endif

            mLongitude_Latitude_Range.X = mLongitude_Latitude_Range.Y = mGlobalCentor.X = mGlobalCentor.Y = 0;
        }

        /// <summary>
        ///  傳入資訊給control給他去畫路線，回傳被view culling過後存在的點數量
        ///  回傳全部路線的長度有幾公尺
        /// </summary>
        /// <param name="pointCollection">路徑規化的經緯度座標陣列</param>
        public int SetGlobalLines(List<Point> pointCollection)
        {
            double allDistanceInMeter = 0;

            //因為設定SetGlobalLines都會是導航的最開始，所以一些路徑計算參數在這邊做初始化
            miNowIndexOnRoute = 0;
            miOldIndexOnRoute = 0;

            //  _SetGlobalCenter_WithOffset(ref globalPositionCentor);

            // if (scaleGlobalRange != null)
            //     mLongitude_Latitude_Range = scaleGlobalRange.Value;

            //if (mPolyline == null)
            //{
            //    mPolyline = new Polyline();
            //    SolidColorBrush brush = new SolidColorBrush(Colors.Brown);
            //    mPolyline.Stroke = brush;
            //    mPolyline.StrokeThickness = 2;
            //    mPolyline.FillRule = FillRule.EvenOdd;
            //    mRenderRetangle.Children.Add(mPolyline);
            //}

            // Add the Polyline Element
            mGlobalPositionList = pointCollection;

            //   PointCollection lineInView = GetLineInRange_GlobalPosition();
            //   mRoutePolyline.Points = TranslateGlobalPositionToViewCoord(lineInView);

            //  return lineInView.Count;


            //將路線計算cross product做到達檢查點用
            mRouteSectionArray = new RouteSectionData[mGlobalPositionList.Count - 1];//有n個點，就有n-1個線段
            Point prevPoint = new Point();
            Point tempPoint = new Point();
            int count = 0;
            foreach (Point nowPoint in mGlobalPositionList)
            {
                if (count > 0)
                {
                    tempPoint.X = nowPoint.X - prevPoint.X;
                    tempPoint.Y = nowPoint.Y - prevPoint.Y;

                    RouteSectionData data = new RouteSectionData();
                    data.vector.X = (float)tempPoint.X;
                    data.vector.Y = (float)tempPoint.Y;
                    data.vector.Z = 0;

                    //data.length = data.vector.Length();
                    data.LengthMeter = Utility.LonLat2Meter(prevPoint.X, prevPoint.Y, nowPoint.X, nowPoint.Y);
                    data.startPoint = prevPoint;
                    // data.endPoint = nowPoint;

                    data.normallizeDir = //data.vector / data.length;
                        data.vector;
                    data.normallizeDir.Normalize();

                    //data.crossproduct = Vector3.Cross(data.normallizeDir, Vector3.UnitZ * -1);

                    data.plane = new Plane(
                        new Vector3((float)nowPoint.X, (float)nowPoint.Y, 0),
                        new Vector3((float)prevPoint.X, (float)prevPoint.Y, 0),
                        new Vector3((float)nowPoint.X, (float)nowPoint.Y, 1));

                    mRouteSectionArray[count - 1] = data;

                    allDistanceInMeter += data.LengthMeter;
                }

                prevPoint = nowPoint;
                count++;
            }


            //allDistanceInMeter = (float)Utility.LonLat2Kilometer(allDistanceInMeter) * 1000;
            return (int)allDistanceInMeter;
        }


        /// <summary>
        /// 設定轉彎點的位置
        /// </summary>
        public void SetTurnCircleList(List<Point> turnPointList)
        {
            mGlobalPositionTurnList = turnPointList;

            _ClearAllTurnCircle();

            /*   //處理計算資料
               mTurnpointArray = new TurnpointData[mGlobalPositionTurnList.Count];
               for (int a = 0; a < mTurnpointArray.Length; a++)
               {
                   TurnpointData turnpointData = new TurnpointData();
               
                   //計算所有的路徑，看他位於哪個線段ID上面

                   mTurnpointArray[a] = turnpointData;
               }*/
        }

        /// <summary>
        /// 將轉彎的圓圈提示都刪除
        /// </summary>
        private void _ClearAllTurnCircle()
        {
            List<UIElement> removeList = new List<UIElement>();
            foreach (UIElement uie in LayoutRoot.Children)
            {
                Ellipse ellipse = uie as Ellipse;
                if (ellipse != null && ellipse.Tag != null && ellipse.Tag.GetType() == typeof(TurnCircleData))
                {
                    removeList.Add(uie);
                }
            }

            foreach (UIElement uie in removeList)
            {
                LayoutRoot.Children.Remove(uie);
            }
        }



        /// <summary>
        /// 單純做建立一個轉彎提示的圓圈圈動態物件
        /// </summary>
        /// <param name="circleRadius"></param>
        /// <returns></returns>
        private static Ellipse CreateNewAnimateCircle(float circleRadius, Point globalPosToView, int id, Color color, float StrokeThickness, bool bBigCircle, int secondDuration)
        {
          
            Ellipse ellipse = new Ellipse();

            ModifyTurnCirclePosition(ellipse, circleRadius, globalPosToView);

            //StrokeThickness
            ellipse.StrokeThickness = StrokeThickness;

            //brush
            SolidColorBrush brush = new SolidColorBrush(color);
            ellipse.Stroke = brush;

            //strokeDashArray
            DoubleCollection strokeDashArray = new DoubleCollection();

            if (bBigCircle)
                strokeDashArray.Add(4);
            else
                strokeDashArray.Add(1.5);

            strokeDashArray.Add(1);
            ellipse.StrokeDashArray = strokeDashArray;

            //設置動畫
            DoubleAnimation anima = new DoubleAnimation();
            anima.AutoReverse = false;
            anima.RepeatBehavior = RepeatBehavior.Forever;
            anima.Duration = new Duration(TimeSpan.FromSeconds(secondDuration));  //设置动画耗时，这里为0.5秒
            anima.From = 0;

            if (bBigCircle)
                anima.To = -360;
            else
                anima.To = 360;

            // 设置附加属性
            RotateTransform rotTran = null;
            TransformGroup group = ellipse.RenderTransform as TransformGroup;
            foreach (Transform transform in group.Children)
            {
                rotTran = transform as RotateTransform;
                if (rotTran != null)
                    break;
            }
            Storyboard.SetTarget(anima, rotTran);  //使动画与特定对象关联
            Storyboard.SetTargetProperty(anima, new PropertyPath(RotateTransform.AngleProperty));  //使动画与特定属性关联

            // 生成storyboard，把动画加进去并启动
            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(anima);
            storyboard.Begin();

            TurnCircleData TurnCircleData = new TurnCircleData();
            TurnCircleData.id = id;
            TurnCircleData.bBigCircle = bBigCircle;
            //TurnCircleData.translateTransform = translateTran;
            ellipse.Tag = TurnCircleData;

            //  BitmapCache bitmapCache = new BitmapCache();
            //   ellipse.CacheMode = bitmapCache;

            return ellipse;
        }


        private class TurnCircleData
        {
            public int id = -1;

            /// <summary>
            /// 判斷大圓或小圓，小圓不給setColor改顏色
            /// </summary>
            public bool bBigCircle = true;

            //public TranslateTransform translateTransform;
        }

        /// <summary>
        /// 修改轉彎提示圓的座標,半徑
        /// </summary>
        private static void ModifyTurnCirclePosition(Ellipse ellipse,
            float circleRadius, Point globalPosToView)
        {
            ellipse.Width = ellipse.Height = circleRadius * 2;

            TransformGroup group = ellipse.RenderTransform as TransformGroup;

            //再第一次時建立新的
            if (group == null)
            {
                TranslateTransform translateTran = new TranslateTransform();
                translateTran.X = globalPosToView.X - ellipse.Width * 0.5f;
                translateTran.Y = globalPosToView.Y - ellipse.Height * 0.5f;

                RotateTransform rotTran = new RotateTransform();
                rotTran.Angle = 0;
                rotTran.CenterX = rotTran.CenterY = circleRadius;

                group = new TransformGroup();
                group.Children.Add(rotTran);
                group.Children.Add(translateTran);
                ellipse.RenderTransform = group;
            }

            foreach (Transform transform in group.Children)
            {
                TranslateTransform translateTran = transform as TranslateTransform;
                RotateTransform rotTran = transform as RotateTransform;
                if (translateTran != null)
                {
                    translateTran.X = globalPosToView.X - ellipse.Width * 0.5f;
                    translateTran.Y = globalPosToView.Y - ellipse.Height * 0.5f;
                }

                if (rotTran != null)
                {
                    rotTran.CenterX = rotTran.CenterY = circleRadius;
                }
            }

        }

        /// <summary>
        /// 讓陣列位址編號為OOXX的圓圈圈不要畫出來(由LayoutRoot.children中移除)
        /// </summary>
        /// <param name="searchEllipseID"></param>
        private void _RemoveEllipseFromLayoutRoot(int searchEllipseID)
        {
            //因為轉彎提是點有大圓及小圓，我把它們都用同一個ID，所以，迴圈一直跑，直到移除沒有 ID為止
            bool bRemoveAll = false;
            while (bRemoveAll == false)
            {
                bRemoveAll = true;
                foreach (UIElement uie in LayoutRoot.Children)
                {
                    Ellipse ellipse = uie as Ellipse;
                    if (ellipse != null)
                    {
                        TurnCircleData turnData = ellipse.Tag as TurnCircleData;
                        if (turnData != null && turnData.id == searchEllipseID)
                        {
                            LayoutRoot.Children.Remove(uie);
                            bRemoveAll = false;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 檢查layout.children下面是否已經有ellipse
        /// </summary>
        /// <param name="ellipseID"></param>
        /// <returns></returns>
        private List<Ellipse> _SearchLayoutRootForEllipse(int searchEllipseID)
        {
            List<Ellipse> getEllipseList = new List<Ellipse>(2);

            foreach (UIElement uie in LayoutRoot.Children)
            {
                if (uie.GetType() == typeof(Ellipse))
                {
                    Ellipse ellipse = uie as Ellipse;
                    if (ellipse.Tag != null && ((TurnCircleData)ellipse.Tag).id == searchEllipseID)
                    {
                        getEllipseList.Add(ellipse);
                    }
                }
            }

            return getEllipseList;
        }

        float mSmallTurnCircleRadius;
        /// <summary>
        /// 建立一個轉彎提示的Ellipse圓物件，並將他畫出來(LayoutRoot.Children.Add(ellipse); )
        /// 回傳目前有幾個圓在畫面中被畫出
        /// </summary>
        private int CreateTurnCircle_Inview(float circleRadius)
        {
            mSmallTurnCircleRadius = (float)mRoutePolyline.StrokeThickness * 0.5f;

            int circleInViewCount = 0;
            int TurnID_count = 0;
            foreach (Point pos in mGlobalPositionTurnList)
            {
                //1.先檢查這個轉彎圓是否位於視野內(檢查圓心及半徑的範圍是否有進入)
                //2.mGlobalPositionTurnList的點有在範圍內，有要加入LayoutRoot.Children中的話
                //就要先檢查是否LayoutRoot.Children中已經有該circle(使用indexof陣列位址當作辨別碼判斷)
                //建立一個名單有哪些要由LayoutRoot.Children移除，還有哪些要新增進入 LayoutRoot.Children

                if (Math.Abs(mGlobalCentor.X - pos.X) - 0 < mLongitude_Latitude_Range.X && //原本有要考慮半徑當作範圍計算，但要拿經緯座標的半徑計算才有用
                    Math.Abs(mGlobalCentor.Y - pos.Y) - 0 < mLongitude_Latitude_Range.Y
                   )
                {
                    List<Ellipse> ellipseList = _SearchLayoutRootForEllipse(TurnID_count);//使用陣列位址當作ID

                    //有就修改無就新增(新增2個一樣ID的圓，大圓&小圓)
                    if (ellipseList.Count() == 0)
                    {
                        Point globalPosToView = _TranslateGlobalPositionToViewCoord(pos);

                        Ellipse ellipseBig = CreateNewAnimateCircle(circleRadius,
                                                                    globalPosToView,
                                                                    TurnID_count,
                                                                    mNowColor,
                                                                    2,
                                                                    true,
                                                                    20);
                        if (ellipseBig != null)
                            LayoutRoot.Children.Add(ellipseBig);

                        Ellipse ellipseSmall = CreateNewAnimateCircle(mSmallTurnCircleRadius,
                                                                        globalPosToView,
                                                                        TurnID_count,
                                                                        Color.FromArgb(255, 255, 255, 255),
                                                                        2,
                                                                        false,
                                                                        1);
                        if (ellipseSmall != null)
                            LayoutRoot.Children.Add(ellipseSmall);
                    }
                    else
                    {
                        Point globalPos = _TranslateGlobalPositionToViewCoord(pos);

                        foreach (Ellipse ellipse in ellipseList)
                        {
                            TurnCircleData turnData = ellipse.Tag as TurnCircleData;
                            if (turnData.bBigCircle)
                            {
                                ModifyTurnCirclePosition(ellipse,
                                                            circleRadius,
                                                            globalPos
                                                           );
                            }
                            else
                            {
                                ModifyTurnCirclePosition(ellipse,
                                                            mSmallTurnCircleRadius,
                                                            globalPos
                                                           );
                            }
                        }
                    }
                    circleInViewCount++;
                }
                else
                {
                    _RemoveEllipseFromLayoutRoot(TurnID_count);
                }

                TurnID_count++;
            }

            return circleInViewCount;
        }

        /// <summary>
        /// 把設定mGlobalCentor集中起來，必須在所有其他計算的FUNC之前(因為很多計算都直接用到mGlobalCentor)
        /// </summary>
        private void _SetGlobalCenter_WithOffset(Point globalPositionCentor)
        {
            mGlobalCentor = globalPositionCentor;

            //中心點往螢幕下方移動，變成軸心在3/4的位置上
            mGlobalCentor.Y += mLongitude_Latitude_Range.Y * Global_Center_Offset;
        }

        int mOldStartRec = -1;//記錄前一次路線規畫的start，給SetGlobalCentor_WithOffset裡面的
        // GetLineInRange_GlobalPosition做view culling的時候，當找不到任何線段點在view裡面時，
        //  拿錢一次的start來用

        /// <summary>
        /// 傳入經緯度，當作view的中心點，回傳被view culling過後存在的點數量
        /// </summary>
        public int SetGlobalCentor_WithOffset(Point? globalPositionCentor, Point? globalRange_InView, float TurnCircleRadius)
        {
            if (globalRange_InView != null)
            {
                mLongitude_Latitude_Range.X = globalRange_InView.Value.X;
                mLongitude_Latitude_Range.Y = globalRange_InView.Value.Y;

                if (mLongitude_Latitude_Range.X < 0.00001f)
                    mLongitude_Latitude_Range.X = 0.00001f;
                if (mLongitude_Latitude_Range.Y < 0.00001f)
                    mLongitude_Latitude_Range.Y = 0.00001f;
            }

            if (globalPositionCentor.HasValue)
            {
                _SetGlobalCenter_WithOffset(globalPositionCentor.Value);

                //根據目前control可視範圍，找出要畫出的line
                List<Point> lineInView = GetLineInRange_GlobalPosition(mGlobalPositionList, ref mOldStartRec);
                mRoutePolyline.Points = TranslateGlobalPositionToViewCoord(lineInView);

                //處理畫出轉彎提示圓，並寫個LOG看一下
                //float radius = (float)Utility.Kilometer2LonLat(Utility.TURN_NOTICE_DIS_METER / 1000f);
                float ratio = TurnCircleRadius / (float)mLongitude_Latitude_Range.X;
                TurnCircleRadius = (float)Width * ratio;

#if DEBUG
                int circleCount = 
#endif
                                    CreateTurnCircle_Inview(TurnCircleRadius);
#if DEBUG
                MyUtility.Utility.OutputDebugString("SetTurnCircle => circle in view : " + circleCount);
#endif

                return lineInView.Count;
            }
            return 0;
        }

        /// <summary>
        /// 根據view的中心點經緯度座標，過濾看得見的路徑
        /// (如果有傳入oldStart，就會去考慮之前的start，當狀況發生在start及end都找到-1時，就拿舊的值出來用，
        /// 因為有時一條路線又大又直，會出現沒任何線段點位於view內，變成線段消失了)
        /// </summary>
        /// <returns></returns>
        public List<Point> GetLineInRange_GlobalPosition(List<Point> globalPositionList, ref int oldStart)
        {
#if RUN_MY_VIEW_CLIP || !DEBUG
            //為預防路線斷掉，擷取最開始與最後的點之間的的所有點
            int iStart = -1;
            int iEnd = -1;

            int iCount = 0;
            IEnumerator<Point> itr = globalPositionList.GetEnumerator();
            while (itr.MoveNext())
            {
                if (Math.Abs(mGlobalCentor.X - itr.Current.X) < mLongitude_Latitude_Range.X &&
                    Math.Abs(mGlobalCentor.Y - itr.Current.Y) < mLongitude_Latitude_Range.Y
                    )
                {
                    if (iStart < 0)
                        iStart = iCount;
                    iEnd = iCount;
                }
                iCount++;
            }
            itr.Dispose();

            List<Point> newPointCollection = new List<Point>(32);

            if (iStart < 0 || iEnd < 0)
            {
                if (oldStart == -1)
                {
#if DEBUG
                    MyUtility.Utility.OutputDebugString("GetLineInRange_GlobalPosition => after view culling lines : " + newPointCollection.Count);
#endif
                    return newPointCollection;
                }
                else
                {
#if DEBUG
                    MyUtility.Utility.OutputDebugString("GetLineInRange_GlobalPosition => use the oldStart : " + oldStart);
#endif
                    iStart = iEnd = oldStart;
                }
            }
            oldStart = iStart;

            iCount = 0;
            itr = globalPositionList.GetEnumerator();
            while (itr.MoveNext())
            {
                if (
                    (iCount >= iStart - 1)
                    &&
                    (iCount <= iEnd + 2)
                    )
                    newPointCollection.Add(itr.Current);

                if (iCount >= iEnd + 2)
                    break;

                iCount++;
            }
            itr.Dispose();

#if DEBUG
            MyUtility.Utility.OutputDebugString("GetLineInRange_GlobalPosition => after view culling lines : " + newPointCollection.Count);
#endif


            return newPointCollection;
#else
            PointCollection newPointCollection = new PointCollection();
            IEnumerator<Point> itr = mPointCollection_GlobalPosition.GetEnumerator();
            while (itr.MoveNext())
            {
                newPointCollection.Add(itr.Current);
            }
            itr.Dispose();
            return newPointCollection;
#endif
        }

        /// <summary>
        /// 將傳入PointCollection的經緯度位置，根據view的Scale，換成view的2D座標
        /// </summary>
        private PointCollection TranslateGlobalPositionToViewCoord(List<Point> outPointCollection)
        {
            PointCollection viewPoints = new PointCollection();

            //畫面座標左上角是0,0, max=Width, Height
            // Point viewCentor = new Point(Width * 0.5f, Height * 0.5f);

            IEnumerator<Point> itr = outPointCollection.GetEnumerator();
            //  Point nowCoord = new Point();
            while (itr.MoveNext())
            {
                //double ratio = (itr.Current.X - mGlobalCentor.X) / mLongitude_Latitude_Range.X;
                //ratio *= Width * 0.5;
                //nowCoord.X = viewCentor.X + ratio;

                //ratio = (itr.Current.Y - mGlobalCentor.Y) / mLongitude_Latitude_Range.Y;
                //ratio *= Height * 0.5;
                //nowCoord.Y = viewCentor.Y + ratio;
                Point nowCoord = _TranslateGlobalPositionToViewCoord(itr.Current);

                viewPoints.Add(nowCoord);
            }

            return viewPoints;
        }

        /// <summary>
        /// 轉換GPS座標->螢幕座標
        /// </summary>
        private Point _TranslateGlobalPositionToViewCoord(Point globalPos)
        {
            Point viewCentor = new Point(Width * 0.5f, Height * 0.5f);

            double ratio = (globalPos.X - mGlobalCentor.X) / mLongitude_Latitude_Range.X;
            ratio *= Width * 0.5;
            globalPos.X = viewCentor.X + ratio;

            ratio = (globalPos.Y - mGlobalCentor.Y) / mLongitude_Latitude_Range.Y;
            ratio *= Height * 0.5;
            globalPos.Y = viewCentor.Y + ratio;

            return globalPos;
        }

        //  float mRotateRec = 0;
        /// <summary>
        /// 設定control的旋轉角度
        /// </summary>
        public void SetRotationTransform(float degree)
        {
            if (float.IsNaN(degree) || float.IsInfinity(degree))
                return;

            Dispatcher.BeginInvoke(() =>
            {
                //fix目前角度皆為正0~360之間
                if (mRotateRoute.Angle < 0)
                {
                    mRotateRoute.Angle += 360;
                }

                if (mRotateRoute.Angle > 360)
                {
                    mRotateRoute.Angle -= 360;
                }

                //設定最短角度轉向
                if (degree < 0)
                {
                    degree += 360;
                }

                if (degree > 360)
                {
                    degree -= 360;
                }

                if (mRotateRoute.Angle < 180 && Math.Abs(mRotateRoute.Angle - degree) > 180)
                    degree -= 360;

                if (mRotateRoute.Angle > 180 && Math.Abs(degree - mRotateRoute.Angle) > 180)
                    mRotateRoute.Angle -= 360;

                DoubleAnimation anima = new DoubleAnimation();
                anima.From = mRotateRoute.Angle;
                anima.To = degree;
                //  mRotateRec += degree;

                anima.Duration = new Duration(TimeSpan.FromSeconds(1));  //设置动画耗时，这里为0.5秒

                // 设置附加属性
                Storyboard.SetTarget(anima, mRotateRoute);  //使动画与特定对象关联
                Storyboard.SetTargetProperty(anima, new PropertyPath(RotateTransform.AngleProperty));  //使动画与特定属性关联

                // 生成storyboard，把动画加进去并启动
                Storyboard storyboard = new Storyboard();
                storyboard.Children.Add(anima);
                storyboard.Begin();

            });
        }

#if DEBUG
        /// <summary>
        /// 計算目前位於的路線上，要將行進方向的路線設定成行進朝上，需要轉幾度
        /// 在2D座標上算角度的算法，耗效能，單存開發測試用
        /// </summary>
        public float GetNowRouteUpDegree(int onRouteIndex)
        {
            //   PointCollection pc = new PointCollection();
            //  foreach (Point ppp in mGlobalPositionList)
            //       pc.Add(ppp);
            PointCollection pc = TranslateGlobalPositionToViewCoord(mGlobalPositionList);

            if (onRouteIndex + 1 >= pc.Count())
                return 0;

            Microsoft.Xna.Framework.Vector2 A = new Microsoft.Xna.Framework.Vector2(
                (float)pc[onRouteIndex + 1].X - (float)pc[onRouteIndex].X,
                (float)pc[onRouteIndex + 1].Y - (float)pc[onRouteIndex].Y);
            A.Normalize();
            Microsoft.Xna.Framework.Vector2 B = Microsoft.Xna.Framework.Vector2.UnitY;//計算與(0,1)的夾角，(0,1)代表緯度朝上的向量
            float dotY = Microsoft.Xna.Framework.Vector2.Dot(A, Microsoft.Xna.Framework.Vector2.UnitY);
            float dotX = Microsoft.Xna.Framework.Vector2.Dot(A, Microsoft.Xna.Framework.Vector2.UnitX);

            float theta = (float)Math.Acos(dotY);
            float degree = Microsoft.Xna.Framework.MathHelper.ToDegrees(theta);
            if (dotY > 0)
            {
                if (dotX > 0)
                {
                    degree = -1 * degree;
                }
                else
                {
                    // degree = -1 * degree;
                }
            }
            else
            {
                if (dotX > 0)
                {
                    degree = 360 - degree;
                }
                else
                {
                    // degree = 360 - degree;
                }
            }

            return degree;
        }
#endif

        /// <summary>
        /// 儲存目前所在導航路線規畫的線段資訊
        /// </summary>
        public class _OnRouteData
        {
            /// <summary>
            /// 前一次計算，所在路線的ID(若是沒換路線那就會跟nowIndex一樣)
            /// </summary>
            public int oldIndex = 0;

            /// <summary>
            /// 目前所在路線的ID
            /// </summary>
            public int nowIndex = 0;

            /// <summary>
            /// 在路線上的投影點
            /// </summary>
            public Point onRoutePosition = new Point();

            /// <summary>
            /// 目前定位,到路線線段上的最短距離
            /// </summary>
            public double minDisLonLat;

            /// <summary>
            /// 目前所在線段上位置,往前算到起點的距離總和
            /// </summary>
            public double drivedDistanceMeter;

            /// <summary>
            /// 目前所在線段上已經走的距離
            /// </summary>
            public double nowRouteMovingDistanceMeter;

            /// <summary>
            /// 目前所在線段的距離長度
            /// </summary>
            public double nowRouteTotalDistanceMeter;


        }

        /// <summary>
        /// 更新目前路線上的Index (miNowIndexOnRoute)，回傳舊的index，
        /// 若是，都找不到目前位置歸屬的線段點，或是距離路寬太遠已經脫離路徑，就回傳目前路線ID為-1，
        /// 根據目前定位座標，找出對印在 規劃路線上的位置
        /// (搜尋方式為 https://skydrive.live.com/#!/view.aspx?cid=9F3D3CF3F9BC9DA2&resid=9F3D3CF3F9BC9DA2%211739&app=PowerPoint)
        /// http://www.cnblogs.com/lauer0246/archive/2009/06/24/1510363.html;
        /// http://www.verydemo.com/demo_c92_i95074.html;
        /// </summary>
        public _OnRouteData GetNowIndexOnRoute(double nowLongitude, double nowLatitude)
        {
            _OnRouteData outData = new _OnRouteData();

            if (miNowIndexOnRoute != -1)
                miOldIndexOnRoute = miNowIndexOnRoute;

            // CheckpointData nowData = mRouteCheckpointArray[miNowIndexOnRoute];
            // Point toPosition = mPointCollection_GlobalPosition[miNowIndexOnRoute + 1];

            //1.針對每個線段檢查内積，檢查線段起始，線段結束，是否在內積的範圍內
            //2.若是該線段有在範圍內，就計算點到線的最短距離
            //p.s. 檢查範圍為，miNowIndexOnRoute 前後各10段路線

            float minDistanceLonLat = 99999999;
            int closestRouteIndex = -1;
            Vector3 myPosition = new Vector3((float)nowLongitude, (float)nowLatitude, 0);
            Point onRoutePosition = new Point();

            //  bool inLineRange = false;


            //往前偵測10個
            if (miOldIndexOnRoute < mRouteSectionArray.Length)
            {
                for (int a = 0; a < 10; a++)
                {
                    int index = miOldIndexOnRoute + a;
                    if (index >= mRouteSectionArray.Length)
                        break;

                    RouteSectionData nowData = mRouteSectionArray[index];
                    Point p1 = mGlobalPositionList[index];
                    Point p2 = mGlobalPositionList[index + 1];

                    // bool inRange = 
                    _CheckDistance(index, ref minDistanceLonLat, ref closestRouteIndex, ref myPosition,
                    ref p1, ref p2, nowData, ref onRoutePosition);
                    //    if (inLineRange == false && inRange == true)
                    //         inLineRange = true;
                }

                //往後偵測10個
                for (int a = 0; a < 10; a++)
                {
                    int index = miOldIndexOnRoute - a;
                    if (index < 0)
                        break;

                    RouteSectionData nowData = mRouteSectionArray[index];
                    Point p1 = mGlobalPositionList[index];
                    Point p2 = mGlobalPositionList[index + 1];

                    //     bool inRange = 
                    _CheckDistance(index, ref minDistanceLonLat, ref closestRouteIndex, ref myPosition,
                    ref p1, ref p2, nowData, ref onRoutePosition);
                    //     if (inLineRange == false && inRange == true)
                    //         inLineRange = true;
                }
            }

            //檢查check point半徑(有可能在夾角)  
            bool bInCorner = false;

            if (miOldIndexOnRoute < mGlobalPositionList.Count)
            {
                //往前偵測10個
                for (int a = 0; a < 10; a++)
                {
                    int index = miOldIndexOnRoute + a;
                    if (index >= mGlobalPositionList.Count)
                        break;

                    Point point = mGlobalPositionList[index];
                    _CheckDistance(index, ref minDistanceLonLat, ref closestRouteIndex,
                                   ref myPosition, ref point, ref onRoutePosition, ref bInCorner);
                }

                //往後偵測10個
                for (int a = 0; a < 10; a++)
                {
                    int index = miOldIndexOnRoute - a;
                    if (index < 0)
                        break;

                    Point point = mGlobalPositionList[index];
                    _CheckDistance(index, ref minDistanceLonLat, ref closestRouteIndex,
                                   ref myPosition, ref point, ref onRoutePosition, ref bInCorner);
                }
            }

            miNowIndexOnRoute = closestRouteIndex;

            outData.nowIndex = miNowIndexOnRoute;
            outData.oldIndex = miOldIndexOnRoute;
            outData.onRoutePosition.X = onRoutePosition.X;
            outData.onRoutePosition.Y = onRoutePosition.Y;

            //計算行進的總和
            int OK_Index = miNowIndexOnRoute;
            if (miNowIndexOnRoute < 0 || miNowIndexOnRoute >= mGlobalPositionList.Count())
            {
                return null;
                //OK_Index = miOldIndexOnRoute;
            }

            RouteSectionData nowRouteData = null;
            outData.drivedDistanceMeter = 0;
            for (int a = 0; a < OK_Index; a++)
            {
                nowRouteData = mRouteSectionArray[a];
                outData.drivedDistanceMeter += nowRouteData.LengthMeter;
            }

            if (bInCorner == false)
            {
                Point nowRoutePoint = mGlobalPositionList[OK_Index];
                Vector3 myVec = new Vector3((float)(myPosition.X - nowRoutePoint.X), (float)(myPosition.Y - nowRoutePoint.Y), 0);
                //outData.nowRouteMovingDistance =
                //    Vector3.Dot(myVec, mRouteSectionArray[OK_Index].normallizeDir);
                outData.nowRouteMovingDistanceMeter = Utility.LonLat2Meter(
                    nowLongitude,
                    nowLatitude,
                    mRouteSectionArray[OK_Index].startPoint.X,
                    mRouteSectionArray[OK_Index].startPoint.Y);
            }
            else
            {
                //位於轉角，的資料計算
                outData.nowRouteMovingDistanceMeter = 0;
            }



            outData.drivedDistanceMeter += outData.nowRouteMovingDistanceMeter;

            //outData.nowRouteTotalDistance = -1;//沒有section 表示已經到終點ㄌ(外部可以根據這個判斷到達終點ㄉ處理)
            if (OK_Index < mRouteSectionArray.Length)
                outData.nowRouteTotalDistanceMeter = mRouteSectionArray[OK_Index].LengthMeter;

            outData.minDisLonLat = minDistanceLonLat;

            return outData;
        }

        private static void _CheckDistance(int index, ref float minDistance, ref int closestRouteIndex,
            ref Vector3 myPosition, ref Point point, ref Point onRoutePosition, ref bool inCorner)
        {
            double diffX = myPosition.X - (float)point.X;
            double diffY = myPosition.Y - (float)point.Y;

            double newDistanceSquare = diffX * diffX + diffY * diffY;

            if (//newDistanceSquare < ROAD_WIDTH * ROAD_WIDTH &&
                newDistanceSquare < minDistance * minDistance)
            {
                closestRouteIndex = index;
                minDistance = (float)Math.Sqrt(newDistanceSquare);
                onRoutePosition.X = point.X;
                onRoutePosition.Y = point.Y;
                inCorner = true;
            }
        }

        private static bool _CheckDistance(int index, ref float minDistanceLonLat, ref int closestRouteIndex,
            ref Vector3 myPosition,
            ref  Point p1, ref Point p2, RouteSectionData nowData, ref Point onRoutePosition)
        {
            //檢查目前線段內，二個方向的內積
            Vector3 vec1, vec2;
            vec1 = vec2 = Vector3.Zero;
            float dot1, dot2;

            vec1.X = myPosition.X - (float)p1.X;
            vec1.Y = myPosition.Y - (float)p1.Y;
            dot1 = Vector3.Dot(nowData.vector, vec1);

            vec2.X = myPosition.X - (float)p2.X;
            vec2.Y = myPosition.Y - (float)p2.Y;
            dot2 = Vector3.Dot(nowData.vector * -1, vec2);

            bool bInRange = false;
            if(float.IsNaN(dot1) || float.IsNaN(dot2))
            {
#if DEBUG
                Utility.ShowMessageBox("GetNowIndexOnRoute 造成 float.IsNaN(dot1) || float.IsNaN(dot2)");
#endif
                return bInRange;
            }
            
            //若是在範圍內，就計算點到線的距離
            if (dot1 > 0 && dot2 > 0)
            {
                Ray ray = new Ray(myPosition, nowData.plane.Normal);
                float dot = Vector3.Dot(vec1, nowData.plane.Normal);
                if (float.IsNaN(dot))
                {
#if DEBUG
                    Utility.ShowMessageBox("GetNowIndexOnRoute 造成 float.IsNaN(dot)");
#endif
                    return bInRange;
                }
                else if (dot > 0)
                {
                    ray.Direction *= -1;
                }
               


                float? distance = ray.Intersects(nowData.plane);
                if (distance.HasValue &&
                    // distance.Value * distance.Value < ROAD_WIDTH * ROAD_WIDTH &&
                    distance.Value < minDistanceLonLat)
                {
                    closestRouteIndex = index;
                    minDistanceLonLat = distance.Value;
                    onRoutePosition.X = ray.Position.X + ray.Direction.X * distance.Value;
                    onRoutePosition.Y = ray.Position.Y + ray.Direction.Y * distance.Value;
                    bInRange = true;
                }
            }
            return bInRange;
        }

#if DEBUG
        public void RenderDebug_MyWay()
        {
            RenderDebug_MyWay(null, Utility.ROAD_WIDTH_LonLat);
        }

        //   private List<Point> mPointCollection_RenderMyWay = null;
        //    private Polyline mPolylineMe_Debug = null;//用來將我目前處於的線段 標示出來
        //   private Polyline mPolylineUp_Debug = null;//用來將我目前處於的線段 標示出來
        // private PointCollection myWayPoint = new PointCollection();
        public void RenderDebug_MyWay(DriveNoticeControl.DriveNoticeData driveNoticeData, float roadWidth)
        //int iNowIndexOnRoute, List<Point> pointCollection_RenderMyWay)
        {
            //畫出每個路線點的cross product 
            List<Point> pointList = new List<Point>(mRouteSectionArray.Length);
            // PointCollection pointsCrossProduct = new PointCollection();
            int count = 0;
            foreach (RouteSectionData data in mRouteSectionArray)
            {
                Point position = mGlobalPositionList[count + 1];
                //pointsCrossProduct.Add(position);
                pointList.Add(position);

                position.X = position.X + data.plane.Normal.X * roadWidth;
                position.Y = position.Y + data.plane.Normal.Y * roadWidth;
                //pointsCrossProduct.Add(position);
                pointList.Add(position);

                count++;
            }

            int oldStart = -1;
            List<Point> pointsCrossProduct = GetLineInRange_GlobalPosition(pointList, ref oldStart);
            mPolylineMe_Debug.Points = TranslateGlobalPositionToViewCoord(pointsCrossProduct);

            //有driveNoticeData表示目前線段有轉彎點
            if (driveNoticeData != null && driveNoticeData.onRouteID > 0)
            {
                RouteSectionData routeSectionData = mRouteSectionArray[driveNoticeData.onRouteID];

                List<Point> pointCollection = new List<Point>();
                Point turnPoint = new Point(driveNoticeData.longitude, driveNoticeData.latitude);
                pointCollection.Add(turnPoint);

                turnPoint.X = turnPoint.X + routeSectionData.normallizeDir.X * Utility.TURN_NOTICE_DIS_METER;
                turnPoint.Y = turnPoint.Y + routeSectionData.normallizeDir.Y * Utility.TURN_NOTICE_DIS_METER;
                pointCollection.Add(turnPoint);
                mPolylineUp_Debug.Points = TranslateGlobalPositionToViewCoord(pointCollection);
            }
            else
                mPolylineUp_Debug.Points.Clear();


            //  Dispatcher.BeginInvoke(() =>
            //    {
            //System.Windows.Media.PointCollection pointsMyWay = new System.Windows.Media.PointCollection();
            //pointsMyWay.Add(pointCollection_RenderMyWay[iNowIndexOnRoute + 0]);
            //pointsMyWay.Add(pointCollection_RenderMyWay[iNowIndexOnRoute + 1]);

            // if (mPolylineMe_Debug == null)
            // {
            // mPolylineMe_Debug = new Polyline();
            // SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(100, 0, 255, 0));
            //  mPolylineMe_Debug.Stroke = brush;
            //  mPolylineMe_Debug.StrokeThickness = 5;
            //  mPolylineMe_Debug.FillRule = FillRule.EvenOdd;
            //  mRenderRetangle.Children.Add(mPolylineMe_Debug);
            // }

            //if (mPolylineUp_Debug == null)
            //{
            //    mPolylineUp_Debug = new Polyline();
            //    SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(100, 0, 0, 255));
            //    mPolylineUp_Debug.Stroke = brush;
            //    mPolylineUp_Debug.StrokeThickness = 5;
            //    mPolylineUp_Debug.FillRule = FillRule.EvenOdd;
            //    mRenderRetangle.Children.Add(mPolylineUp_Debug);
            //}

            //PointCollection pointsUPWay = new PointCollection();
            //pointsUPWay.Add(pointCollection_RenderMyWay[0]);
            //pointsUPWay.Add(new Point(pointCollection_RenderMyWay[0].X, pointCollection_RenderMyWay[0].Y + 1));
            //mPolylineUp_Debug.Points = TranslateGlobalPositionToViewCoord(pointsUPWay);

            //if (iNowIndexOnRoute < mRoutePolyline.Points.Count - 1)
            //{
            //    mPolylineMe_Debug.Points = TranslateGlobalPositionToViewCoord(pointsMyWay);
            //}


            // });
        }
#endif

        Color mNowColor = Color.FromArgb(255, 255, 255, 255);
        public void Interface_SetColor(Color color)
        {
            SolidColorBrush solidColorBrush = new SolidColorBrush(color);
            mNowColor = color;
            mRoutePolyline.Stroke = solidColorBrush;

            mForEditorTurnCircle.Stroke = mForEditorTurnCircleSmall.Stroke = solidColorBrush;

            //轉彎ㄉ圓圈圈
            foreach (UIElement uie in LayoutRoot.Children)
            {
                Ellipse circle = uie as Ellipse;
                if (circle != null)
                {
                    TurnCircleData turnCircleData = circle.Tag as TurnCircleData;
                    if (turnCircleData != null && turnCircleData.bBigCircle == true
                        )
                        circle.Stroke = solidColorBrush;
                }
            }
        }
    }



}
