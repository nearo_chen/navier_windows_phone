﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompassApp
{
    public class CompassType
    {
        /// <summary>
        /// 看以後有幾種指南針樣式都編號在這邊
        /// </summary>
        public enum COMPASS_TYPE
        {
            Big,
            Small,//目前沒用到
        }
    }
}
