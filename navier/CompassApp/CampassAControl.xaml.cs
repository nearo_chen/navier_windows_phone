﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyUtility;
using System.Windows.Media;
using System.Windows.Input;

namespace CompassApp
{
    public partial class CampassAControl : UserControl, ISetColor, ISetDragColor
    {
        MyCompass mMyCompass = null;

        public static CampassAControl AddToRoot(System.Windows.Controls.Panel layoutRoot, int viewPosX, int viewPosY,
            int w, int h)
        {
            CampassAControl campassAControl = new CampassAControl();
            layoutRoot.Children.Add(campassAControl);

            //移動control座標
            Utility.SetControlPosition(campassAControl, viewPosX, viewPosY, true);
           // Point wh = Utility.PercentToResulution(WRatio, HRatio);
            Utility.SetControlWH(campassAControl, w, h, true);
            //Utility.SetControlScale(campassAControl, 0.5f, 0.5f, true);
            return campassAControl;
        }

        //public static void RemoveFromRoot(CampassAControl campassAControl)
        //{
        //    System.Windows.Controls.Panel layoutRoot = campassAControl.Parent as System.Windows.Controls.Panel;
        //    layoutRoot.Children.Remove(campassAControl);
        //}

        public CampassAControl()
        {
            InitializeComponent();

            Width = Height = 80;
        }

        private bool _initOnce = false;
        public void initCompass()
        {
            if (_initOnce)
                return;
            _initOnce = true;
            mMyCompass = MyCompass.Create();
            mMyCompass.init(true, Callback_Compass_CurrentValueChanged);
        }

        public void SetRotationAngle(float angle)
        {
            mCompassRotation.Rotation = angle -90;
        }

        public void Callback_Compass_CurrentValueChanged(float degree)
        {
            SetRotationAngle(degree);
        }

        public void Interface_SetColor(System.Windows.Media.Color color)
        {
            mOutsideCircle.Stroke = mOutsideCircle_Line.Stroke = 
                mNorth.Foreground = mSouth.Foreground = mEast.Foreground = mWest.Foreground
                = mOutsideCircle.Fill = mInsideCircle.Stroke = new SolidColorBrush(color);
        }

        //************************************************************************************************
        // 為了編輯介面可以換顏色的功能
        //************************************************************************************************
        public void SetDragColor()
        {
            Interface_SetColor(Utility.DragControl_Color);

            mBackGroundColor.Fill = Utility.DragControl_ColorBrush;
            mBackGroundColor.Stroke = null;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        public void RevertDragColor()
        {
            Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));

            //用成這樣才不會有點不到的現象(透空的地方點不到)
            mBackGroundColor.Fill = new SolidColorBrush(Color.FromArgb(5, 0, 0, 0));
            //mBackGroundColor.Stroke = //Utility.DragControl_ColorBrush;
            //mBackGroundColor.StrokeThickness = 10;
            mBackGroundColor.Visibility = System.Windows.Visibility.Visible;
            mBackGroundColor.Opacity = 1f;
        }

        //************************************************************************************************
        // 為了編輯介面可以拖拉的功能
        //************************************************************************************************ 
        private void UserControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Utility.DragableGrid_ControlSelected(this);
        }
    }
}
