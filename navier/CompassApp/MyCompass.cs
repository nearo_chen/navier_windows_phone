﻿using Microsoft.Devices.Sensors;
using MyUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

using Windows.Foundation;

namespace CompassApp
{
    class MyCompass : IDisposable
    {
        private Windows.Devices.Sensors.Inclinometer _inclinometer;
        private float _rollDegree = 0;

        Microsoft.Devices.Sensors.Compass mCompass = null;
        bool mIsLandscape = true;

        /// <summary>
        /// 給CAMPOSS的更新朝向用，degree會計算目前朝向與北朝上差了幾度
        /// </summary>
        public delegate void Callback_Compass_CurrentValueChanged(float degree);
        Callback_Compass_CurrentValueChanged mCallback_Compass_CurrentValueChanged = null;

        public static MyCompass Create()
        {
            return new MyCompass();
        }

        private MyCompass()
        {
            //水平感測
            _inclinometer = Windows.Devices.Sensors.Inclinometer.GetDefault();
            if (_inclinometer != null)
            {
                // Establish the report interval for all scenarios
                uint minReportInterval = _inclinometer.MinimumReportInterval;
                uint reportInterval = minReportInterval > 100 ? minReportInterval : 100;
                _inclinometer.ReportInterval = reportInterval;

                // Establish the event handler
                _inclinometer.ReadingChanged +=
                    new TypedEventHandler
                        <Windows.Devices.Sensors.Inclinometer, Windows.Devices.Sensors.InclinometerReadingChangedEventArgs>
                        (_readingChanged);
            }
        }

        ~MyCompass()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (mCompass != null)
                mCompass.Dispose();
            mCompass = null;
            mCallback_Compass_CurrentValueChanged = null;
        }



        public void init(bool isLandscape, Callback_Compass_CurrentValueChanged callback_Compass_CurrentValueChanged)
        {
            if (!Compass.IsSupported)
            {
                // The device on which the application is running does not support
                // the compass sensor. Alert the user and hide the
                // application bar.
                //   Utility.ShowMessageBox();
                Utility.ShowToast("device does not support compass", 3000, true);
                return;
            }

            SetIfLandscape(isLandscape);

            mCallback_Compass_CurrentValueChanged = callback_Compass_CurrentValueChanged;

            if (mCompass == null)
            {
                // Instantiate the compass.
                mCompass = new Microsoft.Devices.Sensors.Compass();

                // Specify the desired time between updates. The sensor accepts
                // intervals in multiples of 20 ms.
                mCompass.TimeBetweenUpdates = TimeSpan.FromMilliseconds(500);

#if DEBUG
                // The sensor may not support the requested time between updates.
                // The TimeBetweenUpdates property reflects the actual rate.
                Utility.OutputDebugString("[MyCompass_init()]---the actual TimeBetweenUpdates rate:" + mCompass.TimeBetweenUpdates.TotalMilliseconds + " ms");
#endif

                mCompass.CurrentValueChanged +=
                    new EventHandler<SensorReadingEventArgs<CompassReading>>(compass_CurrentValueChanged);
                mCompass.Calibrate +=
                    new EventHandler<CalibrationEventArgs>(compass_Calibrate);//如果系统检测到罗盘航向精度大于 +/20 度，则引发此事件
            }

            try
            {
                mCompass.Start();
            }
            catch (InvalidOperationException)
            {
                Utility.ShowMessageBox("unable to start compass.");
            }
        }

        const float _rollThreshold = 30;
        float? _rollDegreeOld = null;
        private void _readingChanged(object sender, Windows.Devices.Sensors.InclinometerReadingChangedEventArgs e)
        {
            Windows.Devices.Sensors.InclinometerReading reading = e.Reading;
            if (_rollDegreeOld == null)
            {
                if (reading.RollDegrees < 0)
                    _rollDegree = 0;
                else if (reading.RollDegrees > 0)
                    _rollDegree = 180;
            }
            else
            {
                if (reading.RollDegrees < -_rollThreshold && _rollDegreeOld >= -_rollThreshold)
                    _rollDegree = 0;
                else if (reading.RollDegrees > _rollThreshold && _rollDegreeOld <= _rollThreshold)
                    _rollDegree = 180;
            }
            _rollDegreeOld = reading.RollDegrees;
        }


        private void compass_CurrentValueChanged(object sender, SensorReadingEventArgs<CompassReading> e)
        {
            if (mCompass.IsDataValid != true)
                return;

            // Note that this event handler is called from a background thread
            // and therefore does not have access to the UI thread. To update 
            // the UI from this handler, use Dispatcher.BeginInvoke() as shown.
            // Dispatcher.BeginInvoke(() => { statusTextBlock.Text = "in CurrentValueChanged"; });

            double mTrueHeading = e.SensorReading.TrueHeading;

            if (mCallback_Compass_CurrentValueChanged != null)
            {
                System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (mIsLandscape)
                    {
                        //http://msdn.microsoft.com/zh-tw/magazine/jj190804.aspx 這裡說的這樣可以得到角度
                        //transform.Angle = 360 - TrueHeading;

                        double degree = 360 - mTrueHeading +_rollDegree;

                        //degree -= 90;

                        mCallback_Compass_CurrentValueChanged((float)degree);
                    }
                    else
                    {
                        //Portrait 目前不處理
                    }
                });
            }
        }

        bool showCalibrateOnce = false;

        /// <summary>
        /// 如果系统检测到罗盘航向精度大于 +/20 度，则引发此事件
        /// </summary>
        void compass_Calibrate(object sender, CalibrationEventArgs e)
        {
            if (showCalibrateOnce)
                return;

            System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                Utility.OutputDebugString("you need to calibrate compass");
                showCalibrateOnce = true;
            });

        }

        public void SetIfLandscape(bool isLandscape)
        {
            mIsLandscape = isLandscape;
        }
    }
}
