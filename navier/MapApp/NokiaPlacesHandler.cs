﻿using Microsoft.Phone.Net.NetworkInformation;
using MyUtility;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MapApp
{
    public class NokiaPlacesHandler
    {
        //---------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------
        [DataContract]
        public class NokiaPlacesGeoCollection
        {
            public NokiaPlacesGeoCollection() { }

            [DataMember]
            public NokiaPlaces_results results { get; set; }
        }

        [DataContract]
        public class NokiaPlaces_results
        {
            public NokiaPlaces_results() { }

            [DataMember]
            public List<NokiaPlaces_items> items { get; set; }
        }

        [DataContract]
        public class NokiaPlaces_items
        {
            public NokiaPlaces_items() { }

            [DataMember]
            public List<float> position { get; set; }

            [DataMember]
            public string title { get; set; }

            [DataMember]
            public string vicinity { get; set; }
        }
        //---------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------


        private MapApp.GeocodeQueryCombine.Callback_QuerySearchTerm mCallback_QuerySearchTerm = null;

        //---------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 搜尋路標，住址
        /// GeocodeQuery can be used to search location for a given search term, for example a town name or street address.
        /// </summary>
        public void SearchForTerm(String sSearchTerm, GeoCoordinate myCoordinate, MapApp.GeocodeQueryCombine.Callback_QuerySearchTerm callback_QuerySearchTerm)
        {
            mCallback_QuerySearchTerm = callback_QuerySearchTerm;


            GeocodingQuery_NokiaPlaces(myCoordinate.Longitude, myCoordinate.Latitude, sSearchTerm, mCallback_QuerySearchTerm);


#if DEBUG
            MyUtility.Utility.OutputDebugString("[geoQuery_SearchForTerm]----" + sSearchTerm + "(" + myCoordinate.Latitude + " ; " + myCoordinate.Longitude + ")");
#endif
        }

        //---------------------------------------------------------------------------------------------------------------------------------
        public void GeocodingQuery_NokiaPlaces(double longitude, double latitude, string pAddress,
            MapApp.GeocodeQueryCombine.Callback_QuerySearchTerm callback_QuerySearchTerm)
        {
            if (Utility.IsNetworkAvailable())
            {
                // build URL for Here.net REST service
                string currentgeoLoc = latitude.ToString() + "," + longitude.ToString();

                //這裡的ID和 CODE，可以換成MapsSettings.ApplicationContext.ApplicationId以及AuthenticationToken
                //看情況要是真的流量過大再考慮更換，應為使用NOKIA APP開發的好處是，他的網站上可以看見流量
                string appID = "0wQGDP1AkvQZ4ZDR3a0o"; // MAKE SURE TO GET YOUR OWN from developers.here.net
                object appCode = "9PEl2dZ36IKu29ln5wNMWA"; // MAKE SURE TO GET YOUR OWN from developers.here.net
                if (Utility.mHideAdvertisement)
                {
                    appID = "jx2Zg4kFgmTQPMymY07J";
                    appCode = "NGXBGrgiyOFWo-6bhAP5dw";
                }

                string hereNetUrl = string.Format(
                     "http://demo.places.nlp.nokia.com/places/v1/discover/search?at={0}&q={1}&app_id={2}&app_code={3}&accept=application/json",
                        currentgeoLoc, pAddress, appID, appCode);


                // get data from HERE.net REST API
                WebClient tWebClient = new WebClient();
                tWebClient.Headers["Accept-Language"] = Utility.GetCurrentLanguageRegionName();//做localization ref:http://developer.nokia.com/Community/Discussion/showthread.php/234951-API-Globalization-and-Language
                tWebClient.DownloadStringCompleted +=
                                    new DownloadStringCompletedEventHandler(tWebClient_DownloadStringCompleted);
                tWebClient.DownloadStringAsync(new Uri(hereNetUrl));
            }
            else
            {
                callback_QuerySearchTerm(null, this);
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// 取得指定URL回傳的結果，觸發事件。
        /// </summary>
        void tWebClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            //for test
            // mCallback_QuerySearchTerm(null, this);
            //  return;

            NokiaPlacesGeoCollection tCollection = null;

            if (e.Error == null)
            {
                try
                {
                    tCollection = new NokiaPlacesGeoCollection();
                    DataContractJsonSerializer tJsonSerial = new DataContractJsonSerializer(typeof(NokiaPlacesGeoCollection));
                    MemoryStream tMS = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));
                    tCollection = tJsonSerial.ReadObject(tMS) as NokiaPlacesGeoCollection;
                    //判斷搜尋結果不為Null時，才是真正可以使用的資料
                    if (tCollection != null && tCollection.results != null)
                    {
                        List<MapApp.GeocodeQueryCombine.SearchResult> result_list = new List<GeocodeQueryCombine.SearchResult>();

                        foreach (NokiaPlaces_items item in tCollection.results.items)
                        {
                            GeocodeQueryCombine.SearchResult data = new GeocodeQueryCombine.SearchResult();

                            string[] devid = new string[] { "<br/>" };
                            if (item.vicinity != null)
                            {
                                string[] sss = item.vicinity.Split(devid, StringSplitOptions.None);

                                if (sss.Length == 1)
                                {
                                    data.City = "";
                                    data.District = sss[0];
                                    data.Street = "";
                                }
                                else if (sss.Length == 2)
                                {
                                    data.City = sss[1];
                                    data.District = sss[0];
                                    data.Street = "";
                                }
                                else
                                {
                                    data.City = sss[2];
                                    data.District = sss[1];
                                    data.Street = sss[0];
                                }
                                data.formatted_address = item.title + ", " + data.Street;
                            }
                            else
                            {
                                data.formatted_address = item.title;
                            }

                            data.lat = item.position[0].ToString();
                            data.lng = item.position[1].ToString();

                            result_list.Add(data);
                            //result_list.Insert(0, data);
                        }

                        mCallback_QuerySearchTerm(result_list, this);
                    }
                }
                catch (Exception ex)
                {
                    //Utility.ShowMessageBox("[NokiaPlacesHandler::tWebClient_DownloadStringCompleted] =>" + ex.Message);
                    mCallback_QuerySearchTerm(null, this);
                }
            }
            else
            {
                mCallback_QuerySearchTerm(null, this);
            }
        }
    }
}
