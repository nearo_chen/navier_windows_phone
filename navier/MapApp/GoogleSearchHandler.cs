﻿using MyUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace MapApp
{
    public partial class GoogleSearchHandler
    {
        private MapApp.GeocodeQueryCombine.Callback_QuerySearchTerm mCallback_QuerySearchTerm = null;

        // private const string googlemapkey = "AIzaSyAH8DmeYmsN6w50LjWku_Wha-cUSRUezcM";

        string mRegionRec;

        /// <summary>
        /// 搜尋地址，並轉換成座標。
        /// </summary>
        public void GeocodingQuery_GOOGLEMAP(double longitude, double latitude, string pAddress, MapApp.GeocodeQueryCombine.Callback_QuerySearchTerm callback_QuerySearchTerm)
        {
            //if (DeviceNetworkInformation.IsWiFiEnabled ||
            //    DeviceNetworkInformation.IsCellularDataEnabled ||
            //    DeviceNetworkInformation.IsNetworkAvailable)
            // {

            if (Utility.IsNetworkAvailable())
            {
                try
                {

                    //先用周邊請求，沒問到在用文本請求
                //https://developers.google.com/places/documentation/search 
 
                    //https://developers.google.com/maps/documentation/geocoding/?hl=zh-tw

                    string language = Utility.GetCurrentLanguageRegionName();
                    string region = Utility.GetCurrentRegionName();
                    string gSearchURL = "http://maps.googleapis.com/maps/api/geocode/json?latlng={0},{1}&language={2}&address={3}&region={4}&sensor=true";

                    mCallback_QuerySearchTerm = callback_QuerySearchTerm;

                    mRegionRec = region;

                    gSearchURL = string.Format(gSearchURL, latitude, longitude, language, pAddress, region);
                    WebClient tWebClient = new WebClient();
                    tWebClient.DownloadStringCompleted +=
                                new DownloadStringCompletedEventHandler(tWebClient_DownloadStringCompleted);
                    tWebClient.DownloadStringAsync(new Uri(gSearchURL));
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                callback_QuerySearchTerm(null, this);
            }


            // }
        }

        /// <summary>
        /// 取得指定URL回傳的結果，觸發事件。
        /// </summary>
        void tWebClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            //for test
            //mCallback_QuerySearchTerm(null, this);
            //return;

            GAddressGeoCollection tCollection = null;

            if (e.Error == null)
            {
                try
                {
                    tCollection = new GAddressGeoCollection();
                    DataContractJsonSerializer tJsonSerial = new DataContractJsonSerializer(typeof(GAddressGeoCollection));
                    MemoryStream tMS = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));
                    tCollection = tJsonSerial.ReadObject(tMS) as GAddressGeoCollection;
                    //判斷搜尋結果不為Null時，才是真正可以使用的資料
                    if (tCollection != null && tCollection.results != null)
                    {
                        // tCollection
                        List<MapApp.GeocodeQueryCombine.SearchResult> results_list = new List<GeocodeQueryCombine.SearchResult>();

                        foreach (GAddressGeoResult item in tCollection.results)
                        {
                            //對於google的資料型態來說，https://developers.google.com/maps/documentation/geocoding/?hl=zh-TW#JSON
                            //抓最後1個political，當作city,倒數第2個當作district
                            string sLast1Political = "";
                            string sLast2Political = "";
                            string street, destrict, city;
                            street = "";
                           // bool bGotoNext = false;
                            foreach (GoogleSearchHandler.GAddressComponent component in item.address_components)
                            {
                                //過濾掉非正確地區位址
                                //if (component.types.Contains("political") && component.types.Contains("country") &&
                                //    component.short_name != mRegionRec)
                                //{
                                //    bGotoNext = true;
                                //    break;
                                //}

                                if (component.types.Contains("political"))
                                {
                                    sLast2Political = sLast1Political;
                                    sLast1Political = component.long_name;
                                }
                                else if (component.types.Contains("street_address"))
                                {
                                    street = component.long_name;
                                }
                            }
                          //  if (bGotoNext)
                          //      continue;

                            city = sLast1Political;
                            destrict = sLast2Political;

                            if (destrict != "")
                            {
                                if (street != "")
                                    street += ", ";
                                street += destrict;
                            }

                            if (city != "")
                            {
                                if (street != "")
                                    street += ", ";
                                street += city;
                            }

                            //-------------------------------------------------------------
                            GeocodeQueryCombine.SearchResult data = new GeocodeQueryCombine.SearchResult();
                            data.City = city;
                            data.District = destrict;
                            data.Street = street;
                            data.formatted_address = item.formatted_address;

                            data.lat = item.geometry.location.lat;
                            data.lng = item.geometry.location.lng;

                           // results_list.Add(data);//去搜尋taoyuan會出現大陸的桃園在第一位
                            results_list.Insert(0, data);
                        }


                        mCallback_QuerySearchTerm(results_list, this);
                    }
                }
                catch (Exception ex)
                {
                    //Utility.ShowMessageBox("[GoogleSearchHandler::tWebClient_DownloadStringCompleted] =>" + ex.Message);
                    mCallback_QuerySearchTerm(null, this);
                }
            }
            else
            {
                mCallback_QuerySearchTerm(null, this);
            }
        }






    }
}
