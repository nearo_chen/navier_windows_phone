﻿//#define POSITION_SHIFT_TEST//每次position change都定位越偏越遠，預期造成路線重新規劃
//#define POSITION_SHIFT_TEST_THREAD//將POSITION_SHIFT_TEST做的事改為thread裡面跑

using Microsoft.Phone.Maps.Services;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Windows.Devices.Geolocation;
using Windows.Foundation;



namespace MapApp
{
    /// <summary>
    /// Get the phone's current location.
    /// </summary>
    public class MyGeolocatorTracking : IDisposable
    {
#if PCTEST
        //, 
        double PCTEST_LAT = 24.986797;//25.01342;
        double PCTEST_LON = 121.539484;//121.468982;
#endif

        private TypedEventHandler<Geolocator, StatusChangedEventArgs> mStatusChangedHandler = null;


        public delegate void Callback_QueryGlobalPosition(ref System.Windows.Point outPoint);
        public delegate void Callback_PositionChanged(ref System.Windows.Point outPoint, double speedMeterPerSec, double accuracyMeter);
        private Callback_PositionChanged mCallback_PositionChanged;

        private TypedEventHandler<Geolocator, PositionChangedEventArgs> mPositionChangedHandler = null;

        private Geolocator mGeolocator = null;
        //  private Geoposition mGeoPosition = null;


        /// <summary>
        /// 傳入幾豪秒要定位一次，幾公尺要更新一次(小於 0 "表示不設定)
        /// </summary>
        private MyGeolocatorTracking(int intervalMS, int moveThreshold)
        {
            mGeolocator = new Geolocator();
            mGeolocator.DesiredAccuracy = PositionAccuracy.High;

            _setTrackingTimeDistance(intervalMS, moveThreshold);

            mStatusChangedHandler = geolocator_StatusChanged;
            mGeolocator.StatusChanged += mStatusChangedHandler;

#if POSITION_SHIFT_TEST
        __positionchange_debug_timer.Interval = TimeSpan.FromSeconds(0.1);
        __positionchange_debug_timer.Tick += __positionchange_debug_timer_Tick;
            __positionchange_debug_timer.Start();
#endif


        }

#if POSITION_SHIFT_TEST_THREAD
        System.Threading.Thread POSITION_SHIFT_TEST_THREAD;
        private void Start_POSITION_SHIFT_TEST_THREAD()
        {
            POSITION_SHIFT_TEST_THREAD = new System.Threading.Thread(DoSomething_Start_POSITION_SHIFT_TEST_THREAD);
            POSITION_SHIFT_TEST_THREAD.Start();
        }

        private void Stop_POSITION_SHIFT_TEST_THREAD()
        {
            POSITION_SHIFT_TEST_THREAD.Abort();
        }

        double POSITION_SHIFT_TEST_THREAD_StartTime;
        private void DoSomething_Start_POSITION_SHIFT_TEST_THREAD()
        {
            POSITION_SHIFT_TEST_THREAD_StartTime = System.Environment.TickCount;

            while (true)
            {
                // Do stuff
                TimeSpan runTime = TimeSpan.FromMilliseconds(System.Environment.TickCount - POSITION_SHIFT_TEST_THREAD_StartTime);
                if (runTime.TotalMilliseconds < 300)
                    continue;
                else
                    POSITION_SHIFT_TEST_THREAD_StartTime = System.Environment.TickCount;

                System.Windows.Point gp = new System.Windows.Point(PCTEST_LAT, PCTEST_LON);

                //gpShift.X += 0.000500;
                //gpShift.Y += 0.000500;
                gp.X += gpShift.X;
                gp.Y += gpShift.Y;
                //  gp.X += position_shift_count_lat;
                //  gp.Y += position_shift_count_lon;

                if (mCallback_PositionChanged != null)
                    mCallback_PositionChanged(ref gp, 0, 0);
            }
        }
#endif

#if POSITION_SHIFT_TEST || POSITION_SHIFT_TEST_THREAD        
        System.Windows.Point gpShift = new System.Windows.Point();
#endif

#if POSITION_SHIFT_TEST
        bool _bEnablePosShift = false;
        public void EnablePosShiftTest()
        {
            _bEnablePosShift = true;
        }
        public void DisablePosShiftTest()
        {
            _bEnablePosShift = false;
        }

        private void __positionchange_debug_timer_Tick(object sender, EventArgs e)
        {
            if (_bEnablePosShift == false)
                return;

            System.Windows.Point gp = new System.Windows.Point(PCTEST_LAT, PCTEST_LON);

            gpShift.X += 0.000500;
            gpShift.Y += 0.000500;
            gp.X += gpShift.X;
            gp.Y += gpShift.Y;
            //  gp.X += position_shift_count_lat;
            //  gp.Y += position_shift_count_lon;

            if (mCallback_PositionChanged != null)
                mCallback_PositionChanged(ref gp, 0, 0);
        }
#else
        public void EnablePosShiftTest()
        {
            
        }
        public void DisablePosShiftTest()
        {
          
        }
#endif

        /// <summary>
        /// 傳入幾豪秒要定位一次，幾公尺要更新一次(小於 0 "表示不設定)
        /// </summary>
        private void _setTrackingTimeDistance(int intervalMS, int moveThreshold)
        {
            if (intervalMS >= 0)
                mGeolocator.ReportInterval = (uint)intervalMS;

            if (moveThreshold >= 0)
                mGeolocator.MovementThreshold = moveThreshold;// in meters ; 超过多少米引发位置改变事件

#if DEBUG
            if (intervalMS >= 0)
                MyUtility.Utility.OutputDebugString("更新時間間格為" + intervalMS + "毫秒");
            if (moveThreshold >= 0)
                MyUtility.Utility.OutputDebugString("更新距離為" + moveThreshold + "公尺");
#endif
        }

        ~MyGeolocatorTracking()
        {
            Dispose();
        }

        public void Dispose()
        {
            StopTracking();

            //if (_timer != null)
            //    _timer.Stop();
            //_timer = null;

            if (mPositionChangedHandler != null)
            {
                mGeolocator.PositionChanged -= mPositionChangedHandler;
                mPositionChangedHandler = null;
            }

            if (mStatusChangedHandler != null)
            {
                mGeolocator.StatusChanged -= mStatusChangedHandler;
                mStatusChangedHandler = null;
            }

            MyUtility.Utility.OutputDebugString("[geolocator_Dispose]----Dispose()");


            mGeolocator = null;
            //mGeoPosition = null;
        }

        public static MyGeolocatorTracking Create(int intervalMS, int moveThreshold)
        {
#if DEBUG
            MyUtility.Utility.OutputDebugString("[geolocator_Create]----new MyGeolocator()");
#endif
            return new MyGeolocatorTracking(intervalMS, moveThreshold);
        }


        //目前這個不使用了，耗時且有時GetGeopositionAsync不會return，不穩定
        //請改用 StartPositionTracking 方法
        //        /// <summary>
        //        /// 取得本機裝置目前的定位座標
        //        /// 根據實驗測試，有時使用這個GetGeopositionAsync會收不到回傳
        //        /// 如果可以的話，請直接使用StartPositionTracking()
        //        /// </summary>
        //        public async void QueryGlobalPosition(Callback_QueryGlobalPosition QueryDone)
        //        {

        ////            try
        ////            {
        ////#if DEBUG
        ////                MyUtility.Utility.OutputDebugString("MyGeolocator => await mGeolocator.GetGeopositionAsync start");
        ////#endif

        ////                mGeoPosition = await mGeolocator.GetGeopositionAsync(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(10));                

        ////#if DEBUG
        ////                MyUtility.Utility.OutputDebugString("MyGeolocator => await mGeolocator.GetGeopositionAsync done");
        ////#endif

        ////                System.Windows.Point outPoint = new System.Windows.Point(mGeoPosition.Coordinate.Latitude, mGeoPosition.Coordinate.Longitude);

        ////#if PCTEST //若是在電腦上用模擬器，定位都會壞掉，所以就直接給座標
        ////                outPoint = new System.Windows.Point(25.034866, 121.513081);
        ////#endif
        ////                QueryDone(ref outPoint);
        ////            }
        ////            catch (UnauthorizedAccessException)
        ////            {
        ////                MessageBox.Show("Location is disabled in phone settings or capabilities are not checked.");
        ////            }
        ////            catch (Exception ex)
        ////            {
        ////                // Something else happened while acquiring the location.
        ////                MessageBox.Show(ex.Message);
        ////            }

        //        }

        //每秒更新一次
        // private DispatcherTimer _timer = null;
        //  private int secondCount;

        /// <summary>
        /// 不斷追蹤目前定位是否改變
        /// </summary> 
        /// <param name="positionChangedCallback"></param>
        public void StartPositionTracking(Callback_PositionChanged positionChangedCallback)
        {
            mCheckIsInChina = false;
            if (mPositionChangedHandler != null)
                return;

            mCallback_PositionChanged = positionChangedCallback;

            mPositionChangedHandler = geolocator_PositionChanged;
            mGeolocator.PositionChanged += mPositionChangedHandler;

            //計時超過10秒要是還沒定位成功，就自動重來一次
            //if (_timer == null)
            //{
            //    _timer = new DispatcherTimer();
            //    _timer.Interval = TimeSpan.FromMilliseconds(1000);
            //    _timer.Tick += Timer_Tick;
            //}
            //_timer.Start();
            //secondCount = 0;

#if DEBUG
            iStartTrackingTime = Environment.TickCount;
            MyUtility.Utility.OutputDebugString("[定位開始][geolocator_StartPositionTracking]----time: " + iStartTrackingTime);
#endif

#if POSITION_SHIFT_TEST_THREAD
            Start_POSITION_SHIFT_TEST_THREAD();
#endif
        }

#if DEBUG
        int iStartTrackingTime = -1;
#endif

        //private void Timer_Tick(object sender, EventArgs e)
        //{
        //    if (mPositionChangedHandler == null)
        //        return;
        //    MyUtility.Utility.ShowProgressIndicator_updateInfo("gps..." + secondCount);
        //    secondCount++;
        //    if (secondCount > 10)//表示有在TRACKING中
        //    {
        //        MyUtility.Utility.ShowProgressIndicator_updateInfo("gps restart");
        //        Callback_PositionChanged rec = mCallback_PositionChanged;
        //        StopTracking();
        //        StartPositionTracking(rec);
        //    }
        //}

        public void StopTracking()
        {
#if POSITION_SHIFT_TEST_THREAD
            Stop_POSITION_SHIFT_TEST_THREAD();
#endif
            //mGeolocator.StatusChanged -= mStatusChangedHandler;
            //mStatusChangedHandler = null;

            MyUtility.Utility.OutputDebugString("[定位停止][geolocator_StopTracking]----");

            if (mPositionChangedHandler != null)
            {
                mGeolocator.PositionChanged -= mPositionChangedHandler;
                mPositionChangedHandler = null;
                mCallback_PositionChanged = null;
            }


            if (mStatusChangedHandler != null)
            {
                mGeolocator.StatusChanged -= mStatusChangedHandler;
                mStatusChangedHandler = null;
            }
        }

        // Reverse geocode query 用來判斷是否位於中國地區
        //http://blogs.msdn.com/b/jrspinella/archive/2012/11/02/using-reversegeocodequery-for-windows-phone-8.aspx
        private ReverseGeocodeQuery mMyReverseGeocodeQuery = null;
        private void ReverseGeocodeQuery_QueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
            ReverseGeocodeQuery rg = sender as ReverseGeocodeQuery;
            GeoCoordinate geocoord = rg.GeoCoordinate;

            System.Windows.Point gp = new System.Windows.Point();
            //MyUtility.Utility.GetCurrentDispatcher().BeginInvoke(() =>
            lock (mCheckIsInChinaLock)
            {
                if (mCheckIsInChina)
                    return;

                if (e.Error == null)
                {
                    if (e.Result.Count > 0)
                    {
                        MapAddress address = e.Result[0].Information.Address;
                        mIsInChina = false;
                        //http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3
                        if (address.CountryCode == "CHN")
                        {
                            mIsInChina = true;
                            geocoord = GeoOffset4China.GeoCoordinateOffset4China.GetOffsetCoordinate(geocoord);
                        }
                        //gp =
                        //#if DEBUG && PCTEST //若是在電腦上用模擬器，定位都會壞掉，所以就直接給座標
                        //                            new System.Windows.Point(25.01342, 121.468982);
                        //#else
                        //new System.Windows.Point(geocoord.Latitude, geocoord.Longitude);
                        //#endif
                        gp.X = geocoord.Latitude;
                        gp.Y = geocoord.Longitude;

                        //幫忙觸發一次position changed，因為第一次進入position change時，判斷是否位於china，跳過處理position change了
                        mCallback_PositionChanged_ConsiderChina(ref gp, 0, 0);
                    }
                }

                mCheckIsInChina = true;

                //已經判斷是否位於中國地區，就可以將mMyReverseGeocodeQuery釋放掉
                {
                    mCallback_PositionChanged_ConsiderChina = null;
                    mMyReverseGeocodeQuery.QueryCompleted -= ReverseGeocodeQuery_QueryCompleted;
                    mMyReverseGeocodeQuery.Dispose();
                    mMyReverseGeocodeQuery = null;
                }
            }
            //);
        }
        bool mIsInChina = false;
        bool mCheckIsInChina = false;
        object mCheckIsInChinaLock = new object();
        private Callback_PositionChanged mCallback_PositionChanged_ConsiderChina;

#if POSITION_SHIFT_TEST
        private DispatcherTimer __positionchange_debug_timer = new DispatcherTimer();
#endif

        void geolocator_PositionChanged(Geolocator sender, PositionChangedEventArgs eventArgs)
        {
            GeoCoordinate geocoordinate =
                new GeoCoordinate(eventArgs.Position.Coordinate.Latitude, eventArgs.Position.Coordinate.Longitude);

            lock (mCheckIsInChinaLock)
            {
                //第一次position change進來時，必須要拿目前座標，去判斷是否位於CHINA，才能回傳給外面的position change
                if (mCheckIsInChina == false &&
                    (mMyReverseGeocodeQuery == null || mMyReverseGeocodeQuery.IsBusy == false)
                    )
                {
                    mCallback_PositionChanged_ConsiderChina = mCallback_PositionChanged;

                    MyUtility.Utility.GetCurrentDispatcher().BeginInvoke(() =>
                    {
                        mIsInChina = false;
                        mMyReverseGeocodeQuery = new ReverseGeocodeQuery();
                        mMyReverseGeocodeQuery.GeoCoordinate =

#if DEBUG && PCTEST //若是在電腦上用模擬器，定位都會壞掉，所以就直接給座標
 new GeoCoordinate(PCTEST_LAT, PCTEST_LON);
#else
 new GeoCoordinate(geocoordinate.Latitude, geocoordinate.Longitude);
#endif
                        mMyReverseGeocodeQuery.QueryCompleted += ReverseGeocodeQuery_QueryCompleted;
                        mMyReverseGeocodeQuery.QueryAsync();
                    }
                    );
                }

                //在chack 是否位於china前，都不處理position changed
                if (mCheckIsInChina == false)
                    return;

                if (mIsInChina)
                    geocoordinate = GeoOffset4China.GeoCoordinateOffset4China.GetOffsetCoordinate(geocoordinate);

                System.Windows.Point gp =
                         new System.Windows.Point(geocoordinate.Latitude, geocoordinate.Longitude);


#if DEBUG
                MyUtility.Utility.OutputDebugString("[geolocator_PositionChanged]----" + gp.X + " ; " + gp.Y);
                if (iStartTrackingTime != -1)
                    MyUtility.Utility.OutputDebugString("[定位成功花了 : " + (Environment.TickCount - iStartTrackingTime) + " ms]");
                iStartTrackingTime = -1;
#endif

#if DEBUG && PCTEST //若是在電腦上用模擬器，定位都會壞掉，所以就直接給座標
                gp = new System.Windows.Point(PCTEST_LAT, PCTEST_LON);
#endif

                double speedMeterPerSec = 0;
                if (eventArgs.Position.Coordinate != null)
                {
                    if (eventArgs.Position.Coordinate.Speed == null ||
                        eventArgs.Position.Coordinate.Speed.HasValue == false ||
                        double.IsNaN(eventArgs.Position.Coordinate.Speed.Value)
                        )
                        speedMeterPerSec = 0;
                    else
                        speedMeterPerSec = eventArgs.Position.Coordinate.Speed.Value;
                }
                mCallback_PositionChanged(ref gp, speedMeterPerSec, eventArgs.Position.Coordinate.Accuracy);
            }
        }

        void geolocator_StatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            string status = args.Status.ToString("f");
            MyUtility.Utility.ShowProgressIndicator_updateInfo(status);


            //switch (args.Status)
            //{
            //    case PositionStatus.Disabled:
            //        // the application does not have the right capability or the location master switch is off
            //        status = "the application does not have the right capability or the location master switch is off";
            //        break;
            //    case PositionStatus.Initializing:
            //        // the geolocator started the tracking operation
            //        status = "the geolocator started the tracking operation";
            //        break;
            //    case PositionStatus.NoData:
            //        // the location service was not able to acquire the location
            //        status = "the location service was not able to acquire the location";
            //        break;
            //    case PositionStatus.Ready:
            //        // the location service is generating geopositions as specified by the tracking parameters
            //        status = "the location service is generating geopositions as specified by the tracking parameters";
            //        break;
            //    case PositionStatus.NotAvailable:
            //        status = "not used in WindowsPhone, Windows desktop uses this value to signal that there is no hardware capable to acquire location information";
            //        // not used in WindowsPhone, Windows desktop uses this value to signal that there is no hardware capable to acquire location information
            //        break;
            //    case PositionStatus.NotInitialized:
            //        status = "the initial state of the geolocator, once the tracking operation is stopped by the user the geolocator moves back to this state";
            //        break;
            //}

            ////MyUtility.Utility.ShowMessageBox(args.Status.ToString("f"));
            //MyUtility.Utility.OutputDebugString(status);




        }

    }
}
