﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapApp
{
    public static class MapUtility
    {
        private static object mMapUtilityLock = new object();

     //   private static MyGeolocator myGeolocator = null;
      //  private static MyGeocodeQuery myGeocodeQuery = null;
        private static MyRouteQuery myRouteQuery = null;
        private static bool bOnlyOneComponent = true;
      /*  public static MyGeolocator GetMyGeolocator(uint intervalMS, int moveThreshold)
        {
            //intervalMS 設定時間間格更新，根本沒有用，

            lock (mMapUtilityLock)
            {
                //if (bOnlyOneComponent)
                //{
                //    if (myGeolocator == null)
                //        myGeolocator = MyGeolocator.Create(intervalMS, moveThreshold);
                //    myGeolocator.StopTracking();
                //    myGeolocator.SetTrackingTimeDistance(intervalMS, moveThreshold);
                //    return myGeolocator;
                //}
                //else
                {
                    return MyGeolocator.Create(intervalMS, moveThreshold);
                }
            }
        }*/

/*
        public static MyGeocodeQuery GetMyGeocodeQuery()
        {
            lock (mMapUtilityLock)
            {
                if (bOnlyOneComponent)
                {
                    if (myGeocodeQuery == null)
                        myGeocodeQuery = MyGeocodeQuery.Create();
                    return myGeocodeQuery;
                }
                else
                {
                    return MyGeocodeQuery.Create();
                }
            }
        }
        */

        public static MyRouteQuery GetMyRouteQuery()
        {
            lock (mMapUtilityLock)
            {
                if (bOnlyOneComponent)
                {
                    if (myRouteQuery == null)
                        myRouteQuery = MyRouteQuery.Create();
                    return myRouteQuery;
                }
                else
                {
                    return MyRouteQuery.Create();
                }
            }
        }

    /*    public static void ReleaseMyGeolocator(ref MyGeolocator mmm)
        {
            lock (mMapUtilityLock)
            {
                //if (bOnlyOneComponent == false && mmm != null)
                //    mmm.Dispose();
                //else
                    mmm.StopTracking();
                  
                mmm = null;
            }
        }*/

        /*
        public static void ReleaseMyGeocodeQuery(ref MyGeocodeQuery mmm)
        {
            lock (mMapUtilityLock)
            {
                if (bOnlyOneComponent == false && mmm != null)
                    mmm.Dispose();
                mmm = null;
            }
        }
        */

        public static void ReleaseMyRouteQuery(ref MyRouteQuery mmm)
        {
            lock (mMapUtilityLock)
            {
                if (bOnlyOneComponent == false && mmm != null)                
                    mmm.Dispose();                
                else if (mmm != null)                
                    mmm.CancelQuery();               

                mmm = null;
            }
        }
    }
}
