﻿using MyUtility;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MapApp
{
    public class GeocodeQueryCombine : IDisposable
    {
        private GoogleGeocodeQuery mGoogleGeocodeQuery;
        private MyGeocodeQuery mMyGeocodeQuery;
        private NokiaPlacesHandler mNokiaPlacesGeocodeQuery;

        public delegate void Callback_QuerySearchTerm(List<SearchResult> result, object source);
        private Callback_QuerySearchTerm mCallback_QuerySearchTerm = null;

        private object mCombineSearchLock = new object();
        private List<SearchResult> mSearchResult = null;

        private bool mFromGoogle, mFromMapService, mFromNokiaPlace;


        public GeocodeQueryCombine()
        {
            mGoogleGeocodeQuery = GoogleGeocodeQuery.Create();
            mMyGeocodeQuery = MyGeocodeQuery.Create();
            mNokiaPlacesGeocodeQuery = new NokiaPlacesHandler();
        }

        public void Dispose()
        {
            mGoogleGeocodeQuery.Dispose();
            mMyGeocodeQuery.Dispose();

            mGoogleGeocodeQuery = null;
            mMyGeocodeQuery = null;
            mNokiaPlacesGeocodeQuery = null;
        }

        String sSearchTermREC; GeoCoordinate myCoordinateREC;
        public void SearchForTerm(String sSearchTerm, GeoCoordinate myCoordinate, Callback_QuerySearchTerm callback_QuerySearchTerm)
        {
            mFromNokiaPlace = mFromGoogle = mFromMapService = false;
            mSearchResult = new List<SearchResult>();

            mCallback_QuerySearchTerm = callback_QuerySearchTerm;



            sSearchTermREC = sSearchTerm;
            myCoordinateREC = myCoordinate;

            //mNokiaPlacesGeocodeQuery.SearchForTerm(sSearchTerm, myCoordinate, Callback_ListPicker_SearchHistory_SelectionSearch);
            mGoogleGeocodeQuery.SearchForTerm(sSearchTerm, myCoordinate, _Callback_Combine_QuerySearchTerm);
            // mMyGeocodeQuery.SearchForTerm(sSearchTerm, myCoordinate, Callback_ListPicker_SearchHistory_SelectionSearch);

        }

        public class SearchResult
        {
            public string City { get; set; }
            public string District { get; set; }
            public string Street { get; set; }
            public string formatted_address { get; set; }
            public string lat { get; set; }
            public string lng { get; set; }
        }

        private void _Callback_Combine_QuerySearchTerm(List<SearchResult> results, object source)
        {
            //資料都蒐集到才丟去顯示在LIST上
            lock (mCombineSearchLock)
            {
                bool bNoData = false;
                if (source.GetType() == typeof(GoogleSearchHandler))
                    mFromGoogle = true;
                else if (source.GetType() == typeof(MyGeocodeQuery))
                    mFromMapService = true;
                else if (source.GetType() == typeof(NokiaPlacesHandler))
                    mFromNokiaPlace = true;

                if (results == null)
                {
                    bNoData = true;
                }
                else
                {
                    if (results.Count == 0)
                    {
                        bNoData = true;
#if DEBUG
                        if (source.GetType() == typeof(GoogleSearchHandler))
                            Utility.OutputDebugString("[GeocodeQueryCombine] : google map search for nothing");
                        else if (source.GetType() == typeof(MyGeocodeQuery))
                            Utility.OutputDebugString("[GeocodeQueryCombine] : mapservice search for nothing");
                        else if (source.GetType() == typeof(NokiaPlacesHandler))
                            Utility.OutputDebugString("[GeocodeQueryCombine] : NokiaPlaces search for nothing");
#endif
                    }
                    else
                    {
                        mSearchResult.AddRange(results);
                        ////NokiaPlacesHandler 放前面
                        //if (source.GetType() == typeof(NokiaPlacesHandler))
                        //    mSearchResult.InsertRange(0, results);
                        //else if (source.GetType() == typeof(MyGeocodeQuery))
                        //    mSearchResult.AddRange(results);

                    }
                }

                //如果GOOGLE搜尋失敗的話，就使用Mapservice + Nokia place的搜尋功能
                if (source.GetType() == typeof(GoogleSearchHandler))
                {
                    if (bNoData)
                    {
                        //GOOGLE沒有才去用NOKIA PLACE(只有地標和只到巷的住址)
                        mNokiaPlacesGeocodeQuery.SearchForTerm(sSearchTermREC, myCoordinateREC, _Callback_Combine_QuerySearchTerm);
                    }
                    else
                    {
                        mCallback_QuerySearchTerm(mSearchResult, this);
                    }
                }
                else if (source.GetType() == typeof(NokiaPlacesHandler))
                {
                    if (bNoData)
                    {
                        //google 和 nokia place都搜不到，只好用這個最爛的
                        mMyGeocodeQuery.SearchForTerm(sSearchTermREC, myCoordinateREC, _Callback_Combine_QuerySearchTerm);
                    }
                    else
                    {
                        mCallback_QuerySearchTerm(mSearchResult, this);
                    }
                }
                else
                {

                    if (//mFromGoogle &&
                         mFromMapService
                        //  && mFromNokiaPlace
                        )
                    {
                        if (bNoData)
                        {
                            mCallback_QuerySearchTerm(null, this);
                        }
                        else
                            mCallback_QuerySearchTerm(mSearchResult, this);
                    }
                }
            }
        }



    }
}
