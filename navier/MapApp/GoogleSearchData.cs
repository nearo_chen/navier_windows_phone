﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace MapApp
{
    public partial class GoogleSearchHandler
    {
        /// <summary>
        /// Google Map API, Address convert to Location, Data Entity. by Json format.
        /// </summary>
        [DataContract]
        public class GAddressGeoCollection
        {
            public GAddressGeoCollection() { }
            /// <summary>
            /// JSON回傳的results集合。
            /// </summary>
            [DataMember]
            public List<GAddressGeoResult> results { get; set; }
            /// <summary>
            /// JSON回傳的status字串。
            /// </summary>
            [DataMember]
            public string status { get; set; }
        }

        /// <summary>
        /// results資料結構。
        /// </summary>
        [DataContract]
        public class GAddressGeoResult
        {
            public GAddressGeoResult() { }
            [DataMember]
            public List<GAddressComponent> address_components { get; set; }
            [DataMember]
            public string formatted_address { get; set; }
            /// <summary>
            /// 座標資料集合。
            /// </summary>
            [DataMember]
            public GAddressGeoEntity geometry { get; set; }
        }

        [DataContract]
        public class GAddressComponent
        {
            public GAddressComponent() { }
            [DataMember]
            public string long_name { get; set; }
            [DataMember]
            public string short_name { get; set; }
            [DataMember]
            public List<string> types;
        }

        [DataContract]
        public class GAddressGeoEntity
        {
            public GAddressGeoEntity() { }
            [DataMember]
            public GAddressLocation location { get; set; }
            [DataMember]
            public string location_type { get; set; }
        }

        [DataContract]
        public class GAddressLocation
        {
            public GAddressLocation() { }
            [DataMember]
            public string lat { get; set; }
            [DataMember]
            public string lng { get; set; }
        }

    }
}
