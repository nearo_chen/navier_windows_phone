﻿#define Use_MyClass

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MapApp.Resources;
using Microsoft.Phone.Maps.Controls;
using System.Device.Location; // Contains the GeoCoordinate class.
using Windows.Devices.Geolocation; // Contains the Geocoordinate class.
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Phone.Maps.Services;

namespace MapApp
{
    public partial class MainPage : PhoneApplicationPage
    {
        List<GeoCoordinate> SearchTermCoordinates = new List<GeoCoordinate>();

#if Use_MyClass
#else
        RouteQuery MyQuery = null;
        GeocodeQuery Mygeocodequery = null;
#endif

        MyGeolocatorTracking mMyGeolocator;
        MyGeocodeQuery mMyGeocodeQuery;
        MyRouteQuery mMyRouteQuery;

        Map MyMap = null;
        MapOverlay MyOverlay;
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            MyMap = new Map();
            MyMap.ZoomLevel = 10;
            MyMap.Pitch = 30;
            // MyMap.CartographicMode = MapCartographicMode.Aerial;

            //Set the Map center by using Center property
            MyMap.Center = new GeoCoordinate(47.6097, -122.3331);
            MyMap.Opacity = 1f;

            ContentPanel.Children.Add(MyMap);

            //    UserControl ccc = new UserControl();
            //    ccc.Opacity

            MyOverlay = new MapOverlay();
            MyOverlay.Content = GetGrid();
            MyOverlay.GeoCoordinate = new GeoCoordinate(47.6097, -122.3331);
            MyOverlay.PositionOrigin = new Point(0, 0.5);
            MapLayer MyLayer = new MapLayer();
            MyLayer.Add(MyOverlay);
            MyMap.Layers.Add(MyLayer);


            SearchTermCoordinates.Clear();

#if Use_MyClass
            mMyGeolocator = MyGeolocatorTracking.Create(1, -1);
            mMyGeolocator.StartPositionTracking(Callback_PositionChanged);
            //  mMyGeolocator.QueryGlobalPosition(Callback_QueryGlobalPosition);
#else
            this.GetCoordinates();
#endif

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();


           
        }

#if Use_MyClass


        GeoCoordinate nowMyGeoCoordinate;
        //取得自己做標
        void Callback_PositionChanged(ref System.Windows.Point outPoint, double speedValue, double accuracyMeter)
        {
            nowMyGeoCoordinate = new GeoCoordinate(outPoint.X, outPoint.Y);
            SearchTermCoordinates.Add(nowMyGeoCoordinate);

            Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    MyMap.Center = nowMyGeoCoordinate;
                    MyOverlay.GeoCoordinate = nowMyGeoCoordinate;

                    mMyGeocodeQuery = MyGeocodeQuery.Create();
                    mMyGeocodeQuery.SearchForTerm(
                        //"taipei"
                        "台北"
                        ,
                        nowMyGeoCoordinate, Callback_QuerySearchTerm);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            });

           
        }

        public void Callback_QuerySearchTerm(List<MapApp.GeocodeQueryCombine.SearchResult> results, object source)
        {
            if (results == null || results.Count == 0)
            {
               // MessageBox.Show(result.status + " => No match found.");
            }
            else
            {
                mMyRouteQuery = MyRouteQuery.Create();
                GeoCoordinate geo = new GeoCoordinate(
                    Double.Parse(results[0].lat, System.Globalization.CultureInfo.InvariantCulture),
                    Double.Parse(results[0].lng, System.Globalization.CultureInfo.InvariantCulture));
                SearchTermCoordinates.Add(geo);//searchCoordinates[0]);
                mMyRouteQuery.QueryRoute(SearchTermCoordinates, Callback_RouteQuery, TravelMode.Driving);
            }

            
        }

        //void Callback_QuerySearchTerm(List<GeoCoordinate> searchCoordinates, IList<MapLocation> mapLocations)
        //{
        //    if (mapLocations.Count == 0)// searchCoordinates.Count == 0)
        //    {
        //        MessageBox.Show("SearchTerm => Callback_QuerySearchTerm(List<GeoCoordinate> searchCoordinates) => searchCoordinates.size 0");
        //    }
        //    else
        //    {
        //        mMyRouteQuery = MyRouteQuery.Create();
        //        SearchTermCoordinates.Add(mapLocations[0].GeoCoordinate);//searchCoordinates[0]);
        //        mMyRouteQuery.QueryRoute(SearchTermCoordinates, Callback_RouteQuery, TravelMode.Driving);
        //    }
        //}

        void Callback_RouteQuery(Route route, Object tag)
        {
            MapRoute mapRoute = new MapRoute(route);
            MyMap.AddRoute(mapRoute);
        }

#else
        private async void GetCoordinates()
        {
            // Get the phone's current location.
            Geolocator MyGeolocator = new Geolocator();
            MyGeolocator.DesiredAccuracyInMeters = 5;
            Geoposition MyGeoPosition = null;
            try
            {
                MyGeoPosition = await MyGeolocator.GetGeopositionAsync(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(10));
                MyCoordinates.Add(new GeoCoordinate(MyGeoPosition.Coordinate.Latitude, MyGeoPosition.Coordinate.Longitude));
                
                Mygeocodequery = new GeocodeQuery();
                Mygeocodequery.SearchTerm = "Seattle, WA";
                Mygeocodequery.GeoCoordinate = new GeoCoordinate(MyGeoPosition.Coordinate.Latitude, MyGeoPosition.Coordinate.Longitude);

                Mygeocodequery.QueryCompleted += Mygeocodequery_QueryCompleted;
                Mygeocodequery.QueryAsync();
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Location is disabled in phone settings or capabilities are not checked.");
            }
            catch (Exception ex)
            {
                // Something else happened while acquiring the location.
                MessageBox.Show(ex.Message);
            }
        }

        void Mygeocodequery_QueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
            if (e.Error == null)
            {
                MyQuery = new RouteQuery();
                MyCoordinates.Add(e.Result[0].GeoCoordinate);
                MyQuery.Waypoints = MyCoordinates;
                MyQuery.QueryCompleted += MyQuery_QueryCompleted;
                MyQuery.QueryAsync();
                Mygeocodequery.Dispose();
            }
        }

        void MyQuery_QueryCompleted(object sender, QueryCompletedEventArgs<Route> e)
        {
            if (e.Error == null)
            {
                Route MyRoute = e.Result;
                MapRoute MyMapRoute = new MapRoute(MyRoute);
                MyMap.AddRoute(MyMapRoute);

                List<string> RouteList = new List<string>();
                foreach (RouteLeg leg in MyRoute.Legs)
                {
                    foreach (RouteManeuver maneuver in leg.Maneuvers)
                    {
                        RouteList.Add(maneuver.InstructionText);
                    }
                }

                RouteLLS.ItemsSource = RouteList;

                MyQuery.Dispose();
            }
        }
#endif
        Grid GetGrid()
        {
            Grid MyGrid = new Grid();
            MyGrid.RowDefinitions.Add(new RowDefinition());
            MyGrid.RowDefinitions.Add(new RowDefinition());
            MyGrid.Background = new SolidColorBrush(Colors.Transparent);

            //Creating a Rectangle
            Rectangle MyRectangle = new Rectangle();
            MyRectangle.Fill = new SolidColorBrush(Colors.Black);
            MyRectangle.Height = 20;
            MyRectangle.Width = 20;
            MyRectangle.SetValue(Grid.RowProperty, 0);
            MyRectangle.SetValue(Grid.ColumnProperty, 0);

            //Adding the Rectangle to the Grid
            MyGrid.Children.Add(MyRectangle);

            //Creating a Polygon
            Polygon MyPolygon = new Polygon();
            MyPolygon.Points.Add(new Point(2, 0));
            MyPolygon.Points.Add(new Point(22, 0));
            MyPolygon.Points.Add(new Point(2, 40));
            MyPolygon.Stroke = new SolidColorBrush(Colors.Red);
            MyPolygon.Fill = new SolidColorBrush(Colors.Red);
            MyPolygon.SetValue(Grid.RowProperty, 1);
            MyPolygon.SetValue(Grid.ColumnProperty, 0);

            //Adding the Polygon to the Grid
            MyGrid.Children.Add(MyPolygon);

            return MyGrid;
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}