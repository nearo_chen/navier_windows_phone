﻿using Microsoft.Phone.Maps.Services;
using MyUtility;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Windows.Devices.Geolocation;

namespace MapApp
{
    /// <summary>
    /// MyGeocodeQuery地標住址搜索器
    /// </summary>
    public class MyGeocodeQuery : IDisposable
    {

        private GeocodeQuery mGeocodequery = null;

        //public delegate void Callback_QuerySearchTerm(List<GeoCoordinate> searchCoordinates, IList<MapLocation> mapLocations);
        private MapApp.GeocodeQueryCombine.Callback_QuerySearchTerm mCallback_QuerySearchTerm = null;

        private MyGeocodeQuery()
        {
        }

        ~MyGeocodeQuery()
        {
            Dispose();
        }

        public void Dispose()
        {

            if (mGeocodequery != null)
            {
                mGeocodequery.Dispose();
            }
            mGeocodequery = null;

#if DEBUG
            Utility.OutputDebugString("[geoQuery_Dispose]----Dispose()");
#endif
        }

        public static MyGeocodeQuery Create()
        {
#if DEBUG
            Utility.OutputDebugString("[geoQuery_Create]----new MyGeocodeQuery()");
#endif
            return new MyGeocodeQuery();//目前先一律使用google做搜尋
        }


        ///// <summary>
        ///// 傳入目的地，導航將由目前位置規畫路線
        ///// </summary>
        //public void QueryPath()
        //{
        //    mIsRouteSearch = true;

        //    if (mMyGeolocator != null)
        //        mMyGeolocator.Dispose();
        //    mMyGeolocator = new MyGeolocator();
        //    mMyGeolocator.QueryGlobalPosition(Callback_QueryGlobalPosition);

        //    mMyGeolocator.Dispose();
        //    mMyGeolocator = null;
        //}

        //private void Callback_QueryGlobalPosition(ref System.Windows.Point outPoint)
        //{
        //    SearchForTerm("Seattle, WA", new GeoCoordinate(outPoint.X, outPoint.Y));
        //}


        /// <summary>
        /// 搜尋路標，住址
        /// GeocodeQuery can be used to search location for a given search term, for example a town name or street address.
        /// </summary>
        /// <param name="searchTerm"></param>
        public void SearchForTerm(String sSearchTerm, GeoCoordinate myCoordinate, MapApp.GeocodeQueryCombine.Callback_QuerySearchTerm callback_QuerySearchTerm)
        {
            mCallback_QuerySearchTerm = callback_QuerySearchTerm;

            if (mGeocodequery != null)
                mGeocodequery.Dispose();

            // Searching for an address
            mGeocodequery = new GeocodeQuery();
            mGeocodequery.SearchTerm = sSearchTerm;

            mGeocodequery.GeoCoordinate = myCoordinate == null ? new GeoCoordinate(0, 0) : myCoordinate;// new GeoCoordinate(outPoint.X, outPoint.Y);
            mGeocodequery.QueryCompleted += Callback_QuerySearchTerm;
            mGeocodequery.QueryAsync();

#if DEBUG
            Utility.OutputDebugString("[geoQuery_SearchForTerm]----" + sSearchTerm + "(" + myCoordinate.Latitude + " ; " + myCoordinate.Longitude + ")");
#endif
        }



        public static string ConvertMapLocation2FormatAddress(MapAddress address)
        {
            //MapAddress address = value as MapAddress;
            String addressText = "";

            if (address != null)
            {
             //   if (address.Country.Length > 0) addressText += address.Country;
              //  if (address.City.Length > 0) addressText += address.City;
                if (address.Street.Length > 0)
                {
                    addressText += address.Street;
                    if (address.HouseNumber.Length > 0) addressText += " " + address.HouseNumber;
                }
                if (address.Neighborhood.Length > 0) addressText += address.Neighborhood;
                if (address.PostalCode.Length > 0) addressText += " " + address.PostalCode;
            }

            return addressText;
        }

        private void Callback_QuerySearchTerm(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
            if (e.Error == null)
            {
                //List<GeoCoordinate> searchCoordinates = new List<GeoCoordinate>();

                if (e.Result.Count > 0)
                {
                   
                    List<MapApp.GeocodeQueryCombine.SearchResult> result_list = new List<GeocodeQueryCombine.SearchResult>();
                    // Add all results to MyCoordinates for drawing the map markers
                    foreach (MapLocation mapLocation in e.Result)
                    {
                        GeocodeQueryCombine.SearchResult data = new GeocodeQueryCombine.SearchResult();

                        data.formatted_address = ConvertMapLocation2FormatAddress(mapLocation.Information.Address);
                        data.City = mapLocation.Information.Address.City;
                        data.District = mapLocation.Information.Address.District;
                        data.Street = mapLocation.Information.Address.Street;

                        data.lat = mapLocation.GeoCoordinate.Latitude.ToString();
                        data.lng = mapLocation.GeoCoordinate.Longitude.ToString();

                        result_list.Add(data);
                    }

                    //我要整理一下結果為google的格式，再往外丟
                    //MapApp.GoogleSearchHandler.GAddressGeoCollection result_list = new GoogleSearchHandler.GAddressGeoCollection();
                    //result_list.results = new List<GoogleSearchHandler.GAddressGeoResult>();
                    //result_list.status = "???";

                    //// Add all results to MyCoordinates for drawing the map markers
                    //foreach (MapLocation mapLocation in e.Result)
                    //{
                    //    MapApp.GoogleSearchHandler.GAddressGeoResult result = new GoogleSearchHandler.GAddressGeoResult();

                    //    result.formatted_address = ConvertMapLocation2FormatAddress(mapLocation.Information.Address);
                    //    //result.formatted_address = mapLocation.Information.Name;
                    //    //if (result.formatted_address == "")
                    //    //    result.formatted_address = mapLocation.Information.Address.Street;
                    //    //if (result.formatted_address == "")
                    //    //    result.formatted_address = mapLocation.Information.Address.District;
                    //    //if (result.formatted_address == "")
                    //    //    result.formatted_address = mapLocation.Information.Address.City;

                    //    result.address_components = new List<GoogleSearchHandler.GAddressComponent>();

                    //    MapApp.GoogleSearchHandler.GAddressComponent component;
                    //    //City
                    //    component = new GoogleSearchHandler.GAddressComponent();
                    //    component.long_name = component.short_name = mapLocation.Information.Address.City;
                    //    component.types = new List<string>();
                    //    component.types.Add("political");
                    //    result.address_components.Add(component);

                    //    //District
                    //    component = new GoogleSearchHandler.GAddressComponent();
                    //    component.long_name = component.short_name = mapLocation.Information.Address.District;
                    //    component.types = new List<string>();
                    //    component.types.Add("political");
                    //    result.address_components.Add(component);

                    //    //Street
                    //    component = new GoogleSearchHandler.GAddressComponent();
                    //    component.long_name = component.short_name = mapLocation.Information.Address.Street;
                    //    component.types = new List<string>();
                    //    component.types.Add("street_address");
                    //    result.address_components.Add(component);

                    //    MapApp.GoogleSearchHandler.GAddressLocation loc = new GoogleSearchHandler.GAddressLocation();
                    //    loc.lat = mapLocation.GeoCoordinate.Latitude.ToString();
                    //    loc.lng = mapLocation.GeoCoordinate.Longitude.ToString();

                    //    MapApp.GoogleSearchHandler.GAddressGeoEntity entity = new GoogleSearchHandler.GAddressGeoEntity();
                    //    entity.location = loc;
                    //    entity.location_type = "???";
                    //    result.geometry = entity;

                    //    result_list.results.Add(result);

                    //    //searchCoordinates.Add(mapLocation.GeoCoordinate);
                    //}

                    mCallback_QuerySearchTerm(result_list, this);
                }
                else
                {
                    GeocodeQuery geocodeQuery = sender as GeocodeQuery;
                    // MessageBox.Show(geocodeQuery.SearchTerm + " => No match found.");
                    Utility.OutputDebugString(geocodeQuery.SearchTerm + " => No match found.");

                    mCallback_QuerySearchTerm(null, this);
                }

                // mCallback_QuerySearchTerm(null, e.Result);
            }

            mCallback_QuerySearchTerm = null;
            mGeocodequery.Dispose();
            mGeocodequery = null;
        }

    }
}
