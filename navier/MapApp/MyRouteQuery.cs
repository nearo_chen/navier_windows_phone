﻿using Microsoft.Phone.Maps.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapApp
{
    /// <summary>
    /// 給定起始，終點做路徑規劃Route path Query
    /// </summary>
    public class MyRouteQuery : IDisposable
    {
        private object _routeQueryLock = new object();
        private RouteQuery mRouteQuery = null;
        private Object mTag = null;

        /// <summary>
        /// routeGlobalPoints為 Route.Geometry路徑規劃的經緯度定位點
        /// </summary>
        public delegate void Callback_RouteQuery(Route MyRoute, Object tag);
        private Callback_RouteQuery mCallback_RouteQuery = null;

        private MyRouteQuery()
        {
        }

        private bool mIsDestructure = false;
        ~MyRouteQuery()
        {
            mIsDestructure = true;
            Dispose();
        }

        public void Dispose()
        {
            lock (_routeQueryLock)
            {
                if (mRouteQuery != null)
                {
                    if (mIsDestructure == false)
                        mRouteQuery.CancelAsync();
                    mRouteQuery.Dispose();
                }
                mRouteQuery = null;
                mTag = null;
            }
        }

        public static MyRouteQuery Create()
        {
#if DEBUG
            MyUtility.Utility.OutputDebugString("return new MyRouteQuery();");
#endif
            return new MyRouteQuery();
        }

        public void QueryRoute(List<GeoCoordinate> waypoints, Callback_RouteQuery callback_RouteQuery, TravelMode travelMode)
        {
            QueryRoute(waypoints, callback_RouteQuery, travelMode, null);
        }

        /// <summary>
        /// RouteQuery要在UI thread中去RUN，呼叫時要注意
        /// 規劃waypoints[0]~waypoints[1]之間的路線，若是有目前正在規劃中路線，會將他Dispose，無需注意是否之前Query是否已經完成
        /// </summary>
        public void QueryRoute(List<GeoCoordinate> waypoints, Callback_RouteQuery callback_RouteQuery, TravelMode travelMode, Object tag)
        {
            lock (_routeQueryLock)
            {
                if (mRouteQuery != null)
                    mRouteQuery.Dispose();

                mCallback_RouteQuery = callback_RouteQuery;

                mRouteQuery = new RouteQuery();
                mRouteQuery.TravelMode = travelMode;

                mRouteQuery.Waypoints = waypoints;

                //   mRouteQuery.InitialHeadingInDegrees = 
                //    mRouteQuery.RouteOptimization = RouteOptimization.MinimizeDistance


                mRouteQuery.QueryCompleted += RouteQuery_QueryCompleted;

#if DEBUG
                iQueryRouteStartTime = Environment.TickCount;
                MyUtility.Utility.OutputDebugString("[MyRouteQuery] => QueryRoute-----start");
#endif

                mRouteQuery.QueryAsync();



                mTag = tag;

            }
        }

#if DEBUG
        int iQueryRouteStartTime = -1;
#endif

        public void CancelQuery()
        {
            lock (_routeQueryLock)
            {
                if (mRouteQuery != null)
                {
#if DEBUG
                    MyUtility.Utility.OutputDebugString("[MyRouteQuery] CancelQuery => 在RouteQuery_QueryCompleted之前取消路線規畫了");
#endif
                    mRouteQuery.QueryCompleted -= RouteQuery_QueryCompleted;
                    mRouteQuery.CancelAsync();
                }
            }
        }

        private void RouteQuery_QueryCompleted(object sender, QueryCompletedEventArgs<Route> e)
        {
            lock (_routeQueryLock)
            {
#if DEBUG
                MyUtility.Utility.OutputDebugString("[MyRouteQuery] => QueryRoute-----RouteQuery_QueryCompleted() 花了: " + (Environment.TickCount - iQueryRouteStartTime) + " ms");
#endif

                if (e.Error == null && e.Cancelled == false)
                {
                    Route MyRoute = e.Result;
                    /*List<GeoCoordinate> routesPoints = new List<GeoCoordinate>();
                    foreach (GeoCoordinate geo in MyRoute.Geometry)
                    {
                        routesPoints.Add(geo);
                    }*/

                    mCallback_RouteQuery(MyRoute, mTag);
                }
                else if (e.Cancelled)
                {
                    //剛剛取消ㄉ就當作沒事情讓他過
                }
                else
                    mCallback_RouteQuery(null, null);

                mRouteQuery.Dispose();
                mRouteQuery = null;
            }
        }

    }
}
