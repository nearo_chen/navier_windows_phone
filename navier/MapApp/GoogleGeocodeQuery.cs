﻿using MyUtility;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace MapApp
{
    public class GoogleGeocodeQuery
    {
        private MapApp.GeocodeQueryCombine.Callback_QuerySearchTerm mCallback_QuerySearchTerm = null;
        private GoogleSearchHandler mGoogleSearchHandler = new GoogleSearchHandler();

        private GoogleGeocodeQuery()
        {
        }

        ~GoogleGeocodeQuery()
        {
            Dispose();
        }

        public void Dispose()
        {
#if DEBUG
            MyUtility.Utility.OutputDebugString("[GoogleGeocodeQuery_Dispose]----Dispose()");
#endif
            mGoogleSearchHandler = null;
        }

        public static GoogleGeocodeQuery Create()
        {
#if DEBUG
            MyUtility.Utility.OutputDebugString("[GoogleGeocodeQuery_Create]----new GoogleGeocodeQuery()");
#endif
            return new GoogleGeocodeQuery();
        }

        /// <summary>
        /// 搜尋路標，住址
        /// GeocodeQuery can be used to search location for a given search term, for example a town name or street address.
        /// </summary>
        public void SearchForTerm(String sSearchTerm, GeoCoordinate myCoordinate, MapApp.GeocodeQueryCombine.Callback_QuerySearchTerm callback_QuerySearchTerm)
        {
            mCallback_QuerySearchTerm = callback_QuerySearchTerm;

            mGoogleSearchHandler.GeocodingQuery_GOOGLEMAP(myCoordinate.Longitude, myCoordinate.Latitude, sSearchTerm, mCallback_QuerySearchTerm);
           

#if DEBUG
            MyUtility.Utility.OutputDebugString("[geoQuery_SearchForTerm]----" + sSearchTerm + "(" + myCoordinate.Latitude + " ; " + myCoordinate.Longitude + ")");
#endif
        }


        // google的直接把結果往外丟ㄌ
        //public void Callback_QuerySearchTerm(List<MapApp.GoogleSearchHandler.GAddressGeoCollection> result)
        //{
        //    mCallback_QuerySearchTerm(result_list);
        //}

    }
}
