﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using MyUtility;
using System.Windows.Controls.Primitives;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.ComponentModel;

using System.Windows.Input;
using Microsoft.Phone.Controls.Primitives;
using Windows.Devices.Sensors;
using Windows.Foundation;

namespace NavierHUD
{
    public partial class SettingsControl : UserControl
    {
        private Inclinometer _inclinometer;

        //Popup control類別
        private Popup gPopupControl;
        //獨立宣告一個SelectionChangedEventHandler事件
        // public event SelectionChangedEventHandler mPageListSelectHandler;
        HUD.SaveUserSetting.UserSetting mUserSetting;

        public SettingsControl(HUD.SaveUserSetting.UserSetting userSetting)
        {
            mUserSetting = userSetting;

            InitializeComponent();

            SettingsData_ListBox settingsData_ListBox = new SettingsData_ListBox();

            SettingsUIData data = new SettingsUIData();
            data.name = ResourceApp.Resources.AppResources.Settings_BrightnessMax;
            data.description = ResourceApp.Resources.AppResources.Settings_BrightnessMax_detail;
            data.type = 3;
            settingsData_ListBox.Add(data);

            data = new SettingsUIData();
            data.name = ResourceApp.Resources.AppResources.Settings_Use24HR;
            data.type = 0;
            data.isChecked = userSetting.ShowTime24H;
            settingsData_ListBox.Add(data);

            data = new SettingsUIData();
            data.name = ResourceApp.Resources.AppResources.Settings_SpeedAlert;
            data.type = 1;
            data.speedLimitKM = userSetting.SpeedLimitKMH;
            data.speedLimitMil = userSetting.SpeedLimitMPH;
            settingsData_ListBox.Add(data);

            data = new SettingsUIData();
            data.name = ResourceApp.Resources.AppResources.Settings_SpeechVoice;
            data.type = 2;
            if (userSetting.SpeachVoice == 0)
                data.speachVoice_No = true;
            else if (userSetting.SpeachVoice == 1)
                data.speachVoice_Male = true;
            else if (userSetting.SpeachVoice == 2)
                data.speachVoice_Female = true;
            data.speachVoiceName_NO = Utility.GetTranslate_VoiceNO();
            data.speachVoiceName_Male = Utility.GetTranslate_VoiceMale();
            data.speachVoiceName_Female = Utility.GetTranslate_VoiceFemale();
            settingsData_ListBox.Add(data);

            //該死的微軟驗證說要加定位服務的開關
//            App Policies: 2.7.3 Location Services Access
//Apps that receive the location of a user’s mobile device must provide in-app settings that allow the user to enable and disable your app's access to and use of location from the Location Service API.
            data = new SettingsUIData();
            data.name = "enable and disable Location Services";
            data.description = "";
            data.type = 3;
            settingsData_ListBox.Add(data);


            mListBox_Settings.ItemsSource = settingsData_ListBox;

            mRotate.Rotation = 90;

            //水平感測
            _inclinometer = Inclinometer.GetDefault();
            if (_inclinometer != null)
            {
                // Establish the report interval for all scenarios
                uint minReportInterval = _inclinometer.MinimumReportInterval;
                uint reportInterval = minReportInterval > 100 ? minReportInterval : 100;
                _inclinometer.ReportInterval = reportInterval;

                // Establish the event handler
                _inclinometer.ReadingChanged += new TypedEventHandler<Inclinometer, InclinometerReadingChangedEventArgs>(ReadingChanged);
            }
        }

        private void ReadingChanged(object sender, InclinometerReadingChangedEventArgs e)
        {
            InclinometerReading reading = e.Reading;
            Dispatcher.BeginInvoke(() =>
            {
                if (reading.RollDegrees < 0 && mRotate.Rotation < 0)
                    mRotate.Rotation = 90;
                else if (reading.RollDegrees > 0 && mRotate.Rotation > 0)
                    mRotate.Rotation = -90;
            });
        }

        //該方法用於告知Popup Control要產生的Position
        public Popup Create(double x, double y)
        {
            gPopupControl = new Popup();
            gPopupControl.Child = this;

            gPopupControl.IsOpen = true;


            //設定X與Y軸
            gPopupControl.HorizontalOffset = x;
            gPopupControl.VerticalOffset = y;

            //  mPageListSelectHandler = pageListSelectHandler;

            //這邊設定FOCUS等下LOST FOCUS才會有作用　
            //this.Focus();

            return gPopupControl;
        }

        /*  private void ListBox_Colors_SelectionChanged(object sender, SelectionChangedEventArgs e)
          {
              mPageListSelectHandler(sender, e);
          }*/

        private void ToggleChecked(object sender, RoutedEventArgs e)
        {
            ToggleSwitch sss = sender as ToggleSwitch;

            string sTag = sss.Tag as string;
            if (sTag == ResourceApp.Resources.AppResources.Settings_Use24HR)//要是有別人共用同樣的templete就要以NAME區分
            {
                mUserSetting.ShowTime24H = sss.IsChecked.Value;
            }
        }

        private void TextBox_MouseLeftButtonUp_KM(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TextBlock textBlock = sender as TextBlock;
            textBlock.Tag = "KM";
            gPopupControl.IsOpen = false;

            //秀出輸入速限，彈出輸入框
            Utility.ShowInputPrompt(true, sender,
                MyPopUpPromptStringCompleted,
                ResourceApp.Resources.AppResources.Settings_SpeedAlert,
                ResourceApp.Resources.AppResources.Settings_KMH,
                textBlock.Text,
                InputScopeNameValue.Number);
        }

        private void TextBox_MouseLeftButtonUp_Mil(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TextBlock textBlock = sender as TextBlock;
            textBlock.Tag = "Mil";
            gPopupControl.IsOpen = false;

            //秀出輸入速限，彈出輸入框
            Utility.ShowInputPrompt(true, sender,
                MyPopUpPromptStringCompleted,
                ResourceApp.Resources.AppResources.Settings_SpeedAlert,
                ResourceApp.Resources.AppResources.Settings_MPH,
                textBlock.Text,
                InputScopeNameValue.Number);
        }

        //輸入速限，彈出輸入框的接收
        public void MyPopUpPromptStringCompleted(object sender, string sResult, bool isOK)
        {
            gPopupControl.IsOpen = true;

            if (isOK)
            {
                TextBlock editingSpeedTextBox = sender as TextBlock;
                editingSpeedTextBox.Text = sResult;

                int kmREC = mUserSetting.SpeedLimitKMH;
                int mphREC = mUserSetting.SpeedLimitMPH;
                try
                {
                    if ((string)editingSpeedTextBox.Tag == "KM")
                        mUserSetting.SpeedLimitKMH = int.Parse(editingSpeedTextBox.Text);
                    else if ((string)editingSpeedTextBox.Tag == "Mil")
                        mUserSetting.SpeedLimitMPH = int.Parse(editingSpeedTextBox.Text);

                    if (mUserSetting.SpeedLimitKMH > Utility.SpeedMaxKM)
                    {
                        Utility.ShowMessageBox("Max KM is : " + Utility.SpeedMaxKM);
                        mUserSetting.SpeedLimitKMH = Utility.SpeedMaxKM;
                    }
                    else if (mUserSetting.SpeedLimitMPH > Utility.SpeedMaxMil)
                    {
                        Utility.ShowMessageBox("Max Mile is : " + Utility.SpeedMaxMil);
                        mUserSetting.SpeedLimitMPH = Utility.SpeedMaxMil;
                    }
                }
                catch (Exception e)
                {
                    if ((string)editingSpeedTextBox.Tag == "KM")
                        mUserSetting.SpeedLimitKMH = kmREC;
                    else if ((string)editingSpeedTextBox.Tag == "Mil")
                        mUserSetting.SpeedLimitMPH = mphREC;
                }

                if ((string)editingSpeedTextBox.Tag == "KM")
                    editingSpeedTextBox.Text = mUserSetting.SpeedLimitKMH.ToString();
                else if ((string)editingSpeedTextBox.Tag == "Mil")
                    editingSpeedTextBox.Text = mUserSetting.SpeedLimitMPH.ToString();

            }
        }

        private void SpeachVoiceClick(object sender, RoutedEventArgs e)
        {
            RadioButton rrr = e.OriginalSource as RadioButton;

            //目前有radio button只有speach再用，但要是以後還以別的東西是三個radio button也用同樣templete的時候，就要以group name區分
            if (rrr.GroupName == ResourceApp.Resources.AppResources.Settings_SpeechVoice)
            {
                switch (rrr.Name)
                {
                    //導航語音，0 無聲，1男聲，2女聲
                    case "a"://無聲
                        {
                            mUserSetting.SpeachVoice = 0;
                        }
                        break;
                    case "b"://男
                        {
                            mUserSetting.SpeachVoice = 1;
                        }
                        break;
                    case "c"://女
                        {
                            mUserSetting.SpeachVoice = 2;
                        }
                        break;
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

       async private void MessageBoxButton_Click_1(object sender, RoutedEventArgs e)
        {
            Button bbb = sender as Button;
            string name = bbb.Content as string;
            if (name == ResourceApp.Resources.AppResources.Settings_BrightnessMax)
            {
                Utility.ShowMessageBox(Utility.GetTranslate_BrightnessMax());
            }
            else if (name == "enable and disable Location Services")
            {
                //Utility.ShowMessageBox("enable and disable Location Services... Are you sure?");

                //該死的微軟驗證說要加這個，讓USER隨時可以開關LOCATION
                await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings-location:"));
            }
        }


    }

    //這下面都是LISTBOX要用的--------------------------------------------------------------------------
    //給list box用
    public class SettingsData_ListBox : ObservableCollection<SettingsUIData>
    {
    }

    /// <summary>
    /// 紀錄編輯好的各種元件資訊
    /// </summary>
    public class SettingsDataList
    {
        public List<SettingsUIData> mSettingsList = new List<SettingsUIData>();
    }
    /// <summary>
    /// 記錄一個元件的資訊
    /// </summary>
    public class SettingsUIData
    {
        //不能有建構子，好像會CRASH

        //一定要用get set，不然data 會bind不到
        public string name { get; set; }                    //設定的項目名稱
        public int type { get; set; }                    //這個選項的型態 0為 check box, 1為 輸入速限框, 2為語音選項radio button, 
        //3 name就是一個button，會開啟一個有打勾按鈕的messagebox

        //第一種格是
        public bool isChecked { get; set; }                 //設定的內容(自己填該設定該有的內容)


        //第二種格是
        public int speedLimitKM { get; set; }
        public int speedLimitMil { get; set; }

        //第三
        public bool speachVoice_No { get; set; }
        public bool speachVoice_Male { get; set; }
        public bool speachVoice_Female { get; set; }
        public string speachVoiceName_NO { get; set; }
        public string speachVoiceName_Male { get; set; }
        public string speachVoiceName_Female { get; set; }

        //第四 用name就可以判斷了
        public string description { get; set; }//看要不要加描述
        //public int messageBoxType { get; set; }//這個type值是看messagebox要哪種字串內容
        //1為 調整螢幕輛度的說明

    }



    public abstract class SettingsDataTemplateSelector : ContentControl
    {
        public virtual DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            return null;
        }

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);

            ContentTemplate = SelectTemplate(newContent, this);
        }
    }

    public class SettingsTemplateSelector : SettingsDataTemplateSelector
    {
        public DataTemplate TypeCheckBox //有Check box的原件
        {
            get;
            set;
        }

        public DataTemplate Type2TextBox  //有 輸入速限框
        {
            get;
            set;
        }

        public DataTemplate Type3RadioButton  //語音選項radio button
        {
            get;
            set;
        }

        public DataTemplate Type4MessageBoxButton  //目前是秀一個messagebox，用來顯示如何將螢幕開啟最亮
        {
            get;
            set;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            SettingsUIData foodItem = item as SettingsUIData;
            if (foodItem != null)
            {
                if (foodItem.type == 0)
                {
                    return TypeCheckBox;
                }
                else if (foodItem.type == 1)
                {
                    return Type2TextBox;
                }
                else if (foodItem.type == 2)
                {
                    return Type3RadioButton;
                }
                else
                {
                    return Type4MessageBoxButton;
                }
            }

            return base.SelectTemplate(item, container);
        }
    }

}
