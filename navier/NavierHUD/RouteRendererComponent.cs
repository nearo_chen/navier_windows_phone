﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RouteInstructorApp;
using MapApp;
using System.Device.Location;
using System.Windows;
using Microsoft.Phone.Maps.Services;
using System.Collections.ObjectModel;
using MyUtility;
using Microsoft.Xna.Framework;
using ResourceApp.Resources;

using Point = System.Windows.Point;
using GoogleVoiceApp1;
using Microsoft.Phone.Maps.Controls;

namespace NavierHUD
{
    /// <summary>
    /// 控制導航資訊，以及將導航資訊的路線繪出，提供GetRouteRendererControl()
    /// </summary>
    public partial class RouteRendererComponent : IDisposable
    {
        #region 計算大家都用得到的資料
        //public struct RouteREC
        //{ public float lat; public float lon; }
        //做紀錄給外部使用，清除的時機在HUD回mainpage以及shareFBPage專案裡面(GoTo_NavierMainPage()最後離開才清除，因為也只有這個地方可以離開重新導航)
        public static GeoCoordinateCollection mRouteRECList = null;//new GeoCoordinateCollection();
        public static object mRouteRECListLock = new object();

        //做紀錄給外部使用，清除的時機在HUD回mainpage以及shareFBPage專案裡面(GoTo_NavierMainPage()最後離開才清除，因為也只有這個地方可以離開重新導航)
        static DateTime mDateTimeStart = DateTime.MaxValue;//紀錄開始導航的時間
        public static double mMovedDistanceMeterCount = 0;
        static SpeedShowControlApp.MovedDistanceCalculator mMovedDistanceCalculator = new SpeedShowControlApp.MovedDistanceCalculator();
        static SpeedShowControlApp.AverageSpeedCalculator mAverageSpeedCalculator = new SpeedShowControlApp.AverageSpeedCalculator();
        public static void ReCreatDistanceSpeedCalculator()
        {
            mDateTimeStart = DateTime.MaxValue;
            mMovedDistanceMeterCount = 0;
            mMovedDistanceCalculator = new SpeedShowControlApp.MovedDistanceCalculator();
            mAverageSpeedCalculator = new SpeedShowControlApp.AverageSpeedCalculator();

            lock (mRouteRECListLock)
            {
                if (mRouteRECList != null)
                    mRouteRECList.Clear();
                mRouteRECList = null;
            }
        }
        public static double GetSpeedAVG_KMHR() { return mAverageSpeedCalculator.GetSpeedAVG_KMHR(); }
        public static DateTime GetDateTimeStart() { return mDateTimeStart; }
        #endregion

        System.Windows.Controls.Panel mLayoutRoot = null;

        private bool bFirstEnter = true;

        private object mRouteQueryDoneLock = new object();
        private bool mRouteQueryDone = false;

        /// <summary>
        /// 記錄整個導航路線總距離
        /// </summary>
        int mTotalDistanceMeter = 0;

        //將RouteQuery的呼叫，擋住，要是還沒query完成要擋掉
        //private bool mRouteQueryIsWorking = false;//這樣還是沒用，作法改成有Position_Change就不要取得QueryGlobalPosition了

        //要是有Geolocator表示有打開GPS
        public bool IfHasGeolocator() { if (mMyGeolocator != null) { return true; } return false; }

        //取得訂位及路線規劃資訊
        private MyGeolocatorTracking mMyGeolocator = null;
        private MyRouteQuery mMyRouteQuery = null;

        //路線指示view
        private RouteInstructorView mRouteInstructorView = null;
        public RouteInstructorView GetRouteInstructorView() { return mRouteInstructorView; }

        //將路線畫出
        private RouteRendererControl mRouteRendererControl = null;
        public RouteRendererControl GetRouteRendererControl() { return mRouteRendererControl; }

        //用來餵起點終點資訊，做路線規劃
        private List<GeoCoordinate> _SearchTermCoordinateList = new List<GeoCoordinate>(2);

        //顯示"處理中"
        // private MyProgressIndicator mMyProgressIndicator = null;

        //目前我的定位座標
        private GeoCoordinate mNowMyGeoCoordinate = null;
        public GeoCoordinate GetNowMyGeoCoordinate() { return mNowMyGeoCoordinate; }

        //路徑規劃的路線point array
        private List<Point> _routeQueryPointArray = new List<Point>(128);
        public List<Point> GetRouteQueryPointArray() { return _routeQueryPointArray; }
        //    System.Windows.Media.PointCollection mRouteQueryPointArray = new System.Windows.Media.PointCollection();

        //目前我位於路線規劃陣列的位址，(搜尋方式為根據我目前mRouteQueryPoints的陣列位址，往前後3個陣列位址計算距離取最小者，為目前的最新路線上)
        //int miNowIndexOnRoute = 0;

        //目的地定位座標
        private GeoCoordinate _GoalCoordinate = null;
        private TravelMode _TravelMode;

        //轉彎提示
        private DriveNoticeControl mDriveNoticeControl;
        public DriveNoticeControl GetDriveNoticeControl() { return mDriveNoticeControl; }

        //脫離路線的記數，若是超過3次，就路線重新規劃
        private int mLeaveRoadCount = 0;

        private double mNowSpeedKMHR;         //目前時速

        /// <summary>
        ///根據目前時速所計算出來，目前要的路線指示範圍(單位是LonLat)
        /// </summary>
        public double mViewRangeInSpeedLonLat = 0;

        /// <summary>
        /// 紀錄是否有建立GPS或是路徑規劃
        /// </summary>
        //private bool mbGeolocator, mbRouteQuery; 

        //用google TTS來唸語音
        GoogleTranslateVoice mGoogleTranslateVoice = null;



        /// <summary>
        /// 因為GPS以及路線規劃都放在RouteRendererComponent裡面，所以，要把他NEW出來，但是另外要用的畫出路線這邊不去做處理
        /// </summary>
        /// <returns></returns>
        public static RouteRendererComponent CreateRouteRendererComponent(
            System.Windows.Controls.Panel layoutRoot,
            bool bGeolocator, bool bRouteQuery, bool bCreateNotic)
        {
            RouteRendererComponent routeRendererComponent = new RouteRendererComponent();
            routeRendererComponent.mLayoutRoot = layoutRoot;

            //建立元件
            if (bGeolocator)
            {
                routeRendererComponent.mMyGeolocator = MyGeolocatorTracking.Create(-1, 5);
                //MapUtility.GetMyGeolocator(0, 5);//設定時間間格更新，根本沒有用，設定為5公尺更新


            }

            if (bRouteQuery)
            {
#if DEBUG
                MyUtility.Utility.OutputDebugString("RouteRendererComponent CreateRouteRendererComponent : bRouteQuery=true");
#endif
                routeRendererComponent.mMyRouteQuery = MapUtility.GetMyRouteQuery();

                if (bCreateNotic)
                {
                    //建立轉彎提示control
                    if (ResourceApp.Resources.AppResources.ResourceFlowDirection == "RightToLeft")
                        routeRendererComponent.mDriveNoticeControl = new DriveNoticeControl(FlowDirection.RightToLeft);
                    else
                        routeRendererComponent.mDriveNoticeControl = new DriveNoticeControl(null);
                    if (GoogleTranslateVoice.CheckTTS_Language(Utility.GetCurrentLanguageRegionName()) == false)
                        routeRendererComponent.mDriveNoticeControl.CreateGoogleTTS();
                }
            }

            //routeRendererComponent.mbGeolocator = bGeolocator;
            //routeRendererComponent.mbRouteQuery = bRouteQuery;

            return routeRendererComponent;
        }

        public static void AddToRoot(RouteRendererComponent routeRendererComponent,
            int viewpPosX, int viewpPosY, int w, int h)
        {
            routeRendererComponent._createRouteView(viewpPosX, viewpPosY, w, h);

            routeRendererComponent.mLayoutRoot.Children.Add(routeRendererComponent.GetRouteInstructorView());
            routeRendererComponent.mLayoutRoot.Children.Add(routeRendererComponent.GetDriveNoticeControl());
        }

        /// <summary>
        /// DriveNoticeControl要蓋住所有的東西，要移到最後一個，不然覆蓋的順序會出問題
        /// </summary>
        /// <param name="layoutRoot"></param>
        public static void MoveDriveNoticeControlToLast(System.Windows.Controls.Panel layoutRoot)
        {
            DriveNoticeControl driveNotic = null;
            foreach (UIElement uie in layoutRoot.Children)
            {
                driveNotic = uie as DriveNoticeControl;
                if (driveNotic != null)
                {
                    layoutRoot.Children.Remove(driveNotic);
                    break;
                }
            }

            if (driveNotic != null)
                layoutRoot.Children.Add(driveNotic);
        }

        public static void RemoveFromRoot(System.Windows.Controls.Panel layoutRoot,
            RouteInstructorView routeInstructorView, DriveNoticeControl driveNoticControl)
        {
            if (routeInstructorView != null)
                layoutRoot.Children.Remove(routeInstructorView);
            if (driveNoticControl != null)
                layoutRoot.Children.Remove(driveNoticControl);
        }


        public RouteRendererComponent()
        {
            if (mDateTimeStart.Equals(DateTime.MaxValue))
            {
                mDateTimeStart = DateTime.Now;
            }
        }

        private void _createRouteView(int viewPosX, int viewPosY, int w, int h)
        {
            if (ResourceApp.Resources.AppResources.ResourceFlowDirection == "RightToLeft")
                mRouteInstructorView = new RouteInstructorView(FlowDirection.RightToLeft);
            else
                mRouteInstructorView = new RouteInstructorView();


            //mRouteInstructorView.Width = w;
            //mRouteInstructorView.Height = h;

            mRouteRendererControl = mRouteInstructorView.GetRouteRendererControl();

            mRouteRendererControl.initLine(false);

            //移動control座標
            Utility.SetControlPosition(mRouteInstructorView, viewPosX, viewPosY, true);
            Utility.SetControlWH(mRouteInstructorView, w, h, true);
        }

        public void StartRouteQuery(GeoCoordinate goalCoordinate, TravelMode travelMode)
        {
            bFirstEnter = true;
            _GoalCoordinate = goalCoordinate;
            _TravelMode = travelMode;

            StartPositionTracking();
        }

        public void StartPositionTracking()
        {
            Utility.ShowProgressIndicator(ResourceApp.Resources.AppResources.Utility_Positioning_PleaseWait, true);

            Utility.GetCurrentDispatcher().BeginInvoke(() =>
            {
                //建立定位元件，不斷更新目前座標
                //mMyGeolocator.QueryGlobalPosition(Callback_QueryGlobalPosition);
                mMyGeolocator.StartPositionTracking(Callback_PositionChanged);
            });
        }

        public void Dispose()
        {
            lock (mDisposingLock)
            {
                bIsDisposing = true;
#if DEBUG
                Utility.OutputDebugString("RouteRendererComponents disposing...bDisposing = true;");
#endif


                if (mMyGeolocator != null)
                    mMyGeolocator.Dispose();
                mMyGeolocator = null;
                // MapUtility.ReleaseMyGeolocator(ref mMyGeolocator);
                //if (mMyGeolocator != null)
                //    mMyGeolocator.Dispose();
                //mMyGeolocator = null;

                MapUtility.ReleaseMyRouteQuery(ref mMyRouteQuery);
                //if (mMyRouteQuery != null)
                //    mMyRouteQuery.Dispose();
                //mMyRouteQuery = null;

                mRouteRendererControl = null;

                if (mDriveNoticeControl != null)
                {
                    mDriveNoticeControl.ReleaseGoogleTTS();
                    mDriveNoticeControl.ClearVocalReadText();
                }

                _releaseMyReverseGeocodeQuery_street();
            }
        }

        //取得自己做標
        //private void Callback_QueryGlobalPosition(ref System.Windows.Point outPoint)
        //{
        //    //assign 值給他會壞，要用new的
        //    //mNowMyGeoCoordinate.Longitude = outPoint.X;
        //    //mNowMyGeoCoordinate.Latitude = outPoint.Y;
        //    mNowMyGeoCoordinate = new GeoCoordinate(outPoint.X, outPoint.Y);

        //    _SearchTermCoordinateList.Clear();
        //    _SearchTermCoordinateList.Add(mNowMyGeoCoordinate);
        //    _SearchTermCoordinateList.Add(_GoalCoordinate);

        //    mRouteInstructorView.Dispatcher.BeginInvoke(() =>
        //    {
        //        QueryRoute(_SearchTermCoordinateList, _TravelMode);
        //    });
        //}

#if DEBUG
        int Callback_PositionChanged_Count = 0;
#endif

        double mPositionChangeSavePowerDis = -1;
        double mPositionChangeSavePowerDisCount = 0;


        object mDisposingLock = new object();
        bool bIsDisposing = false;
        //定位改變檢測
        public void Callback_PositionChanged(ref Point newPositionPoint, double speedMeterPerSec, double accuracyMeter)
        {

#if DEBUG
            MyUtility.Utility.OutputDebugString("Callback_PositionChanged begin");
#endif

            if (accuracyMeter > 70)
            {
                MyUtility.Utility.OutputDebugString("*******************");
                MyUtility.Utility.OutputDebugString(
                    "[Callback_PositionChanged]----Accuracy too bad : " + accuracyMeter);
                //return;拜託這邊千萬別return 會出BUG
            }

#if DEBUG
            MyUtility.Utility.OutputDebugString("mRouteRECList.Add(rec); begin   :  " + newPositionPoint.ToString());
#endif
            lock (mDisposingLock)
            {
                if (bIsDisposing)
                    return;
            }

            lock (mRouteRECListLock)
            {
                try
                {
#if DEBUG
                    MyUtility.Utility.OutputDebugString("lock (mRouteRECListLock) done");
#endif
                    //RouteREC rec;
                    //rec.lat = (float)newPositionPoint.X; rec.lon = (float)newPositionPoint.Y;
                    GeoCoordinate rec = new GeoCoordinate();
                    rec.Latitude = newPositionPoint.X;
                    rec.Longitude = newPositionPoint.Y;

#if DEBUG
                    MyUtility.Utility.OutputDebugString("mRouteRECList.Add(rec); begin  :  " + rec.ToString());
#endif

                    if (mRouteRECList == null)
                        mRouteRECList = new GeoCoordinateCollection();
                    mRouteRECList.Add(rec);

#if DEBUG
                    MyUtility.Utility.OutputDebugString("mRouteRECList.Add(rec); end");
#endif
                }
                catch (Exception e)
                {
                    MyUtility.Utility.OutputDebugString(e.Message);
                }
            }



#if DEBUG
            Utility.OutputDebugString("[RouteRendererComponent]---PositionChanged : " + Callback_PositionChanged_Count);
            Callback_PositionChanged_Count++;
#endif

            //assign 值給他會壞，要用new的
            //mNowMyGeoCoordinate.Longitude = newPositionPoint.X;
            //mNowMyGeoCoordinate.Latitude = newPositionPoint.Y;
            mNowMyGeoCoordinate = new GeoCoordinate(newPositionPoint.X, newPositionPoint.Y);

            //根據speed計算view range
            double kmhr = 0;
            //if (geocoord != null)
            // {
            //轉乘 KM/HR
            //   if (geocoord.Speed == null || geocoord.Speed.HasValue == false || double.IsNaN(geocoord.Speed.Value))
            //      kmhr = 0;
            //  else
            kmhr = speedMeterPerSec * 60 * 60;
            kmhr /= 1000;
            // }
            _setNowSpeedKMHR(kmhr);
            double moveDistanceMeter = _setGPSAcuracy(accuracyMeter);

            if (bFirstEnter && _GoalCoordinate != null)//第一次進入position change，要去做query route第一次
            {
#if DEBUG
                MyUtility.Utility.OutputDebugString("Callback_PositionChanged (bFirstEnter && _GoalCoordinate != null)");
#endif

                Utility.ShowProgressIndicator(ResourceApp.Resources.AppResources.Goal_Route_Planning, true);

                bFirstEnter = false;
                mRouteQueryDone = false;
                mPositionChangeSavePowerDis = -1;

                //Utility.HideProgressIndicator();
                //Utility.ShowProgressIndicator(System.Windows.Deployment.Current, "Route query...", true);

                _SearchTermCoordinateList.Clear();
                _SearchTermCoordinateList.Add(mNowMyGeoCoordinate);
                _SearchTermCoordinateList.Add(_GoalCoordinate);

                Utility.GetCurrentDispatcher().BeginInvoke(() =>
                {
                    lock (mDisposingLock)
                    {
                        if (bIsDisposing)
                        {
#if DEBUG
                            MyUtility.Utility.OutputDebugString("Callback_PositionChanged bIsDisposing...QueryRoute");
#endif
                            return;
                        }
                        QueryRoute(_SearchTermCoordinateList, _TravelMode);
                    }
                });

                return;
            }
            else
            {
#if DEBUG
                MyUtility.Utility.OutputDebugString("Callback_PositionChanged esle 2");
#endif

                //有mMyRouteQuery，代表有路線規劃就走這邊(只有導航進度元件而沒路線規劃元件，是不被允許的)，有路線規畫依訂有GPS
                if (mMyRouteQuery != null)
                {
                    lock (mRouteQueryDoneLock)
                    {
                        //這邊要注意一點，bFirstEnter之後去route query，但馬上來第二次position change，可是route query卻還沒好，
                        //跑下去就會crash了，所以，這裡要注意，有route query才能執行
                        if (mRouteQueryDone == false)
                            return;

#if DEBUG
                        MyUtility.Utility.OutputDebugString("Callback_PositionChanged mRouteQueryDone");
#endif

                        //計算目前距離與剛剛距離是否有超過可以計算CHANGE直
                        mPositionChangeSavePowerDisCount += moveDistanceMeter;
                        if (mPositionChangeSavePowerDis < 0)
                        {
                            mPositionChangeSavePowerDis = kmhr / 2;//時速100就過100公尺再更新                           
                        }
                        else
                        {
                            if (mPositionChangeSavePowerDisCount < mPositionChangeSavePowerDis)
                            {
                                return;
                            }
                            else
                            {
                                mPositionChangeSavePowerDis = kmhr / 2;//時速100就過100公尺再更新
                                //mPositionChangeSavePowerDisCount -= mPositionChangeSavePowerDis;
                                mPositionChangeSavePowerDisCount = 0;
                            }
                        }

#if DEBUG
                        MyUtility.Utility.OutputDebugString("Callback_PositionChanged GetCurrentDispatcher begin");
#endif
                        Utility.GetCurrentDispatcher().BeginInvoke(() =>
                        {
                            lock (mDisposingLock)
                            {
                                if (bIsDisposing)
                                {
#if DEBUG
                                    MyUtility.Utility.OutputDebugString("Callback_PositionChanged bIsDisposing");
#endif
                                    return;
                                }


                                Point position = new Point(mNowMyGeoCoordinate.Longitude, mNowMyGeoCoordinate.Latitude);
                                Point range = new Point(mViewRangeInSpeedLonLat, mViewRangeInSpeedLonLat);
                                float turnCircleRadius = (float)Utility.Kilometer2LonLat(Utility.TURN_NOTICE_DIS_METER / 1000f);

#if DEBUG
                                MyUtility.Utility.OutputDebugString("Callback_PositionChanged SetGlobalCentor_WithOffset begin");
#endif
                                mRouteRendererControl.SetGlobalCentor_WithOffset(position, range, turnCircleRadius);
#if DEBUG
                                MyUtility.Utility.OutputDebugString("Callback_PositionChanged SetGlobalCentor_WithOffset end");


                                MyUtility.Utility.OutputDebugString("Callback_PositionChanged CorrectRouteDirection_RotateView begin");
#endif
                                double remainDisToNextTurn = 0;
                                //位置有變，就偵測一下目前的路徑完成位置，更新路線指示畫面
                                RouteRendererControl._OnRouteData data =
                                    CorrectRouteDirection_RotateView(mNowMyGeoCoordinate.Longitude, mNowMyGeoCoordinate.Latitude,
                                    out remainDisToNextTurn);
#if DEBUG
                                MyUtility.Utility.OutputDebugString("Callback_PositionChanged CorrectRouteDirection_RotateView end");
#endif
                                if (data == null)//路線偏離之後，重新QueryRoute，在得到新路線資訊之前，這邊會得到null並return
                                    return;

#if DEBUG
                                MyUtility.Utility.OutputDebugString("Callback_PositionChanged _updateSpeedDistanceData begin");
#endif
                                //更新速度與距離資訊給有註冊的UI 
                                _updateSpeedDistanceData(kmhr,
                                    (int)data.drivedDistanceMeter,
                                    // (int)(Utility.LonLat2Kilometer(data.drivedDistanceLonLat) * 1000f),
                                        mTotalDistanceMeter,
                                        remainDisToNextTurn//data.nowRouteMovingDistanceMeter
                                        );
#if DEBUG
                                MyUtility.Utility.OutputDebugString("Callback_PositionChanged _updateSpeedDistanceData end");
#endif

                                //航道偏離的判斷
                                if (data.minDisLonLat > MyUtility.Utility.ROAD_WIDTH_LonLat)
                                {
#if DEBUG
                                    Utility.OutputDebugString("航道偏離 ++mLeaveRoadCount = ++" + mLeaveRoadCount);
                                    if (mDriveNoticeControl != null)
                                        mDriveNoticeControl.SetNotice("偏" + mLeaveRoadCount, 2, true, 1, 1, 14, 6, CallbackShowTextTimeOut);
#endif


                                    if (mLeaveRoadCount >= 3)
                                    {
#if DEBUG
                                        Utility.OutputDebugString("[RouteRendererComponent]---重新規劃路線 mLeaveRoadCount = " + mLeaveRoadCount);
#endif
                                        //把之前的語音清空
                                        if (mDriveNoticeControl != null)
                                        {
                                            mDriveNoticeControl.ClearVocalReadText();
                                            mDriveNoticeControl.SetNotice(AppResources.ROUTE_REPLANNING, 2, true, 1, 1, 14, 6, CallbackShowTextTimeOut);
                                        }


                                        //query好新的路線之前，先不要position tracking                     
                                        mMyGeolocator.StopTracking();

                                        //這邊要射程false，因為position tracking被關掉了，只能利用route query來開啟position tracking
                                        bFirstEnter = false;

                                        //如果路徑偏差太多，再去query road，
                                        _SearchTermCoordinateList.Clear();
                                        _SearchTermCoordinateList.Add(mNowMyGeoCoordinate);
                                        _SearchTermCoordinateList.Add(_GoalCoordinate);
                                        QueryRoute(_SearchTermCoordinateList, _TravelMode);

                                        mMyGeolocator.DisablePosShiftTest();
                                    }
                                    mLeaveRoadCount++;
                                }
                            }
                        }
                            );

#if DEBUG
                        MyUtility.Utility.OutputDebugString("Callback_PositionChanged GetCurrentDispatcher end");
#endif
                    }
                }
                else if (mMyGeolocator != null)//(數位儀表版)只有GPS沒有導航路線規劃，就不需要無謂的計算，只需要GPS定位就夠了
                {
                    Utility.HideProgressIndicator();//直接GPS定位，結束顯示"處理中"

                    Utility.GetCurrentDispatcher().BeginInvoke(() =>
                    {
                        lock (mDisposingLock)
                        {
                            if (bIsDisposing)
                            {
#if DEBUG
                                MyUtility.Utility.OutputDebugString("Callback_PositionChanged bIsDisposing..._updateSpeedDistanceData");
#endif
                                return;
                            }
                            //更新速度與距離資訊給有註冊的UI 
                            _updateSpeedDistanceData(kmhr, 0, 0, 0);
                        }
                    });
                }
            }

#if DEBUG
            MyUtility.Utility.OutputDebugString("Callback_PositionChanged end");
#endif

        }

        //#if DEBUG
        //        int QueryRouteCount = 0;
        //#endif

        /// <summary>
        /// RouteQuery要在UI thread中去RUN，呼叫時要注意
        /// 規劃路線資訊(若是目前航道偏離，可用來重新規劃路線用)
        /// </summary>
        private void QueryRoute(List<GeoCoordinate> routeQuery, TravelMode travelMode)
        {
            Utility.PlaySound("Assets/music/bip1.wav");

            //#if DEBUG
            //            QueryRouteCount++;
            //            mDriveNoticeControl.SetNotice(ResourceApp.Resources.AppResources.Goal_Route_Planning + QueryRouteCount, 2, true, 1, 1, 14, 6);
            //#endif

            //            if (mRouteQueryIsWorking)
            //            {
            //#if DEBUG
            //                MyUtility.Utility.OutputDebugString("RouteRendererComponent => mRouteQueryIsWorking");
            //#endif
            //                return;
            //            }
            //            mRouteQueryIsWorking = true;

            //     mMyProgressIndicator.ShowProgressIndicator("AppResources.CalculatingRouteProgressText");
            mMyRouteQuery.QueryRoute(routeQuery, Callback_RouteQuery, travelMode);


            //我幹嘛在這邊打了StopTracking，造成我的tracking都失敗，操...
            //mMyGeolocator.StopTracking();
        }

        private class MyRouteManeuver
        {
            public MyRouteManeuver() { }
            public double lat, lon;
            public string InstructionText;
            public int LengthInMeters;
        }

        List<RouteInstructorApp.DriveNoticeControl.DriveNoticeData> mDriveNoticeDataList = null;

        void Callback_RouteQuery(Route route, Object tag)
        {
#if DEBUG
            Utility.OutputDebugString("Callback_RouteQuery begin");
#endif
#if DEBUG
            try
            {
#endif

                lock (mRouteQueryDoneLock)
                {
                    if (route == null)
                    {
                        Utility.ShowMessageBox(ResourceApp.Resources.AppResources.Goal_Route_Planning_Fail);

                        QueryRoute(_SearchTermCoordinateList, _TravelMode);
                        return;
                    }

                    if (route.Legs.Count == 0)
                        return;

                    //把之前的語音清空
                    //mDriveNoticeControl.ClearVocalReadText();

                    //因為路線脫離那邊，會把定位STOP(路線重新query想先把定位停止)，所以這邊得到新路線後還需要start
                    StartPositionTracking();

                    Utility.HideProgressIndicator();

                    //將經緯度路線點存成 point array
                    _routeQueryPointArray.Clear();
                    foreach (GeoCoordinate geo in route.Legs[0].Geometry)
                        _routeQueryPointArray.Add(new Point(geo.Longitude, geo.Latitude));

                    //有元件，像是"導航進度"，要是只有導航進度在HUD中，這時沒有new出mRouteRendererControl，
                    //進度就根本無法計算，但是mRouteRendererControl卻不行attach在root下面被畫出來，這邊_createRouteView有new就好
                    //會這樣的主因是因為，我沒有將導航路徑計算功能拆出來，導致跟mRouteRendererControl牽扯不清，但因為目前只有導航進度有牽扯，所以先暫時這樣弄
                    if (mRouteRendererControl == null)
                        _createRouteView(0, 0, 0, 0);

                    mTotalDistanceMeter = mRouteRendererControl.SetGlobalLines(_routeQueryPointArray);

#if DEBUG
                    Utility.OutputDebugString("Callback_RouteQuery 目前的轉彎點位置，往後偵測一個轉彎點");
#endif
                    //目前的轉彎點位置，往後偵測一個轉彎點，檢查是否超級短(<10m)要是超級短，就將2個路徑點的轉彎提示合併在目前這個路徑點上
                    List<MyRouteManeuver> routeManeuverList = new List<MyRouteManeuver>();
                    int turnPointCount = 0;
                    bool nextWillContinue = false;
                    foreach (RouteManeuver mneuver in route.Legs[0].Maneuvers)
                    {
                        if (nextWillContinue)
                        {
                            nextWillContinue = false;
                            continue;
                        }

                        MyRouteManeuver newRouteManeuver = new MyRouteManeuver();
                        newRouteManeuver.lat = mneuver.StartGeoCoordinate.Latitude;
                        newRouteManeuver.lon = mneuver.StartGeoCoordinate.Longitude;
                        newRouteManeuver.LengthInMeters = mneuver.LengthInMeters;
                        newRouteManeuver.InstructionText = mneuver.InstructionText;

                        if (turnPointCount + 1 < route.Legs[0].Maneuvers.Count)
                        {
                            RouteManeuver mneuverNext = route.Legs[0].Maneuvers[turnPointCount + 1];

                            //檢查是否<10m，小於10m就用接下來將2點合為一點
                            double length = Utility.LonLat2Meter(
                                newRouteManeuver.lon,
                                newRouteManeuver.lat,
                                mneuverNext.StartGeoCoordinate.Longitude,
                                mneuverNext.StartGeoCoordinate.Latitude);
                            if (length < 20)
                            {
                                if (newRouteManeuver.InstructionText.Equals(mneuverNext.InstructionText) == false)//如果下一個轉彎點講的話也是重複，就不要將2句合併了
                                    newRouteManeuver.InstructionText += " " + Utility.GetTranslate_Next() + " " + mneuverNext.InstructionText;

                                newRouteManeuver.LengthInMeters += mneuverNext.LengthInMeters;
                                nextWillContinue = true;
                            }
                        }

                        routeManeuverList.Add(newRouteManeuver);

                        turnPointCount++;
                    }
#if DEBUG
                    Utility.OutputDebugString("Callback_RouteQuery 設定轉彎點給轉彎提示去偵測");
#endif
                    //設定轉彎點給轉彎提示去偵測
                    List<Point> turnCircleList = new List<Point>();
                    mDriveNoticeDataList = new List<DriveNoticeControl.DriveNoticeData>(32);
                    foreach (MyRouteManeuver mneuver in routeManeuverList)
                    {
                        //rangeSquare設定一個範圍，超過就不鳥他
                        //計算位於哪個路線ID上(使用轉彎點若是位於導航點很接近的範圍內，就將導航點的路線ID歸給這轉彎點)
                        double rangeSquare = Utility.Kilometer2LonLat(5f / 1000f);//5公尺內的轉彎點，就當作是該導航點的
                        rangeSquare *= rangeSquare;

                        int id = -2;//因為-1是導航路線最一開始的轉彎點(自己的位置開始)，0以上才是導航線段的陣列位址
                        int count = 0;
                        int hasDetectedCount = -1;
                        double minDisSquare = 99999999999;
                        double tmpDisSquare;
                        Point tmpDis = new Point();
                        foreach (Point pos in _routeQueryPointArray)
                        {
                            tmpDis.X = mneuver.lon - pos.X;
                            tmpDis.Y = mneuver.lat - pos.Y;
                            tmpDisSquare = tmpDis.X * tmpDis.X + tmpDis.Y * tmpDis.Y;
                            if (tmpDisSquare < rangeSquare &&
                                tmpDisSquare < minDisSquare)
                            {
                                minDisSquare = tmpDisSquare;
                                id = count - 1;
                                hasDetectedCount = count;
                            }
                            count++;

                            //用來做，如果已經抓到1個點，然後又距離count已經超過20，就表示越找越遠了，後面根本不可能會有範圍內的點，直接可以掰了
                            if (hasDetectedCount >= 0 && count - hasDetectedCount > 20)
                                break;
                        }

                        // 目前的轉彎點位置，往後偵測一個轉彎點，檢查是否超級短(<10m)要是超級短，就將2個路徑點的轉彎提示合併在目前這個路徑點上
                        //if (id >= 0)
                        //{
                        //    route.Legs[0].Maneuvers[turnPointCount+1]
                        //}


                        //上面寫那堆就是要找目前轉彎點位於哪個島航線段上，找到id後下面就將資料設定吧
                        RouteInstructorApp.DriveNoticeControl.DriveNoticeData driveNoticeData =
                            new RouteInstructorApp.DriveNoticeControl.DriveNoticeData(
                                mneuver.InstructionText,
                                mneuver.lon,
                                mneuver.lat,
                                id);

                        mDriveNoticeDataList.Add(driveNoticeData);

                        if (mneuver != routeManeuverList[0])
                            turnCircleList.Add(new Point(mneuver.lon, mneuver.lat));


                    }
                    if (mDriveNoticeControl != null)
                        mDriveNoticeControl.SetDriveNoticeList(mDriveNoticeDataList);
                    mRouteRendererControl.SetTurnCircleList(turnCircleList);

                    //mMyProgressIndicator.HideProgressIndicator();

                    mLeaveRoadCount = 0;
                    //mLeaveRoadCount-=2;

                    ////先暫時設定看見整個路線
                    //Point position = new Point(mNowMyGeoCoordinate.Longitude, mNowMyGeoCoordinate.Latitude);
                    //mRouteRendererControl.SetGlobalCentor_WithOffset(position,
                    //    new Point(route.BoundingBox.WidthInDegrees, route.BoundingBox.HeightInDegrees));//根據bounding box做縮放

                    Point position = new Point(mNowMyGeoCoordinate.Longitude, mNowMyGeoCoordinate.Latitude);
                    Point range = new Point(mViewRangeInSpeedLonLat, mViewRangeInSpeedLonLat);
                    float turnCircleRadius = (float)Utility.Kilometer2LonLat(Utility.TURN_NOTICE_DIS_METER / 1000f);

                    mRouteRendererControl.SetGlobalCentor_WithOffset(position, range, turnCircleRadius);
#if DEBUG
                    Utility.OutputDebugString("Callback_RouteQuery 根據我的路線位置轉角度");
#endif
                    //根據我的路線位置轉角度
                    double reaminDosToNextTurn;
                    RouteRendererControl._OnRouteData data =
                        CorrectRouteDirection_RotateView(position.X, position.Y, out reaminDosToNextTurn);
#if DEBUG
                    Utility.OutputDebugString("Callback_RouteQuery 更新路線距離資訊");
#endif
                    Utility.GetCurrentDispatcher().BeginInvoke(() =>
                    {
                        _updateSpeedDistanceData(0, 0, mTotalDistanceMeter, reaminDosToNextTurn);//更新路線距離資訊
                    });

                    mRouteQueryDone = true;
                }

                mMyGeolocator.EnablePosShiftTest();
#if DEBUG
            }
            catch (Exception e)
            {
                Utility.OutputDebugString("Callback_RouteQuery Exception");
                Utility.ShowMessageBox(e.Message);
            }
#endif

#if DEBUG
            Utility.OutputDebugString("Callback_RouteQuery end");
#endif
        }

        public int Set_Longitude_Latitude_Range(float scaleXY)
        {
            Point ppp = new Point(mNowMyGeoCoordinate.Longitude, mNowMyGeoCoordinate.Latitude);
            float turnCircleRadius = (float)Utility.Kilometer2LonLat(Utility.TURN_NOTICE_DIS_METER / 1000f);

            return mRouteRendererControl.SetGlobalCentor_WithOffset(ppp, new Point(scaleXY, scaleXY), turnCircleRadius);
        }

        ///// <summary>
        ///// 更新目前路線上的Index (miNowIndexOnRoute)，回傳舊的index
        ///// 根據目前定位座標，找出對印在 規劃路線上的位置
        ///// (搜尋方式為根據我目前mRouteQueryPoints的陣列位址，往前後3個陣列位址計算距離取最小者，為目前的最新路線上)
        ///// </summary>
        //private int RefreshNowIndexOnRoute(double nowLongitude, double nowLatitude)
        //{
        //    int oldIndex = miNowIndexOnRoute;
        //    float tempDiffX, tempDiffY;
        //    float newDistance, smallerDistance;
        //    int new_miNowOnRouteIndex;
        //    Point nowMyGP = new Point(nowLongitude, nowLatitude);

        //    //拿定位座標跟目前判定的路線上座標先算距離
        //    tempDiffX = (float)nowMyGP.X - (float)mRouteQueryPointArray[miNowIndexOnRoute].X;
        //    tempDiffY = (float)nowMyGP.Y - (float)mRouteQueryPointArray[miNowIndexOnRoute].Y;
        //    smallerDistance = tempDiffX * tempDiffX + tempDiffY * tempDiffY;
        //    new_miNowOnRouteIndex = miNowIndexOnRoute;

        //    //往前偵測3個
        //    for (int a = 1; a <= 3; a++)
        //    {
        //        int index = miNowIndexOnRoute + a;
        //        if (index >= mRouteQueryPointArray.Count)
        //            break;

        //        tempDiffX = (float)nowMyGP.X - (float)mRouteQueryPointArray[index].X;
        //        tempDiffY = (float)nowMyGP.Y - (float)mRouteQueryPointArray[index].Y;
        //        newDistance = tempDiffX * tempDiffX + tempDiffY * tempDiffY;
        //        if (newDistance < smallerDistance)
        //        {
        //            smallerDistance = newDistance;
        //            new_miNowOnRouteIndex = index;
        //        }
        //    }

        //    //往後偵測3個
        //    for (int a = 1; a <= 3; a++)
        //    {
        //        int index = miNowIndexOnRoute - a;
        //        if (index < 0)
        //            break;

        //        tempDiffX = (float)nowMyGP.X - (float)mRouteQueryPointArray[index].X;
        //        tempDiffY = (float)nowMyGP.Y - (float)mRouteQueryPointArray[index].Y;
        //        newDistance = tempDiffX * tempDiffX + tempDiffY * tempDiffY;
        //        if (newDistance < smallerDistance)
        //        {
        //            smallerDistance = newDistance;
        //            new_miNowOnRouteIndex = index;
        //        }
        //    }

        //    //miNowIndexOnRoute = new_miNowOnRouteIndex;

        //    //若是有接近下ㄧ個點，就檢查他到達檢查點了沒
        //    if (new_miNowOnRouteIndex > oldIndex)
        //    {
        //        //如果到達下ㄧ個點的非常接近距離，就當作到達(必須提示，現在馬上轉彎)
        //    }

        //    return oldIndex;
        //}

        bool nReachEnd = false;

        /// <summary>
        /// 給顯示接下來要怎麼走的導航提示DriveNoticeControl顯示完文字之後對上層manager(RoutRenderComponent)的通知，
        /// Manager收到DriveNoticeControl結束提示之後，還要通知RouteInstructorView去持續顯示目前要怎麼走的文字提示
        /// instrustionTextIsClose 傳入DriveNoticeControl是否正在顯示提示，或是關閉提示，
        /// DriveNoticeControl關閉提示的話，mRouteInstructorView要顯示提示
        /// </summary>
        void CallbackShowTextTimeOut(bool instrustionTextIsClose)
        {
            mRouteInstructorView.ShowCurrentRouteInstruction(!instrustionTextIsClose);
        }

        /// <summary>
        ///  根據我目前位於路線上的位置，計算目前行進方向朝上，做轉角度的動作，
        ///  另外，還判斷了是否接近轉彎點的偵測，有觸發就會提示轉彎
        ///  基本上就是給GPS呼叫Position Changed的時候要去計算一次，
        ///  (public 狀態為，給外部呼叫做測試用)
        ///  回傳根據目前位置偵測路線上的資訊(給外部判斷是否黎路線太遠，要重新規劃路線之類的)
        /// </summary>     
        public RouteRendererControl._OnRouteData CorrectRouteDirection_RotateView(double nowLongitude, double nowLatitude,
            out double outRemainDisMeterToNextTurnPoint)
        {
            outRemainDisMeterToNextTurnPoint = 0;
            RouteRendererControl._OnRouteData onRouteData = mRouteRendererControl.GetNowIndexOnRoute(nowLongitude, nowLatitude);

            if (onRouteData == null)
                return null;

            if (onRouteData.nowIndex >= _routeQueryPointArray.Count - 1)//到終點了
            {
                if (nReachEnd != true)
                {
                    nReachEnd = true;
                    if (mDriveNoticeControl != null)
                        mDriveNoticeControl.YouAreGoalNotice(true, CallbackShowTextTimeOut);
                }
                return null;
            }

            double remainDisMeterToNextTurnPointNow = 0;
            RouteRendererControl.RouteSectionData[] routeSectionDataArray = mRouteRendererControl.GetRouteSectionArray();
            if (onRouteData.nowIndex >= onRouteData.oldIndex)//路徑點變換要轉方向為行徑方向朝上
            {
                //計算 miNowIndexOnRoute 往前的向量
                Vector2 baseDir = new Vector2(routeSectionDataArray[onRouteData.nowIndex].vector.X, routeSectionDataArray[onRouteData.nowIndex].vector.Y);
                //Vector2 baseDir = new Vector2(
                //    (float)mRouteQueryPointArray[onRouteData.nowIndex + 1].X - (float)mRouteQueryPointArray[onRouteData.nowIndex].X,
                //    (float)mRouteQueryPointArray[onRouteData.nowIndex + 1].Y - (float)mRouteQueryPointArray[onRouteData.nowIndex].Y);

                float degree = GetDegree2Vector(Microsoft.Xna.Framework.Vector2.UnitY, baseDir);

                //degree = mRouteRendererControl.GetNowRouteUpDegree(miNowIndexOnRoute);
                mRouteRendererControl.SetRotationTransform(degree);


                if (mDriveNoticeControl != null)
                {
                    DriveNoticeControl.DriveNoticeData nowRouteDriveNoticeData;
                    DriveNoticeControl.DriveNoticeData nextTurnRouteDriveNoticeData;

                    //語音轉彎提示要出現箭頭時，要計算現在路線一直到下一個轉彎點的路線的轉彎角度，去畫出箭頭來，
                    //這邊要特別處理先讓ConsiderNotice先去計算出，下一個轉彎路線線段，然後丟出來外部計算角度，再去做一次語音轉彎提示
                    DriveNoticeControl.TempDistanceNoticData tempDistanceNoticData;
                    int nextTurnRouteID;
                    DriveNoticeControl.DriveNoticeData driveNoticeData =
                        mDriveNoticeControl.ConsiderNotice(
                                                            mNowSpeedKMHR,
                                                            onRouteData,
                                                            routeSectionDataArray,
                                                            true,
                                                            out tempDistanceNoticData,
                                                            out nextTurnRouteID,
                                                            out nowRouteDriveNoticeData,
                                                            out nextTurnRouteDriveNoticeData,
                                                            CallbackShowTextTimeOut);

                    //當 tempDistanceNoticData != null的時候，表示ConsiderNotice(...)裡面沒去做語音提示(SetNotice(...))，而是將更多資訊丟出來
                    //塞在tempDistanceNoticData裡面，這邊要做的是要去呼叫SetNotice(...)畫出有路線轉彎圖示的版本。
                    if (tempDistanceNoticData != null && nextTurnRouteID != -1 && nextTurnRouteID < routeSectionDataArray.Length)
                    {
#if DEBUG
                        Utility.OutputDebugString("=================================");
                        Utility.OutputDebugString("=================================");
#endif
                        baseDir = new Vector2(
                            routeSectionDataArray[nextTurnRouteID - 1].vector.X,
                            routeSectionDataArray[nextTurnRouteID - 1].vector.Y);

                        //檢查是否要提示轉彎(找出下個轉彎點，要轉多大)
                        Vector2 nextDir =
                            new Vector2(routeSectionDataArray[nextTurnRouteID].vector.X, routeSectionDataArray[nextTurnRouteID].vector.Y);
                        float turnDegree = GetDegree2Vector(nextDir, baseDir);

                        //去做有畫出轉彎圖示的Notic
                        mDriveNoticeControl.DistanceNotic(tempDistanceNoticData, turnDegree, CallbackShowTextTimeOut);
                    }
                    else
                    {
                        //   Utility.OutputDebugString("語音判斷失敗了 : tempDistanceNoticData " + tempDistanceNoticData);
                    }


#if (DEBUG && DRAW_DEBUG)
                    mRouteRendererControl.RenderDebug_MyWay(driveNoticeData, Utility.ROAD_WIDTH_LonLat);
#endif

                    //新增持續的提示目前路線以及下一段路線
                    //必須判斷是否有mDriveNoticeControl才做這種提示，因為這些都是路線導航的提示訊息，要一起共進退
                    //像是只有導航進度(NavigationProgress)沒有路線指示的HUD，根本就不需要做語音與文字提示
                    //if (nowRouteDriveNoticeData != null)不會是NULL
                    {
                        //改為使用reverse geocoordinate去做目前street名稱的搜尋了
                        _queryStreetName(new GeoCoordinate(nowLatitude, nowLongitude));

                        //string ttt = nowRouteDriveNoticeData.speak;
                        //int startIndex = ttt.IndexOf(' ');
                        //if (startIndex >= 0)
                        //{
                        //    startIndex += 1;
                        //    int secondSpace = ttt.IndexOf(' ', startIndex);
                        //    if (secondSpace > startIndex)
                        //    {
                        //        ttt = ttt.Substring(startIndex, secondSpace - startIndex);// = 取出路名 nowRouteDriveNoticeData.speak
                        //        mRouteInstructorView.SetCurrentRouteInstruction(ttt);
                        //    }
                        //    else//找不到第二個空格 如"保持靠右並維持在 景平路。"，就讓它顯示"景平路。"
                        //    {
                        //        ttt.Substring(startIndex);
                        //        mRouteInstructorView.SetCurrentRouteInstruction(ttt);
                        //    }
                        //}
                    }

                    remainDisMeterToNextTurnPointNow = nextTurnRouteDriveNoticeData.remainDisMeter;
                    mDriveNoticeControl.SetNextRouteInstruction(
                        nextTurnRouteDriveNoticeData.speak,
                        remainDisMeterToNextTurnPointNow);
                }
            }
            //路徑沒變換，但是還是要計算離下一個轉彎點多遠，給mDriveNoticeControl用
            else if (mDriveNoticeControl != null)
            {
                //由 DriveNoticeData.ConsiderNotice，土法取得目前轉彎點的資訊
                DriveNoticeControl.DriveNoticeData nextTurnRouteDriveNoticeData = null;
                DriveNoticeControl.DriveNoticeData nowRouteDriveNoticeData = null;
                //bool bTheFirst = false;
                List<DriveNoticeControl.DriveNoticeData>.Enumerator itr = mDriveNoticeDataList.GetEnumerator();
                while (itr.MoveNext())
                {
                    DriveNoticeControl.DriveNoticeData data = itr.Current;
                    if (data.onRouteID == -1 && data.ifPass == false)//!= DriveNoticeData.Pass_Level.pass)
                    {
                        //導航最一開始的資訊
                        nextTurnRouteDriveNoticeData = nowRouteDriveNoticeData = data;

                        if (itr.MoveNext())
                            nextTurnRouteDriveNoticeData = itr.Current;

                        //bTheFirst = true;
                        break;
                    }
                    else if (onRouteData.nowIndex > data.onRouteID && data.ifPass == false)
                    {
                        nowRouteDriveNoticeData = data;
                    }
                    else if (onRouteData.nowIndex <= data.onRouteID && data.ifPass == false)
                    {
                        nextTurnRouteDriveNoticeData = data;
                        break;
                    }
                }
                itr.Dispose();


                //如果上面mDriveNoticeControl.ConsiderNotice沒計算還剩餘多少距離，那就這邊自己計算吧
                remainDisMeterToNextTurnPointNow = onRouteData.nowRouteTotalDistanceMeter - onRouteData.nowRouteMovingDistanceMeter;//目前線段上的剩餘距離Meter
                if (onRouteData.nowIndex < nextTurnRouteDriveNoticeData.onRouteID)//轉彎點在其他線段上
                {
                    for (int a = onRouteData.nowIndex + 1; a <= onRouteData.nowIndex; a++)
                        remainDisMeterToNextTurnPointNow += routeSectionDataArray[a].LengthMeter;
                }
            }

            outRemainDisMeterToNextTurnPoint = remainDisMeterToNextTurnPointNow;
            return onRouteData;
        }

        /// <summary>
        /// 取得forwardDir基於baseDir的角度
        /// </summary>
        /// <returns></returns>
        static float GetDegree2Vector(Vector2 baseDir, Vector2 forwardDir)
        {
            baseDir.Normalize();
            Vector3 DirRight3D = Vector3.Cross(new Vector3(baseDir, 0), Vector3.UnitZ);

            Vector2 baseDirRight = new Vector2(DirRight3D.X, DirRight3D.Y);

            //這邊抄來的 http://www.wretch.cc/blog/fredxxx123/10695920
            // C# with XNA
            //Vector2 A = forwardDir;
            //A.Normalize();
            //Vector2 B = Vector2.UnitY;//計算與(0,1)的夾角，(0,1)代表緯度朝上的向量
            //float A_dot_B = Vector2.Dot(A, B);
            //float ALen_mul_BLen = A.Length() * B.Length();

            //// 基本上A & B皆不應該為零，就略過檢查分母為零的狀況
            //float theta = (float)Math.Acos(A_dot_B / ALen_mul_BLen);

            // C# with XNA，將向量化的動作提前，結果是一樣的…吧～          
            Vector2 A = forwardDir;
            A.Normalize();
            // Vector2 B = Vector2.UnitY;//計算與(0,1)的夾角，(0,1)代表緯度朝上的向量

            float dotY = Microsoft.Xna.Framework.Vector2.Dot(A, baseDir);//Microsoft.Xna.Framework.Vector2.UnitY);
            float dotX = Microsoft.Xna.Framework.Vector2.Dot(A, baseDirRight);//Microsoft.Xna.Framework.Vector2.UnitX);

            float theta = (float)Math.Acos(dotY);

            // 這裡計算出來的theta是弧度(單位為π)，可依照需求轉成度數
            // double degree = theta * 180 / 3.14;
            float degree = MathHelper.ToDegrees(theta);

            if (dotY > 0)
            {
                if (dotX > 0)
                {
                    degree = -1 * degree;
                }
                else
                {
                    //degree = -1 * degree;
                }
            }
            else
            {
                if (dotX > 0)
                {
                    degree = 360 - degree;
                }
                else
                {
                    // degree = 360 - degree;
                }
            }

            return degree;
        }


        /// <summary>
        /// 回傳moveDismeter
        /// </summary>
        private double _setGPSAcuracy(double acuracyMeter)
        {
            double moveDistanceMeter = mMovedDistanceCalculator.CalculateMoveDistanceMeter(
                mNowMyGeoCoordinate.Latitude, mNowMyGeoCoordinate.Longitude);
            mMovedDistanceMeterCount += moveDistanceMeter;

            Utility.GetCurrentDispatcher().BeginInvoke(() =>
            {
                //System.Windows.Controls.Panel root = mRouteInstructorView.Parent as System.Windows.Controls.Panel;

                float base2DViewWidth = 0;
                if (mRouteRendererControl != null)
                    base2DViewWidth = (float)mRouteRendererControl.Width;

                Utility.SetHUDGPSAcuracy(mLayoutRoot,
                    acuracyMeter, (float)mViewRangeInSpeedLonLat, base2DViewWidth,
                    mNowMyGeoCoordinate.Latitude, mNowMyGeoCoordinate.Longitude,
                    moveDistanceMeter);

                //更新GPS 精確度
                // mRouteInstructorView.Interface_SetGPSAcuracy(
                //     acuracy, (float)mViewRangeInSpeedLonLat, (float)mRouteRendererControl.Width,
                //     mNowMyGeoCoordinate.Latitude, mNowMyGeoCoordinate.Longitude);
            });

            return moveDistanceMeter;
        }


        private void _updateSpeedDistanceData(double speedKMHR, int nowMovedDistanceMeter, int totalDistanceMeter,
            double remainDistanceToNextTurnPoint)
        {
            mAverageSpeedCalculator.CalculateAVGSpeed_KMHR(speedKMHR);

            //更新速度與距離資訊給有註冊的UI       
            foreach (ISetSpeed setSpeed in mSetSpeedList)
            {
                setSpeed.Interface_SetSpeedKMHR(
                    speedKMHR,
                    nowMovedDistanceMeter,
                    totalDistanceMeter,
                    mAverageSpeedCalculator.GetSpeedAVG_KMHR(),
                    remainDistanceToNextTurnPoint);
            }
        }


        /// <summary>
        /// 得知外部設定speed limit的接口
        /// </summary>
        //public void SetSpeedLimit(int speedLimitKMH, int speedLimitMPH)
        //{
        //    mSpeedLimitKMH = speedLimitKMH;
        //    mSpeedLimitMPH = speedLimitMPH;
        //}

        /// <summary>
        /// 設定目前時速，處理與時速有關的事
        /// </summary>
        private void _setNowSpeedKMHR(double speedKMHR)
        {
            if (double.IsNaN(speedKMHR))
                speedKMHR = 0;

            mNowSpeedKMHR = speedKMHR;

#if DEBUG
            Utility.OutputDebugString("[RouteRendererComponent]---目前時速KM/HR = " + speedKMHR);
#endif

            //目前時速，時速 0 -> 可視範圍 200M，時速 120 -> 可視範圍 1000M
            //可視範圍要400或更大的原因是，route renderer control是以720,720寬高去畫超過，所以設定200其實只看得見差不多100的範圍
            //所以下面將TURN_NOTICE_DIS_METER x N，讓時速0時，可視範圍大一些(這邊可視範圍的計算會影響到計算轉彎圓的半徑大小)
            //最大到1000也因為control放大畫超過的原因值也改成2000(如果不是2000就是我在調縮放比率)


            double ViewRangeMeter = Utility.VIEW_RANGE_MIN_METER + (1000 - Utility.VIEW_RANGE_MIN_METER) * (speedKMHR / 120f);



            //設定路線指示的view 可視範圍
            ////0   是 0M
            ////120 是 800M

            ////再另外加200得到
            ////0   是 200M
            ////120 是 1000M

            //double ViewRange = 800f / 120f * speedKMHR;
            //ViewRange += 200;
            //ViewRange /= 1000f;
            mViewRangeInSpeedLonLat = Utility.Kilometer2LonLat(ViewRangeMeter / 1000f);
        }



        private List<ISetSpeed> mSetSpeedList = new List<ISetSpeed>(4);

        /// <summary>
        /// 加入設定時速表
        /// </summary>
        public void AddSetSpeedList(ISetSpeed setSpeed)
        {
            mSetSpeedList.Add(setSpeed);
        }


        public void SetSpeechVoice(bool? isMale)
        {
            if (mDriveNoticeControl == null)
                return;
            mDriveNoticeControl.ClearVocalReadText();
            mDriveNoticeControl.SetSpeechVoice(isMale);
        }
    }
}
