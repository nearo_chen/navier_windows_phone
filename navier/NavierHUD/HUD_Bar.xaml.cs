﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using ResourceApp.Resources;
using System.Windows.Controls.Primitives;
using MyUtility;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Threading.Tasks;
using System.IO.IsolatedStorage;
using System.Windows.Threading;

namespace NavierHUD
{



    /// <summary>
    /// HUD page是一個抬頭顯示儀的概念，USER編輯增加的各種component(如:時速表，路線導航，電池用量，等化器...各種元件)，
    /// 都可以放在此HUD中...
    /// </summary>
    public partial class HUD : PhoneApplicationPage
    {
        private SaveUserSetting.UserSetting mUserSetting;


        //紀錄目前的popup up window是哪一個，原則是，同一時間只能有一個popup window
        Popup mNowPopup = null;

        private DispatcherTimer _timerAppBar = null;
        private int _timeCountAppbar;

        void ShowAppBar()
        {
            /*
             * ApplicationBar - 应用程序栏
             *     Mode - 应用程序栏的样式。ApplicationBarMode.Default：默认（显示图标）；ApplicationBarMode.Minimized：最小化（右侧有3个圆点，用于提示单击可弹出 ApBar）
             *     IsVisible - 是否显示 ApBar
             *     IsMenuEnabled - 是否显示 ApBar 的 MenuItem
             *     BackgroundColor - AppBar 的背景色
             *     ForegroundColor - AppBar 的前景色
             *
             * ApplicationBarIconButton - ApBar 的 IconButon
             *     Buttons - IconButon 的集合
             *     IconUri - 按钮图标的地址
             *     Text - 用于描述按钮功能的文本
             *     Click - 单击按钮后所触发的事件
             *
             * ApplicationBarMenuItem - ApBar 的 MenuItem
             *     MenuItems - MenuItem 的集合
             *     Text - 菜单项的文本
             *     Click - 单击菜单项后所触发的事件
             */

            if (ApplicationBar == null)
            {
                ApplicationBar = new ApplicationBar();
                ApplicationBar.StateChanged += OnApplicationBarStateChanged;
                ApplicationBar.Mode = ApplicationBarMode.Default;
                ApplicationBar.Opacity = 0.5;
                ApplicationBar.IsVisible = true;
                ApplicationBar.IsMenuEnabled = true;

                ApplicationBarIconButton btn = new ApplicationBarIconButton();
                btn.IconUri = new Uri("/Assets/Images/appbar/menu_mode.png", UriKind.Relative);
                btn.Text = AppResources.HUDAP_SwitchMirror;
                btn.Click += SwitchHUDMode_EventHandler;
                ApplicationBar.Buttons.Add(btn);

                btn = new ApplicationBarIconButton();
                btn.IconUri = new Uri("/Assets/Images/appbar/menu_grid.png", UriKind.Relative);
                btn.Text = AppResources.HUDAP_ShowGrid;
                btn.Click += SwitchGrid_EventHandler;
                ApplicationBar.Buttons.Add(btn);

                btn = new ApplicationBarIconButton();
                btn.IconUri = new Uri("/Assets/Images/appbar/menu_color.png", UriKind.Relative);
                btn.Text = AppResources.HUDAP_Colors;
                btn.Click += SelectColors_EventHandler;
                ApplicationBar.Buttons.Add(btn);

                btn = new ApplicationBarIconButton();
                btn.IconUri = new Uri("/Assets/Images/appbar/menu_speed.png", UriKind.Relative);
                btn.Text = AppResources.HUDAP_SwitchUnit;
                btn.Click += SwitchUnit_EventHandler;
                ApplicationBar.Buttons.Add(btn);



                ApplicationBarMenuItem item1 = new ApplicationBarMenuItem();
                item1.Text = AppResources.Main_SETTINGS;
                item1.Click += SettingsControl_EventHandler;
                ApplicationBar.MenuItems.Add(item1);

                //ApplicationBarMenuItem item2 = new ApplicationBarMenuItem();
                //item2.Text = "menu item 2";
                ////item1.Click += delegate { MessageBox.Show("选择了 menu item 1 菜单"); };
                //ApplicationBar.MenuItems.Add(item2);
            }

            ApplicationBar.IsVisible = true;

            if (_timerAppBar == null)
            {
                _timerAppBar = new DispatcherTimer();
                _timerAppBar.Interval = TimeSpan.FromMilliseconds(100);
                _timerAppBar.Tick += Timer_Tick_Appbar;
            }

            if (_timerAppBar.IsEnabled == false)
                _timerAppBar.Start();
            _timeCountAppbar = 0;
        }

        private void OnApplicationBarStateChanged(object sender, ApplicationBarStateChangedEventArgs e)
        {
            _timeCountAppbar = 0;
        }

        private void Timer_Tick_Appbar(object sender, EventArgs e)
        {
            _timeCountAppbar += 100;

            if (_timeCountAppbar > 2000)
            {
                _hideAppBar();

            }
        }

        //http://social.msdn.microsoft.com/Forums/wpapps/en-us/2b866c2d-62bd-4a63-a5d8-a0d6c7505c11/applicationbarstatechanged-event-is-not-firing
        // If you don't have anything in the MenuItems collection, the event doesn't fire.
        //private void OnApplicationBarStateChanged(object sender, ApplicationBarStateChangedEventArgs e)
        //{
        //    var appBar = sender as ApplicationBar;
        //    if (appBar == null) return;

        //    appBar.Opacity = e.IsMenuVisible ? 1 : .65;
        //}

        private bool _hideAppBar()
        {
            if (_timerAppBar != null)
                _timerAppBar.Stop();

            if (ApplicationBar != null && ApplicationBar.IsVisible)
            {
                ApplicationBar.IsVisible = false;

                //關閉BAR就將設定存檔          
                mUserSetting.ShowGrid = false;
                if (mGridRoot != null && mGridRoot.Visibility == System.Windows.Visibility.Visible)
                    mUserSetting.ShowGrid = true;
                SaveUserSetting.SaveSettingData(mUserSetting);
                return true;
            }
            return false;
        }


        void SwitchAppBarShow()
        {
            if (ApplicationBar == null || ApplicationBar.IsVisible == false)
                ShowAppBar();
            else
                _hideAppBar();
        }

        Grid mGridRoot = null;
        private void SwitchGrid_EventHandler(object sender, EventArgs e)
        {
            _timeCountAppbar = 0;

            if (mGridRoot != null)
            {
                if (mGridRoot.Visibility == System.Windows.Visibility.Visible)
                    mGridRoot.Visibility = System.Windows.Visibility.Collapsed;
                else
                    mGridRoot.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                double screenHeight = Utility.GetDeviceHeight(true);
                double screenWidth = Utility.GetDeviceWidth(true);
                Color color = Utility.GetHUDColor(Utility.SelectColor.color_orangered);
                mGridRoot = Utility.DrawGridLine(screenWidth,screenHeight, Utility.GridThickness, Utility.GridOpacity, color);
                
                LayoutRoot.Children.Add(mGridRoot);
                mGridRoot.Visibility = System.Windows.Visibility.Visible;
            }

            SetGridColor(Utility.GetHUDColor(mUserSetting.SelectColor));

            mUserSetting.ShowGrid = false;
            if (mGridRoot.Visibility == System.Windows.Visibility.Visible)
                mUserSetting.ShowGrid = true;
        }

        void SetGridColor(Color color)
        {
            if (mGridRoot == null)
                return;

            SolidColorBrush brush = new SolidColorBrush(color);
            foreach (UIElement uie in mGridRoot.Children)
            {
                Line line = uie as Line;
                if (line != null)
                    line.Stroke = brush;
            }


        }



        private void SwitchUnit_EventHandler(object sender, EventArgs e)
        {
            _timeCountAppbar = 0;

            //直接拿設定檔來用
            mUserSetting.ShowUnit = !mUserSetting.ShowUnit;

            Utility.SetHUDUnit(LayoutRoot, mUserSetting.ShowUnit);
        }

        private void SwitchHUDMode_EventHandler(object sender, EventArgs e)
        {
            _timeCountAppbar = 0;

            HUDSwitch();
        }

        private void SelectColors_EventHandler(object sender, EventArgs e)
        {
            _timeCountAppbar = 0;

            SelectColors selectColorsControl = new SelectColors();

            selectColorsControl.Width = selectColorsControl.Height = Utility.GetDeviceHeight(true);

            // 因為landscape所以X跟Y反過來
            Popup popup = selectColorsControl.Create(0,
                Utility.GetDeviceWidth(true) * 0.5f - Utility.GetDeviceHeight(true) * 0.5f,
                SelectColors_SelectionChangedEventHandler);

            SetPopupWindow(popup);
        }

        private void SettingsControl_EventHandler(object sender, EventArgs e)
        {
            _timeCountAppbar = 0;

            SettingsControl settingsControl = new SettingsControl(mUserSetting);
            double hostWidth = Utility.GetDeviceWidth(true);// -ApplicationBar.DefaultSize;
            double hostHeight = Utility.GetDeviceHeight(true);
            settingsControl.Width = hostWidth;
            settingsControl.Height = hostHeight;

            // 因為landscape所以X跟Y反過來
            Popup popup = settingsControl.Create(
                (hostHeight - hostWidth) * 0.5f,
                (hostWidth - hostHeight) * 0.5f);

            SetPopupWindow(popup);

            _hideAppBar();
        }

        void SelectColors_SelectionChangedEventHandler(object sender, SelectionChangedEventArgs e)
        {
            //選到哪個顏色應該是看這邊抓

            ListBox box = sender as ListBox;
            NavierHUD.SelectColors.ColorData searchInfo = box.SelectedItem as NavierHUD.SelectColors.ColorData;

            Utility.SelectColor selectColor = Utility.GetColorSelect(searchInfo.fillcolor);

            //修改HUD顏色
            ChangeHUDColor(selectColor);

            //更新設定資料
            mUserSetting.SelectColor = selectColor;
        }

        public void ChangeHUDColor(Utility.SelectColor selectcolor)
        {
            Utility.SetHUDColor(LayoutRoot, selectcolor);
            SetGridColor(Utility.GetHUDColor(selectcolor));
        }

        public void ChangeHUDSpeedLimit(Utility.SelectColor selectcolor)
        {
            Utility.SetHUDColor(LayoutRoot, selectcolor);
            SetGridColor(Utility.GetHUDColor(selectcolor));
        }

        /// <summary>
        /// 設定目前popup control是哪個，原則上同一時間只能有一個popup，
        /// 控管將目前POPUP關掉，然後將新的做紀錄
        /// </summary>
        /// <param name="popup"></param>
        void SetPopupWindow(Popup popup)
        {
            ClosePopupWindow();
            mNowPopup = popup;
        }

        /// <summary>
        /// 檢查有沒有POPUP將他關掉
        /// </summary>
        /// <returns></returns>
        bool ClosePopupWindow()
        {
            if (mNowPopup != null && mNowPopup.IsOpen == true)
            {
                //關閉Settings control，要及時將修改的設定更新在畫面上
                if (mNowPopup.Child.GetType() == typeof(SettingsControl))
                    _updateSettingsDataToHUDControl();

                mNowPopup.IsOpen = false;
                mNowPopup = null;
                return true;
            }
            return false;
        }


        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ClosePopupWindow())
                e.Cancel = true;
            else if (_hideAppBar())
                e.Cancel = true;
            else
            {
                //要是還在最一開始定位中的loading畫面，就直接讓他back鍵可以離開
                if (Utility.ShowProgressIndicator_IsShowing)
                {
                    return;
                }

                // Cancel the back button press
                e.Cancel = true;

                Image _pinIMG = new Image();
                _pinIMG.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri("Assets/Leave.png", UriKind.Relative));
                _pinIMG.Width = _pinIMG.Height = 100;
                MyUtility.Utility.ShowIfOOXX(true, "Leave ???", _pinIMG, _IfLeaveOK, new SolidColorBrush(Color.FromArgb(180, 255, 0, 0)));
            }
        }

        /// <summary>
        /// 根據settings control修改的內容，設定給HUD
        /// </summary>
        /// <param name="?"></param>
        void _updateSettingsDataToHUDControl()
        {
            Utility.SetHUDTimeFormat(LayoutRoot, mUserSetting.ShowTime24H);

            Utility.SetHUDSpeedLimit(LayoutRoot, mUserSetting.ShowUnit, mUserSetting.SpeedLimitKMH, mUserSetting.SpeedLimitMPH);
            Utility.SetHUDUnit(LayoutRoot, mUserSetting.ShowUnit);

            if (mRouteRendererComponent != null)
            {
                bool? isMale = null;
                //0 無聲，1男聲，2女聲
                if (mUserSetting.SpeachVoice == 1)
                    isMale = true;
                else if (mUserSetting.SpeachVoice == 2)
                    isMale = false;
                mRouteRendererComponent.SetSpeechVoice(isMale);
            }
        }
    }
}