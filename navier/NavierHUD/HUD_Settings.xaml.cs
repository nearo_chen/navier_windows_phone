﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using ResourceApp.Resources;
using System.Windows.Controls.Primitives;
using MyUtility;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Threading.Tasks;
using System.IO.IsolatedStorage;

namespace NavierHUD
{ 
    public partial class HUD : PhoneApplicationPage
    {
        /// <summary>
        /// 用來儲存User在HUD變更的設定值
        /// </summary>
        public static class SaveUserSetting
        {
            private const string UserSettingFileName = "DefaultUserSetting.txt";

            static public void SaveSettingData(SaveUserSetting.UserSetting settingData)
            {
                if (IsolatedStorageSettings.ApplicationSettings.Contains(UserSettingFileName) == false)
                    IsolatedStorageSettings.ApplicationSettings.Add(UserSettingFileName, settingData);
                else
                    IsolatedStorageSettings.ApplicationSettings[UserSettingFileName] = settingData;
                IsolatedStorageSettings.ApplicationSettings.Save();

                //byte[] bbb = IsolatedStorageHelper.Serialize_JSON(settingData);
                //await IsolatedStorageHelper.WriteToFile_Local(UserSettingFileName, bbb);
            }

            static public SaveUserSetting.UserSetting LoadSettingData()
            {
                if (IsolatedStorageSettings.ApplicationSettings.Contains(UserSettingFileName))
                {
                    return IsolatedStorageSettings.ApplicationSettings[UserSettingFileName] as UserSetting;
                }
                return null;
            }

            /// <summary>
            /// 讀取設定檔
            /// </summary>
            public static async Task<UserSetting> get()
            {
                byte[] bytearray = await IsolatedStorageHelper.ReadFile_Installation(UserSettingFileName);
                UserSetting data = (UserSetting)IsolatedStorageHelper.Deserialize_JSON(bytearray, typeof(UserSetting));
                return data;
            }

            /// <summary>
            /// 紀錄編輯好的各種元件資訊
            /// </summary>
            public class UserSetting
            {
                //一定要用get set，不然data 會bind不到
                public Utility.SelectColor SelectColor { get; set; }    //USER選擇的顏色
                public bool ShowGrid { get; set; }

                /// <summary>
                /// TRUE為公制
                /// </summary>
                public bool ShowUnit { get; set; }                      

                //setting control裡面的
                public bool ShowTime24H { get; set; }                   //使用２４小時時間格是
                public int SpeedLimitKMH { get; set; }                 //設定速限
                public int SpeedLimitMPH { get; set; }                 //設定速限

                /// <summary>
                /// 導航語音，0 無聲，1男聲，2女聲
                /// </summary>
                public int SpeachVoice { get; set; }                  
            }

            //#if DEBUG
            //        /// <summary>
            //        /// 用來建立一開始的設定檔用
            //        /// </summary>
            //        static public async void SaveDefault()
            //        {
            //            UserSetting data = new UserSetting();
            //            data.SelectColor = Utility.SelectColor.color_cyan;
            //            data.ShowGrid = false;
            //            data.ShowUnit = true;

            //            byte[] bbb = IsolatedStorageHelper.Serialize_JSON(data);
            //            await IsolatedStorageHelper.WriteToFile_Local("test.txt", bbb);
            //        }

            //        /// <summary>
            //        /// 用來讀取一開始的設定檔用
            //        /// </summary>
            //        static public async void LoadDefault()
            //        {
            //            byte[] bbb = await IsolatedStorageHelper.ReadFile_Local("test.txt");
            //            if (bbb == null)
            //                return;

            //            UserSetting data = (UserSetting)IsolatedStorageHelper.Deserialize_JSON(bbb, typeof(UserSetting));
            //        }
            //#endif
        }
    }
}