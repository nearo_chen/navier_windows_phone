﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using MyUtility;
using System.Windows.Controls.Primitives;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.ComponentModel;

namespace NavierHUD
{
    public partial class SelectColors : UserControl
    {
        //Popup control類別
        private Popup gPopupControl;
        //獨立宣告一個SelectionChangedEventHandler事件
        public event SelectionChangedEventHandler mPageListSelectHandler;

        public SelectColors()
        {
            InitializeComponent();
        }

        //該方法用於告知Popup Control要產生的Position
        public Popup Create(double x, double y, SelectionChangedEventHandler pageListSelectHandler)
        {
            gPopupControl = new Popup();
            gPopupControl.Child = this;
           
            gPopupControl.IsOpen = true;
            
            //設定X與Y軸
            gPopupControl.HorizontalOffset = x;
            gPopupControl.VerticalOffset = y;

         
            mPageListSelectHandler = pageListSelectHandler;

            //這邊設定FOCUS等下LOST FOCUS才會有作用
            this.Focus();
           
            return gPopupControl;
        }

        public void LostFocus_RoutedEventHandler(object sender, RoutedEventArgs e)
        {
            gPopupControl.IsOpen = false;
        }

        private void ListBox_Colors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            mPageListSelectHandler(sender, e);
            gPopupControl.IsOpen = false;            
        }

        private async void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            //建立顏色的資訊
            ColorsDataList colorsDataList = await ColorsSetting.get("Colors.txt");

            ColorData_ListBox colorData_ListBox = new ColorData_ListBox();
            foreach (ColorData data in colorsDataList.mColorsList)
            {
                colorData_ListBox.Add(data);
            }
            ListBox_Colors.ItemsSource = colorData_ListBox;
        }

        /// <summary>
        /// 專門處理由Default設定檔中取得資訊
        /// </summary>
        public static class ColorsSetting
        {
            public async static Task<ColorsDataList> get(string filename)
            {
                byte[] bytearray = await IsolatedStorageHelper.ReadFile_Installation(filename);
                ColorsDataList data = (ColorsDataList)IsolatedStorageHelper.Deserialize_JSON(bytearray, typeof(ColorsDataList));
                return data;
            }

            public static void AddComponent(ColorsDataList colorsDataList,
                string name, string fillcolor)
            {
                ColorData data = new ColorData();
                data.name = name;
                data.fillcolor = fillcolor;

                colorsDataList.mColorsList.Add(data);
            }
        }

        //給list box用
        public class ColorData_ListBox : ObservableCollection<ColorData>
        {
        }

        /// <summary>
        /// 紀錄編輯好的各種元件資訊
        /// </summary>
        public class ColorsDataList
        {
            public List<ColorData> mColorsList = new List<ColorData>();
        }

        /// <summary>
        /// 記錄一個元件的資訊
        /// </summary>
        public class ColorData
        {
            //不能有建構子，好像會CRASH
            //public ComponentData(int posX, int posY, int w, int h)
            //{
            //    mPosition.posX = posX;
            //    mPosition.posY = posY;
            //    mRange.width = w;
            //    mRange.height = h;
            //}

            //一定要用get set，不然data 會bind不到
            public string name { get; set; }                         //顏色的名稱
            public string fillcolor { get; set; }                    //顏色的ARGB "#ffF40000"
        }

#if DEBUG
        private async void SaveDefault()
        {
            ColorsDataList data = new ColorsDataList();
            //data.mRouteInstructor = new ComponentData(0, 0, 100 ,100);
            //data.mSpeedShow = new ComponentData(0, 0, 100, 100);
            //data.mSpeedNumberSmall = new ComponentData(0, 0, 100, 100);
            //data.mCompassA = new ComponentData(0, 0, 100, 100);

            ColorsSetting.AddComponent(data, "red", "#ffF40000");
            ColorsSetting.AddComponent(data, "aaa", "#ffF40330");
            ColorsSetting.AddComponent(data, "sss", "#ffF40ff0");
            ColorsSetting.AddComponent(data, "ffg", "#ffF400ff");
            ColorsSetting.AddComponent(data, "jjj", "#ffF4ff0f");
            ColorsSetting.AddComponent(data, "red", "#ffF40ff0");


            byte[] bbb = IsolatedStorageHelper.Serialize_JSON(data);

            await IsolatedStorageHelper.WriteToFile_Local("test.txt", bbb);
        }

        private async void LoadDefault()
        {
            byte[] bbb = await IsolatedStorageHelper.ReadFile_Local("test.txt", null);
            if (bbb == null)
                return;

            ColorsDataList data = (ColorsDataList)IsolatedStorageHelper.Deserialize_JSON(bbb, typeof(ColorsDataList));
        }
#endif

    }

}
