﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using SpeedShowControlApp;
using RouteInstructorApp;
using System.Collections.ObjectModel;
using System.Device.Location;
using MyUtility;
using CompassApp;
using ComponentEditorApp;
using System.Windows.Media;
using System.IO.IsolatedStorage;

namespace NavierHUD
{

    /// <summary>
    /// HUD page是一個抬頭顯示儀的概念，USER編輯增加的各種component(如:時速表，路線導航，電池用量，等化器...各種元件)，
    /// 都可以放在此HUD中...
    /// </summary>
    public partial class HUD : PhoneApplicationPage
    {
        // private List<RouteRendererComponent> mRouteRendererList = new List<RouteRendererComponent>();
        RouteRendererComponent mRouteRendererComponent = null;//只給加一個路線指示

        SpeedNumberSmallControl mSpeedNumberSmallControl = null;//數字小時速表
        SpeedNumberBigControl mSpeedNumberBigControl = null;//數字小時速表
        SpeedRendererControl mSpeedRendererControl = null;      //刻度時速表
        CampassAControl mCampassBigControl = null;
        CampassAControl mCampassSmallControl = null;
        NavigationProgressControl mNavigationProgressControl = null;
        MovedDistanceControl mMovedDistanceControl = null;
        AverageSpeedControl mAverageSpeedControl = null;
        CurrentTimeControl mCurrentTimeControl = null;
        GPSAccuracyControl mGPSAcuracy = null;
        BatteryControl mBatteryControl = null;
        TotalTimeControl mTotalTimeControl = null;


        ////請找個PAGE釋放的地方把這LIST也DISPOSE
        // if (mRouteRendererList != null)
        //        {
        //            foreach (RouteRendererComponent component in mRouteRendererList)
        //                component.Dispose();
        //            mRouteRendererList.Clear();
        //        }
        //        mRouteRendererList = null;





        /// <summary>
        /// 建立mRouteRendererComponent，路線指示VIEW
        /// </summary>
        private void AddRouteInstructor(int viewPosX, int viewPosY, int w, int h)
        {
            if (mRouteRendererComponent.GetRouteInstructorView() != null)
            {
                mRouteRendererComponent.AddSetSpeedList(mRouteRendererComponent.GetRouteInstructorView());
                return;
            }

            RouteRendererComponent.AddToRoot(mRouteRendererComponent, viewPosX, viewPosY, w, h);

            mRouteRendererComponent.AddSetSpeedList(mRouteRendererComponent.GetRouteInstructorView());

#if DEBUG
            mRouteRendererComponent.AddSetSpeedList(this);
#endif
        }

        /// <summary>
        /// 新增時速表圓圈
        /// </summary>
        private void AddSpeedShow(int viewPosX, int viewPosY, int w, int h)
        {
            if (mSpeedRendererControl != null)
            {
                mRouteRendererComponent.AddSetSpeedList(mSpeedRendererControl);
                return;
            }

            mSpeedRendererControl = SpeedRendererControl.AddToRoot(LayoutRoot, viewPosX, viewPosY, w, h);

            //Set給RouteRendererComponent讓他去幫忙跟新時速
            mRouteRendererComponent.AddSetSpeedList(mSpeedRendererControl);
        }

        /// <summary>
        /// 新增時速表電子數字，有分大跟小時速表
        /// </summary>
        private void AddSpeedNumber(bool isBigSpeedNumber, int viewPosX, int viewPosY, int w, int h)
        {
            if (isBigSpeedNumber == false)
            {
                if (mSpeedNumberSmallControl != null)
                {
                    mRouteRendererComponent.AddSetSpeedList(mSpeedNumberSmallControl);
                    return;
                }

                mSpeedNumberSmallControl = SpeedNumberSmallControl.AddToRoot(LayoutRoot, viewPosX, viewPosY, w, h);

                //Set給RouteRendererComponent讓他去幫忙跟新時速
                mRouteRendererComponent.AddSetSpeedList(mSpeedNumberSmallControl);
            }
            else
            {
                if (mSpeedNumberBigControl != null)
                {
                    mRouteRendererComponent.AddSetSpeedList(mSpeedNumberBigControl);
                    return;
                }

                mSpeedNumberBigControl = SpeedNumberBigControl.AddToRoot(LayoutRoot, viewPosX, viewPosY, w, h);

                //Set給RouteRendererComponent讓他去幫忙跟新時速
                mRouteRendererComponent.AddSetSpeedList(mSpeedNumberBigControl);
            }
        }

        private void AddCompass(CompassType.COMPASS_TYPE type, int viewPosX, int viewPosY, int w, int h)
        {
            //Compass Big 或是 Small 只有WH的差別
            if (type == CompassType.COMPASS_TYPE.Big)
            {
                if (mCampassBigControl != null)
                    return;

                mCampassBigControl = CampassAControl.AddToRoot(LayoutRoot, viewPosX, viewPosY, w, h);
                mCampassBigControl.initCompass();
            }
            else if (type == CompassType.COMPASS_TYPE.Small)
            {
                if (mCampassSmallControl != null)
                    return;

                mCampassSmallControl = CampassAControl.AddToRoot(LayoutRoot, viewPosX, viewPosY, w, h);
                mCampassSmallControl.initCompass();
            }
        }

        private void AddMovedDistance(int viewPosX, int viewPosY, int w, int h)
        {
            if (mMovedDistanceControl != null)
                return;

            mMovedDistanceControl = MovedDistanceControl.AddToRoot(LayoutRoot, viewPosX, viewPosY, w, h);
            mMovedDistanceControl.init();
        }

        private void AddAverageSpeed(int viewPosX, int viewPosY, int w, int h)
        {
            if (mAverageSpeedControl != null)
            {
                mRouteRendererComponent.AddSetSpeedList(mAverageSpeedControl);
                return;
            }

            mAverageSpeedControl = AverageSpeedControl.AddToRoot(LayoutRoot, viewPosX, viewPosY, w, h);
            mRouteRendererComponent.AddSetSpeedList(mAverageSpeedControl);
            mAverageSpeedControl.init();
        }

        private void AddCurrentTime(int viewPosX, int viewPosY, int w, int h)
        {
            if (mCurrentTimeControl != null)
                return;

            mCurrentTimeControl = CurrentTimeControl.AddToRoot(LayoutRoot, viewPosX, viewPosY, w, h);
        }

        private void AddGPSAcuracy(int viewPosX, int viewPosY, int w, int h)
        {
            if (mGPSAcuracy != null)
                return;
            mGPSAcuracy = GPSAccuracyControl.AddToRoot(LayoutRoot, viewPosX, viewPosY, w, h);
        }

        private void AddBatteryUI(int viewPosX, int viewPosY, int w, int h)
        {
            if (mBatteryControl != null)
                return;
            mBatteryControl = BatteryControl.AddToRoot(LayoutRoot, viewPosX, viewPosY, w, h);
        }

        private void AddTotalTime(int viewPosX, int viewPosY, int w, int h)
        {
            if (mTotalTimeControl != null)
                return;
            mTotalTimeControl = TotalTimeControl.AddToRoot(LayoutRoot, viewPosX, viewPosY, w, h);
        }

        private void AddNavigationProgress(int viewPosX, int viewPosY, int w, int h)
        {
            if (mNavigationProgressControl != null)
            {
                mRouteRendererComponent.AddSetSpeedList(mNavigationProgressControl);
                return;
            }

            mNavigationProgressControl = NavigationProgressControl.AddToRoot(LayoutRoot, viewPosX, viewPosY, w, h);
            mRouteRendererComponent.AddSetSpeedList(mNavigationProgressControl);

            if (mTravelMode == Microsoft.Phone.Maps.Services.TravelMode.Driving)
                mNavigationProgressControl.SetMyVehicle(true);
            else
                mNavigationProgressControl.SetMyVehicle(false);
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
#if DEBUG
            Utility.OutputDebugString("[HUD]->OnNavigatedTo()");
#endif

            base.OnNavigatedTo(e);

            //在Uri上找出變數為Id取出等於後方值放到textBox1元件
            double longitude = 0;
            double latitude = 0;
            int travelMode = 0;
            mGoalName = null;

            if (this.NavigationContext.QueryString.ContainsKey("longitude"))
                double.TryParse(this.NavigationContext.QueryString["longitude"], out longitude);

            if (this.NavigationContext.QueryString.ContainsKey("latitude"))
                double.TryParse(this.NavigationContext.QueryString["latitude"], out latitude);

            if (this.NavigationContext.QueryString.ContainsKey("travelMode"))
                int.TryParse(this.NavigationContext.QueryString["travelMode"], out travelMode);

            if (this.NavigationContext.QueryString.ContainsKey("goalName"))
                mGoalName = this.NavigationContext.QueryString["goalName"];

            //string tmpHUDSettingFileName = null;
            //if (this.NavigationContext.QueryString.ContainsKey("HUDSettingFileName"))
            //    tmpHUDSettingFileName = this.NavigationContext.QueryString["HUDSettingFileName"];

            mGoalCoordinate.Longitude = longitude;
            mGoalCoordinate.Latitude = latitude;
            mTravelMode = (Microsoft.Phone.Maps.Services.TravelMode)travelMode;

            if (longitude == 0 && latitude == 0 && travelMode == 0)
                return;

            //   if (tmpHUDSettingFileName == null)
            //       tmpHUDSettingFileName = "Default.xml";

            //ComponentEditorData UIData = await ComponentEditorApp.EditorSetting.get(tmpHUDSettingFileName);
            ComponentEditorData UIData = IsolatedStorageSettings.ApplicationSettings["SelectedHUDFile"] as ComponentEditorData;

            //根據有要用到GPS或是路線導航，去NEW出RouteRendererComponent裡面該有的東西
            bool bGeolocator = false;
            bool bRouteQuery = false;
            bool bCreateNotic = false;
            if (EditorSetting.CheckHasNavigationComponent(UIData))
            {
                bRouteQuery = true;
                bGeolocator = true;
            }
            else if (EditorSetting.CheckHasGPSComponent(UIData))
            {
                bGeolocator = true;
            }

            bCreateNotic = EditorSetting.CheckIfNeedNavigationNotic(UIData);

            mRouteRendererComponent =
                RouteRendererComponent.CreateRouteRendererComponent(LayoutRoot, bGeolocator, bRouteQuery, bCreateNotic);//RouteRendererComponent new出該有的東西

            //這邊view的2D座標，於此時再去讀檔找出
            double screenWidth = Utility.GetDeviceWidth(true);
            foreach (ComponentData data in UIData.mComponents)
            {                
                int x = Utility.GetPixelX(screenWidth, data.mPosition.X);
                int y = Utility.GetPixelY(screenWidth, data.mPosition.Y,
                    //LayoutRoot.ActualHeight 這邊actual height還是0，我直接抓全螢幕的高來設定
                    Utility.GetDeviceHeight(true)
                    );
                int w = Utility.GetPixelLength(screenWidth, data.mRange.X);
                int h = Utility.GetPixelLength(screenWidth, data.mRange.Y);

                if (data.name == EditorSetting.ComponentNames.RouteInstructor.ToString("f"))
                {
                    AddRouteInstructor(x, y, w, h);
                }
                else if (data.name == EditorSetting.ComponentNames.SpeedShow.ToString("f"))
                {
                    AddSpeedShow(x, y, w, h);
                }
                else if (data.name == EditorSetting.ComponentNames.SpeedNumberSmall.ToString("f"))
                {
                    AddSpeedNumber(false, x, y, w, h);
                }
                else if (data.name == EditorSetting.ComponentNames.SpeedNumberBig.ToString("f"))
                {
                    AddSpeedNumber(true, x, y, w, h);
                }
                else if (data.name == EditorSetting.ComponentNames.CompassBig.ToString("f"))
                {
                    AddCompass(CompassType.COMPASS_TYPE.Big, x, y, w, h);
                }
                else if (data.name == EditorSetting.ComponentNames.CompassSmall.ToString("f"))
                {
                    AddCompass(CompassType.COMPASS_TYPE.Small, x, y, w, h);
                }
                else if (data.name == EditorSetting.ComponentNames.NavigationProgress.ToString("f"))
                {
                    AddNavigationProgress(x, y, w, h);
                }
                else if (data.name == EditorSetting.ComponentNames.MovedDistance.ToString("f"))
                {
                    AddMovedDistance(x, y, w, h);
                }
                else if (data.name == EditorSetting.ComponentNames.AverageSpeed.ToString("f"))
                {
                    AddAverageSpeed(x, y, w, h);
                }
                else if (data.name == EditorSetting.ComponentNames.CurrentTime.ToString("f"))
                {
                    AddCurrentTime(x, y, w, h);
                }
                else if (data.name == EditorSetting.ComponentNames.GPSAcuracy.ToString("f"))
                {
                    AddGPSAcuracy(x, y, w, h);
                }
                else if (data.name == EditorSetting.ComponentNames.Battery.ToString("f"))
                {
                    AddBatteryUI(x, y, w, h);
                }
                else if (data.name == EditorSetting.ComponentNames.TotalTime.ToString("f"))
                {
                    AddTotalTime(x, y, w, h);
                }
            }
            RouteRendererComponent.MoveDriveNoticeControlToLast(LayoutRoot);

            if (bRouteQuery)
            {
                mRouteRendererComponent.StartRouteQuery(mGoalCoordinate, mTravelMode);
                if (!string.IsNullOrEmpty(mGoalName))
                    Utility.ShowProgressIndicator_updateInfo(mGoalName);
            }
            else if (bGeolocator)
            {
                mRouteRendererComponent.StartPositionTracking();

            }

            Utility.SetScreenAutoSleep(false);

            //讀取User 設定檔
            mUserSetting = SaveUserSetting.LoadSettingData();
            if (mUserSetting == null)
                mUserSetting = await SaveUserSetting.get();

            //根據設定修改HUD顏色
            ChangeHUDColor(mUserSetting.SelectColor);

            //根據設定修改其他ㄉ東西
            _updateSettingsDataToHUDControl();

        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            RouteRendererComponent.RemoveFromRoot(LayoutRoot,
                mRouteRendererComponent.GetRouteInstructorView(),
                mRouteRendererComponent.GetDriveNoticeControl()
                );
            //  SpeedRendererControl.RemoveFromRoot(LayoutRoot, mSpeedRendererControl);
            // SpeedNumberSmallControl.RemoveFromRoot(mSpeedNumberSmallControl);
            // CampassAControl.RemoveFromRoot(mCampassBigControl);

            mRouteRendererComponent.Dispose();
            mRouteRendererComponent = null;
            //    mSpeedRendererControl = null;
            //     mSpeedNumberSmallControl = null;
            //    mCampassBigControl = null;

            Utility.SetScreenAutoSleep(true);

            Utility.HideProgressIndicator();


            //開著BAR案返回建就不會跑到關閉BAR要存檔，所以這邊也丟一個    
            if (mUserSetting != null)
            {
                mUserSetting.ShowGrid = false;
                if (mGridRoot != null && mGridRoot.Visibility == System.Windows.Visibility.Visible)
                    mUserSetting.ShowGrid = true;
                SaveUserSetting.SaveSettingData(mUserSetting);
            }
        }


        private void _IfLeaveOK(bool ifYes)
        {
            if (ifYes)
            {
                if (mRouteRendererComponent.IfHasGeolocator())
                    GoTo_ShareOnFBPage();
                else
                    GoTo_NavierMainPage();
                //NavigationService.GoBack();
            }
        }

        void GoTo_ShareOnFBPage()
        {
            string addr = string.Format("/ShareOnFBApp;component/ShareFBPage.xaml");
            NavigationService.Navigate(new Uri(addr, UriKind.Relative));
        }

        void GoTo_NavierMainPage()
        {
            RouteRendererComponent.ReCreatDistanceSpeedCalculator();

            if (MyUtility.Utility.mHideAdvertisement)
            {
                string addr = string.Format("/NavierMainApp;component/NavierMain.xaml");
                NavigationService.Navigate(new Uri(addr, UriKind.Relative));
            }
            else
            {
                string addr = string.Format("/NavierMainAppFree;component/NavierMain.xaml");
                NavigationService.Navigate(new Uri(addr, UriKind.Relative));
            }
        }
    }
}