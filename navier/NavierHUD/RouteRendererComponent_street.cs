﻿

using Microsoft.Phone.Maps.Services;
using System.Collections.Generic;
using System.Device.Location;
namespace NavierHUD
{
    /// <summary>
    /// 控制導航資訊，以及將導航資訊的路線繪出，提供GetRouteRendererControl()
    /// </summary>
     public partial class RouteRendererComponent
    {
        private ReverseGeocodeQuery mMyReverseGeocodeQuery_street = null;
        private object mMyReverseGeocodeQuery_street_lock = new object();
        private void _queryStreetName(GeoCoordinate coordinate)
        {
            lock (mMyReverseGeocodeQuery_street_lock)
            {
                if (mMyReverseGeocodeQuery_street == null || mMyReverseGeocodeQuery_street.IsBusy == false)
                {
                    if (mMyReverseGeocodeQuery_street == null)
                    {
                        mMyReverseGeocodeQuery_street = new ReverseGeocodeQuery();
                        mMyReverseGeocodeQuery_street.QueryCompleted += __reverseGeocodeQueryStreet_QueryCompleted;
                    }
                    mMyReverseGeocodeQuery_street.GeoCoordinate = coordinate;
                    mMyReverseGeocodeQuery_street.QueryAsync();
                }
            }
        }

        private void _releaseMyReverseGeocodeQuery_street()
        {
            lock (mMyReverseGeocodeQuery_street_lock)
            {
                if (mMyReverseGeocodeQuery_street != null)
                {
                    mMyReverseGeocodeQuery_street.QueryCompleted -= __reverseGeocodeQueryStreet_QueryCompleted;
                    mMyReverseGeocodeQuery_street.Dispose();
                    mMyReverseGeocodeQuery_street = null;
                }
            }
        }

        private void __reverseGeocodeQueryStreet_QueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
            
            lock (mMyReverseGeocodeQuery_street_lock)
            {
                if (e.Error == null)
                {
                    if (e.Result.Count > 0)
                    {
                        MapAddress address = e.Result[0].Information.Address;
                        if(string.IsNullOrEmpty(address.Street) == false)
                        {
                            MyUtility.Utility.GetCurrentDispatcher().BeginInvoke(() =>
                            mRouteInstructorView.SetCurrentRouteInstruction(address.Street)
                            );
                        }
                    }
                }
            }
            
        }
    }
}
