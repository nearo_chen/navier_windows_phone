﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using RouteInstructorApp;
using System.Collections.ObjectModel;
using System.Device.Location;
using MyUtility;
using System.Windows.Media;


namespace NavierHUD
{
    /// <summary>
    /// HUD page是一個抬頭顯示儀的概念，USER編輯增加的各種component(如:時速表，路線導航，電池用量，等化器...各種元件)，
    /// 都可以放在此HUD中...
    /// </summary>
    public partial class HUD : PhoneApplicationPage

#if DEBUG
, ISetSpeed
#endif

    {
        private string mGoalName;

        //前一個page的設定目第地定位，為null表示前一個PAGE沒設定目標(像是只有時速表的HUD就不會有設定目第地)
        private GeoCoordinate mGoalCoordinate = new GeoCoordinate();
        private Microsoft.Phone.Maps.Services.TravelMode mTravelMode;


        public HUD()
        {
            InitializeComponent();
#if DEBUG
            Utility.OutputDebugString("[HUD建構子]王建民今天再度投出無責失分先發助藍鳥奪勝，", null);
#endif
            Utility.SetCurrentDispatcher(this.Dispatcher);
        }

#if DEBUG
        int nowOnRoute = 0;
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //            HUDSwitch();

            Point globalCentor = mRouteRendererComponent.GetRouteQueryPointArray()[nowOnRoute];
            Point scaleRange = new Point(mRouteRendererComponent.mViewRangeInSpeedLonLat, mRouteRendererComponent.mViewRangeInSpeedLonLat);

            float turnCircleRadius = (float)Utility.Kilometer2LonLat(Utility.TURN_NOTICE_DIS_METER / 1000f);

            mRouteRendererComponent.GetRouteRendererControl().SetGlobalCentor_WithOffset(globalCentor, scaleRange, turnCircleRadius);

            //單存DEBUG用轉角度
            float degree = mRouteRendererComponent.GetRouteRendererControl().GetNowRouteUpDegree(nowOnRoute);
            mRouteRendererComponent.GetRouteRendererControl().SetRotationTransform(degree);

            mRouteRendererComponent.GetRouteRendererControl().RenderDebug_MyWay();

            nowOnRoute++;
        }
#endif

        /// <summary>
        /// 切換是否為投射到玻璃上的HUD模式
        /// </summary>
        public void HUDSwitch()
        {
            mirrorY.ScaleY *= -1;
        }

#if DEBUG
        int POSCHANGECOUNT = 0;
        public void Interface_SetSpeedKMHR(double speedKMHR, int nowMovedDistanceMeter, int totalDistanceMeter,
            double speedAVG_KMHR,
            double remainDistanceToNextTurnPoint)
        {
            Dispatcher.BeginInvoke(() =>
                {
                    //RouteRendererComponent._setNowSpeedKMHR() 摳過來的
                    double ViewRange = 800f / 120f * speedKMHR;
                    ViewRange += 200;
                    ViewRange /= 1000f;
                    double viewRangeInSpeed = Utility.Kilometer2LonLat(ViewRange);
                    double viewRangeInSpeedLL = ViewRange;// Utility.LonLat2Kilometer(viewRangeInSpeed);

                    string mmm = "[HUD]---pos changed: " + POSCHANGECOUNT + " , viewRange = " + viewRangeInSpeed + " ; " + viewRangeInSpeedLL;
                    Utility.OutputDebugString(mmm);

                    POSCHANGECOUNT++;
                });
        }


        private void Button_Click_RangePlus(object sender, RoutedEventArgs e)
        {
            int culling = mRouteRendererComponent.Set_Longitude_Latitude_Range(0.001f);
            System.Diagnostics.Debug.WriteLine("Set_Longitude_Latitude_Range after culling points => " + culling);

            mRouteRendererComponent.GetRouteRendererControl().RenderDebug_MyWay();
        }

        private void Button_Click_fix_dir(object sender, RoutedEventArgs e)
        {
            //根據我的路線位置轉角度
            double remainDIS;
            Point pos = mRouteRendererComponent.GetRouteRendererControl().GetGlobalCentor_WithoutOffset();
            RouteRendererControl._OnRouteData data =
                mRouteRendererComponent.CorrectRouteDirection_RotateView(pos.X, pos.Y, out remainDIS);

            //if (data.minDis > MyUtility.Utility.ROAD_WIDTH)
            //{
            //    MyUtility.Utility.ShowMessageBox("你脫離路線了");
            //}

            //Point newPoint = new Point(longitude, latitude);
            //mRouteRendererComponent.Callback_PositionChanged(ref newPoint, null);
        }

        private void Button_Click_Forward(object sender, RoutedEventArgs e)
        {
            // 沒法Callback_PositionChanged 裡面new Geocordinate
            //Point pos = mRouteRendererComponent.GetRouteRendererControl().GetGlobalCentor_WithoutOffset();
            //pos.Y += 0.0001f;
            //GeoCoordinate ccc = new GeoCoordinate(pos.X, pos.Y);
            //mRouteRendererComponent.mTEST_POSITION_COORD = ccc;
            //mRouteRendererComponent.Callback_PositionChanged(ref pos, null);


            Point pos = mRouteRendererComponent.GetRouteRendererControl().GetGlobalCentor_WithoutOffset();
            Point scaleRange = new Point(mRouteRendererComponent.mViewRangeInSpeedLonLat, mRouteRendererComponent.mViewRangeInSpeedLonLat);
            pos.Y += 0.0001f;

            float turnCircleRadius = (float)Utility.Kilometer2LonLat(Utility.TURN_NOTICE_DIS_METER / 1000f);
            mRouteRendererComponent.GetRouteRendererControl().SetGlobalCentor_WithOffset(pos, scaleRange, turnCircleRadius);
            mRouteRendererComponent.GetRouteRendererControl().RenderDebug_MyWay();

            //Button_Click_fix_dir(null, null);
        }

        private void Button_Click_Backward(object sender, RoutedEventArgs e)
        {
            Point pos = mRouteRendererComponent.GetRouteRendererControl().GetGlobalCentor_WithoutOffset();
            Point scaleRange = new Point(mRouteRendererComponent.mViewRangeInSpeedLonLat, mRouteRendererComponent.mViewRangeInSpeedLonLat);
            pos.Y -= 0.0001f;

            float turnCircleRadius = (float)Utility.Kilometer2LonLat(Utility.TURN_NOTICE_DIS_METER / 1000f);
            mRouteRendererComponent.GetRouteRendererControl().SetGlobalCentor_WithOffset(pos, scaleRange, turnCircleRadius);
            mRouteRendererComponent.GetRouteRendererControl().RenderDebug_MyWay();
            Button_Click_fix_dir(null, null);
        }

        private void Button_Click_ShiftLEFT(object sender, RoutedEventArgs e)
        {
            Point pos = mRouteRendererComponent.GetRouteRendererControl().GetGlobalCentor_WithoutOffset();
            Point scaleRange = new Point(mRouteRendererComponent.mViewRangeInSpeedLonLat, mRouteRendererComponent.mViewRangeInSpeedLonLat);
            pos.X -= 0.0001f;

            float turnCircleRadius = (float)Utility.Kilometer2LonLat(Utility.TURN_NOTICE_DIS_METER / 1000f);
            mRouteRendererComponent.GetRouteRendererControl().SetGlobalCentor_WithOffset(pos, scaleRange, turnCircleRadius);
            mRouteRendererComponent.GetRouteRendererControl().RenderDebug_MyWay();
            Button_Click_fix_dir(null, null);
        }

        private void Button_Click_ShiftRIGHT(object sender, RoutedEventArgs e)
        {
            Point pos = mRouteRendererComponent.GetRouteRendererControl().GetGlobalCentor_WithoutOffset();
            Point scaleRange = new Point(mRouteRendererComponent.mViewRangeInSpeedLonLat, mRouteRendererComponent.mViewRangeInSpeedLonLat);
            pos.X += 0.0001f;

            float turnCircleRadius = (float)Utility.Kilometer2LonLat(Utility.TURN_NOTICE_DIS_METER / 1000f);
            mRouteRendererComponent.GetRouteRendererControl().SetGlobalCentor_WithOffset(pos, scaleRange, turnCircleRadius);
            mRouteRendererComponent.GetRouteRendererControl().RenderDebug_MyWay();
            Button_Click_fix_dir(null, null);
        }
#endif


        private void LayoutRoot_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SwitchAppBarShow();
        }

        private void PhoneApplicationPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            Utility.ShowProgressIndicator_OrientationChange(e.Orientation);
        }

        private void LayoutRoot_DoubleTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            HUDSwitch();
        }

     
       

        //private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        //{
        //     RemoveAllDebugButton();
        //}


        ///// <summary>
        ///// 把除錯用的按鈕拿掉(移掉所有的BUTTON)
        ///// </summary>
        //void RemoveAllDebugButton()
        //{
        //    bool bRemoveAll = false;
        //    while (bRemoveAll == false)
        //    {
        //        bRemoveAll = true;
        //        foreach (UIElement uie in LayoutRoot.Children)
        //        {
        //            Button button = uie as Button;
        //            if (button != null)
        //            {
        //                LayoutRoot.Children.Remove(uie);
        //                bRemoveAll = false;
        //                break;
        //            }
        //        }
        //    }
        //}
    }
}