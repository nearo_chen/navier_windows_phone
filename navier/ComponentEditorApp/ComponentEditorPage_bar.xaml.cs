﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyUtility;
using System.Windows.Media;
using System.Windows.Controls.Primitives;
using System.IO;

namespace ComponentEditorApp
{
    public partial class ComponentEditorPage : PhoneApplicationPage
    {
        public const string FOLDER = "ComponentPage";

        //紀錄目前的popup up window是哪一個，原則是，同一時間只能有一個popup window
        Popup mNowPopup = null;

        void ShowAppBar()
        {
            /*
             * ApplicationBar - 应用程序栏
             *     Mode - 应用程序栏的样式。ApplicationBarMode.Default：默认（显示图标）；ApplicationBarMode.Minimized：最小化（右侧有3个圆点，用于提示单击可弹出 ApBar）
             *     IsVisible - 是否显示 ApBar
             *     IsMenuEnabled - 是否显示 ApBar 的 MenuItem
             *     BackgroundColor - AppBar 的背景色
             *     ForegroundColor - AppBar 的前景色
             *
             * ApplicationBarIconButton - ApBar 的 IconButon
             *     Buttons - IconButon 的集合
             *     IconUri - 按钮图标的地址
             *     Text - 用于描述按钮功能的文本
             *     Click - 单击按钮后所触发的事件
             *
             * ApplicationBarMenuItem - ApBar 的 MenuItem
             *     MenuItems - MenuItem 的集合
             *     Text - 菜单项的文本
             *     Click - 单击菜单项后所触发的事件
             */

            if (ApplicationBar == null)
            {
                ApplicationBar = new ApplicationBar();
                //ApplicationBar.StateChanged += OnApplicationBarStateChanged;
                ApplicationBar.Mode = ApplicationBarMode.Default;
                ApplicationBar.Opacity = 0.5;
                ApplicationBar.IsVisible = true;
                ApplicationBar.IsMenuEnabled = false;

                ApplicationBarIconButton btn;

                btn = new ApplicationBarIconButton(new Uri("/Assets/Icon/btn_new.png", UriKind.Relative));
                btn.Text = ResourceApp.Resources.AppResources.Editor_ADD;
                btn.Click += AddNew_EventHandler;
                ApplicationBar.Buttons.Add(btn);

                btn = new ApplicationBarIconButton(new Uri("/Assets/Icon/btn_save.png", UriKind.Relative));
                btn.Text = ResourceApp.Resources.AppResources.Editor_Save;
                btn.Click += Save_EventHandler;
                ApplicationBar.Buttons.Add(btn);

                //btn = new ApplicationBarIconButton(new Uri("/Assets/appbarIcon/DownloadMap.png", UriKind.Relative));
                //btn.Text = AppResources.GOALBAR_MapDownloader;
                //btn.Click += MapDownload_EventHandler;
                //ApplicationBar.Buttons.Add(btn);

                //ApplicationBarMenuItem item1 = new ApplicationBarMenuItem();
                //item1.Text = ResourceApp.Resources.AppResources.LocationPrivacyStatement;
                //item1.Click += Privacy_EventHandler;
                //ApplicationBar.MenuItems.Add(item1);

                //ApplicationBarMenuItem item2 = new ApplicationBarMenuItem();
                //item2.Text = AppResources.Main_ABOUT;
                //item1.Click += About_EventHandler;
                //ApplicationBar.MenuItems.Add(item2);
            }
            ApplicationBar.IsVisible = true;
        }

        void AddNew_EventHandler(object sender, EventArgs e)
        {
            ComponentsListControl componentsListControl = new ComponentsListControl();

            double hostWidth = Utility.GetDeviceWidth(true);// -ApplicationBar.DefaultSize;
            double hostHeight = Utility.GetDeviceHeight(true);
            componentsListControl.Width = hostWidth;
            componentsListControl.Height = hostHeight;

            // 因為landscape所以X跟Y反過來
            Popup popup = componentsListControl.Create(
                (hostHeight - hostWidth) * 0.5f,
                (hostWidth - hostHeight) * 0.5f,
                EditRegion, (int)EditRegion.ActualWidth);

            SetPopupWindow(popup);

            ApplicationBar.IsVisible = false;
            mDeleteButton.Visibility = mYesButton.Visibility = System.Windows.Visibility.Collapsed;
           
        }

        //拖拉CONTROL時，右邊秀出確認是否固定元件
        void ShowConfirmDragging()
        {
            ApplicationBar.IsVisible = false;
            mDeleteButton.Visibility = mYesButton.Visibility = System.Windows.Visibility.Visible;
           
        }

        void HideConfirmDragging()
        {
            ApplicationBar.IsVisible = true;
            mDeleteButton.Visibility = mYesButton.Visibility = System.Windows.Visibility.Collapsed;
           
        }

        async void Save_EventHandler(object sender, EventArgs e)
        {
            mComponentEditorData.ShowName = mComponentPageName.Text;
            byte[] bbb = IsolatedStorageHelper.Serialize_JSON(mComponentEditorData);

           // await IsolatedStorageHelper.WriteToFile_Local(FOLDER + "\\" + mComponentPageName.Text + ".txt", bbb);
            await IsolatedStorageHelper.WriteToFile_Local(mComponentEditorData.FileName,
                 new MemoryStream(bbb),
                 _writeToFile_Local_Success,
                 FOLDER);
        }

        void _writeToFile_Local_Success(string fileName)
        {
            Utility.ShowToast("FILE SAVED", 1000, true);
        }

        /// <summary>
        /// 設定目前popup control是哪個，原則上同一時間只能有一個popup，
        /// 控管將目前POPUP關掉，然後將新的做紀錄
        /// </summary>
        /// <param name="popup"></param>
        void SetPopupWindow(Popup popup)
        {
            ClosePopupWindow();
            mNowPopup = popup;
        }

        /// <summary>
        /// 檢查有沒有POPUP將他關掉
        /// </summary>
        /// <returns></returns>
        bool ClosePopupWindow()
        {
            if (mNowPopup != null && mNowPopup.IsOpen == true)
            {
                mNowPopup.IsOpen = false;
                mNowPopup = null;
                return true;
            }
            return false;
        }
    }
}