﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using RouteInstructorApp;
using MyUtility;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using Windows.Devices.Sensors;

namespace ComponentEditorApp
{


    public partial class ComponentsListControl : UserControl
    {
        private Inclinometer _inclinometer;

        Panel mEditorPageLayoutRoot;
        int mEditorPageScreenWidth;

        //Popup control類別
        private Popup gPopupControl;

        public ComponentsListControl()
        {
            InitializeComponent();

            //水平感測
            _inclinometer = Inclinometer.GetDefault();
            if (_inclinometer != null)
            {
                // Establish the report interval for all scenarios
                uint minReportInterval = _inclinometer.MinimumReportInterval;
                uint reportInterval = minReportInterval > 100 ? minReportInterval : 100;
                _inclinometer.ReportInterval = reportInterval;

                // Establish the event handler
                _inclinometer.ReadingChanged += new Windows.Foundation.TypedEventHandler<Inclinometer, InclinometerReadingChangedEventArgs>(ReadingChanged);
            }
        }

        private void ReadingChanged(object sender, InclinometerReadingChangedEventArgs e)
        {
            InclinometerReading reading = e.Reading;
            Dispatcher.BeginInvoke(() =>
            {
                if (reading.RollDegrees < 0 && mRotate.Rotation < 0)
                    mRotate.Rotation = 90;
                else if (reading.RollDegrees > 0 && mRotate.Rotation > 0)
                    mRotate.Rotation = -90;
            });
        }

        //該方法用於告知Popup Control要產生的Position
        public Popup Create(double x, double y,
            Panel editorPageLayoutRoot, int editorPageScreenWidth)
        {
            mEditorPageLayoutRoot = editorPageLayoutRoot;
            mEditorPageScreenWidth = editorPageScreenWidth;

            gPopupControl = new Popup();
            gPopupControl.Child = this;

            gPopupControl.IsOpen = true;


            //設定X與Y軸
            gPopupControl.HorizontalOffset = x;
            gPopupControl.VerticalOffset = y;

            //這邊設定FOCUS等下LOST FOCUS才會有作用
            this.Focus();

            return gPopupControl;
        }

        private void LayoutRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //ScrollViewer_Left.Width = 400;
            //ScrollViewer_Left.Height = 400;

            ScrollViewer_Left.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
            ScrollViewer_Left.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;

            ScrollViewer_Left.Content = StackPanel_Left;
        }

        private void _addDragComponent(EditorSetting.ComponentNames componentNames, int pixelW, int pixelH)
        {
            Control control = null;
            Rect r;

            //如果想要新增的該元件已經在編輯室窗內，就直接找出control
            foreach (UIElement uie in mEditorPageLayoutRoot.Children)
            {
                Control c = uie as Control;
                if (c == null)
                    continue;
                ComponentData data = c.Tag as ComponentData;
                if (data == null)
                    continue;
                if (data.name == componentNames.ToString("f"))
                {
                    control = c;
                    break;
                }
            }

            r = new Rect(0, 0,
                       Utility.GetGridLength(mEditorPageScreenWidth, pixelW),
                       Utility.GetGridLength(mEditorPageScreenWidth, pixelH));

            if (control == null)
            {
                control = EditorMainPage.AddComponents(mEditorPageLayoutRoot, 0, 0, pixelW, pixelH, componentNames.ToString("f"));

                //第一次新增元件
                ComponentData data = new ComponentData();
                data.name = componentNames.ToString("f");
                data.mPosition.X = (int)r.X;
                data.mPosition.Y = (int)r.Y;
                data.mRange.X = (int)r.Width;
                data.mRange.Y = (int)r.Height;
                control.Tag = data;
            }

            gPopupControl.IsOpen = false;
            Utility.DragableGrid_ControlSelected(control, r);
        }

        //*****************************************************************************************************************************
        //這邊以下都放右邊導航小元件的callback
        //*****************************************************************************************************************************
        private void Grid_Tap_RouteDirection(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 8);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 8);
            _addDragComponent(EditorSetting.ComponentNames.RouteInstructor, w, h);
        }

        private void Grid_Tap_RouteProgress(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 2);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 8);
            _addDragComponent(EditorSetting.ComponentNames.NavigationProgress, w, h);
        }

        private void Grid_Tap_speedometer(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 8);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 8);
            _addDragComponent(EditorSetting.ComponentNames.SpeedShow, w, h);
        }

        private void Grid_Tap_DigitalSpeed(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 4);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 2);
            _addDragComponent(EditorSetting.ComponentNames.SpeedNumberSmall, w, h);
        }

        private void Grid_Tap_BigDigitalSpeed(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 8);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 6);
            _addDragComponent(EditorSetting.ComponentNames.SpeedNumberBig, w, h);
        }

        private void Grid_Tap_CurrentTime(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 4);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 2);
            _addDragComponent(EditorSetting.ComponentNames.CurrentTime, w, h);
        }

        private void Grid_Tap_TotalDistance(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 4);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 2);
            _addDragComponent(EditorSetting.ComponentNames.MovedDistance, w, h);
        }

        private void Grid_Tap_TotalTime(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 4);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 2);
            _addDragComponent(EditorSetting.ComponentNames.TotalTime, w, h);
        }

        private void Grid_Tap_GPSAccuracy(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 4);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 2);
            _addDragComponent(EditorSetting.ComponentNames.GPSAcuracy, w, h);
        }

        private void Grid_Tap_Battery(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 2);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 1);
            _addDragComponent(EditorSetting.ComponentNames.Battery, w, h);
        }

        private void Grid_Tap_AVGSpeed(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 4);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 2);
            _addDragComponent(EditorSetting.ComponentNames.AverageSpeed, w, h);
        }



        //這邊以下都放左邊小元件的callback
        private void Grid_Tap_Compass(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 4);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 4);
            _addDragComponent(EditorSetting.ComponentNames.CompassBig, w, h);
        }

        private void Grid_Tap_CompassTiny(object sender, System.Windows.Input.GestureEventArgs e)
        {
            int w = Utility.GetPixelLength(mEditorPageScreenWidth, 2);
            int h = Utility.GetPixelLength(mEditorPageScreenWidth, 2);
            _addDragComponent(EditorSetting.ComponentNames.CompassSmall, w, h);
        }

    }
}
