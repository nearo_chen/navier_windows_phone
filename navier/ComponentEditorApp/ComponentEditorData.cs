﻿using MyUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ComponentEditorApp
{
    /// <summary>
    /// 專門處理由Default設定檔中取得資訊
    /// </summary>
    public static class EditorSetting
    {
        public enum ComponentNames
        {
            //需要導航，要打開ＧＯＡＬ　ＰＡＧＥ
            RouteInstructor,
            NavigationProgress,

            //需要ＧＰＳ
            SpeedShow,
            SpeedNumberSmall,
            SpeedNumberBig,
            MovedDistance,
            AverageSpeed,
            GPSAcuracy,

            //需要指南針
            CompassBig,
            CompassSmall,

            //ＤＡＴＥ　ＴＩＭＥ
            CurrentTime,
            TotalTime,

            //Battery
            Battery,
        }

        public delegate void ComponentEditorData_LoadFileDone(ComponentEditorData componentEditorData, string filename);
        public async static Task get_Installation(string filename, ComponentEditorData_LoadFileDone done)
        {
            byte[] bytearray = await IsolatedStorageHelper.ReadFile_Installation(filename);
            ComponentEditorData data = 
                (ComponentEditorData)IsolatedStorageHelper.Deserialize_JSON(bytearray, typeof(ComponentEditorData));
            done(data, filename);
        }

        public async static Task get_Local(string filename, string folder, ComponentEditorData_LoadFileDone done)
        {
            byte[] bytearray = await IsolatedStorageHelper.ReadFile_Local(filename, folder);
            ComponentEditorData data = 
                (ComponentEditorData)IsolatedStorageHelper.Deserialize_JSON(bytearray, typeof(ComponentEditorData));
            done(data, filename);
        }

        private static int _requestIDCount(ComponentEditorData componentEditorData)
        {
            componentEditorData.idcount++;
            return componentEditorData.idcount - 1;
        }

        public static void AddComponent(ComponentEditorData componentEditorData,
           ComponentData data)
        {
            if (data.id <= 0)
            {
                data.id = _requestIDCount(componentEditorData);
                componentEditorData.mComponents.Add(data);
            }
            //大於0的id表示ComponentData的指標已經處於control的TAG裡面，
            //直接去修改就行了
        }

        public static void AddComponent(ComponentEditorData componentEditorData,
            string name, int posX, int posY, int w, int h)
        {
            ComponentData data = new ComponentData();
            data.name = name;
            data.mPosition.X = posX;
            data.mPosition.Y = posY;
            data.mRange.X = w;
            data.mRange.Y = h;           

            AddComponent(componentEditorData, data);
        }

        public static void RemoveComponent(ComponentEditorData componentEditorData,
            int removeid)
        {
            if (removeid <= 0)
                return;
            foreach (ComponentData d in componentEditorData.mComponents)
            {
                if (d.id == removeid)
                {
                    componentEditorData.mComponents.Remove(d);
                    return;
                }
            }
        }


        /// <summary>
        /// 檢查看看UI編輯中，是否有需要GPS定位的元件
        /// </summary>
        public static bool CheckHasGPSComponent(ComponentEditorData componentEditorData)
        {
            //需要ＧＰＳ
            //SpeedShow,
            //SpeedNumberSmall,
            //SpeedNumberBig,
            //MovedDistance,
            //AverageSpeed,

            //一個一個檢查是否有導航元件
            foreach (ComponentData data in componentEditorData.mComponents)
            {
                if (
                        data.name == EditorSetting.ComponentNames.SpeedShow.ToString("f")
                     || data.name == EditorSetting.ComponentNames.SpeedNumberSmall.ToString("f")
                     || data.name == EditorSetting.ComponentNames.SpeedNumberBig.ToString("f")
                     || data.name == EditorSetting.ComponentNames.MovedDistance.ToString("f")
                     || data.name == EditorSetting.ComponentNames.AverageSpeed.ToString("f")
                     || data.name == EditorSetting.ComponentNames.GPSAcuracy.ToString("f")
                    )
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 檢查看看UI編輯中，是否有需要導航路線規畫的元件
        /// </summary>
        public static bool CheckHasNavigationComponent(ComponentEditorData componentEditorData)
        {
            //一個一個檢查是否有導航元件
            foreach (ComponentData data in componentEditorData.mComponents)
            {
                if (
                       (data.name == EditorSetting.ComponentNames.NavigationProgress.ToString("f"))
                    || (data.name == EditorSetting.ComponentNames.RouteInstructor.ToString("f"))
                    )
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 只有路徑導航有存在時，才會需要有NOTIC語音導航(因為路徑計算被扯在導航指示裡面，所以當HUD只有導航進度而沒有路徑導航時，
        /// 計算路近完不要讓他講話)
        /// </summary>
        public static bool CheckIfNeedNavigationNotic(ComponentEditorData componentEditorData)
        {
            //一個一個檢查是否有導航元件
            foreach (ComponentData data in componentEditorData.mComponents)
            {
                if (
                       (data.name == EditorSetting.ComponentNames.RouteInstructor.ToString("f"))                    
                    )
                {
                    return true;
                }
            }
            return false;
        }
    }







    ///// <summary>
    ///// range存放的是以螢幕寬高最小編為基礎的百分比數值
    ///// </summary>
    //public class range
    //{
    //    public float W = 0;
    //    public float H = 0;
    //}
}
