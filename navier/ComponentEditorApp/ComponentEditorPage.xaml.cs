﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyUtility;
using System.Windows.Media;
using System.Windows.Input;
using Windows.Devices.Sensors;

namespace ComponentEditorApp
{
    public partial class ComponentEditorPage : PhoneApplicationPage
    {
        private Inclinometer _inclinometer;
        private Windows.Foundation.TypedEventHandler<Inclinometer, InclinometerReadingChangedEventArgs> _inclinometerTypedEventHandler = null;


        /// <summary>
        /// 打開編輯介面時，一定會有的元件清單(看是讀檔或是新增)
        /// </summary>
        ComponentEditorData mComponentEditorData = null;

        Grid mEditGridRoot;
        Rect mEditRegionRect;


        public ComponentEditorPage()
        {
            InitializeComponent();


            mDeleteButton.Visibility = mYesButton.Visibility = System.Windows.Visibility.Collapsed;
            //   double screenHeight = Utility.GetDeviceHeight(true);
            //   double screenWidth = Utility.GetDeviceWidth(true);
            //   EditRegion.Height = screenHeight - 80;
            //    EditRegion.Width = LayoutRoot.Width;//screenWidth;

            try
            {
                ShowAppBar();
            }
            catch (Exception e)
            {
                Utility.ShowMessageBox(e.Message);
            }

            Utility.DragableGrid_MustSetShowConfirmDraggingCallback(ShowConfirmDragging, HideConfirmDragging
                // mComponentEditorData
                );

            //水平感測
            _inclinometer = Inclinometer.GetDefault();
            if (_inclinometer != null)
            {
                // Establish the report interval for all scenarios
                uint minReportInterval = _inclinometer.MinimumReportInterval;
                uint reportInterval = minReportInterval > 100 ? minReportInterval : 100;
                _inclinometer.ReportInterval = reportInterval;

                // Establish the event handler
                _inclinometerTypedEventHandler =
                    new Windows.Foundation.TypedEventHandler<Inclinometer, InclinometerReadingChangedEventArgs>(ReadingChanged);
                _inclinometer.ReadingChanged += _inclinometerTypedEventHandler;
            }
        }



        private void EditRegion_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            EditRegion.Width = EditRegionRoot.ActualWidth;
            EditRegion.Height = EditRegionRoot.ActualHeight;

            EditRegion.Children.Clear();
            double width = EditRegion.ActualWidth;// Utility.GetDeviceHeight(true);
            double height = EditRegion.ActualHeight;//Utility.GetDeviceWidth(true);
            Color color = Utility.GetHUDColor(Utility.SelectColor.color_cyan);
            mEditGridRoot = Utility.DrawGridLine(width, Utility.GridThickness, Utility.GridOpacity, color);
            mEditGridRoot.Background = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            //mEditGridRoot.VerticalAlignment = System.Windows.VerticalAlignment.Center;沒屁用，因為他是接在CANVAS下面
            EditRegion.Children.Add(mEditGridRoot);

            mEditRegionRect = new Rect(0, 0, mEditGridRoot.Width, mEditGridRoot.Height);
            //EditorMainPage.AddComponents(EditRegion, 0, 0, 300, 300,
            //    EditorSetting.ComponentNames.RouteInstructor.ToString("f"));
        }

        private void EditRegion_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point newPoint = e.GetPosition(EditRegion);
            Utility.DragableGrid_EditRegionMouseDown(ref newPoint, mEditGridRoot.Width, mEditGridRoot.Height);
        }

        private void EditRegion_MouseMove(object sender, MouseEventArgs e)
        {
            Point newPoint = e.GetPosition(EditRegion);
            Utility.DragableGrid_EditRegionMouseMove(ref newPoint, ref mEditRegionRect);

        }

        private void EditRegion_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Point newPoint = e.GetPosition(EditRegion);
            Utility.DragableGrid_EditRegionMouseUp(ref newPoint, ref mEditRegionRect);
        }



        //   EditorSetting.ComponentNames mNowComponentNames;

        private void Button_OK_Click(object sender, RoutedEventArgs e)
        {
            Utility.OutputDebugString("Button_OK_Click");
            mDeleteButton.Visibility = mYesButton.Visibility = System.Windows.Visibility.Collapsed;

            ApplicationBar.IsVisible = true;

            //將確定的元件加入元件清單
            Control draggingControl = Utility.DragableGrid_GetDraggingControl();
            ComponentData componentData = draggingControl.Tag as ComponentData;

            Rect nowDraggingRect = Utility.DragableGrid_GetNowDraggingRect();
            componentData.mPosition.X = (int)nowDraggingRect.X;
            componentData.mPosition.Y = (int)nowDraggingRect.Y;
            componentData.mRange.X = (int)nowDraggingRect.Width;
            componentData.mRange.Y = (int)nowDraggingRect.Height;

            ComponentEditorApp.EditorSetting.AddComponent(mComponentEditorData,
                componentData);

            ISetDragColor setDragColor = draggingControl as ISetDragColor;
            if (setDragColor != null)
                setDragColor.RevertDragColor();

        }

        private void Button_Delete_Click(object sender, RoutedEventArgs e)
        {
            //案取消的話，就找看看是不是位於編輯清單中的物件，將他移除
            Control draggingControl = Utility.DragableGrid_GetDraggingControl();
            ComponentData componentData = draggingControl.Tag as ComponentData;
            ComponentEditorApp.EditorSetting.RemoveComponent(mComponentEditorData,
                componentData.id);

            Utility.DragableGrid_DeleteNowDraggingControl();
        }


        public const string DefaultNavi_Filename = "DefaultNavi.txt";
        public const string DefaultHUD_Filename = "DefaultHUD.txt";
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            try
            {
                string sOpenFileName = "";
                if (this.NavigationContext.QueryString.ContainsKey("openfilename"))
                    sOpenFileName = this.NavigationContext.QueryString["openfilename"];

                if (!string.IsNullOrEmpty(sOpenFileName))
                {
                    //讀檔並且將元件都加在畫面上
                    if (sOpenFileName.Equals(DefaultNavi_Filename) || sOpenFileName.Equals(DefaultHUD_Filename))
                    {
                        await EditorSetting.get_Installation("HUDData/" + sOpenFileName, ComponentEditorData_LoadFileDone);
                    }
                    else
                    {
                        await EditorSetting.get_Local(sOpenFileName, ComponentEditorPage.FOLDER, ComponentEditorData_LoadFileDone);
                    }
                }
                else
                {
                    //user正要開始建立新的設定檔
                    mComponentEditorData = new ComponentEditorData();
                    mComponentEditorData.FileName = Utility.GenerateUniqueID() + ".txt";
                    mComponentEditorData.ShowName = ResourceApp.Resources.AppResources.Editor_MyLayout;
                }
                mComponentPageName.Text = mComponentEditorData.ShowName;
            }
            catch(Exception exception)
            {
#if DEBUG
                Utility.ShowMessageBox(exception.Message);
                Utility.ShowMessageBox(exception.StackTrace);
#endif
            }
        }

        void ComponentEditorData_LoadFileDone(ComponentEditorData data, string openFileName)
        {
            mComponentEditorData = data;
           
            foreach (ComponentData cd in mComponentEditorData.mComponents)
            {
                Control control = EditorMainPage.AddComponents(EditRegion,
                     Utility.GetPixelX(EditRegion.ActualWidth, cd.mPosition.X),
                      Utility.GetPixelY(EditRegion.ActualWidth, cd.mPosition.Y, mEditGridRoot.ActualHeight),
                  Utility.GetPixelLength(EditRegion.ActualWidth, cd.mRange.X),
                  Utility.GetPixelLength(EditRegion.ActualWidth, cd.mRange.Y), cd.name);
                control.Tag = cd;

                ISetDragColor sdc = control as ISetDragColor;
                sdc.RevertDragColor();
            }

            if (openFileName.Contains(DefaultNavi_Filename) || openFileName.Contains(DefaultHUD_Filename))
            {
                //編輯default設定檔，就自動生一個新的LAYOUT給USER編輯
                mComponentEditorData.FileName = Utility.GenerateUniqueID() + ".txt";
                mComponentEditorData.ShowName = ResourceApp.Resources.AppResources.Editor_MyLayout;
            }
            else
            {
                //編輯原本就是user自訂的設定檔                    
            }
        }

        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ClosePopupWindow())
            {
                ApplicationBar.IsVisible = true;
                e.Cancel = true;
            }
        }

        private void PhoneApplicationPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            //因為手機一開始進入PAGE時，不會觸發OrientationChanged，所以在這之前還是需要水平儀
            if (_inclinometerTypedEventHandler != null)
            {
                _inclinometer.ReadingChanged -= _inclinometerTypedEventHandler;
                _inclinometerTypedEventHandler = null;
            }

            if (ApplicationBar.IsVisible)//如果是問是否的狀態時就不需要調整了
            {
                if (e.Orientation == PageOrientation.LandscapeRight)
                    Utility.SetPanelPosition(EditRegion, ApplicationBar.DefaultSize, 0, true);
                else if (e.Orientation == PageOrientation.LandscapeLeft)
                    Utility.SetPanelPosition(EditRegion, 0, 0, true);
            }
        
        }

        private void ReadingChanged(object sender, InclinometerReadingChangedEventArgs e)
        {
            InclinometerReading reading = e.Reading;
            Dispatcher.BeginInvoke(() =>
            {
                Point pos = Utility.GetPanelPosition(EditRegion, true);

                if (reading.RollDegrees > 0 && pos.X < ApplicationBar.DefaultSize)
                    Utility.SetPanelPosition(EditRegion, ApplicationBar.DefaultSize, 0, true);
                else if (reading.RollDegrees < 0 && pos.X >= ApplicationBar.DefaultSize)
                    Utility.SetPanelPosition(EditRegion, 0, 0, true);

                if ((reading.RollDegrees > 30 || reading.RollDegrees < -30) 
                    && _inclinometerTypedEventHandler != null)
                {
                     _inclinometer.ReadingChanged -= _inclinometerTypedEventHandler;
                     _inclinometerTypedEventHandler = null;
                }
            });
        }

    }
}