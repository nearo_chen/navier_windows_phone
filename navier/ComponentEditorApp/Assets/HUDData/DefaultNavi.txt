﻿{
"idcount":1,
"mComponents":[

{"mPosition":{"X":8,"Y":0},"mRange":{"X":8,"Y":8},"name":"RouteInstructor"},
{"mPosition":{"X":0,"Y":0},"mRange":{"X":8,"Y":8},"name":"SpeedShow"},
{"mPosition":{"X":2,"Y":6},"mRange":{"X":4,"Y":2},"name":"SpeedNumberSmall"},
{"mPosition":{"X":2,"Y":2},"mRange":{"X":4,"Y":4},"name":"CompassBig"},
{"mPosition":{"X":14,"Y":0},"mRange":{"X":2,"Y":8},"name":"NavigationProgress"},
{"mPosition":{"X":10,"Y":6},"mRange":{"X":4,"Y":2},"name":"GPSAcuracy"},
{"mPosition":{"X":0,"Y":0},"mRange":{"X":2,"Y":1},"name":"Battery"}
]}