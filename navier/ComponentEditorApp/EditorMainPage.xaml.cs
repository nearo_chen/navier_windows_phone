﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ComponentEditorApp.Resources;
using MyUtility;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Shapes;
using System.Windows.Media;
using SpeedShowControlApp;
using CompassApp;
using RouteInstructorApp;

namespace ComponentEditorApp
{
    public partial class EditorMainPage : PhoneApplicationPage
    {


        // Constructor
        public EditorMainPage()
        {
            InitializeComponent();

            double screenHeight = Utility.GetDeviceHeight(true);
            double screenWidth = Utility.GetDeviceWidth(true);
            Color color = Utility.GetHUDColor(Utility.SelectColor.color_cyan);
            Grid gridRoot = Utility.DrawGridLine(screenWidth, Utility.GridThickness, Utility.GridOpacity, color);
            LayoutRoot.Children.Insert(0, gridRoot);

            Utility.SetScreenAutoSleep(false);

            //  Utility.EnableDebugSetting();
        }


        private async void Click_SaveDefault(object sender, RoutedEventArgs e)
        {
            ComponentEditorData data = new ComponentEditorData();
            //data.mRouteInstructor = new ComponentData(0, 0, 100 ,100);
            //data.mSpeedShow = new ComponentData(0, 0, 100, 100);
            //data.mSpeedNumberSmall = new ComponentData(0, 0, 100, 100);
            //data.mCompassA = new ComponentData(0, 0, 100, 100);

            ComponentEditorApp.EditorSetting.AddComponent(data,
                EditorSetting.ComponentNames.RouteInstructor.ToString("f"), 0, 0, 100, 100);
            ComponentEditorApp.EditorSetting.AddComponent(data,
                EditorSetting.ComponentNames.SpeedShow.ToString("f"), 0, 0, 100, 100);
            ComponentEditorApp.EditorSetting.AddComponent(data,
                EditorSetting.ComponentNames.SpeedNumberSmall.ToString("f"), 0, 0, 100, 100);
            ComponentEditorApp.EditorSetting.AddComponent(data,
                EditorSetting.ComponentNames.CompassBig.ToString("f"), 0, 0, 100, 100);

            byte[] bbb = IsolatedStorageHelper.Serialize_JSON(data);

            await IsolatedStorageHelper.WriteToFile_Local("test.txt", bbb);
        }

        private async void click_Load(object sender, RoutedEventArgs e)
        {
            byte[] bbb = await IsolatedStorageHelper.ReadFile_Local("test.txt", null);
            if (bbb == null)
                return;

            ComponentEditorData data = (ComponentEditorData)IsolatedStorageHelper.Deserialize_JSON(bbb, typeof(ComponentEditorData));



        }

        public static Control AddComponents(Panel layoutRoot, int pixelX, int pixelY, int pixelWidth, int pixelHeight, string name)
        {
            Control control = null;
            if (name == EditorSetting.ComponentNames.RouteInstructor.ToString("f"))
            {
                control = CreateRouteInstructorViewUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }
            else if (name == EditorSetting.ComponentNames.NavigationProgress.ToString("f"))
            {
                control = CreateNavigationProgressUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }
            else if (name == EditorSetting.ComponentNames.SpeedShow.ToString("f"))
            {
                control = CreateSpeedShowUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }
            else if (name == EditorSetting.ComponentNames.SpeedNumberSmall.ToString("f"))
            {
                control = CreateSpeedNumberSmallUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }
            else if (name == EditorSetting.ComponentNames.SpeedNumberBig.ToString("f"))
            {
                control = CreateSpeedNumberBigUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }
            else if (name == EditorSetting.ComponentNames.CompassBig.ToString("f"))
            {
                control = CreateCompassBigUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }
            else if (name == EditorSetting.ComponentNames.CompassSmall.ToString("f"))
            {
                control = CreateCompassSmallUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }

            else if (name == EditorSetting.ComponentNames.MovedDistance.ToString("f"))
            {
                control = CreateMovedDistanceUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }
            else if (name == EditorSetting.ComponentNames.AverageSpeed.ToString("f"))
            {
                control = CreateAverageSpeedUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }
            else if (name == EditorSetting.ComponentNames.CurrentTime.ToString("f"))
            {
                control = CreateCurrentTimeUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }
            else if (name == EditorSetting.ComponentNames.TotalTime.ToString("f"))
            {
                control = CreateTotalTimeUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }
            else if (name == EditorSetting.ComponentNames.GPSAcuracy.ToString("f"))
            {
                control = CreateGPSAcuracyUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }
            else if (name == EditorSetting.ComponentNames.Battery.ToString("f"))
            {
                control = CreateBatteryUI(layoutRoot, pixelX, pixelY, pixelWidth, pixelHeight);
            }

            return control;
        }


        private async void click_FromAsset(object sender, RoutedEventArgs e)
        {


            //byte[] bbb = await IsolatedStorageHelper.ReadFile_Installation("Default.txt");
            //ComponentEditorData data = (ComponentEditorData)IsolatedStorageHelper.Deserialize_JSON(bbb, typeof(ComponentEditorData));

            await EditorSetting.get_Installation("Default.txt", ComponentEditorData_LoadFileDone);

        }

        void ComponentEditorData_LoadFileDone(ComponentEditorData componentEditorData, string filename)
        {
            ComponentEditorData UIData = componentEditorData;
            double screenWidth = Utility.GetDeviceWidth(true);
            foreach (ComponentData data in UIData.mComponents)
            {

                int x = Utility.GetPixelX(screenWidth, data.mPosition.X);
                int y = Utility.GetPixelY(screenWidth, data.mPosition.Y, LayoutRoot.ActualHeight);
                int w = Utility.GetPixelLength(screenWidth, data.mRange.X);
                int h = Utility.GetPixelLength(screenWidth, data.mRange.Y);

                AddComponents(LayoutRoot, x, y, w, h, data.name);
            }
        }


        public static RouteInstructorView CreateRouteInstructorViewUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            RouteInstructorView routeInstructorView = new RouteInstructorView();

            routeInstructorView.GetRouteRendererControl().initLine(true);

            //移動control座標
            Utility.SetControlPosition(routeInstructorView, viewPosX, viewPosY, true);
            Utility.SetControlWH(routeInstructorView, w, h, true);

            layoutRoot.Children.Add(routeInstructorView);

            // routeInstructorView.Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_orangered));

            return routeInstructorView;
        }

        public static SpeedRendererControl CreateSpeedShowUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            SpeedRendererControl speedRendererControl = SpeedRendererControl.AddToRoot(layoutRoot, viewPosX, viewPosY, w, h);
            speedRendererControl.Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_orangered));
            return speedRendererControl;
        }

        public static SpeedNumberSmallControl CreateSpeedNumberSmallUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            SpeedNumberSmallControl speedNumberSmallControl = SpeedNumberSmallControl.AddToRoot(layoutRoot, viewPosX, viewPosY, w, h);
            return speedNumberSmallControl;
        }

        public static SpeedNumberBigControl CreateSpeedNumberBigUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            SpeedNumberBigControl speedNumberBigControl = SpeedNumberBigControl.AddToRoot(layoutRoot, viewPosX, viewPosY, w, h);
            return speedNumberBigControl;
        }

        public static CampassAControl CreateCompassBigUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            CampassAControl campassAControl = CampassAControl.AddToRoot(layoutRoot, viewPosX, viewPosY, w, h);
            campassAControl.initCompass();
            return campassAControl;
        }

        public static CampassAControl CreateCompassSmallUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            CampassAControl campassAControl = CampassAControl.AddToRoot(layoutRoot, viewPosX, viewPosY, w, h);
            campassAControl.initCompass();
            return campassAControl;
        }

        public static MovedDistanceControl CreateMovedDistanceUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            MovedDistanceControl movedDistanceControl = MovedDistanceControl.AddToRoot(layoutRoot, viewPosX, viewPosY, w, h);
            return movedDistanceControl;
        }

        public static NavigationProgressControl CreateNavigationProgressUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            NavigationProgressControl navigationProgressControl = NavigationProgressControl.AddToRoot(layoutRoot, viewPosX, viewPosY, w, h);

            navigationProgressControl.SetMyVehicle(true);
            navigationProgressControl.Interface_SetColor(Utility.GetHUDColor(Utility.SelectColor.color_cyan));
            navigationProgressControl.SetProgress(30, 400);

            return navigationProgressControl;
        }

        public static AverageSpeedControl CreateAverageSpeedUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            AverageSpeedControl averageSpeedControl = AverageSpeedControl.AddToRoot(layoutRoot, viewPosX, viewPosY, w, h);

            return averageSpeedControl;
        }

        public static CurrentTimeControl CreateCurrentTimeUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            CurrentTimeControl currentTimeControl = CurrentTimeControl.AddToRoot(layoutRoot, viewPosX, viewPosY, w, h);

            return currentTimeControl;
        }

        public static TotalTimeControl CreateTotalTimeUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            TotalTimeControl totalTimeControl = TotalTimeControl.AddToRoot(layoutRoot, viewPosX, viewPosY, w, h);

            return totalTimeControl;
        }

        public static GPSAccuracyControl CreateGPSAcuracyUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            GPSAccuracyControl gpsaccuracyControl = GPSAccuracyControl.AddToRoot(layoutRoot, viewPosX, viewPosY, w, h);

            return gpsaccuracyControl;
        }

        public static BatteryControl CreateBatteryUI(Panel layoutRoot, int viewPosX, int viewPosY, int w, int h)
        {
            BatteryControl batteryControl = BatteryControl.AddToRoot(layoutRoot, viewPosX, viewPosY, w, h);

            return batteryControl;
        }


        bool isKM = true;
        private void click_SetColor(object sender, RoutedEventArgs e)
        {
            //  var toast = GetBasicToast();
            //  toast.Width = Utility.GetDeviceWidth(true);
            ////  toast.Height = Utility.GetDeviceHeight(true);
            //  toast.MillisecondsUntilHidden = 500;
            //  toast.Show();

            Utility.SetHUDColor(LayoutRoot, Utility.SelectColor.color_yellow);
            Utility.SetHUDUnit(LayoutRoot, isKM);

            isKM = !isKM;
        }


    }
}