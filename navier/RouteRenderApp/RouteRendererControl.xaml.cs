﻿#define VIEW_CLIP //設定veiw clip 關起來可以偵錯用，正常要打開
#define RUN_MY_VIEW_CLIP //設定是否要我自己判斷篩選(我先過濾可省效能) mPointCollection_GlobalPosition的點是否超出範圍，正常要打開

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Media.Animation;
using MyUtility;

namespace RouteInstructorApp
{
    public partial class RouteRendererControl : UserControl
    {
        private Rectangle mMyPositionBox = null;//畫我的位置
        private Polyline mPolyline = null;//畫路線用
        private Point mGlobalCentor;      //紀錄畫面中央的經緯度座標
        public Point GetGlobalCentor() { return mGlobalCentor; }
        private List<Point> mPointCollection_GlobalPosition = null; //紀錄外部傳入的經緯度座標

        /// <summary>
        /// 控制目前view寬高，要跨多少經緯度，
        /// 設定view中心點到上或下左或右經緯度的範圍
        /// </summary>
        private Point mLongitude_Latitude_Range;

        public RouteRendererControl()
        {
            InitializeComponent();

            Width = 400;
            Height = 400;
            //mViewCoord2D = new Point(Margin.Left, Margin.Top);

#if VIEW_CLIP || !DEBUG
            RectangleGeometry clipRetangle = new RectangleGeometry();
            clipRetangle.Rect = new Rect(0, 0, Width, Height);
            LayoutRoot.Clip = clipRetangle;
#endif

            mLongitude_Latitude_Range.X = mLongitude_Latitude_Range.Y = mGlobalCentor.X = mGlobalCentor.Y = 0;
        }

        /// <summary>
        ///  傳入資訊給control給他去畫路線，回傳被view culling過後存在的點數量
        /// </summary>
        /// <param name="pointCollection">路徑規化的經緯度座標陣列</param>
        /// <param name="globalPositionCentor">畫面中心點的經緯度座標</param>
        /// <param name="scaleGlobalRange">顯示的縮放程度</param>
        public int SetGlobalLines(List<Point> pointCollection, Point globalPositionCentor, Point? scaleGlobalRange)
        {
            mGlobalCentor = globalPositionCentor;
            if (scaleGlobalRange != null)
                mLongitude_Latitude_Range = scaleGlobalRange.Value;

            if (mPolyline == null)
            {
                mPolyline = new Polyline();
                SolidColorBrush brush = new SolidColorBrush(Colors.Brown);
                mPolyline.Stroke = brush;
                mPolyline.StrokeThickness = 2;
                mPolyline.FillRule = FillRule.EvenOdd;
                LayoutRoot.Children.Add(mPolyline);
            }

            // Add the Polyline Element
            mPointCollection_GlobalPosition = pointCollection;

            PointCollection lineInView = GetLineInRange_GlobalPosition();
            mPolyline.Points = TranslateGlobalPositionToViewCoord(lineInView);

            return lineInView.Count;
        }

        /// <summary>
        /// 傳入經緯度，當作view的中心點，回傳被view culling過後存在的點數量
        /// </summary>
        public int SetGlobalCentor(ref Point globalPositionCentor, Point? scaleGlobalRange)
        {
            mGlobalCentor = globalPositionCentor;
            if (scaleGlobalRange != null)
            {
                mLongitude_Latitude_Range.X = scaleGlobalRange.Value.X;
                mLongitude_Latitude_Range.Y = scaleGlobalRange.Value.Y;

                if (mLongitude_Latitude_Range.X < 0.00001f)
                    mLongitude_Latitude_Range.X = 0.00001f;
                if (mLongitude_Latitude_Range.Y < 0.00001f)
                    mLongitude_Latitude_Range.Y = 0.00001f;
            }

            //根據目前control可視範圍，找出要畫出的line
            PointCollection lineInView = GetLineInRange_GlobalPosition();
            mPolyline.Points = TranslateGlobalPositionToViewCoord(lineInView);

            return lineInView.Count;
        }



        //public Point GetLongitude_Latitude_Range()
        //{
        //    return mLongitude_Latitude_Range;
        //}

        /// <summary>
        /// 根據view的中心點經緯度座標，過濾看得見的路徑
        /// </summary>
        /// <returns></returns>
        private PointCollection GetLineInRange_GlobalPosition()
        {
#if RUN_MY_VIEW_CLIP || !DEBUG
            //為預防路線斷掉，擷取最開始與最後的點之間的的所有點
            int iStart = -1;
            int iEnd = -1;

            int iCount = 0;
            IEnumerator<Point> itr = mPointCollection_GlobalPosition.GetEnumerator();
            while (itr.MoveNext())
            {
                if (Math.Abs(mGlobalCentor.X - itr.Current.X) < mLongitude_Latitude_Range.X &&
                    Math.Abs(mGlobalCentor.Y - itr.Current.Y) < mLongitude_Latitude_Range.Y
                    )
                {
                    if (iStart < 0)
                        iStart = iCount;
                    iEnd = iCount;
                }
                iCount++;
            }
            itr.Dispose();

            PointCollection newPointCollection = new PointCollection();

            if (iStart < 0 || iEnd < 0)
                return newPointCollection;

            iCount = 0;
            itr = mPointCollection_GlobalPosition.GetEnumerator();
            while (itr.MoveNext())
            {
                if (
                    (iCount >= iStart - 1)
                    &&
                    (iCount <= iEnd + 1)
                    )
                    newPointCollection.Add(itr.Current);

                if (iCount >= iEnd + 1)
                    break;

                iCount++;
            }
            itr.Dispose();

            return newPointCollection;
#else
            return mPointCollection_GlobalPosition;
#endif
        }

        /// <summary>
        /// 將傳入PointCollection的經緯度位置，根據view的Scale，換成view的2D座標
        /// </summary>
        private PointCollection TranslateGlobalPositionToViewCoord(PointCollection outPointCollection)
        {
            PointCollection viewPoints = new PointCollection();
            //畫面座標左上角是0,0, max=Width, Height
            Point viewCentor = new Point(Width * 0.5f, Height * 0.5f);

            IEnumerator<Point> itr = outPointCollection.GetEnumerator();
            Point nowCoord = new Point();
            while (itr.MoveNext())
            {
                double ratio = (itr.Current.X - mGlobalCentor.X) / mLongitude_Latitude_Range.X;
                ratio = ratio * (Width * 0.5);
                nowCoord.X = viewCentor.X + ratio;

                ratio = (itr.Current.Y - mGlobalCentor.Y) / mLongitude_Latitude_Range.Y;
                ratio = ratio * (Height * 0.5);
                nowCoord.Y = viewCentor.Y + ratio;

                viewPoints.Add(nowCoord);
            }

            return viewPoints;
        }

        //  float mRotateRec = 0;
        /// <summary>
        /// 設定control的旋轉角度
        /// </summary>
        public void SetRotationTransform(float degree)
        {
            Dispatcher.BeginInvoke(() =>
            {
                DoubleAnimation anima = new DoubleAnimation();

                //fix目前角度皆為正0~360之間
                if (rotateRoot.Angle < 0)
                {
                    rotateRoot.Angle += 360;
                }

                if (rotateRoot.Angle > 360)
                {
                    rotateRoot.Angle -= 360;
                }

                //設定最短角度轉向
                if (degree < 0)
                {
                    degree += 360;
                }

                if (degree > 360)
                {
                    degree -= 360;
                }

                if (rotateRoot.Angle < 180 && Math.Abs(rotateRoot.Angle - degree) > 180)
                    degree -= 360;

                if (rotateRoot.Angle > 180 && Math.Abs(degree - rotateRoot.Angle) > 180)
                    rotateRoot.Angle -= 360;


                anima.From = rotateRoot.Angle;
                anima.To = degree;
                //  mRotateRec += degree;

                anima.Duration = new Duration(TimeSpan.FromSeconds(1));  //设置动画耗时，这里为0.5秒

                // 设置附加属性
                Storyboard.SetTarget(anima, rotateRoot);  //使动画与特定对象关联
                Storyboard.SetTargetProperty(anima, new PropertyPath(RotateTransform.AngleProperty));  //使动画与特定属性关联

                // 生成storyboard，把动画加进去并启动
                Storyboard storyboard = new Storyboard();
                storyboard.Children.Add(anima);
                storyboard.Begin();
            });
        }

      

        /// <summary>
        /// 計算目前位於的路線上，要將行進方向的路線設定成行進朝上，需要轉幾度
        /// 在2D座標上算角度的算法，耗效能，單存開發測試用
        /// </summary>
        public float GetNowRouteUpDegree(int onRouteIndex)
        {
            PointCollection pc = new PointCollection();
            foreach (Point ppp in mPointCollection_GlobalPosition)
                pc.Add(ppp);
            pc = TranslateGlobalPositionToViewCoord(pc);

            Microsoft.Xna.Framework.Vector2 A = new Microsoft.Xna.Framework.Vector2(
                (float)pc[onRouteIndex + 1].X - (float)pc[onRouteIndex].X,
                (float)pc[onRouteIndex + 1].Y - (float)pc[onRouteIndex].Y);
            A.Normalize();
            Microsoft.Xna.Framework.Vector2 B = Microsoft.Xna.Framework.Vector2.UnitY;//計算與(0,1)的夾角，(0,1)代表緯度朝上的向量
            float dotY = Microsoft.Xna.Framework.Vector2.Dot(A, Microsoft.Xna.Framework.Vector2.UnitY);
            float dotX = Microsoft.Xna.Framework.Vector2.Dot(A, Microsoft.Xna.Framework.Vector2.UnitX);

            float theta = (float)Math.Acos(dotY);
            float degree = Microsoft.Xna.Framework.MathHelper.ToDegrees(theta);
            if (dotY > 0)
            {
                if (dotX > 0)
                {
                    degree = -1 * degree;
                    //  degree = 360 + degree;
                }
                else
                {

                }
            }
            else
            {
                if (dotX > 0)
                {
                    degree = 360 - degree;
                }
                else
                {
                    //degree = degree;
                }
            }

            return degree;
        }

#if DEBUG
        //   private List<Point> mPointCollection_RenderMyWay = null;
        private Polyline mPolylineMe_Debug = null;//用來將我目前處於的線段 標示出來
        private Polyline mPolylineUp_Debug = null;//用來將我目前處於的線段 標示出來
        // private PointCollection myWayPoint = new PointCollection();
        public void RenderDebug_MyWay(int iNowIndexOnRoute, List<Point> pointCollection_RenderMyWay)
        {
            Dispatcher.BeginInvoke(() =>
            {
                System.Windows.Media.PointCollection pointsMyWay = new System.Windows.Media.PointCollection();
                pointsMyWay.Add(pointCollection_RenderMyWay[iNowIndexOnRoute + 0]);
                pointsMyWay.Add(pointCollection_RenderMyWay[iNowIndexOnRoute + 1]);

                if (mPolylineMe_Debug == null)
                {
                    mPolylineMe_Debug = new Polyline();
                    SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(100, 0, 255, 0));
                    mPolylineMe_Debug.Stroke = brush;
                    mPolylineMe_Debug.StrokeThickness = 5;
                    mPolylineMe_Debug.FillRule = FillRule.EvenOdd;
                    LayoutRoot.Children.Add(mPolylineMe_Debug);
                }

                if (mPolylineUp_Debug == null)
                {
                    mPolylineUp_Debug = new Polyline();
                    SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(100, 0, 0, 255));
                    mPolylineUp_Debug.Stroke = brush;
                    mPolylineUp_Debug.StrokeThickness = 5;
                    mPolylineUp_Debug.FillRule = FillRule.EvenOdd;
                    LayoutRoot.Children.Add(mPolylineUp_Debug);
                }

                PointCollection pointsUPWay = new PointCollection();
                pointsUPWay.Add(pointCollection_RenderMyWay[0]);
                pointsUPWay.Add(new Point(pointCollection_RenderMyWay[0].X, pointCollection_RenderMyWay[0].Y + 1));
                mPolylineUp_Debug.Points = TranslateGlobalPositionToViewCoord(pointsUPWay);

                if (iNowIndexOnRoute < mPolyline.Points.Count - 1)
                {
                    mPolylineMe_Debug.Points = TranslateGlobalPositionToViewCoord(pointsMyWay);
                }
            });
        }
#endif


    }
}
