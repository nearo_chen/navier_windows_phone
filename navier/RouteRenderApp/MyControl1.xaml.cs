﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;

namespace RouteInstructorApp
{
    public partial class MyControl1 : UserControl
    {
        public MyControl1()
        {
            InitializeComponent();
        }

        //定义Image的Source属性。这样这个用户控件就能从外面访问到这个Image控件的Source属性了  
        private string _imageUri;

        public string ImageUri
        {
            get
            {
                return _imageUri;
            }
            set
            {
                _imageUri = value;
                BitmapImage bmp = new BitmapImage(new Uri(value, UriKind.Relative));
                WImg.Source = bmp;
            }
        }

        //定义了TextBlock的Text属性。外头就可以给这个Text属性赋值了  
        private string _weekday;

        public string Weekday
        {
            get
            {
                return _weekday;
            }
            set
            {
                _weekday = value; ;
                weekday.Text = value;
            }
        }


        private string _temp;

        public string Temp
        {
            get
            {
                return _temp;
            }
            set
            {
                _temp = value;
                temp.Text = value;
            }
        }  
    }
}
