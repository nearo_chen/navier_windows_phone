﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using RouteInstructorApp.Resources;

using System.Windows.Media;
using System.Windows.Shapes;

namespace RouteInstructorApp
{
    public partial class MainPage : PhoneApplicationPage
    {
        Polyline myPolyline;
        RouteRendererControl mRouteRenderer;

        List<Point> myPointCollection3;
        int nowOnRoute = 0;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Add the Polyline Element
            myPolyline = new Polyline();
            SolidColorBrush brush = new SolidColorBrush(Colors.Brown);
            myPolyline.Stroke = brush;
            myPolyline.StrokeThickness = 2;
            myPolyline.FillRule = FillRule.EvenOdd;
            Point Point4 = new Point(1, 50);
            Point Point5 = new Point(1, 200);
            Point Point6 = new Point(200, 40);
            PointCollection myPointCollection2 = new PointCollection();
            myPointCollection2.Add(Point4);
            myPointCollection2.Add(Point5);
            myPointCollection2.Add(Point6);
            myPolyline.Points = myPointCollection2;
            ContentPanel.Children.Add(myPolyline);

            mRouteRenderer = new RouteRendererControl();
            ContentPanel.Children.Add(mRouteRenderer);

            myPointCollection3 = new List<Point>();
            Point4 = new Point(41, 41);
            Point5 = new Point(42, 42);
            Point6 = new Point(40.5, 43);
            Point Point7 = new Point(43.5, 44.9);
            Point Point8 = new Point(41.5, 45.9);
            Point Point9 = new Point(42.5, 46.9);

            myPointCollection3.Add(Point5);
            myPointCollection3.Add(Point4);
            myPointCollection3.Add(Point6);
            myPointCollection3.Add(Point7);
            myPointCollection3.Add(Point8);
            myPointCollection3.Add(Point9);


            Point globalCentor = myPointCollection3[0];
            Point scaleRange = new Point(60, 60);
            mRouteRenderer.SetGlobalLines(myPointCollection3, globalCentor, scaleRange);


            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Point globalCentor = myPointCollection3[nowOnRoute];
            Point scaleRange = new Point(0.1, 0.1);
            mRouteRenderer.SetGlobalCentor(ref globalCentor, scaleRange);

          //  mRouteRenderer.CorrectMyPosition(globalCentor, new Point(0, 0));
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Point globalCentor = myPointCollection3[nowOnRoute];
            Point scaleRange = new Point(4, 4);
            mRouteRenderer.SetGlobalCentor(ref globalCentor, scaleRange);
        }

        private void Button_Click_rotate(object sender, RoutedEventArgs e)
        {
            mRouteRenderer.SetRotationTransform(180);
        }

        private void Button_Click_move_next(object sender, RoutedEventArgs e)
        {
            Point globalCentor = myPointCollection3[nowOnRoute];
            Point scaleRange = new Point(4,4);
            mRouteRenderer.SetGlobalCentor(ref globalCentor, scaleRange);

            float degree = mRouteRenderer.GetNowRouteUpDegree(nowOnRoute);
            mRouteRenderer.SetRotationTransform(degree);
            mRouteRenderer.RenderDebug_MyWay(nowOnRoute, myPointCollection3);
            nowOnRoute++;

            degreeText.Text = "degree: " + degree.ToString();
        }

        private void Button_Click_UP(object sender, RoutedEventArgs e)
        {
            Point globalCentor = new Point(mRouteRenderer.GetGlobalCentor().X, mRouteRenderer.GetGlobalCentor().Y + 0.5);

            mRouteRenderer.SetGlobalCentor(ref globalCentor, null);
        }

        private void Button_Click_DOWN(object sender, RoutedEventArgs e)
        {
            Point globalCentor = new Point(mRouteRenderer.GetGlobalCentor().X, mRouteRenderer.GetGlobalCentor().Y - 0.5);

            mRouteRenderer.SetGlobalCentor(ref globalCentor, null);
        }

        private void Button_Click_LEFT(object sender, RoutedEventArgs e)
        {
            Point globalCentor = new Point(mRouteRenderer.GetGlobalCentor().X - 0.5, mRouteRenderer.GetGlobalCentor().Y);

            mRouteRenderer.SetGlobalCentor(ref globalCentor, null);
        }

        private void Button_Click_RIGHT(object sender, RoutedEventArgs e)
        {
            Point globalCentor = new Point(mRouteRenderer.GetGlobalCentor().X + 0.5, mRouteRenderer.GetGlobalCentor().Y);

            mRouteRenderer.SetGlobalCentor(ref globalCentor, null);
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}