﻿using Microsoft.Phone.Maps.Controls;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareOnFBApp
{
    public static class DecreaseRoutePoints
    {
        //Compare http://codingjames.blogspot.tw/2009/09/csorting-custom-array.html
        //class AreaData : IComparable
        //{
        //    public int srcID = -1;
        //    public float area = 0;
        //    public int CompareTo(object obj)
        //    {
        //        if (this.area < (obj as AreaData).area)
        //            return -1;
        //        else if (this.area == (obj as AreaData).area)
        //            return 0;
        //        else
        //            return 1;
        //    }
        //};

        class TriangleData
        {
           // String debugArea;

            public float areaSquared
            {
                get { return mAreaSquared; }
                set { mAreaSquared = value; 
                //    debugArea = mAreaSquared.ToString("#0.000000000000000000"); 
                }
            }

            private float mAreaSquared = -1;
            public int selfID = -1;
            public TriangleData prevTData = null;
            public TriangleData nextTData = null;
        }

        static float CalculateAreaSquare(float aX, float aY, float bX, float bY, float cX, float cY)
        {
            //計算面積
            Vector3 vecA = new Vector3(aX - bX, aY - bY, 0);
            Vector3 vecB = new Vector3(cX - bX, cY - bY, 0);
            Vector3 cross; Vector3.Cross(ref vecA, ref vecB, out cross);
            return cross.LengthSquared();
        }

        static void AddSortedList(List<TriangleData> triangleAreaList, TriangleData triangleData)
        {
            if (triangleAreaList.Count == 0)
            {
                triangleAreaList.Add(triangleData);
                return;
            }
            if (triangleAreaList[triangleAreaList.Count - 1].areaSquared < triangleData.areaSquared)
            {
                triangleAreaList.Add(triangleData);
                return;
            }
            int index = triangleAreaList.BinarySearch(triangleData, SortedByArea.Comparer);
            if (index < 0)
                triangleAreaList.Insert(~index, triangleData);//找不到面積相同，回傳遇到 大於面積的索引直(補數)
            else
                triangleAreaList.Insert(index, triangleData);//找到面積跟他一樣的索引直
        }

        class SortedByArea : Comparer<TriangleData>
        {
            public static SortedByArea Comparer = new SortedByArea();

            public override int Compare(TriangleData x, TriangleData y)
            {
                if (x.areaSquared < y.areaSquared)
                    return -1;
                else if (x.areaSquared == y.areaSquared)
                    return 0;
                else
                    return 1;
            }
        }

        class SortedByTrackingPointID : Comparer<TriangleData>
        {
            public override int Compare(TriangleData x, TriangleData y)
            {
                if (x.selfID < y.selfID)
                    return -1;
                //else if (x.selfID == y.selfID)
                //   return 0;
                else
                    return 1;
            }
        }

        /// <summary>
        /// 路線不需要簡化，沒做任何事，救回傳null
        /// </summary>    
        public static int[] ReduceRoutePoints(int destPointsAmount, GeoCoordinateCollection trackingPoints)
        {
            if (trackingPoints.Count <= destPointsAmount)
                return null;

            GeoCoordinate posPrev = null;
            GeoCoordinate posNext = null;
            GeoCoordinate posNow = null;
            //AreaData[] triangleArea = new AreaData[trackingPoints.Count];

            //http://www.cnblogs.com/hkncd/archive/2011/05/30/2063124.html
            List<TriangleData> triangleAreaList = new List<TriangleData>();
            int count = 0;

            if (trackingPoints.Count < 2)//保護
                return null;

            #region 每個點跑一次計算他們的面積
            TriangleData oldTdata = null;
            foreach (GeoCoordinate pos in trackingPoints)//每個點跑一次計算他們的面積
            {
                if (count == 0)//頭不考慮
                {
                    posNow = pos;
                    posNext = trackingPoints[count + 1];

                    TriangleData triangleData = new TriangleData();                    
                    triangleData.areaSquared = int.MaxValue;
                    triangleData.selfID = count;
                    triangleData.prevTData = null;
                    triangleData.nextTData = null;

                    oldTdata = triangleData;
                    AddSortedList(triangleAreaList, oldTdata);
                    //triangleArea[count] = new AreaData();
                    //triangleArea[count].area = int.MaxValue;
                    //triangleArea[count].srcID = count;
                    count++;
                    continue;
                }

                if (count == trackingPoints.Count - 1)//尾不考慮
                {
                    TriangleData triangleData = new TriangleData();                   
                    triangleData.areaSquared = int.MaxValue;
                    triangleData.prevTData = oldTdata;
                    triangleData.nextTData = null;
                    triangleData.selfID = count;

                    oldTdata.nextTData = triangleData;
                    oldTdata = triangleData;
                    AddSortedList(triangleAreaList, oldTdata);
                    //triangleArea[count] = new AreaData();
                    //triangleArea[count].area = int.MaxValue;
                    //triangleArea[count].srcID = count;
                    break;
                }

                posPrev = posNow;
                posNow = posNext;
                posNext = trackingPoints[count + 1];

                //計算面積
                //Vector3 vecA = new Vector3((float)(posNow.Longitude - posPrev.Longitude), (float)(posNow.Latitude - posPrev.Latitude), 0);
                //Vector3 vecB = new Vector3((float)(posNext.Longitude - posPrev.Longitude), (float)(posNext.Latitude - posPrev.Latitude), 0);
                //Vector3 cross; Vector3.Cross(ref vecA, ref vecB, out cross);
                float areaSquared =
                                    CalculateAreaSquare(
                                    (float)posPrev.Longitude, (float)posPrev.Latitude,
                                    (float)posNow.Longitude, (float)posNow.Latitude,
                                    (float)posNext.Longitude, (float)posNext.Latitude);

                TriangleData TData = new TriangleData();
                TData.areaSquared = areaSquared;
                TData.prevTData = oldTdata;
                TData.nextTData = null;
                TData.selfID = count;

                oldTdata.nextTData = TData;
                oldTdata = TData;
                AddSortedList(triangleAreaList, oldTdata);
                //triangleArea[count] = new AreaData();
                //triangleArea[count].area = cross.LengthSquared();
                //triangleArea[count].srcID = count;
                count++;
            }
            #endregion

            //得到面積之後，根據面積做排序
            //Array.Sort(triangleArea);


            //一直移除點，重算面積並更新左右鄰居的面積以及三角形索引
            while (triangleAreaList.Count > destPointsAmount)
            {
                TriangleData removeTriangleData = triangleAreaList.First();
                triangleAreaList.Remove(removeTriangleData);

                //*******************************************
                //更新左右鄰居的三角形面積，並更新索引
                //*******************************************
                //處理移除點的prev(將移除點的next設定給prev的next)
                TriangleData prevTData = removeTriangleData.prevTData;
                if (prevTData.prevTData != null && prevTData.nextTData != null)//有null表示為最前或最後點，絕對不可能移除，面積我也給最大，不用重新計算
                {
                    triangleAreaList.Remove(prevTData);//先移除等下載ADD做排序

                    prevTData.nextTData = removeTriangleData.nextTData;
                    prevTData.areaSquared =
                                     CalculateAreaSquare(
                    (float)trackingPoints[prevTData.nextTData.selfID].Longitude, (float)trackingPoints[prevTData.nextTData.selfID].Latitude,
                    (float)trackingPoints[prevTData.selfID].Longitude, (float)trackingPoints[prevTData.selfID].Latitude,
                    (float)trackingPoints[prevTData.prevTData.selfID].Longitude, (float)trackingPoints[prevTData.prevTData.selfID].Latitude);
                    AddSortedList(triangleAreaList, prevTData);
                }

                //處理移除點的next(將移除點的prev設定給next的prev)
                TriangleData nextTData = removeTriangleData.nextTData;
                if (nextTData.prevTData != null && nextTData.nextTData != null)//有null表示為最前或最後點，絕對不可能移除，面積我也給最大，不用重新計算
                {
                    triangleAreaList.Remove(nextTData);//先移除等下載ADD做排序

                    nextTData.prevTData = removeTriangleData.prevTData;
                    nextTData.areaSquared =
                                     CalculateAreaSquare(
                    (float)trackingPoints[nextTData.nextTData.selfID].Longitude, (float)trackingPoints[nextTData.nextTData.selfID].Latitude,
                    (float)trackingPoints[nextTData.selfID].Longitude, (float)trackingPoints[nextTData.selfID].Latitude,
                    (float)trackingPoints[nextTData.prevTData.selfID].Longitude, (float)trackingPoints[nextTData.prevTData.selfID].Latitude);
                    AddSortedList(triangleAreaList, nextTData);
                }
            }

            int[] decreasePointArray = new int[destPointsAmount];
            count = 0;
            triangleAreaList.Sort(new SortedByTrackingPointID());
            foreach (TriangleData TData in triangleAreaList)
            {
                decreasePointArray[count] = TData.selfID;
                count++;
            }

            MyUtility.Utility.ShowMessageBox("Decrease tracking points from : [" + trackingPoints.Count + "] to: [" + destPointsAmount + "]");

            return decreasePointArray;
        }
    }
}
