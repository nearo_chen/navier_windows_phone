﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ShareOnFBApp.Resources;
using NavierHUD;
using Microsoft.Phone.Maps.Services;
using Microsoft.Phone.Maps.Controls;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.Windows.Media;
using System.Windows.Threading;
using MyUtility;
using System.Windows.Input;

namespace ShareOnFBApp
{
    public partial class MainPage : PhoneApplicationPage
    {
        private DispatcherTimer _timer = new DispatcherTimer();//不知為何在onNaviTo那邊setview都沒反應所以用個timer一秒之後再setView

        int mCountSetView = 0;
        LocationRectangle mRouteBoundBox = null;
        int[] mDecreasePointsID = null;
        const int mTrackingPointMax = 1140;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            //  for test  380*3個float點是OK的，還可以外加100字的message
            //Random random = new Random(System.Environment.TickCount);
            //for (int a = 0; a < 20; a++) //394
            //{
            //    GeoCoordinate rrr = new GeoCoordinate();
            //    rrr.Latitude = 25.017329f + random.NextDouble() * 0.001f;
            //    rrr.Longitude = 121.458994f + random.NextDouble() * 0.001f;
            //    NavierHUD.RouteRendererComponent.mRouteRECList.Add(rrr);

            //    rrr = new GeoCoordinate();
            //    rrr.Latitude = 25.010406f + random.NextDouble() * 0.001f;
            //    rrr.Longitude = 121.467749f + random.NextDouble() * 0.001f;
            //    NavierHUD.RouteRendererComponent.mRouteRECList.Add(rrr);

            //    rrr = new GeoCoordinate();
            //    rrr.Latitude = 25.017329f + random.NextDouble() * 0.001f;
            //    rrr.Longitude = 121.458994f + random.NextDouble() * 0.001f;
            //    NavierHUD.RouteRendererComponent.mRouteRECList.Add(rrr);
            //}
            //Message.Text = "123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-";




        }
        string mMovedDistanceKM = null;
        TimeSpan mCostTimeOffset = TimeSpan.MaxValue;
        string mAVGSpeed_MeterSec = null;

        private void Timer_Tick(object sender, EventArgs e)
        {
            MyMap.SetView(mRouteBoundBox, MapAnimationKind.Linear);

            mCountSetView++;
            if (mCountSetView >= 2)
            {
                mCountSetView = 0;
                _timer.Tick -= Timer_Tick;
                _timer.Stop();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
        }



        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
           
            lock (NavierHUD.RouteRendererComponent.mRouteRECListLock)
            {
                //show route on map
                MapPolyline _line = new MapPolyline();
                _line.StrokeColor = Colors.Red;
                _line.StrokeThickness = 5;
                _line.Path = RouteRendererComponent.mRouteRECList;
                MyMap.MapElements.Add(_line);

                //計算路線的BUNDBOX
                //if (mRouteBoundBox == null)
                //{
                //double MaxLon = -99999f; double MinLon = 99999f;
                //double MaxLat = -99999f; double MinLat = 99999f;

                //foreach (GeoCoordinate cood in RouteRendererComponent.mRouteRECList)
                //{
                //    if (MaxLon < cood.Longitude)
                //        MaxLon = cood.Longitude;
                //    if (MinLon > cood.Longitude)
                //        MinLon = cood.Longitude;

                //    if (MaxLat < cood.Latitude)
                //        MaxLat = cood.Latitude;
                //    if (MinLat > cood.Latitude)
                //        MinLat = cood.Latitude;
                //}
                //mRouteBoundBox = new LocationRectangle(MaxLat, MaxLon, MinLat, MinLon);
                mRouteBoundBox = LocationRectangle.CreateBoundingRectangle(RouteRendererComponent.mRouteRECList);
                Utility.GetCurrentDispatcher().BeginInvoke(() =>
                {
                    MyMap.SetView(mRouteBoundBox, MapAnimationKind.Linear);

                    _timer.Interval = TimeSpan.FromMilliseconds(500);
                    _timer.Tick += Timer_Tick;
                    _timer.Start();
                });


                //看看是否超過上傳FB的路徑最大數量
                if (mDecreasePointsID == null)
                {
                    mDecreasePointsID = DecreaseRoutePoints.ReduceRoutePoints(mTrackingPointMax, NavierHUD.RouteRendererComponent.mRouteRECList);

                    if (mDecreasePointsID == null)//表示不需要做簡化(給一個完整對應tracking的ID LIST)
                    {
                        mDecreasePointsID = new int[NavierHUD.RouteRendererComponent.mRouteRECList.Count];
                        for (int a = 0; a < mDecreasePointsID.Length; a++)
                            mDecreasePointsID[a] = a;
                    }
                }
            }

            //處理要顯示的資訊
            if (string.IsNullOrEmpty(mMovedDistanceKM))
            {
                double movedDistanceMeter = RouteRendererComponent.mMovedDistanceMeterCount;
                mMovedDistanceKM = (movedDistanceMeter / 1000f).ToString("F1");
            }

            if (string.IsNullOrEmpty(mAVGSpeed_MeterSec))
            {
                double AVGSpeed_KMHR = RouteRendererComponent.GetSpeedAVG_KMHR();
                double AVGSpeed_MeterSec = (AVGSpeed_KMHR * 1000f) / (60f * 60f);
                mAVGSpeed_MeterSec = AVGSpeed_MeterSec.ToString("F1");
            }

            if (mCostTimeOffset == TimeSpan.MaxValue)
            {
                DateTime startTime = RouteRendererComponent.GetDateTimeStart();
                mCostTimeOffset = DateTime.Now.Subtract(startTime);
            }

            information.Text = "distance: " + mMovedDistanceKM + "km | "
                        + "duration: " + mCostTimeOffset.ToString() + " | "
                        + "speed: " + mAVGSpeed_MeterSec + "m/s";
        }

        private async void loginButton_SessionStateChanged(object sender, Facebook.Client.Controls.SessionStateChangedEventArgs e)
        {
            if (e.SessionState == Facebook.Client.Controls.FacebookSessionState.Opened)
            {
                shareButton.IsEnabled = true;
                shareButton.Opacity = 1;
                profilePicture.Visibility = System.Windows.Visibility.Visible;
                profileName.Visibility = System.Windows.Visibility.Visible;
                pleaseLogin.Visibility = System.Windows.Visibility.Collapsed;
            }
            else if (e.SessionState == Facebook.Client.Controls.FacebookSessionState.Closed)
            {
                shareButton.IsEnabled = false;
                shareButton.Opacity = 0.5;
                profilePicture.Visibility = System.Windows.Visibility.Collapsed;
                profileName.Visibility = System.Windows.Visibility.Collapsed;
                pleaseLogin.Visibility = System.Windows.Visibility.Visible;

                //Clear cookie for complate logout: http://stackoverflow.com/questions/15202950/how-to-logout-with-facebook-c-sharp-sdk
                WebBrowser ww = new WebBrowser();
                await ww.ClearCookiesAsync();
            }
        }

        async void postToFB()
        {
            Utility.ShowProgressIndicator("...", true);

            dynamic[] metricsREC = new dynamic[mDecreasePointsID.Length];

            int count = 0;
            foreach (int id in mDecreasePointsID)
            {
                GeoCoordinate pos = NavierHUD.RouteRendererComponent.mRouteRECList[id];
                metricsREC[count] = new
                {
                    location = new
                    {
                        latitude = (float)pos.Latitude,//轉乘float讓字串變小
                        longitude = (float)pos.Longitude,
                        //    altitude = 0,
                    },
                };
                count++;
            }

            //login 成功將路徑PO在這上面
            await this.loginButton.RequestNewPermissions("publish_actions");
            var facebookClient = new Facebook.FacebookClient(this.loginButton.CurrentSession.AccessToken);



            var og_data = new
            {
                app_id = "1394792884092443",
                type = "fitness.course",
                title = "Navier HUD",
                image = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn1/s148x148/1546206_1440455652832977_361772208_a.png",
                description = "Innovation of navigation",
                url = "http://asnippets.blogspot.tw/p/navier-hud.html",

                data = new
                {
                    distance = new
                    {
                        value = mMovedDistanceKM, //"8.0",
                        units = "km",
                    },
                    duration = new
                    {
                        value = mCostTimeOffset.Seconds,
                        units = "s",  //幾秒
                    },
                    speed = new
                    {
                        value = mAVGSpeed_MeterSec,
                        units = "m/s", //每秒幾公尺
                    },
                    metrics = metricsREC,
                }
            };

            //full information
            var postInfo = new Dictionary<string, object>();
            postInfo.Add("course", og_data);
            postInfo.Add("message", Message.Text);
            postInfo.Add("privacy", new
            {
                value = "ALL_FRIENDS",//"EVERYONE",//,
            });
            postInfo.Add("fb:explicitly_shared", "true");

            bool bTryLessInformation = false;
            try
            {
                dynamic fbPostTaskResult = await facebookClient.PostTaskAsync("me/fitness.walks", postInfo);
                var result = (IDictionary<string, object>)fbPostTaskResult;

                Dispatcher.BeginInvoke(() =>
                {
                    // MessageBox.Show("Posted Open Graph Action, id: " + (string)result["id"], "Result", MessageBoxButton.OK);
                    MessageBox.Show(ResourceApp.Resources.AppResources.Share_Success);
                });
            }
            catch (Exception ex)
            {
                bTryLessInformation = true;
                //  Dispatcher.BeginInvoke(() =>
                //  {
                //      MessageBox.Show(ResourceApp.Resources.AppResources.Share_Fail + ": " + ex.Message);
                //  });
            }

            if (bTryLessInformation)
            {
                //如果上面失敗，我這邊降低一堆權限的內容在PO一次
                //less information
                var postParams = new
                {
                    course = og_data,
                    privacy = new
                    {
                        value = "ALL_FRIENDS",//,"EVERYONE"
                    },
                };
                try
                {
                    dynamic fbPostTaskResult = await facebookClient.PostTaskAsync("me/fitness.walks", postParams);
                    var result = (IDictionary<string, object>)fbPostTaskResult;

                    Dispatcher.BeginInvoke(() =>
                    {
                        // MessageBox.Show("Posted Open Graph Action, id: " + (string)result["id"], "Result", MessageBoxButton.OK);
                        MessageBox.Show(ResourceApp.Resources.AppResources.Share_Success);
                    });
                }
                catch (Exception ex)
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        //MessageBox.Show("Exception during post: " + ex.Message, "Error", MessageBoxButton.OK);
                        MessageBox.Show(ResourceApp.Resources.AppResources.Share_Fail + ": " + ex.Message);
                    });
                }
            }

            GoTo_NavierMainPage();

            Utility.HideProgressIndicator();
        }

        private void shareButton_Click(object sender, RoutedEventArgs e)
        {
            postToFB();

        }

        private void Leave_Click_2(object sender, RoutedEventArgs e)
        {
            GoTo_NavierMainPage();
        }

        private void PhoneApplicationPage_BackKeyPress_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //在這邊案返回就跟leave按鈕同樣處理，回main page
            GoTo_NavierMainPage();
        }

        void GoTo_NavierMainPage()
        {
            NavierHUD.RouteRendererComponent.ReCreatDistanceSpeedCalculator();

            if (MyUtility.Utility.mHideAdvertisement)
            {
                string addr = string.Format("/NavierMainApp;component/NavierMain.xaml");
                NavigationService.Navigate(new Uri(addr, UriKind.Relative));
            }
            else
            {
                string addr = string.Format("/NavierMainAppFree;component/NavierMain.xaml");
                NavigationService.Navigate(new Uri(addr, UriKind.Relative));
            }

            while (this.NavigationService.BackStack.Any())
            {
                this.NavigationService.RemoveBackEntry();
            }
        }

        private void Message_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            // if the enter key is pressed 
            if (e.Key == Key.Enter)
            {
                // focus the page in order to remove focus from the text box 
                // and hide the soft keyboard 
                this.Focus();
            }

        }


    }
}