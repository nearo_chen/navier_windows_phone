﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SOMAWP8;

namespace Soma_Sample___Pres_Layer_Execution_Time
{
  public partial class MainPage : PhoneApplicationPage
  {
    SomaAdViewer somaAdViewer;
    ApplicationBarIconButton iconButtonStart;
    ApplicationBarIconButton iconButtonStop;

    // Constructor
    public MainPage()
    {
      InitializeComponent();
      #region Application Bar
      ApplicationBar aB = new ApplicationBar();
      aB.IsMenuEnabled = true;
      aB.IsVisible = true;

      iconButtonStart = new ApplicationBarIconButton(new Uri("/Images/start.png", UriKind.Relative));
      iconButtonStart.Text = "Start";
      iconButtonStart.Click += new EventHandler(iconButtonStart_Click);
      iconButtonStart.IsEnabled = false;
      aB.Buttons.Add(iconButtonStart);

      iconButtonStop = new ApplicationBarIconButton(new Uri("/Images/stop.png", UriKind.Relative));
      iconButtonStop.Text = "Stop";
      iconButtonStop.Click += new EventHandler(iconButtonStop_Click);
      iconButtonStop.IsEnabled = true;
      aB.Buttons.Add(iconButtonStop);


      this.ApplicationBar = aB;
      #endregion

      somaAdViewer = new SomaAdViewer();
      somaAdViewer.Adspace = 0;
      somaAdViewer.Pub = 0;
      somaAdViewer.PopupAd = true;

      somaAdViewer.ShowErrors = true;

      ContentPanel.Children.Add(somaAdViewer);
      somaAdViewer.StartAds();
    }

    void iconButtonStop_Click(object sender, EventArgs e)
    {
      somaAdViewer.StopAds();
      iconButtonStart.IsEnabled = true;
      iconButtonStop.IsEnabled = false;
    }

    void iconButtonStart_Click(object sender, EventArgs e)
    {
      somaAdViewer.StartAds();
      iconButtonStart.IsEnabled = false;
      iconButtonStop.IsEnabled = true;
    }
  }
}