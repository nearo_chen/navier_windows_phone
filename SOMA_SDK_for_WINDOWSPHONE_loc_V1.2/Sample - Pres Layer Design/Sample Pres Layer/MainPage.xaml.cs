﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace SOMA_Sample_Using_Presentation_Layer
{
  public partial class MainPage : PhoneApplicationPage
  {
    // Constructor
    public MainPage()
    {
      InitializeComponent();

      somaAdViewer.Pub = 0;       // Developer pub ID for testing
      somaAdViewer.Adspace = 0;   // Developer adSpace ID for testing
      somaAdViewer.Age = 35;

      somaAdViewer.StartAds();
    }

    private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
    {
      somaAdViewer.StopAds();
    }
  }
}
