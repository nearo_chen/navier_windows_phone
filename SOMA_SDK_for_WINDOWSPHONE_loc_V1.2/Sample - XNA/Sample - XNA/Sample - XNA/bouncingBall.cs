using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using Microsoft.Phone.Tasks;
using SOMAWP8;

namespace SOMA_Sample___XNA___Comm_Layer
{
  /// <summary>
  /// This is the main type for your game
  /// </summary>
  public class bouncingBall : Microsoft.Xna.Framework.Game
  {
    GraphicsDeviceManager graphics;
    SpriteBatch spriteBatch;
    SomaAd somaAd;
    Texture2D textureSomaAd;
    SpriteFont myFont;
    Texture2D balanceBall;

    public bouncingBall()
    {
      graphics = new GraphicsDeviceManager(this);
      graphics.IsFullScreen = true;
      graphics.PreferredBackBufferHeight = 800;
      graphics.PreferredBackBufferWidth = 480;

      Content.RootDirectory = "Content";

      // Frame rate is 30 fps by default for Windows Phone.
      TargetElapsedTime = TimeSpan.FromTicks(333333);
    }

    /// <summary>
    /// Allows the game to perform any initialization it needs to before starting to run.
    /// This is where it can query for any required services and load any non-graphic
    /// related content.  Calling base.Initialize will enumerate through any components
    /// and initialize them as well.
    /// </summary>
    protected override void Initialize()
    {
      // Set up SomaAd to get ads
      somaAd = new SomaAd();
      somaAd.Adspace = 0;   // Developer Ads
      somaAd.Pub = 0;       // Developer Ads
      somaAd.AdSpaceHeight = 80;
      somaAd.AdSpaceWidth = 480;
      somaAd.GetAd();

      base.Initialize();
    }

    /// <summary>
    /// LoadContent will be called once per game and is the place to load
    /// all of your content.
    /// </summary>
    protected override void LoadContent()
    {
      // Create a new SpriteBatch, which can be used to draw textures.
      spriteBatch = new SpriteBatch(GraphicsDevice);

      myFont = Content.Load<SpriteFont>("SegoeFont");

      balanceBall = this.Content.Load<Texture2D>("balanceBall");
      textureSomaAd = this.Content.Load<Texture2D>("sampleAd");

      // setup touch
      TouchPanel.EnabledGestures = GestureType.Tap;
    }

    /// <summary>
    /// UnloadContent will be called once per game and is the place to unload
    /// all content.
    /// </summary>
    protected override void UnloadContent()
    {
      somaAd.Dispose();
    }

    /// <summary>
    /// Allows the game to run logic such as updating the world,
    /// checking for collisions, gathering input, and playing audio.
    /// </summary>
    /// <param name="gameTime">Provides a snapshot of timing values.</param>
    Vector2 direction = new Vector2(3, 2);
    Vector2 position = new Vector2(100, 100);
    Vector2 somaAdPosition = new Vector2(0, 720);
    Vector2 somaAdSize = new Vector2(480, 80);
    string currentAdImageFileName = "";
    protected override void Update(GameTime gameTime)
    {
      // Allows the game to exit
      if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
        this.Exit();

      // if the ad panel was tapped, show the click thru ad
      while (TouchPanel.IsGestureAvailable)
      {
        GestureSample gestureSample = TouchPanel.ReadGesture();

        if (gestureSample.GestureType == GestureType.Tap)
        {
          Vector2 touchPosition = gestureSample.Position;

          if (touchPosition.X >= 0 &&
              touchPosition.X < somaAdSize.X &&
              touchPosition.Y >= somaAdPosition.Y &&
              touchPosition.Y < (somaAdPosition.Y + somaAdSize.Y))
          {
            WebBrowserTask webBrowserTask = new WebBrowserTask();
            webBrowserTask.Uri = new Uri(somaAd.Uri);
            webBrowserTask.Show();
          }
        }
      }

      // calculate new ball position
      position = Vector2.Add(position, direction);
      if ((position.X + 100) > 480 || position.X < 0) direction.X *= -1;
      if ((position.Y + 100) > 720 || position.Y < 0) direction.Y *= -1;

      // if there is a new ad, get it from Isolated Storage and  show it
      if (somaAd.Status == "success" && somaAd.AdImageFileName != null && somaAd.ImageOK)
      {
        try
        {
          if (currentAdImageFileName != somaAd.AdImageFileName)
          {
            currentAdImageFileName = somaAd.AdImageFileName;
            IsolatedStorageFile myIsoStore = IsolatedStorageFile.GetUserStoreForApplication();
            IsolatedStorageFileStream myAd = new IsolatedStorageFileStream(somaAd.AdImageFileName, FileMode.Open, myIsoStore);
            textureSomaAd = Texture2D.FromStream(this.GraphicsDevice, myAd);

            myAd.Close();
            myAd.Dispose();
            myIsoStore.Dispose();
          }
        }
        catch (IsolatedStorageException ise)
        {
          string message = ise.Message;
        }
      }

      base.Update(gameTime);
    }

    /// <summary>
    /// This is called when the game should draw itself.
    /// </summary>
    /// <param name="gameTime">Provides a snapshot of timing values.</param>
    protected override void Draw(GameTime gameTime)
    {
      GraphicsDevice.Clear(Color.CornflowerBlue);

      // draw ball
      this.spriteBatch.Begin();
      this.spriteBatch.Draw(this.balanceBall,
        new Rectangle((int)position.X, (int)position.Y, 100, 100), Color.White);
      this.spriteBatch.End();

      // draw ad
      this.spriteBatch.Begin();
      this.spriteBatch.Draw(textureSomaAd, new Rectangle(0, 720, 480, 80), Color.White);
      this.spriteBatch.End();

      base.Draw(gameTime);
    }
  }
}
