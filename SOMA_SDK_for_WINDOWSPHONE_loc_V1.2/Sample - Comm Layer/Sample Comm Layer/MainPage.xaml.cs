﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Info;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using SOMAWP8;

namespace SOMA_Sample_using_Communications_Layer
{
  public partial class MainPage : PhoneApplicationPage
  {
    #region local variables
    SomaAd somaAd;
    int adCounter = 0;

    string adClickThruUri = "";
    string adPixelHeight = "";
    string adPixelWidth = "";

    // control for state Preserve and Restore routines
    bool newPageInstance = false;

    private const string testHtml1 = @"
      <!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
      <html>
      <meta name=""viewport"" content=""width=";
    private const string testHtml2 = @", height=";
    private const string testHtml3 = @", initial-scale=1.5, user-scalable=no""/>
      <head><title>Smaato Ad Image</title></head>
      <body BGCOLOR=""#00000000"">
      <a>
      <img src=";
    private const string testHtml4 = @"
        alt=Mobile Advertizing - Smaato""/>
      </a>
      </body>
      </html>";
    #endregion

    // Constructor
    public MainPage()
    {
      InitializeComponent();

      // If the constructor has been called, this is not a page that was already in memory.
      newPageInstance = true;

      ApplicationBar aB = new ApplicationBar();
      ApplicationBarMenuItem menuItemAbout = new ApplicationBarMenuItem();
      menuItemAbout.Text = "About";
      menuItemAbout.Click += new EventHandler(menuItemAbout_Click);
      aB.MenuItems.Add(menuItemAbout);

      this.ApplicationBar = aB;

      somaAd = new SomaAd();
      somaAd.AdInterval = 60000;
      // use developer adspace and publisher IDs
      somaAd.Adspace = 0;
      somaAd.Pub = 0;


      // demographics
    /*  somaAd.Age = 30;
      somaAd.Gender = "m";
      somaAd.City = "Fremont";
      somaAd.State = "Ca";
      somaAd.Zip = "94539";
      somaAd.Country = "United States";
      somaAd.Countrycode = "us";
        */
      somaAd.NewAdAvailable += new SomaAd.OnNewAdAvailable(somaAd_NewAdAvailable);
      somaAd.GetAdError += new SomaAd.OnGetAdError(somaAd_GetAdError);
      somaAd.GetAd();
       
    }

    void somaAd_GetAdError(object sender, string ErrorCode, string ErrorDescription)
    {
        textBoxErrorCode.Text = somaAd.ErrorCode;
        textBoxErrorDescription.Text = DateTime.Now.ToShortTimeString() + " " +
                                       somaAd.ErrorDescription;
    }

    void somaAd_NewAdAvailable(object sender, EventArgs e)
    {
      if (somaAd.Status == "error")
      {
        textBoxErrorCode.Text = somaAd.ErrorCode;
        textBoxErrorDescription.Text = DateTime.Now.ToShortTimeString() + " " +
                                       somaAd.ErrorDescription;
      }
      else
      {
        if (somaAd.AdText != String.Empty)
          textBoxTextAd.Text = somaAd.AdText;
        if (somaAd.AdType == "IMG")
        {
          ++adCounter;
          textBoxAdCounter.Text = adCounter.ToString();
          textBoxGifCounter.Text = somaAd.gifImageCount.ToString();
          textBoxImageType.Text = somaAd.ContentType;
          imageAd.Source = somaAd.AdImage;

          // set up size of viewport
          if (somaAd.AdImage.PixelHeight == 0 || 
              somaAd.ContentType == "image/gif" ||
              somaAd.ContentType == "gif")
          {
            // default ad size
            adPixelHeight = "50";
            adPixelWidth = "300";
          }
          else if (somaAd.AdImage.PixelHeight > webBrowserAdImage.ActualHeight)
          {
            adPixelHeight = webBrowserAdImage.ActualHeight.ToString();
            adPixelWidth = webBrowserAdImage.ActualWidth.ToString();
          }
          else
          {
            adPixelHeight = somaAd.AdImage.PixelHeight.ToString();
            adPixelWidth = somaAd.AdImage.PixelWidth.ToString();
          }

          // save click through Uri
          adClickThruUri = somaAd.Uri;

          // display ad in web browser control which handles gif
          webBrowserAdImage.NavigateToString(
            testHtml1 + adPixelWidth +
            testHtml2 + adPixelHeight +
            testHtml3 + somaAd.AdImageUri +
            testHtml4); ;
        }
      }
    }

    void menuItemAbout_Click(object sender, EventArgs e)
    {

      MessageBox.Show("This is the " + ApplicationTitle.Text + " application: " + Globals.MyFullName);
    }

    private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
    {
      somaAd.Dispose();      
      
      MessageBox.Show("Application Closing");
      textBoxTextAd.Text = "Application Closing";
    }

    private void imageAd_MouseEnter(object sender, MouseEventArgs e)
    {
      if (adClickThruUri != "" && adClickThruUri != null)
      {
        // launch web browser to show click thru ad
        WebBrowserTask webBrowserTask = new WebBrowserTask();
        webBrowserTask.Uri = new Uri(adClickThruUri);
        webBrowserTask.Show();
      }
    }

    private void rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      if (adClickThruUri != "" && adClickThruUri != null)
      {
        // launch web browser to show click thru ad
        WebBrowserTask webBrowserTask = new WebBrowserTask();
        webBrowserTask.Uri = new Uri(adClickThruUri);
        webBrowserTask.Show();
      }
    }

    protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
    {
      base.OnNavigatedFrom(e);

      // Call the custom helper functions to preserve the state of the UI
      StateUtils.PreserveState(State, textBoxAdCounter);
      StateUtils.PreserveState(State, textBoxGifCounter);
      StateUtils.PreserveState(State, textBoxImageType);
      StateUtils.PreserveState(State, textBoxTextAd);
      StateUtils.PreserveState(State, textBoxErrorDescription);
      StateUtils.PreserveState(State, textBoxErrorCode);
      StateUtils.PreserveState(State, webBrowserAdImage);
      StateUtils.PreserveFocusState(State, ContentGrid);
      StateUtils.PreserveState(State, "adCounter", adCounter);
      StateUtils.PreserveState(State, "somaAd.gifImageCount", somaAd.gifImageCount);

      // Set newPageInstance back to false. It will be set back to true if the constructor is called again.
      newPageInstance = false;

      // Set a key in the State dictionary that will be checked for in OnNavigatedTo
      this.State["PreservingPageState"] = true;
    }

    protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
    {
      base.OnNavigatedTo(e);

      // If the constructor has been called AND the PreservingPageState key is in the State dictionary,
      // then the UI state for this page should be restored
      if (newPageInstance && this.State.ContainsKey("PreservingPageState"))
      {
        // Call the custom helper functions to restore the state of the UI
        StateUtils.RestoreState(State, textBoxAdCounter, "");
        StateUtils.RestoreState(State, textBoxGifCounter, "");
        StateUtils.RestoreState(State, textBoxImageType, "");
        StateUtils.RestoreState(State, textBoxTextAd, "");
        StateUtils.RestoreState(State, textBoxErrorDescription, "");
        StateUtils.RestoreState(State, textBoxErrorCode, "");
        StateUtils.RestoreState(State, webBrowserAdImage);
        StateUtils.RestoreFocusState(State, ContentGrid);
        adCounter = StateUtils.RestoreState(State, "adCounter", adCounter, 0);
        somaAd.gifImageCount = StateUtils.RestoreState(State, "somaAd.gifImageCount", somaAd.gifImageCount, 0);
      }
    }
  }
}
