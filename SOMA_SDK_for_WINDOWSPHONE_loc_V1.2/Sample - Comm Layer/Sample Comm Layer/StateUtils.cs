﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace SOMA_Sample_using_Communications_Layer
{
  public class StateUtils
  {
    #region Preserve and Restore state of textbox
    /// <summary>
    /// preserve state of textbox
    /// </summary>
    /// <param name="state"></param>
    /// <param name="textBox"></param>
    public static void PreserveState(IDictionary<string, object> state, TextBox textBox)
    {
      state[textBox.Name + "_Text"] = textBox.Text;
      state[textBox.Name + "_SelectionStart"] = textBox.SelectionStart;
      state[textBox.Name + "_SelectionLength"] = textBox.SelectionLength;
    }

    /// <summary>
    /// Restore state of textbox
    /// </summary>
    /// <param name="state"></param>
    /// <param name="textBox"></param>
    /// <param name="defaultValue"></param>
    public static void RestoreState(IDictionary<string, object> state, TextBox textBox, string defaultValue)
    {
      textBox.Text = TryGetValue<string>(state, textBox.Name + "_Text", defaultValue);
      textBox.SelectionStart = TryGetValue<int>(state, textBox.Name + "_SelectionStart", textBox.Text.Length);
      textBox.SelectionLength = TryGetValue<int>(state, textBox.Name + "_SelectionLength", 0);
    }
    #endregion

    #region Preserve and Restore Scrollviewer
    /// <summary>
    /// Preserve State of Scrollviewer
    /// </summary>
    /// <param name="state"></param>
    /// <param name="scrollViewer"></param>
    public static void PreserveState(IDictionary<string, object> state, ScrollViewer scrollViewer)
    {
      state[scrollViewer.Name + "_VerticalOffset"] = scrollViewer.VerticalOffset;
    }

    /// <summary>
    /// Restore State of Scrollviewer
    /// </summary>
    /// <param name="state"></param>
    /// <param name="scrollViewer"></param>
    public static void RestoreState(IDictionary<string, object> state, ScrollViewer scrollViewer)
    {
      double offset = TryGetValue<double>(state, scrollViewer.Name + "_VerticalOffset", 0);
      if (offset > 0)
      {
        scrollViewer.Dispatcher.BeginInvoke(() => scrollViewer.ScrollToVerticalOffset(offset));
      }
    }

    #endregion

    #region Preserve and Restore state of a WebBrowser
    /// <summary>
    /// Preserve State of WebBrowser
    /// </summary>
    /// <param name="state"></param>
    /// <param name="webBrowser"></param>
    public static void PreserveState(IDictionary<string, object> state, WebBrowser webBrowser)
    {
      state[webBrowser.Name + "_Content"] = webBrowser.SaveToString();
    }

    /// <summary>
    /// Restore State of WebBrowser
    /// </summary>
    /// <param name="state"></param>
    /// <param name="webBrowser"></param>
    public static void RestoreState(IDictionary<string, object> state, WebBrowser webBrowser)
    {
      string content = TryGetValue<string>(state, webBrowser.Name + "_Content", "");
      if (!string.IsNullOrEmpty(content))
      {
        webBrowser.Loaded += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e2)
        {
          webBrowser.NavigateToString(content);
        });

      }
    }
    #endregion

    #region Preserve and Restore Focus State
    /// <summary>
    /// Preserve focus state
    /// </summary>
    /// <param name="state"></param>
    /// <param name="parent"></param>
    public static void PreserveFocusState(IDictionary<string, object> state, FrameworkElement parent)
    {
      // Determine which control currently has focus.
      Control focusedControl = FocusManager.GetFocusedElement() as Control;

      // If no control has focus, store null in the State dictionary.
      if (focusedControl == null)
      {
        state["FocusedControlName"] = null;
      }
      else
      {
        // Find the control within the parent
        Control foundFE = parent.FindName(focusedControl.Name) as Control;

        // If the control isn't found within the parent, store null in the State dictionary.
        if (foundFE == null)
        {
          state["FocusedElementName"] = null;
        }
        else
        {
          // otherwise store the name of the control with focus.
          state["FocusedElementName"] = focusedControl.Name;
        }
      }
    }

    /// <summary>
    /// Restore focus state
    /// </summary>
    /// <param name="state"></param>
    /// <param name="parent"></param>
    public static void RestoreFocusState(IDictionary<string, object> state, FrameworkElement parent)
    {
      // Get the name of the control that should have focus.
      string focusedName = TryGetValue<string>(state, "FocusedElementName", null);

      // Check to see if the name is null or empty
      if (!String.IsNullOrEmpty(focusedName))
      {

        // Find the control name in the parent.
        Control focusedControl = parent.FindName(focusedName) as Control;
        if (focusedControl != null)
        {
          // If the control is found, call its Focus method.
          parent.Dispatcher.BeginInvoke(() => { focusedControl.Focus(); });
        }
      }
    }


    #endregion

    #region Preserve and Restore Ints
    public static void PreserveState(IDictionary<string, object> state, string intName, int intValue)
    {
      state[intName] = intValue;
    }
    public static int RestoreState(IDictionary<string, object> state, string intName, int intValue, int defaultValue)
    {
      intValue = TryGetValue<int>(state, intName, defaultValue);
      return intValue;
    }
    #endregion

    #region TryGetValue
    private static T TryGetValue<T>(IDictionary<string, object> state, string name, T defaultValue)
    {
      if (state.ContainsKey(name))
      {
        if (state[name] != null)
        {
          return (T)state[name];
        }
      }
      return defaultValue;
    }
    #endregion

  }
}
